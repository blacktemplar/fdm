@echo off
SETLOCAL ENABLEEXTENSIONS
SETLOCAL EnableDelayedExpansion
SET me=%~n0
SET parent=%~dp0


SET file="C:\Users\benedikt\AppData\Local\Google\Chrome\User Data\Profile 1\File System\000\p\00\00000000"
SET process=firefox.exe
SET nircmd=nircmd


:start

IF EXIST %file% (
  set /p val=<%file%
  echo "!val!"
  IF "!val!"=="1" (
    echo mute
    ::mute process
    %nircmd% setappvolume %process% 0
    (@echo 2)>%file%
  )
  IF "!val!"=="3" (
    echo unmute
    ::unmute process
    %nircmd% setappvolume %process% 1
    (@echo 0)>%file%
   )
)


::sleep 1 second
ping -n 2 127.0.0.1 > NUL
goto start
