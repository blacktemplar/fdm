/*jshint browser: true*/
/*exported initLayout*/
/*global GoldenLayout, document, localStorage, jQuery, window, numGroups, modeIds*/
/*jslint white: true */

function fillLayout(structure, ids) {
  'use strict';
  var config = [],
      key,
      arr;
  for (key in structure) {
    if (structure.hasOwnProperty(key)) {
      arr = structure[key];
      if (arr.type === 'collection') {
        config.push({
          type: 'column',
          title: arr.title,
          isClosable: false,
          reorderEnabled: true,
          content: [{
            type: 'stack',
            isClosable: true,
            title: '',
            content: fillLayout(arr.children, ids)
          }]
        });
      } else {
        config.push({
          type: 'component',
          isClosable: false,
          reorderEnabled: true,
          componentName: 'PrototypeComponent',
          title: arr.title,
          componentState: { id: arr.globalKey }
        });
        ids[arr.globalKey] = true;
      }
    }
  }

  return config;
}

function stripScripts(s) {
  'use strict';
  var div = document.createElement('div'),
      scripts,
      i;
  div.innerHTML = s;
  scripts = div.getElementsByTagName('script');
  i = scripts.length;
  while (i > 0) {
    i -= 1;
    scripts[i].parentNode.removeChild(scripts[i]);
  }

  return div.innerHTML;
}

function verifyStructure(struct, ids) {
  'use strict';
  var i;
  for (i = 0; i < struct.length; i += 1) {
    if (!struct[i].hasOwnProperty('type')) {
      return false;
    }

    if (struct[i].type === 'component') {
      if (!struct[i].hasOwnProperty('componentState')) {
        return false;
      }

      if (!struct[i].componentState.hasOwnProperty('id')) {
        return false;
      }

      if (!ids.hasOwnProperty(struct[i].componentState.id) || ids[struct[i].componentState.id]) {
        return false;
      }

      ids[struct[i].componentState.id] = true;
    } else {
      if (struct[i].hasOwnProperty('content')) {
        if (!verifyStructure(struct[i].content, ids)) {
          return false;
        }
      }
    }
  }

  return true;
}

function verifyLayout(layout, ids) {
  'use strict';
  var key;
  for (key in ids) {
    if (ids.hasOwnProperty(key)) {
      ids[key] = false;
    }
  }

  if (!layout.hasOwnProperty('content')) {
    return false;
  }

  if (layout.content.length < 1 || !layout.content[0].hasOwnProperty('content')) {
    return false;
  }

  if (!verifyStructure(layout.content[0].content, ids)) {
    return false;
  }

  for (key in ids) {
    if (ids.hasOwnProperty(key)) {
      if (!ids[key]) {
        return false;
      }
    }
  }

  return true;
}

function initLayout(structure, phaseId) {
  'use strict';
  var ids = {},
      config =
      {
        settings: {
          hasHeaders: true,
          constraintDragToContainer: true,
          reorderEnabled: true,
          selectionEnabled: false,
          popoutWholeStack: false,
          blockedPopoutsThrowError: true,
          closePopoutsOnUnload: true,
          showPopoutIcon: false,
          showMaximiseIcon: false,
          showCloseIcon: false
        },
        dimensions: {
          borderWidth: 5,
          minItemHeight: 100,
          minItemWidth: 100,
          headerHeight: 20,
          dragProxyWidth: 300,
          dragProxyHeight: 200
        },
        content: [{
          isClosable: false,
          type: 'stack',
          title: '',
          content: fillLayout(structure, ids)
        }]
      },
      saveLocation = 'savedState_' + phaseId,
      saveLocationFallback = 'savedState_' + numGroups,
      savedState = localStorage.getItem(saveLocation),
      component = document.getElementById('fdm-content'),
      myLayout = null,
      conf,
      i;
  for (i = 0; i < modeIds.length; i += 1) {
    saveLocationFallback += '_' + modeIds[i];
  }

  if (savedState === null) {
    savedState = localStorage.getItem(saveLocationFallback);
  } else {
    conf = JSON.parse(savedState);
    if (verifyLayout(conf, ids)) {
      myLayout = new GoldenLayout(conf, component);
    } else {
      savedState = localStorage.getItem(saveLocationFallback);
    }
  }

  if (savedState === null) {
    myLayout = new GoldenLayout(config, component);
  } else if (myLayout === null) {
    conf = JSON.parse(savedState);
    if (verifyLayout(conf, ids)) {
      myLayout = new GoldenLayout(conf, component);
    } else {
      myLayout = new GoldenLayout(config, component);
    }
  }

  myLayout.on('stateChanged', function() {
    var state;
    conf = myLayout.toConfig();
    if (verifyLayout(conf, ids)) {
      state = JSON.stringify(conf);
      localStorage.setItem(saveLocation, state);
      localStorage.setItem(saveLocationFallback, state);
    }
  });

  myLayout.registerComponent('PrototypeComponent', function(container, state) {
    container.getElement().html(
        stripScripts(document.getElementById(state.id).innerHTML));
    jQuery(document).trigger('fdm:componentRegistered', [container, state]);
  });

  jQuery(window).resize(function() {
    //noinspection JSCheckFunctionSignatures
    myLayout.updateSize();
  });

  //myLayout.init();
  jQuery(document).ready(function() {
    myLayout.init();
  });
}
