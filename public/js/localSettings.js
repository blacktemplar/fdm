/*global $, window */
/*jshint browser: true*/
/*jslint white: true */

//noinspection Eslint,JSUnusedGlobalSymbols
function saveLocalSettings() {
  'use strict';
  var tournamentId = $('#hidden_tournamentId').val();
  $('#local-settings-form').find('input').each(function(index, element) {
    var key, value;
    if ($(this).attr('type') !== 'hidden' &&
        $(this).attr('type') !== 'button') {
      key = tournamentId + '_' + $(this).attr('name');
      value = $(this).attr('value');
      if ($(this).attr('type') === 'checkbox') {
        value = element.checked ? '1' : '0';
      }

      window.localStorage.setItem(key, value);
    }
  });
}

//load local settings
$(function() {
  'use strict';
  var tournamentId = $('#hidden_tournamentId').val();
  /*eslint-disable no-unused-vars*/
  //noinspection JSUnusedLocalSymbols
  $('#local-settings-form').find('input').each(function(index, element) {
    /*eslint-enable no-unused-vars*/
    var key, value;
    if ($(this).attr('type') !== 'hidden' &&
        $(this).attr('type') !== 'button') {
      key = tournamentId + '_' + $(this).attr('name');
      value = window.localStorage.getItem(key);
      if (value !== null) {
        if ($(this).attr('type') === 'checkbox') {
          $(this).prop('checked', value === '1');
        } else {
          $(this).val(value);
        }
      }
    }
  });

});
