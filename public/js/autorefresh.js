/*jshint browser: true*/
/*exported blockRefresh */
/*global jQuery, upToDateUrl, window, tournamentId, $, autorefresh*/
/*jslint white: true */

var refreshBlocked = 0;
function setBlockRefresh() {
  'use strict';
  if (!refreshBlocked) {
    refreshBlocked = 0;
  }
}

function blockRefresh(time) {
  'use strict';
  var defTime = time || 60000,
      newStamp = (new Date()).getTime() + defTime;
  setBlockRefresh();
  if (newStamp > refreshBlocked) {
    refreshBlocked = newStamp;
  }
}

function checkUpToDate() {
  'use strict';
  setBlockRefresh();
  //console.log(refreshBlocked);
  if (refreshBlocked < (new Date()).getTime()) {
    jQuery.ajax({
      url: upToDateUrl,
      type: 'GET',
      success: function(response) {
        var url;
        //console.log(response);
        if (response.uptodate === false) {
          if (refreshBlocked < (new Date()).getTime()) {
            url = window.location.href;
            window.location.href = url;
          }
        }
        /*else {
         console.log('up to date');
         }*/
      },

      async: true
    });
  }
}

$(function() {
  'use strict';
  var key;
  //noinspection JSLint
  if (typeof upToDateUrl !== 'undefined' &&
      typeof tournamentId !== 'undefined') {
    //console.log(upToDateUrl);
    key = tournamentId + '_autoRefresh';
    //console.log(key);
    //console.log(window.localStorage.getItem(key, null));
    //noinspection JSLint
    if ((typeof autorefresh !== 'undefined' && autorefresh) ||
        window.localStorage.getItem(key) === '1') {
      window.setInterval(checkUpToDate, 5000);
    }
  }
});
