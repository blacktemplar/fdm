/*global EditableGrid,editableGrid,alert,jQuery,AutocompleteCellEditor,
 CellRenderer, deleteText, deleteConfirm, deleteButton, cancelButton,
 phone, blockRefresh, baseUrl, window, editable,
 alreadyStarted */
/*jshint browser: true*/
/*jslint white: true */
/*exported refreshBlocked*/

var openPlayers = {},
    usedPlayers = {},
    newRequestRunning = {};


/**
 *  highlightRow and highlight are used to show a visual feedback.
 *  If the row has been successfully modified,
 *  it will be highlighted in green. Otherwise, in red
 *
 *  @param {int} rowId      id of the row to highlight
 *  @param {string} bgColor    color with which to highlight
 */
function highlightRow(rowId, bgColor) {
  'use strict';
  var rowSelector = jQuery('#' + rowId);
  rowSelector.css('background-color', bgColor);
  rowSelector.fadeTo('normal', 0.5, function() {
    rowSelector.fadeTo('fast', 1, function() {
      rowSelector.css('background-color', '');
    });
  });
}

function highlight(divId, style) {
  'use strict';
  highlightRow(divId, style === 'error' ? '#e5afaf' :
      style === 'warning' ? '#ffcc00' : '#8dc70a');
}

//noinspection Eslint,JSUnusedGlobalSymbols
var refreshBlocked = 0;

function refreshSuggestions(editableGrid) {
  'use strict';
  var config = { suggestions: [] },
      i,
      key,
      copyConfig;
  config.selectCallback = function(rowInd, colInd, data) {
    //noinspection JSLint
    selectPlayer(editableGrid, rowInd, colInd, data);
  };

  for (key in openPlayers) {
    if (openPlayers.hasOwnProperty(key)) {
      config.suggestions.push(openPlayers[key]);
    }
  }

  for (i = 0; i < config.suggestions.length; i += 1) {
    config.suggestions[i].value = config.suggestions[i].firstName;
  }

  editableGrid.setCellEditor('firstName', new AutocompleteCellEditor(config));
  copyConfig = jQuery.extend(true, {}, config);
  for (i = 0; i < copyConfig.suggestions.length; i += 1) {
    copyConfig.suggestions[i].value = copyConfig.suggestions[i].lastName;
  }

  editableGrid.setCellEditor('lastName',
      new AutocompleteCellEditor(copyConfig));
}

function setPlayerAjax(editableGrid, rowId, data) {
  'use strict';
  var rowIndex = editableGrid.getRowIndex(rowId),
      attendantId = editableGrid.getRowAttribute(rowIndex, 'attendantId'),
      startNumberInd,
      lastStartNumber;

  usedPlayers[data.id] = openPlayers[data.id];
  delete openPlayers[data.id];
  refreshSuggestions(editableGrid);
  //remove key from players dictionary
  if (attendantId === null || attendantId === undefined) {
    attendantId = 'new';
    newRequestRunning[rowId] = true;
    startNumberInd = editableGrid.getColumnIndex('startNumber');
    lastStartNumber = editableGrid.getValueAt(rowIndex, startNumberInd);
    //noinspection JSCheckFunctionSignatures
    editableGrid.addRow(Math.floor(Math.random() * 100000000000),
        { startNumber: lastStartNumber + 1 });
  }

  blockRefresh();
  jQuery.ajax({
    url: baseUrl + '/edit/jsonSetPlayer',
    type: 'POST',
    dataType: 'json',
    data: {
      id: attendantId,
      playerId: data.id
    },
    success: function(response) {
      /* reset old value if failed then highlight row */
      var result = response.result,
          /* by default, a sucessfull reponse can be 'ok' or a database id */
          success = result === 'ok',
          rIndex = editableGrid.getRowIndex(rowId),
          row = editableGrid.getRow(rIndex),
          realId = row.id,
          newObj,
          attId,
          playerIdCol,
          playerId;
      if (success) {
        newObj = response.newValues;
        attId = newObj.id;
        newObj.id = rowId;
        newObj.values.action = rowId;
        if (attendantId !== 'new') {
          playerIdCol = editableGrid.getColumnIndex('playerId');
          playerId = editableGrid.getValueAt(rIndex, playerIdCol);
          if (playerId) {
            openPlayers[playerId] = usedPlayers[playerId];
            delete usedPlayers[playerId];
            refreshSuggestions(editableGrid);
          }
        }

        editableGrid.update({ data: [newObj] });
        if (attendantId === 'new') {
          editableGrid.setRowAttribute(rIndex, 'attendantId', attId);
          newRequestRunning[rowId] = false;
        }
      }

      highlight(realId, success ? 'ok' : 'error');
    },

    error: function(XMLHttpRequest, textStatus, exception) {
      window.alert('Ajax failure\n' + exception.errortext);
      if (attendantId === 'new') {
        newRequestRunning[rowId] = false;
      }

      openPlayers[data.id] = usedPlayers[data.id];
      delete usedPlayers[data.id];
      refreshSuggestions(editableGrid);
    },

    async: true
  });
}

function selectPlayer(editableGrid, rowInd, colInd, data) {
  'use strict';
  var rowId = editableGrid.getRowId(rowInd);
  if (newRequestRunning[rowId] === true) {
    window.setTimeout(function() {
      selectPlayer(editableGrid, rowInd, colInd, data);
    }, 100);
  } else {
    setPlayerAjax(editableGrid, rowId, data);
  }
}

function updateAjax(editableGrid, rowId, columnIndex,
                    oldValue, newValue, onResponse) {
  'use strict';
  var rowIndex = editableGrid.getRowIndex(rowId),
      attendantId = editableGrid.getRowAttribute(rowIndex, 'attendantId'),
      startNumberInd,
      lastStartNumber,
      id;
  if (attendantId === null || attendantId === undefined) {
    attendantId = 'new';
    newRequestRunning[rowId] = true;
    startNumberInd = editableGrid.getColumnIndex('startNumber');
    lastStartNumber = editableGrid.getValueAt(rowIndex, startNumberInd);
    id = Math.floor(Math.random() * 100000000000);
    //noinspection JSCheckFunctionSignatures
    editableGrid.addRow(id, { startNumber: lastStartNumber + 1, action: '' });
  }

  blockRefresh();
  jQuery.ajax({
    url: baseUrl + '/edit/json',
    type: 'POST',
    dataType: 'json',
    data: {
      id: attendantId,
      newvalue: editableGrid.getColumnType(columnIndex) === 'boolean' ?
          (newValue ? 1 : 0) : newValue,
      colname: editableGrid.getColumnName(columnIndex)
    },
    success: function(response) {
      /* reset old value if failed then highlight row */
      var result = response.result,
          /* by default, a sucessfull reponse can be 'ok' or a database id */
          success = onResponse ? onResponse(response) : result === 'ok',
          rIndex = editableGrid.getRowIndex(rowId),
          row = editableGrid.getRow(rIndex),
          realId = row.id,
          newObj;
      if (success) {
        if (attendantId === 'new') {
          newObj = response.newValues;
          editableGrid.setValueAt(rIndex,
              editableGrid.getColumnIndex('action'), rowId, true);

          editableGrid.setRowAttribute(rIndex, 'attendantId', newObj.id);

          newRequestRunning[rowId] = false;
        }
      } else {
        editableGrid.setValueAt(rIndex, columnIndex, oldValue, true);
      }

      highlight(realId, success ? 'ok' : 'error');
    },

    error: function(XMLHttpRequest, textStatus, exception) {
      alert('Ajax failure\n' + exception.errortext);
      if (attendantId === 'new') {
        newRequestRunning[rowId] = false;
      }
    },

    async: true
  });
}


/**
 * updateCellValue calls the PHP script that will update the database.
 *
 * @param {EditableGrid} editableGrid the editableGrid which got updated
 * @param {string} rowId              the id of the row which got updated
 * @param {int} columnIndex           the index of the column which got updated
 * @param {int} oldValue              the old value of this cell
 * @param {int} newValue              the new value of this cell
 * @param {function} onResponse       callback function to check if
 *                                    response was success
 */
function updateCellValue(editableGrid, rowId, columnIndex,
                         oldValue, newValue, onResponse) {
  'use strict';
  if (newRequestRunning[rowId] === true) {
    window.setTimeout(function() {
      updateCellValue(editableGrid, rowId, columnIndex,
          oldValue, newValue, onResponse);
    }, 100);
  } else {
    updateAjax(editableGrid, rowId, columnIndex,
        oldValue, newValue, onResponse);
  }
}

function onOpenedCellEditor(rowIndex, columnIndex, editableGrid) {
  'use strict';
  var cell = editableGrid.getCell(rowIndex, columnIndex);
  jQuery(cell).keydown(function(e) {
    var newRowIndex,
        newRowId,
        height;
    if (e.keyCode === 13) {
      newRowIndex = rowIndex + 1;
      newRowId = editableGrid.getRowId(newRowIndex);
      height = jQuery(editableGrid.getRow(rowIndex)).height();
      if (newRowId === null) {
        newRowIndex = rowIndex;
      }

      window.scrollBy(0, height);
      /* use default column index */
      editableGrid.editCell(newRowIndex,
          columnIndex || editableGrid.getColumnIndex('firstName'));
    }
  });
}

//noinspection FunctionNamingConventionJS
function DatabaseGrid() {
  'use strict';
  this.editableGrid = new EditableGrid('attendants', {
    enableSort: true,
    ignoreLastRow: true,
    allowSimultaneousEdition: false,
    tableLoaded: function() {
      editableGrid.initializeGrid(this);
    },

    modelChanged: function(rowIndex, columnIndex, oldValue, newValue) {
      updateCellValue(this, this.getRowId(rowIndex),
          columnIndex, oldValue, newValue, null);
    },

    openedCellEditor: function(rowIndex, columnIndex) {
      onOpenedCellEditor(rowIndex, columnIndex, this);
    }
  });
  this.fetchGrid();
}


/**
 * Fetches grid via ajax
 */
DatabaseGrid.prototype.fetchGrid = function() {
  'use strict';
  //console.log(baseUrl);
  this.editableGrid.loadJSON(baseUrl + '/jsonFetch');
};

function deleteAjax(editableGrid, rowId) {
  'use strict';
  var rowIndex,
      aId;
  if (!alreadyStarted) {
    rowIndex = editableGrid.getRowIndex(rowId);
    aId = editableGrid.getRowAttribute(rowIndex, 'attendantId');
    blockRefresh();
    jQuery.ajax({
      url: baseUrl + '/edit/jsonDeleteRow',
      type: 'POST',
      dataType: 'json',
      data: {
        id: aId
      },
      success: function(response) {
        //console.log(response);
        var playerIdCol,
            playerId,
            startNumCol,
            startNumber,
            i,
            val;
        if (response.result === 'ok') {
          rowIndex = editableGrid.getRowIndex(rowId);
          playerIdCol = editableGrid.getColumnIndex('playerId');
          playerId = editableGrid.getValueAt(rowIndex, playerIdCol);
          startNumCol = editableGrid.getColumnIndex('startNumber');
          startNumber = editableGrid.getValueAt(rowIndex, startNumCol);
          if (playerId) {
            openPlayers[playerId] = usedPlayers[playerId];
            delete usedPlayers[playerId];
            refreshSuggestions(editableGrid);
          }


          editableGrid.removeRow(rowId);
          for (i = 0; i < editableGrid.getRowCount(); i += 1) {
            val = editableGrid.getValueAt(i, startNumCol);
            if (val > startNumber) {
              editableGrid.setValueAt(i, startNumCol, val - 1, true);
            }
          }
        }
      },

      error: function(XMLHttpRequest, textStatus, exception) {
        alert('Ajax failure\n' + exception.errortext);
      },

      async: true
    });
  }
}

function deleteRow(editableGrid, rowId) {
  'use strict';
  if (!alreadyStarted) {
    if (newRequestRunning[rowId] === true) {
      window.setTimeout(function() {
        deleteRow(editableGrid, rowId);
      }, 100);
    } else {
      deleteAjax(editableGrid, rowId);
    }
  }
}

function newPlayerAjax(editableGrid, rowId) {
  'use strict';
  var rowIndex = editableGrid.getRowIndex(rowId),
      aId = editableGrid.getRowAttribute(rowIndex, 'attendantId');
  blockRefresh();
  jQuery.ajax({
    url: baseUrl + '/edit/jsonNewPlayer',
    type: 'POST',
    dataType: 'json',
    data: {
      id: aId
    },
    success: function(response) {
      //console.log(response);
      var playerIdCol, playerId, rIndex;
      if (response.result === 'ok') {
        rIndex = editableGrid.getRowIndex(rowId);
        playerIdCol = editableGrid.getColumnIndex('playerId');
        playerId = editableGrid.getValueAt(rIndex, playerIdCol);
        openPlayers[playerId] = usedPlayers[playerId];
        delete usedPlayers[playerId];
        editableGrid.setValueAt(rIndex, playerIdCol, '', true);
        refreshSuggestions(editableGrid);
      }
    },

    error: function(XMLHttpRequest, textStatus, exception) {
      alert('Ajax failure\n' + exception.errortext);
    },

    async: true
  });
}

function newPlayer(editableGrid, rowId) {
  'use strict';
  if (newRequestRunning[rowId] === true) {
    window.setTimeout(function() {
      newPlayer(editableGrid, rowId);
    }, 100);
  } else {
    newPlayerAjax(editableGrid, rowId);
  }
}


//noinspection JSUnusedGlobalSymbols
/**
 * Deletes a row via ajax.
 *
 * @param {string} id the id of the row to delete
 */
DatabaseGrid.prototype.deleteRow = function(id) {
  'use strict';
  var rowInd, fnCol, lnCol, firstName, lastName,
      middle, name, editableGrid, buttons, dialog;
  if (!alreadyStarted) {
    //noinspection JSCheckFunctionSignatures
    rowInd = this.editableGrid.getRowIndex(id);
    fnCol = this.editableGrid.getColumnIndex('firstName');
    lnCol = this.editableGrid.getColumnIndex('lastName');
    firstName = this.editableGrid.getValueAt(rowInd, fnCol);
    lastName = this.editableGrid.getValueAt(rowInd, lnCol);
    middle = firstName && lastName ? ' ' : '';
    name = (firstName || '') + middle + (lastName || '');
    editableGrid = this.editableGrid;
    if (phone) {
      //phone version
      jQuery('#dialog-confirm-title').text(deleteConfirm.replace('%s', name));
      jQuery('#dialog-confirm-yes').click(function() {
        //console.log(editableGrid);
        //console.log(id);
        deleteRow(editableGrid, id);
      });

      jQuery(':mobile-pagecontainer').pagecontainer('change',
          '#dialog-confirm', {
            role: 'dialog',
            'data-transition': 'flip'
          });
    } else {
      buttons = {};
      buttons[deleteButton] = function() {
        deleteRow(editableGrid, id);
        jQuery(this).dialog('close');
      };

      buttons[cancelButton] = function() {
        jQuery(this).dialog('close');
      };

      dialog = jQuery('#dialog-confirm');
      dialog.dialog({
        resizable: false,
        title: deleteConfirm.replace('%s', name),
        height: 190,
        width: 350,
        modal: true,
        buttons: buttons
      });
      dialog.dialog('open');
    }
  }
};


/**
 * Sets an attendant row to status new player per ajax.
 * (removes player informations if already there)
 *
 * @param {string} id the row id
 */
DatabaseGrid.prototype.newPlayer = function(id) {
  'use strict';
  newPlayer(this.editableGrid, id);
};


/**
 * Initializes autocomplete by getting player
 * informations of database via ajax.
 */
DatabaseGrid.prototype.initAutocomplete = function() {
  'use strict';
  var editableGrid = this.editableGrid;
  jQuery.ajax({
    dataType: 'json',
    url: baseUrl + '/jsonFetchPlayers',
    type: 'GET',
    success: function(players) {
      var plIdCol = editableGrid.getColumnIndex('playerId'),
          i,
          playerId;
      for (i = 0; i < editableGrid.getRowCount(); i += 1) {
        playerId = editableGrid.getValueAt(i, plIdCol);
        if (playerId) {
          usedPlayers[playerId] = true;
        }
      }

      for (i = 0; i < players.length; i += 1) {
        //check if player is already used
        //noinspection JSLint
        if ((players[i].id in usedPlayers)) {
          usedPlayers[players[i].id] = players[i];
        } else {
          openPlayers[players[i].id] = players[i];
        }
      }

      refreshSuggestions(editableGrid);
    },

    error: function(XMLHttpRequest, textStatus, exception) {
      alert('Ajax failure\n' + exception.errortext);
    },

    async: true
  });
};


/**
 * Initializes grid.
 *
 * @param {EditableGrid} grid the grid to initialize
 */
DatabaseGrid.prototype.initializeGrid = function(grid) {
  'use strict';

  /* render for the action column */
  if (editable) {
    grid.setCellRenderer('action', new CellRenderer({
      render: function(cell, id) {
        cell.innerHTML = '';
        if (id) {
          if (alreadyStarted) {
            cell.innerHTML +=
                '<a href="javascript:void(0)" onclick=' +
                '"editableGrid.newPlayer(\'' +
                id + '\');" class="fa fa-trash-o red" >new Player</a>';
          } else {
            cell.innerHTML +=
                '<a href="javascript:void(0)" onclick=' +
                '"editableGrid.deleteRow(\'' +
                id + '\');" class="fa fa-trash-o red" >' + deleteText + '</a>';
          }
        }
      }
    }));

    this.initAutocomplete();
  }
  //noinspection JSCheckFunctionSignatures
  grid.renderGrid('tablecontent', 'testgrid');
};
