/*jshint browser: true*/
/*exported updateRate, updateVol*/
/*global $, lastTask:true, announcement, getUrl, console,
 window, navigator, PERSISTENT, Audio, SpeechSynthesisUtterance, tournamentId*/
/*jslint white: true, browser:true, single:true, devel:true */


var texts = {};
var order = [];
var isSpeaking = false;
var communicationBegan;
var muted = false;
var voices = [];
var spoken = false;
var lastSpoken = 0;
var msg = new SpeechSynthesisUtterance();

$(function() {
  'use strict';
  if (!isChrome()) {
    window.document.getElementById('title').textContent =
        'The tournament speaker only works in Google Chrome, ' +
        'please open this window in Google Chrome!';
    $('#content').hide();
    return;
  }
  window.setInterval(checkNewTexts, 5000);
  $('#rateSlider').val(window.localStorage.getItem('speak_rate') || -0.15);
  $('#volumeSlider').val(window.localStorage.getItem('speak_volume') || 1);
  updateRate();
  updateVol();
  lastSpoken = window.localStorage.getItem('speak_last_' + tournamentId) || 0;
});

function getRate() {
  'use strict';
  var logVal = $('#rateSlider').val();
  return Math.pow(2, logVal);
}

function updateRate() {
  'use strict';
  window.localStorage.setItem('speak_rate', $('#rateSlider').val());
  $('#rateVal').html(Math.round(getRate() * 100) + '%');
}

function getVolume() {
  'use strict';
  return $('#volumeSlider').val() * 1;
}

function updateVol() {
  'use strict';
  var vol = getVolume();
  window.localStorage.setItem('speak_volume', vol);
  $('#volumeVal').html(Math.round(vol * 100) + '%');
}

function isChrome() {
  'use strict';
  var isChrome = !!window.chrome && !!window.chrome.webstore;
  var pdf;
  var i;
  if (!isChrome) {
    return false;
  }

  pdf = false;
  for (i in window.navigator.plugins) {
    if (window.navigator.plugins[i].name === "Chrome PDF Viewer") {
      pdf = true;
    }
  }
  return pdf;
}

function checkNewTexts() {
  'use strict';
  var i;
  $.ajax({
    url: getUrl(),
    type: 'GET',
    /**
     * @param {[{createdAt:int}]} response
     */
    success: function(response) {
      for (i = 0; i < response.length; i++) {
      response[i].text = response[i].text.trim();
        if (!(response[i].id in texts) && lastSpoken < response[i].createdAt && response[i].text != "") {
          order.push(response[i].id);
        }

        texts[response[i].id] = response[i];
        if (response[i].createdAt > lastTask) {
          //noinspection JSUndeclaredVariable
          lastTask = response[i].createdAt;
        }
      }

      speak();
    },

    async: true
  });
}

function errorHandler(e) {
  'use strict';
  console.log(e.name + ': ' + e.message);
}

function getBlob(text) {
  'use strict';
  //noinspection LocalVariableNamingConventionJS
  var bb, BlobBuilder;
  //noinspection UnusedCatchParameterJS
  try {
    //noinspection Eslint
    return new Blob([text], { type: 'text/plain' });
  } catch (e) {
    //The BlobBuilder API has been deprecated in favour of Blob, but older
    //browsers don't know about the Blob constructor
    //IE10 also supports BlobBuilder, but since the `Blob` constructor
    //also works, there's no need to add `MSBlobBuilder`.

    //noinspection JSUnresolvedVariable
    BlobBuilder = window.WebKitBlobBuilder || window.MozBlobBuilder;
    /** @type {{getBlob}} */
    bb = new BlobBuilder();
    bb.append(text);
    return bb.getBlob('text/plain');
  }
}


/**
 * @param {{root:{getFile}}} fs
 * @param {string} text
 */
function writeToFS(fs, text) {
  'use strict';
  fs.root.getFile('log.txt', { create: true, exclusive: false },
      /**
       * @param {{createWriter}} fileEntry
       */
      function(fileEntry) {

        //Create a FileWriter object for our FileEntry (log.txt).
        fileEntry.createWriter(
            /**
             * @param {{onwriteend,write,onerror}} fileWriter
             */
            function(fileWriter) {
              var blob;
              //noinspection JSUnusedLocalSymbols,Eslint
              fileWriter.onwriteend = function(e) {
                console.log('Write completed. Content: ' + text);
              };

              fileWriter.onerror = function(e) {
                var error = e.toString();
                console.log('Write failed: ' + error);
              };

              //Create a new Blob and write it to log.txt.
              blob = getBlob(text);
              fileWriter.write(blob);

            }, errorHandler);

      }, errorHandler);
}

function readFromFS(fs, handler) {
  'use strict';
  fs.root.getFile('log.txt', { create: true, exclusive: false },
      function(fileEntry) {

        //Get a File object representing the file,
        //then use FileReader to read its contents.
        fileEntry.file(function(file) {
          //noinspection Eslint
          var reader = new FileReader();

          //noinspection JSUnusedLocalSymbols,Eslint
          reader.onloadend = function(e) {
            handler(this.result);
          };

          reader.readAsText(file);
        }, errorHandler);

      }, errorHandler);
}

function accessFileSystem(handler) {
  'use strict';
  var requestedBytes = 1024 * 1024;
  //noinspection JSUnresolvedVariable
  window.requestFileSystem = window.requestFileSystem ||
      window.webkitRequestFileSystem;

  //noinspection JSUnresolvedVariable,JSUnresolvedFunction
  navigator.webkitPersistentStorage.requestQuota(
      requestedBytes, function(grantedBytes) {
        //noinspection JSUnresolvedFunction
        window.webkitRequestFileSystem(PERSISTENT,
            grantedBytes, handler, errorHandler);
      }, function(e) {
        console.log('Error', e);
      }
  );
}

function handleReadValue(val) {
  'use strict';
  var trimmed = val.trim();
  if (trimmed === '2') {
    //muted
    muted = true;
  } else if (trimmed === '0') {
    //unmuted
    muted = false;
  }
}

function initMute(fs) {
  'use strict';
  writeToFS(fs, '1');
}

function initUnMute(fs) {
  'use strict';
  writeToFS(fs, '3');
}

function initCheckFile(fs) {
  'use strict';
  readFromFS(fs, handleReadValue);
}

function setMute(mute) {
  'use strict';
  if (mute) {
    accessFileSystem(initMute);
  } else {
    accessFileSystem(initUnMute);
  }

  communicationBegan = new Date();
}

function checkIfEnded() {
  'use strict';
  accessFileSystem(initCheckFile);
  if (!muted ||
      (communicationBegan !== null && new Date() - communicationBegan > 5000)) {
    isSpeaking = false;
  } else {
    //check again later
    window.setTimeout(checkIfEnded, 500);
  }
}

var speechUtteranceChunker = function(utt, settings, callback) {
  'use strict';
  settings = settings || {};
  var newUtt;
  var txt = (settings && settings.offset !== undefined ? utt.text.substring(settings.offset) : utt.text);
  if (utt.voice && utt.voice.voiceURI === 'native') { // Not part of the spec
    newUtt = utt;
    newUtt.text = txt;
    newUtt.addEventListener('end', function() {
      if (speechUtteranceChunker.cancel) {
        speechUtteranceChunker.cancel = false;
      }
      if (callback !== undefined) {
        callback();
      }
    });
  }
  else {
    var chunkLength = (settings && settings.chunkLength) || 160;
    var pattRegex = new RegExp('^[\\s\\S]{' + Math.floor(chunkLength / 2) + ',' + chunkLength + '}[.!?,]{1}|^[\\s\\S]{1,' + chunkLength + '}$|^[\\s\\S]{1,' + chunkLength + '} ');
    var chunkArr = txt.match(pattRegex);

    if (chunkArr[0] === undefined || chunkArr[0].length <= 2) {
      //call once all text has been spoken...
      if (callback !== undefined) {
        callback();
      }
      return;
    }
    var chunk = chunkArr[0];
    newUtt = new SpeechSynthesisUtterance(chunk);
    var x;
    for (x in utt) {
      if (x !== 'text') {
        console.log(x);
        newUtt[x] = utt[x];
      }
    }
    newUtt.rate = getRate();
    newUtt.volume = getVolume();
    newUtt.addEventListener('end', function() {
      if (speechUtteranceChunker.cancel) {
        speechUtteranceChunker.cancel = false;
        return;
      }
      settings.offset = settings.offset || 0;
      settings.offset += chunk.length - 1;
      speechUtteranceChunker(utt, settings, callback);
    });
  }

  if (settings.modifier) {
    settings.modifier(newUtt);
  }
  console.log(newUtt); //IMPORTANT!! Do not remove: Logging the object out fixes some onend firing issues.
  //placing the speak invocation inside a callback fixes ordering and onend issues.
  window.setTimeout(function() {
    window.speechSynthesis.speak(newUtt);
  }, 0);
};

function speakNext() {
  'use strict';
  var next;
  if (order.length === 0) {
    //finished all speak tasks unmute!
    setMute(false);
    window.setTimeout(checkIfEnded, 500);
    return;
  }

  voices = voices.filter(function(voice) {
    return voice.lang === 'de-DE';
  });
  console.log(voices);
  next = order.shift();
  msg.rate = getRate();
  msg.volume = getVolume();
  if (voices.length > 0) {
    msg.voice = voices[0];
  }
  msg.text = texts[next].text;
  console.log(texts[next].createdAt);
  speechUtteranceChunker(msg, { chunkLength: 120 }, function() {
    //noinspection JSUnresolvedVariable
    console.log('end');
    console.log(texts[next].createdAt);
    window.localStorage.setItem(
        'speak_last_' + tournamentId, texts[next].createdAt);
    speakNext();
  });
}

function doSpeak(force) {
  'use strict';

  if (voices.length === 0) {
    //noinspection JSUnresolvedVariable,JSUnresolvedFunction
    voices = window.speechSynthesis.getVoices();
  }
  if (voices.length === 0 && !force) {
    //wait for voices
    console.log('wait for voices');
    //noinspection JSUnresolvedVariable
    window.speechSynthesis.onvoiceschanged = function() {
      doSpeak(false);
    };
    window.setTimeout(function() {
      doSpeak(true);
    }, 5000);
  } else if (!spoken) {
    spoken = true;
    speakNext();
  }
}

function startSpeak() {
  'use strict';
  var audio = new Audio(announcement);
  audio.play();
  window.setTimeout(function() {
    spoken = false;
    doSpeak(false);
  }, 3000);
}

function checkToStartSpeak() {
  'use strict';
  accessFileSystem(initCheckFile);
  if (muted) {
    window.setTimeout(startSpeak, 100);
  } else if (communicationBegan !== null &&
      new Date() - communicationBegan > 5000) {
    //waited too long, don't expect to communicate anymore
    window.setTimeout(startSpeak, 100);
  } else {
    //check again later
    window.setTimeout(checkToStartSpeak, 500);
  }
}

function speak() {
  'use strict';
  if (!isSpeaking && order.length > 0) {
    isSpeaking = true;
    muted = false;
    setMute(true);
    window.setTimeout(checkToStartSpeak, 100);
  } else if (isSpeaking && order.length > 0) {
    window.setTimeout(speak, 1000);
  }
}
