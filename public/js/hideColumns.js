/*global $, window */
/*jshint browser: true*/
/*jslint white: true */

//noinspection Eslint,JSUnusedGlobalSymbols
function saveHideColumns(rId) {
  'use strict';
  $('#hide-columns-form').find('input').each(function(index, element) {
    var key, value;
    if ($(this).attr('type') === 'checkbox') {
      key = $(this).attr('name');
      value = element.checked ? 1 : 0;
      window.localStorage.setItem('hideColumns_' + rId + "_" + key, value);
    }
  });

  goToRanking();
}

function convertToId(columns, rId) {
  fac = 1;
  result = 0;
  for (i = 0; i < columns.length; i++) {
    value = window.localStorage.getItem('hideColumns_' + rId + "_" + columns[i]['key']);
    if ((value === '0' && columns[i]['default-on']) || (value === '1' && !columns[i]['default-on'])) {
      result += fac;
    }
    fac *= 2;
  }
  return result;
}

function loadLocalSettings(rId) {
  $('#hide-columns-form').find('input').each(function(index, element) {
    var key, value;
    if ($(this).attr('type') === 'checkbox') {
      key = $(this).attr('name');
      value = window.localStorage.getItem('hideColumns_' + rId + "_" + key);
      if (value !== null) {
        $(this).prop('checked', value === '1');
      }
    }
  });
}
//load local settings
/*
 $(function() {
 'use strict';
 var tournamentId = $('#hidden_tournamentId').val();
 //noinspection JSUnusedLocalSymbols
 $('#local-settings-form').find('input').each(function(index, element) {
 var key, value;
 if ($(this).attr('type') !== 'hidden' &&
 $(this).attr('type') !== 'button') {
 key = tournamentId + '_' + $(this).attr('name');
 value = window.localStorage.getItem(key);
 if (value !== null) {
 if ($(this).attr('type') === 'checkbox') {
 $(this).prop('checked', value === '1');
 } else {
 $(this).val(value);
 }
 }
 }
 });

 }); */
