<?php

namespace SingleKO\Factory;

use SingleKO\Service\SingleKOService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SingleKOServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $stableSort = $serviceLocator->get('StableSort');

    return new SingleKOService($stableSort);
  }
//</editor-fold desc="Public Methods">
}

