<?php

namespace SingleKO\Service;

use Doctrine\ORM\EntityManager;
use FDM\Entity\Attendant;
use FDM\Entity\Game;
use FDM\Entity\Group;
use FDM\Entity\Ranking;
use FDM\Service\ModeServiceAbstract;
use FDM\Service\ModeServiceInterface;
use FDM\Service\StableSortService;

class SingleKOService extends ModeServiceAbstract
  implements ModeServiceInterface
{
//<editor-fold desc="Fields">
  protected $stableSort;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(StableSortService $stableSort)
  {
    $this->stableSort = $stableSort;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getExtraStructures(Group $group)
  {
    return array_merge(parent::getExtraStructures($group), [
      "title" => "Bracket",
      "type" => "element",
      "controller" => "SingleKO\Controller\SingleKO",
      "action" => "bracket",
      "parameters" => [
        "id" => $group->getPhase()->getTournament()->getId(),
        "phase" => $group->getPhase()->getId(),
        "group" => $group->getId(),
      ],
    ]);
  }

  public function getNewRanking()
  {
    return new Ranking(["lastRoundPlayed", "numLost"]);
  }

  public function recalculateGames(Group $group, EntityManager $em, &$ended)
  {
    $ended = true;
    //delete old scheduled games
    $this->deleteScheduledGames($group, $em);

    $rankingByStart = [];
    $teams = [];
    $bestRank = [];
    $isWaiting = [];

    $this->getDataStructures($group, $rankingByStart, $teams, $bestRank, $isWaiting, $ended);
    $this->calculateGames($group, $em, $rankingByStart, $teams, $bestRank, $isWaiting, $ended);
  }

  public function roundsReversed()
  {
    return true;
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  protected function calculateGames(Group $group, EntityManager $em, &$rankingByStart, &$teams, &$bestRank, &$isWaiting,
                                    &$ended)
  {
    for ($i = count($teams); $i > 1; $i = ($i + ($i % 2)) / 2) {
      foreach ($group->getGamesByType(Game::TYPE_FINISHED) as $game) {
        if ($game->getRound() < $i &&
          (2 * $game->getRound() >= $i || $i == 2)
        ) {
          $winnerInd = min(
            $bestRank[$game->getTeamA()->first()->getPhaseStartRank()],
            $bestRank[$game->getTeamB()->first()->getPhaseStartRank()]
          );
          $loserInd = max(
            $bestRank[$game->getTeamA()->first()->getPhaseStartRank()],
            $bestRank[$game->getTeamB()->first()->getPhaseStartRank()]
          );
          if ($game->getResultA() > $game->getResultB()) {
            $winner = $game->getTeamA()->first()->getPhaseStartRank();
            $loser = $game->getTeamB()->first()->getPhaseStartRank();
          } else {
            $winner = $game->getTeamB()->first()->getPhaseStartRank();
            $loser = $game->getTeamA()->first()->getPhaseStartRank();
          }

          if ($bestRank[$winner] != $winnerInd) {
            $tmp = $teams[$winnerInd];
            $teams[$winnerInd] = $teams[$loserInd];
            $teams[$loserInd] = $tmp;

            $bestRank[$winner] = $winnerInd;
            $bestRank[$loser] = $loserInd;
          }
        }
      }
    }

    foreach ($group->getRankings() as $ranking) {
      if ($isWaiting[$ranking->getPhaseStartRank()] &&
        $ranking->getLastRoundPlayed() > 1 &&
        ($ranking->getNumLost() == 0 ||
          $ranking->getLastRoundPlayed() == 2)
      ) {
        $r = $bestRank[$ranking->getPhaseStartRank()];
        $nextRound = $ranking->getLastRoundPlayed() / 2;
        $oponent = $nextRound * 2 + 1 - $r;
        if ($ranking->getNumLost() == 1) {
          //special case game for place  3
          $oponent = 7 - $r;
          $nextRound = 0;
        }
        $oRanking = $teams[$oponent];
        if ($isWaiting[$oRanking->getPhaseStartRank()]) {
          //create game
          if ($oRanking->getLastRoundPlayed() ==
            $ranking->getLastRoundPlayed()
          ) {
            $ended = false;
            $game = new Game();
            $game->addToTeamA($ranking);
            $game->addToTeamB($oRanking);
            $game->setRound($nextRound);
            $game->setType(Game::TYPE_SCHEDULED_DYN);
            $game->setRelevance(
              $this->computeRelevance($group) + $nextRound
            );
            $game->setGroup($group);
            $em->persist($game);
            $isWaiting[$ranking->getPhaseStartRank()] = false;
            $isWaiting[$oRanking->getPhaseStartRank()] = false;
          }
        }
      }
    }
  }

  protected function computeRelevance(/** @noinspection PhpUnusedParameterInspection */
    Group $group)
  {
    //TODO!
    return 0;
  }

  protected function deleteScheduledGames(Group $group, EntityManager $em)
  {
    $toRemove = [];
    foreach ($group->getGames() as $game) {
      if ($game->getType() == Game::TYPE_SCHEDULED_DYN) {
        $toRemove[] = $game;
      }
    }
    foreach ($toRemove as $el) {
      foreach ($el->getTeamA() as $ranking) {
        $ranking->getGamesAsA()->removeElement($el);
      }
      foreach ($el->getTeamB() as $ranking) {
        $ranking->getGamesAsB()->removeElement($el);
      }
      $group->getGames()->removeElement($el);
      $em->remove($el);
    }
  }

  protected function getDataStructures(Group $group, &$rankingByStart, &$teams, &$bestRank, &$isWaiting, &$ended)
  {
    foreach ($group->getRankings() as $ranking) {
      $rankingByStart[$ranking->getPhaseStartRank()] = $ranking;
    }

    foreach ($group->getRankings() as $ranking) {
      /** @var Ranking $ranking */
      $r = $ranking->getPhaseStartRank();
      $teams[$r] = $ranking;
      $bestRank[$r] = $r;
      $isWaiting[$r] = true;
      foreach ($ranking->getAttendants() as $att) {
        if ($att->getStatus() != Attendant::STATE_PLAYING) {
          $isWaiting[$r] = false;
          break;
        }
      }
      foreach ($ranking->getGames() as $game) {
        if ($game->getType() != Game::TYPE_FINISHED &&
          $game->getType() != Game::TYPE_SCHEDULED_DYN
        ) {
          $isWaiting[$r] = false;
          $ended = false;
          break;
        }
      }
    }
    ksort($teams);
  }
//</editor-fold desc="Protected Methods">
//<editor-fold desc="Private Methods">
//</editor-fold desc="Private Methods">
}
