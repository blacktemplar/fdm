<?php

namespace SingleKO\Service;

use Doctrine\Common\Collections\ArrayCollection;
use FDM\Entity\Game;
use FDM\Entity\Group;
use FDM\Entity\Ranking;
use FDM\Service\RankingServiceAbstract;
use FDM\Service\RankingServiceInterface;

class SingleKORankingService extends RankingServiceAbstract
  implements RankingServiceInterface
{
//<editor-fold desc="Public Methods">
  public function compareBetweenGroups(
    Ranking $r1, Ranking $r2)
  {
    return 0;
  }

  public function getColumns()
  {
    return [];
  }

  public function recalculateRankings(Group $group)
  {
    $rankings = $this->adjustRankingValues($group);

    $this->setDisplayRanks($group, $rankings);

    //order rankings;
    usort($rankings, [$this, "compare"]);

    //set ranks
    $rank = 1;
    $indexedRankings = [];
    foreach ($rankings as $ranking) {
      $ranking->setRank($rank);
      $indexedRankings[$rank] = $ranking;
      $rank++;
    }
    $group->setRankings(
      new ArrayCollection($indexedRankings)
    );
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  protected function adjustRankingValues(Group $group)
  {
    $rankings = $this->resetRankings($group);
    //loop through games and adjust rankings
    foreach ($group->getGamesByType(Game::TYPE_FINISHED) as $game) {
      $teamAWon = $game->getResultA() > $game->getResultB();
      $teamBWon = $game->getResultB() > $game->getResultA();
      foreach ($game->getTeamA() as $ranking) {
        if ($game->getRound() < $ranking->getLastRoundPlayed()) {
          $ranking->setLastRoundPlayed($game->getRound());
        }
        if ($teamBWon) {
          $ranking->setNumLost($ranking->getNumLost() + 1);
        }
      }
      foreach ($game->getTeamB() as $ranking) {
        if ($game->getRound() < $ranking->getLastRoundPlayed()) {
          $ranking->setLastRoundPlayed($game->getRound());
        }
        if ($teamAWon) {
          $ranking->setNumLost($ranking->getNumLost() + 1);
        }
      }
    }

    return $rankings;
  }

  protected function compare(Ranking $r1, Ranking $r2)
  {
    return $this->compareProperties(
      $r1, $r2, [
        "getDisplayRank" => -1,
        "getNumLost" => -1,
        "getPhaseStartRank" => -1,
      ]
    );
  }

  protected function getLastRound($nTeams)
  {
    $res = 1;
    while ($nTeams > 2) {
      $nTeams = ($nTeams + ($nTeams % 2)) / 2;
      $res *= 2;
    }
    return $res;
  }

  protected function reset(Ranking $ranking, $numTeams)
  {
    $ranking->setNumLost(0);
    $lastRound = $this->getLastRound($numTeams);

    if ($lastRound * 2 - $ranking->getPhaseStartRank() < $numTeams) {
      $ranking->setLastRoundPlayed($lastRound * 2);
    } else {
      $ranking->setLastRoundPlayed($lastRound);
    }

    $ranking->setRank(0);
    $ranking->setDisplayRank(0);
  }

  /**
   * @param Group $group
   * @return Ranking[]
   */
  protected function resetRankings(Group $group)
  {
    $rankings = [];
    //reset rankings
    foreach ($group->getRankings() as $ranking) {
      $this->reset($ranking, count($group->getRankings()));
      $rankings[] = $ranking;
    }

    return $rankings;
  }

  /**
   * @param Group $group
   * @param Ranking[] $rankings
   */
  protected function setDisplayRanks(Group $group, $rankings)
  {
    //set display ranks which is the worst possible rank
    foreach ($rankings as $ranking) {
      $lostRound = $ranking->getLastRoundPlayed();
      if ($ranking->getNumLost() == 0) {
        if ($lostRound == 1) {
          $lostRound = 0;
        } else {
          $lostRound /= 2;
        }
      }
      $ranking->setDisplayRank($lostRound + 1);
      if ($lostRound == 2) {
        //semi final: special case game for place 3 not played
        $ranking->setDisplayRank(4);
      } else if ($lostRound == 0 && $ranking->getNumLost() >= 1) {
        //semi final: special case game for place 3 played
        $ranking->setDisplayRank(2 + $ranking->getNumLost());
      }
    }
  }
//</editor-fold desc="Protected Methods">
//<editor-fold desc="Private Methods">
//</editor-fold desc="Private Methods">
}
