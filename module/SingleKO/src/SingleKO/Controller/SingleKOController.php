<?php
namespace SingleKO\Controller;

use Doctrine\ORM\EntityManager;
use FDM\Entity\Game;
use FDM\Entity\TournamentPrivileges;
use FDM\Service\FlushServiceInterface;
use FDM\Service\LoadingService;
use FDM\Service\PhaseServiceInterface;
use FDM\Service\TournamentAccessServiceInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class SingleKOController extends AbstractActionController
{
//<editor-fold desc="Fields">
  protected $em;

  protected $fs;

  protected $ls;

  protected $ps;

  protected $tas;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    EntityManager $em, FlushServiceInterface $fs, PhaseServiceInterface $ps,
    TournamentAccessServiceInterface $tas, LoadingService $ls
  )
  {
    $this->em = $em;
    $this->fs = $fs;
    $this->ps = $ps;
    $this->tas = $tas;
    $this->ls = $ls;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function bracketAction()
  {
    //build bracket with jquery
    $data = [];

    $groupId = $this->params('group');
    $group = $this->em->find('FDM\Entity\Group', $groupId);

    $rankingByStartRank = $this->getRankingsByStartRank($group);

    $numTeams = 0;
    $order = $this->getOrder(count($group->getRankings()), $numTeams);


    $data["teams"] = [];
    for ($i = 0; $i < $numTeams; $i += 2) {
      $arr = [];
      if (array_key_exists($order[$i], $rankingByStartRank)) {
        $arr[] = $rankingByStartRank[$order[$i]];
      } else {
        $arr[] = null;
      }
      if (array_key_exists($order[$i + 1], $rankingByStartRank)) {
        $arr[] = $rankingByStartRank[$order[$i + 1]];
      } else {
        $arr[] = null;
      }
      $data["teams"][] = $arr;
    }

    $data["results"] = $this->getResults($numTeams, $order, $rankingByStartRank, $group);

    return new ViewModel(
      [
        "data" => $data,
        "groupId" => $groupId,
        "tournamentId" => $this->params('id'),
        "phaseId" => $this->params('phase'),
        "editable" =>
          $this->tas->hasAccessType(
            TournamentPrivileges::TYPE_EDIT
          ),
        "numTables" => $group->getPhase()->getNumTables(),
      ]
    );
  }

  public function editBracketAction()
  {
    $groupId = $this->params('group');
    $group = $this->em->find('FDM\Entity\Group', $groupId);
    if ($group->getPhase()->getId() != $this->params('phase')) {
      //invalid phase !!!
      throw new \Exception("Invalid phase id!");
    }

    $this->ls->loadPhaseCompletely($group->getPhase());


    $numTeams = 0;
    $order = $this->getOrder(count($group->getRankings()), $numTeams);

    $changed = false;

    foreach ($group->getGames() as $game) {
      /** @var Game $game */
      $bracket = 0;
      $round = 0;
      $ind = 0;
      $teams = ["A", "B"];
      $this->getGameIndices(
        $game, $order, $numTeams, $bracket, $round, $ind, $teams
      );
      $getter1 = "getResult" . $teams[0];
      $getter2 = "getResult" . $teams[1];
      $res1 = $game->$getter1();
      $res2 = $game->$getter2();
      if (array_key_exists("results", $_POST)
        && array_key_exists(0, $_POST["results"])
        && array_key_exists($round, $_POST["results"][0])
        && array_key_exists($ind, $_POST["results"][0][$round])
        && array_key_exists(0, $_POST["results"][0][$round][$ind])
        && array_key_exists(1, $_POST["results"][0][$round][$ind])
      ) {
        $newRes1 = $_POST["results"][0][$round][$ind][0];
        $newRes2 = $_POST["results"][0][$round][$ind][1];
        $matchInfo = $_POST["results"][0][$round][$ind][2];
        $winner = null;
        if ($res1 > $res2) {
          $getter = "getTeam" . $teams[0];
          $winner = $game->$getter()->first();
        } else if ($res2 > $res1) {
          $getter = "getTeam" . $teams[1];
          $winner = $game->$getter()->first();
        }
        $newWinner = null;
        if ($newRes1 > $newRes2) {
          $getter = "getTeam" . $teams[0];
          $newWinner = $game->$getter()->first();
        } else if ($newRes2 > $newRes1) {
          $getter = "getTeam" . $teams[1];
          $newWinner = $game->$getter()->first();
        }

        $tmpChanged = false;
        if ($res1 != $newRes1) {
          //$res1 changed
          $setter1 = "setResult" . $teams[0];
          $game->$setter1($newRes1);
          $tmpChanged = true;
        }
        if ($res2 != $newRes2) {
          //$res2 changed
          $setter2 = "setResult" . $teams[1];
          $game->$setter2($newRes2);
          $tmpChanged = true;
        }
        if ($tmpChanged && $newRes1 == $newRes2) {
          //game not finished yet
          $game->setResultA($newRes1);
          $game->setResultB($newRes2);
          if ($newRes1 == 0) {
            if ($game->getTable() == 0) {
              $game->setType(Game::TYPE_SCHEDULED_DYN);
            } else {
              if ($game->getType() == Game::TYPE_SCHEDULED_DYN ||
                $game->getType() == Game::TYPE_SCHEDULED_DYN_CONFLICT ||
                $game->getType() == Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN ||
                $game->getType() == Game::TYPE_SCHEDULED_FIX
              ) {
                $game->setStartTime(new \DateTime());
              }
              $game->setType(Game::TYPE_RUNNING);
            }
          }
          $changed = true;
          //delete dependent games
          $this->deleteDescendantGames($game);
        }
        if ($newRes1 > 0 || $newRes2 > 0 || !$tmpChanged) {
          if ($matchInfo["table"] != $game->getTable() &&
            $matchInfo["table"] != 0
          ) {
            $game->setTable($matchInfo["table"]);
            if ($game->getType() == Game::TYPE_SCHEDULED_DYN &&
              $game->getTable() != 0
            ) {
              $game->setType(Game::TYPE_RUNNING);
              $game->setStartTime(new \DateTime());
            }
            if ($game->getType() == Game::TYPE_RUNNING &&
              $game->getTable() == 0
            ) {
              $game->setType(Game::TYPE_SCHEDULED_DYN);
            }
            $changed = true;
          }
          if ($tmpChanged) {
            if ($game->getType() == Game::TYPE_SCHEDULED_DYN ||
              $game->getType() == Game::TYPE_SCHEDULED_DYN_CONFLICT ||
              $game->getType() == Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN ||
              $game->getType() == Game::TYPE_SCHEDULED_FIX
            ) {
              $game->setStartTime(new \DateTime());
            }
            $game->setType(Game::TYPE_FINISHED);
            $changed = true;
            if ($winner != $newWinner) {
              if ($newWinner != null && $winner != null) {
                foreach ($winner->getGamesAsA() as $g) {
                  if ($g->getRound() < $game->getRound()) {
                    $g->clearTeamA();
                    $g->addToTeamA($newWinner);
                  }
                }
                foreach ($winner->getGamesAsB() as $g) {
                  if ($g->getRound() < $game->getRound()) {
                    $g->clearTeamB();
                    $g->addToTeamB($newWinner);
                  }
                }
              }
            }
          }
        }
      }
    }
    if ($changed) {
      $this->fs->flush();
    }
    return new JsonModel();
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  /** @noinspection PhpTooManyParametersInspection */
  protected function getGameIndices(
    $game, $order, $numTeams, &$bracket, &$round, &$ind, &$teams
  )
  {
    $bracket = 0; //only single bracket
    $teamA = $game->getTeamA()->first()->getPhaseStartRank();
    $teams[0] = "A";
    $teams[1] = "B";

    $ind = array_search($teamA, $order);
    $round = 0;
    $start = $game->getRound();
    $fixInd = -1;
    if ($game->getRound() == 0) {
      $start = 1;
      $fixInd = 1;
    }
    for ($i = $start * 2; $i < $numTeams; $i *= 2) {
      $ind = ($ind - ($ind % 2)) / 2;
      $round++;
    }

    if ($ind % 2 == 1) {
      $teams[0] = "B";
      $teams[1] = "A";
    }
    $ind = ($ind - ($ind % 2)) / 2;
    if ($fixInd >= 0) {
      $ind = $fixInd;
    }
  }

  protected function getResults($numTeams, &$order, &$rankingByStartRank, $group)
  {
    $results = $this->initResults();
    foreach ($group->getGames() as $game) {
      $bracket = 0;
      $round = 0;
      $ind = 0;
      $teams = ["A", "B"];
      $this->getGameIndices(
        $game, $order, $numTeams, $bracket, $round, $ind, $teams
      );
      if ($bracket < 0 || $bracket == "-1") {
        continue;
      }
      $getter1 = "getResult" . $teams[0];
      $getter2 = "getResult" . $teams[1];
      $res1 = $game->$getter1();
      $res2 = $game->$getter2();

      if ($game->getType() == Game::TYPE_FINISHED) {
        $this->setOrAppend($results, [$bracket, $round, $ind, 0], $res1);
        $this->setOrAppend($results, [$bracket, $round, $ind, 1], $res2);
      } else {
        $this->setOrAppend($results, [$bracket, $round, $ind, 0], null);
        $this->setOrAppend($results, [$bracket, $round, $ind, 1], null);
      }
      $results[$bracket][$round][$ind][2] = [
        "round" => $round,
        "ind" => $ind,
        "table" => $game->getTable(),
      ];
    }
    return $results;
  }

  protected function initResults()
  {
    $results = [];
    $results[0] = []; //only one bracket
    $results[0][0] = [];
    $results[0][0][0] = [];
    return $results;
  }
//</editor-fold desc="Protected Methods">

//<editor-fold desc="Private Methods">
  private function deleteDescendantGames(Game $g)
  {
    $this->deleteDescendantsFromTeam($g->getTeamA(), $g->getRound());
    $this->deleteDescendantsFromTeam($g->getTeamB(), $g->getRound());
  }

  private function deleteDescendantsFromTeam($team, $round)
  {
    foreach ($team as $r) {
      for ($i = 0; $i < count($r->getGames()); $i++) {
        $newG = $r->getGames()[$i];
        if ($newG->getRound() < $round) {
          $this->deleteDescendantGames($newG);
          $this->deleteGame($newG);
        }
      }
    }
  }

  private function deleteGame(Game $g)
  {
    foreach ($g->getTeamA() as $ranking) {
      $ranking->getGamesAsA()->removeElement($g);
    }
    foreach ($g->getTeamB() as $ranking) {
      $ranking->getGamesAsB()->removeElement($g);
    }
    $g->getGroup()->getGames()->removeElement($g);
    $this->em->remove($g);
  }

  private function getOrder($numRankings, &$numTeams)
  {
    $numTeams = 1;
    $order = [];
    $order[] = 1;

    while ($numTeams < $numRankings) {
      $numTeams *= 2;
      $tmp = [];
      $betterFirst = true;
      foreach ($order as $num) {
        $op = $numTeams + 1 - $num;
        if ($betterFirst) {
          $tmp[] = $num;
          $tmp[] = $op;
          $betterFirst = false;
        } else {
          $tmp[] = $op;
          $tmp[] = $num;
          $betterFirst = true;
        }
      }
      $order = $tmp;
    }
    return $order;
  }

  private function getRankingsByStartRank($group)
  {
    $rankingByStartRank = [];

    foreach ($group->getRankings() as $ranking) {
      $rankingByStartRank[$ranking->getPhaseStartRank()] = $ranking;
    }

    ksort($rankingByStartRank);
    return $rankingByStartRank;
  }

  private function setOrAppend(&$arr, $inds, $val, $offset = 0)
  {
    $ind = $inds[$offset];
    for ($i = count($arr); $i <= $ind; $i++) {
      if (count($inds) - $offset > 1) {
        $arr[] = [];
      } else {
        $arr[] = null;
      }
    }
    if (count($inds) - $offset > 1) {
      return $this->setOrAppend($arr[$ind], $inds, $val, $offset + 1);
    } else {
      $arr[$ind] = $val;
    }
  }
//</editor-fold desc="Private Methods">
} 
