<?php
return [
  'service_manager' => [
    'factories' => [
      'SingleKOService' => '\SingleKO\Factory\SingleKOServiceFactory',
      '\SingleKO\Service\SingleKOService' => '\SingleKO\Factory\SingleKOServiceFactory'
    ],
    'invokables' => [
      'SingleKO\Service\SingleKORankingService' => 'SingleKO\Service\SingleKORankingService'
    ],
  ],
  'controllers' => [
    'factories' => [
      'SingleKO\Controller\SingleKO' =>
        '\SingleKO\Factory\SingleKOControllerFactory'
    ],
    'invokables' => [],
  ],
  'view_manager' => [
    'template_path_stack' => [
      'single-ko' => __DIR__ . '/../view',
    ],
  ],
  'doctrine' => [
    'eventmanager' => [
      'orm_default' => [
        'subscribers' => [
          'Gedmo\Timestampable\TimestampableListener',
        ],
      ],
    ],
    'driver' => [
      'singleko_entities' => [
        'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
        'cache' => 'array',
        'paths' => [
          __DIR__ . '/../src/SingleKO/Entity',
        ],
      ],
    ],
  ],
  'router' => [
    'routes' => [
      'tournament' => [
        'child_routes' => [
          'phase' => [
            'child_routes' => [
              'group' => [
                'type' => 'segment',
                'may_terminate' => false,
                'options' => [
                  'route' => '/group/:group',
                  'constraints' => [
                    'group' => '[a-f\-0-9]*',
                  ],
                ],
                'child_routes' => [
                  'edit' => [
                    'type' => 'segment',
                    'may_terminate' => false,
                    'options' => [
                      'defaults' => [
                        'controller' => 'SingleKO\Controller\SingleKO',
                      ],
                      'route' => '/edit',
                    ],
                    'child_routes' => [
                      'bracket' => [
                        'type' => 'segment',
                        'may_terminate' => true,
                        'options' => [
                          'route' => '/bracket',
                          'defaults' => [
                            'action' => 'editBracket'
                          ],
                        ],
                      ],
                    ],
                  ],
                ],
              ],
            ],
          ],
        ],
      ]
    ],
  ],
  'strategies' => [
    'ViewJsonStrategy',
  ],
];
