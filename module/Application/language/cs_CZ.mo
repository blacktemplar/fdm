��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <          (     :  
   L     W     ^  #   f     �  %   �  F   �  ^     N   k  4   �  E   �     5                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: David Lukas <david.lukas@zfdaily.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: cs_CZ
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Vyskytla se chyba 404 Další informace Vyskytla se chyba Controller Soubor Zpráva Žádná výjimka není k dispozici Předchozí výjimky Trasování zásobníku (Stack trace) S požadovaným URL nebyla při směrování (routing) nalezena shoda. Požadovaný controller se nepodařilo namapovat na žádnou existující třídu controlleru. Požadovaný controller nepodporuje vyřízení (controller not dispatchable). Požadovaný controller nemohl vyřídit požadavek. Momentálně nedokážeme určit, proč byla vygenerována chyba 404. je mapován na %s 