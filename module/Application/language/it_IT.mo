��          �            h     i     ~     �  
   �     �     �     �     �  2   �  M     .   k  <   �  9   �               %     D     \  
   w     �  	   �  #   �     �  T   �  X     7   x  H   �  V   �     P                      	                                            
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Marco Pivetta <ocramius@gmail.com>
Language-Team: ZF Contributors <zf-devteam@zend.com>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Poedit-Bookmarks: -1,-1,-1,-1,-1,-1,5,-1,-1,-1
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Si è verificato un errore 404 Informazioni aggiuntive Si è verificato un errore Controller File Messaggio Non è disponibile alcuna eccezione Eccezioni precedenti Non è stato possibile effettuare il match dell'indirizzo richiesto tramite routing. Non è stato possibile mappare il controller richiesto ad una classe di tipo controller. Il controller richiesto non è un oggetto dispatchable. Il controller richiesto non è stato in grado di elaborare la richiesta. In questo momento non siamo in grado di determinare perchè sia stato generato un 404. viene risolto in %s 