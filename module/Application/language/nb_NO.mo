��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <          +     C  
   Y     d     h     p     �  	   �  3   �  T   �  -   ,  <   Z  M   �     �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Sven Anders Robbestad <robbestad@gmail.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 En 404 feil oppsto Ytterligere informasjon En feil har oppstått Kontroller Fil Beskjed Ingen unntak tilgjengelig Forrige unntak Stakkspor Den angitte URL kunne ikke finnes i rutingoppsettet Den valgte kontrolleren kunne ikke knyttes opp mot en eksisterende kontrollerklasse. Den forspurte kontrolleren kunne ikke brukes. Den valgte kontrolleren kunne ikke håndtere forespørselen. På dette tidspunkt kan vi ikke bestemme årsaken til at en 404 ble generert. løser til %s 