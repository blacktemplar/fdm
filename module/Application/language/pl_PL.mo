��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <          0     E  	   W     a  	   f     p     �     �  ?   �  P   �  0   :  2   k  B   �     �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Łukasz Rodziewicz <lukasz@rodziewicz.com.pl>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Wystąpił błąd 404 Dodatkowe informacje Wystąpił błąd Kontroler Plik Komunikat Brak dostępnego wyjątku Poprzedni wyjątek Stack trace Żądany adres URL nie mógł zostać powiązany z routing'iem. Żądany kontroler nie mógł być zmapowany na isteniejącą klasę kontrolera. Żądany kontroler nie mógł zostać zmapowany. Żądany kontroler nie mógł zmapować żądania. Nie możemy określić tym razem dlaczego wygenerowano błąd 404. rozwiązuje na %s 