��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <               4     G  	   ^     h  $   w     �     �  S   �  y   %  L   �  U   �  ]   B     �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Do Nhu Vy <donhuvy@hotmail.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: vi_VN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Lỗi 404 xảy ra Thông tin bổ sung Có lỗi xảy ra Trình điều khiển Tập tin Thông điệp Không tồn tại ngoại lệ nào Ngoại lệ trước Vết tiến trình thực thi Đường dẫn được yêu cầu không phù hợp với việc định tuyến. Trình điều khiển được yêu cầu không thể được ánh xạ đến class trình điều khiển sẵn có. Trình điều khiển được yêu cầu không thể xử lý yêu cầu. Trình điều khiển được yêu cầu không thể xử lý được truy vấn. Tại thời điểm này, chúng tôi không thể xác định tại sao lỗi 404 sinh ra. phân giải đến %s 