��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <          (     <     Q     Z  
   c     n     �     �  '   �  E   �  -     -   =  <   k     �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Peter Kokot <peterkokot@gmail.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: sl_SI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Prišlo je do napake 404 Dodatne informacije Prišlo je do napake Krmilnik Datoteka Sporočilo Izjema ni na voljo Prejšnja izjema Skladovna sled Zahtevani URL se ne ujema z usmerjanjem Zahtevani krmilnik ni bil preslikan k obstoječemu razredu krmilnika. Zahtevanega krmilnika ni bilo mogoče poslati Zahtevani krmilnik ni uspel poslati zahtevka. Trenutno ne moremo ugotoviti zakaj je prišlo do napake 404. se razreši v %s 