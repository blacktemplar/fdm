��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  Y  <     �  '   �  !   �     �               (  !   B     d  V   �  �   �  B   e  V   �  �   �      �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Maksym Kobieliev <maximaximums@gmail.com>
Language-Team: Maximaximums@gmail.com <maximaximums@gmail.com>
Language: uk_UA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SearchPath-0: ..
 Помилка 404 Додаткова інформація Трапилася помилка Контролер Файл Повідомлення Немає винятка Попередні винятки Розгортка стека Не вдалося знайти напрямок для запитуваного URL. Не вдалося співвіднести запитуваний контролер з наявним класом контролера. Запитуваний контролер не доступний. Запитуваний контролер не зміг надіслати запит. В даний момент ви не можемо визначити, чому було згенеровано помилку 404. розгортається в %s 