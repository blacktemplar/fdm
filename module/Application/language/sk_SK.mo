��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <          +     @  
   R     ]     d  &   l     �     �  G   �  [     M   ^  <   �  C   �     -                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Štefan Bartko <bartko.stefan@gmail.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: sk_SK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Vyskytla sa chyba 404 Ďalšie informácie Vyskytla sa chyba Controller Súbor Správa Žiadna Exception nie je k dispozícii Predchádzajúca exception Stack trace Požadovaná URL nemohla byť namapovaná na žiadnu existujúcu routu. Požadovaný controller nemohol byť namapovaný na žiadnu existujúcu triedu controlleru. Požadovaný controller nepodporuje  vybavenie (controller not dispatchable). Požadovaný controller nebol schopný vybaviť požiadavku. Momentálne nedokážeme určiť prečo bola generovaná chyba 404. je namapovaný na %s 