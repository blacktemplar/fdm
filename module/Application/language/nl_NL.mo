��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <  #   �          6  
   P     [     c     k     �     �  A   �  >   �  ;   #  ;   _  S   �     �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Walter Tamboer
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 De pagina kon niet worden gevonden. Additionele informatie Er is een fout opgetreden Controller Bestand Bericht Geen exceptie beschikbaar Vorige excepties Stack trace Er is geen route gevonden die overeenkomt met de opgevraagde URL. Er is geen mapping beschikbaar voor de opgevraagde controller. De opgevraagde controller is niet bruikbaar (dispatchable). De opgevraagde controller kon deze aanvraag niet verwerken. We kunnen op dit moment niet achterhalen waarom de pagina niet kon worden gevonden. verwijst naar %s 