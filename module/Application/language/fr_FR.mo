��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <     
     &     D     \     h     p     x     �     �  ;   �  Q   �  /   M  :   }  S   �                           	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Evan Coury <me@evancoury.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Une erreur 404 est survenue Informations complémentaires Une erreur est survenue Contrôleur Fichier Message Aucune exception disponible Exceptions précédentes Pile d'exécution L'URL demandée n'a pas pu trouver de route correspondante. Le contrôleur demandé ne correspond pas à une classe existante de contrôleur. Le contrôleur demandé n'est pas dispatchable. Le contrôleur demandé n'a pas pu dispatcher la requête. Nous ne pouvons pas déterminer pour le moment pourquoi une 404 a été générée. résout en %s 