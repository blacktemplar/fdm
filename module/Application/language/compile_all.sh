#/bin/bash

for f in *.po
do
    echo "msgfmt -o "`basename $f`.mo" $f"
    msgfmt -o "`basename $f .po`.mo" $f
done
