��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <          0     <     N     Z     b     h     u     �  ,   �  H   �  3   
  >   >  ;   }     �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: H.H.G. multistore <info@hhg-multistore.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: tr_TR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Bir 404 hatası oluştu Ek bilgiler An error occurred Denetleyici Klasör Mesaj İstisna yok Önceki istisnalar Denetleyici İstenen URL yönlendirmede tahsis edilemedi Talep edilen denetleyiciye uygun denetleyici sınıfı tahsis edilemedi. Talep edilen denetleyici çağrılabilir değildir. Talep edilen denetleyici işlemi işlemesi mümkün değildir. Neden 404 hatasının oluştuğunu şu an belirleyemiyoruz. Buraya çözümlenir: %s 