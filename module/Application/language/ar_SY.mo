��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <  4   !      V     w     �     �     �     �     �     �  ?   �  q   ;  D   �  J   �  Y   =     �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Tawfek Daghistani <tawfekov@gmail.com>
Language-Team: Arabic <>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 حصل خطأ 404 , الصفحة غير موجودة مزيد من المعلومات حصل خطأ ما  المتحكم  ملف الرسالة لايوجد خطأ الأخطاء السابقة تفاصيل الخطأ الرابط المطلوب غير معرف لدى الموجه لا يمكن ربط المتحكم المطلوب بأي من المتحكمات الموجودة حالياًَ المتحكم المطلوب غير قادر على الإجابة  المتحكم المطلوب غير قادر على إجابة الطلب لا يمكنني التحديد لماذا حصل الخطأ 404 في هذا الوقت  يوصل إلى %s 