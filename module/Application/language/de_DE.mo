��          �      L      �     �     �     �  
   �     
          (     5     =     T     b     v  2   �  M   �  .     <   2  9   o     �  �  �     �     �     �  
   �     �     �                     =     N     a  H   t  N   �  0     M   =  a   �     �                     
                                                       	                        A 404 error occurred Additional information An error occurred Controller File Foosball Drawing Manager Logged in as Message No Exception available Not logged in Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-05 14:12+0200
PO-Revision-Date: 2016-06-05 14:12+0200
Last-Translator: Evan Coury <me@evancoury.com>
Language-Team: ZF Contributors <zf-devteam@zend.com>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Es trat ein 404 Fehler auf Zusätzliche Information Ein Fehler ist aufgetreten Controller Datei Tischfußball Turnier Manager Angemeldet als Meldung Es ist keine Ausnahme verfügbar Nicht angemeldet Vorherige Ausnahme Stapelüberwachung Für die angeforderte URL konnte keine Übereinstimmung gefunden werden. Der angeforderte Controller konnte keiner Controller Klasse zugeordnet werden. Der angeforderte Controller ist nicht aufrufbar. Der angeforderte Controller war nicht in der Lage die Anfrage zu verarbeiten. Zu diesem Zeitpunkt ist es uns nicht möglich zu bestimmen, warum ein 404 Fehler aufgetreten ist. wird aufgelöst in %s 