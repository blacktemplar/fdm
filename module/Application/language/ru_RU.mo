��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <       1        K     k     �     �  .   �  )   �  %   �  m     �   �  F   .  ^   u  _   �     4                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: vragovR <vragovR@yandex.ru>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Ошибка 404 Дополнительная информация Произошла ошибка Контроллер Файл Сообщение Нет имеющихся исключений Предыдущие исключения Развертывание стека Для запрашиваемого URL не может быть достигнуто направление. Запрашиваемый контроллер не может быть сопоставлен с существующими классом контроллера. Запрашиваемый контроллер не доступен. Запрашиваемый контроллер не смог отправить запрос. Мы не можем определить причину создания страницы 404. разрешает для %s 