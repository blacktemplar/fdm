��          �            x     y     �     �  
   �     �     �     �     �     �  2     M   9  .   �  <   �  9   �     -  �  <               3     C     O     W     `     ~     �  7   �  W   �  .   8  E   g  ;   �     �                      	                                           
              A 404 error occurred Additional information An error occurred Controller File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-04 08:43+0200
PO-Revision-Date: 2016-06-04 08:43+0200
Last-Translator: Diogo Melo <dmelo87@gmail.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Ocorreu um erro 404 Informação adicional Ocorreu um erro Controlador Arquivo Mensagem Nenhuma exceção disponível Exceções anteriores Pilha de execução A URL requisitada não pode ser encontrada em uma rota. O controlador requisitados não pode ser mapeado a uma classe de controlador existente. O controlador requisitado não foi despachado. O controlador requisitado não foi capaz de despachar a requisição. Não foi possível determinar o motivo do 404 ter ocorrido. resolve como %s 