<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

return [
  'service_manager' => [
    'abstract_factories' => [
      'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
      'Zend\Log\LoggerAbstractServiceFactory',
    ],
    'factories' => [
      'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
    ],
  ],
  'translator' => [
    'translation_file_patterns' => [
      [
        'type' => 'gettext',
        'base_dir' => __DIR__ . '/../language',
        'pattern' => '%s.mo',
      ],
    ],
  ],
  'slm_locale' => [
    'default' => 'en-US',
    'supported' => [
      'de',
      'de_DE',
      'en',
      'en_US',
    ],
    'mappings' => [
      'de' => 'de_DE',
      'en' => 'en_US'
    ],
    'strategies' => ['acceptlanguage'],
  ],
  'view_manager' => [
    'display_not_found_reason' => true,
    'display_exceptions' => true,
    'doctype' => 'HTML5',
    'not_found_template' => 'error/404',
    'exception_template' => 'error/index',
    'template_map' => [
      'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
      'layout/layout-mobile' => __DIR__ . '/../view/layout/layout-mobile.phtml',
      'error/404' => __DIR__ . '/../view/error/404.phtml',
      'error/index' => __DIR__ . '/../view/error/index.phtml',
      'layout/zfc-datagrid-toolbar' => __DIR__ . '/../view/layout/zfc-datagrid-toolbar.phtml',
    ],
    'template_path_stack' => [
      __DIR__ . '/../view',
    ],
  ],
  // Placeholder for console routes
  'console' => [
    'router' => [
      'routes' => [],
    ],
  ],
];
