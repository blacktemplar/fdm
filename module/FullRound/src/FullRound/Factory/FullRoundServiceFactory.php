<?php

namespace FullRound\Factory;

use FullRound\Service\FullRoundService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FullRoundServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $stableSort = $serviceLocator->get('StableSort');

    return new FullRoundService($stableSort);
  }
//</editor-fold desc="Public Methods">
}

