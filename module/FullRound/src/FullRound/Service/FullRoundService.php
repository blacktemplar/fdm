<?php

namespace FullRound\Service;

use Doctrine\ORM\EntityManager;
use FDM\Entity\Attendant;
use FDM\Entity\Game;
use FDM\Entity\Group;
use FDM\Entity\Ranking;
use FDM\Service\ModeServiceAbstract;
use FDM\Service\ModeServiceInterface;
use FDM\Service\StableSortService;

class FullRoundService extends ModeServiceAbstract
  implements ModeServiceInterface
{
//<editor-fold desc="Fields">
  protected $stableSort;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(StableSortService $stableSort)
  {
    $this->stableSort = $stableSort;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getNewRanking()
  {
    return new Ranking(["gamesPlayed", "goalDifference", "goalsShot", "points"]);
  }

  public function recalculateGames(Group $group, EntityManager $em, &$ended)
  {
    //delete old scheduled games
    $toRemove = [];
    foreach ($group->getGames() as $game) {
      if ($game->getType() == Game::TYPE_SCHEDULED_DYN ||
        $game->getType() == Game::TYPE_SCHEDULED_DYN_CONFLICT ||
        $game->getType() == Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN
      ) {
        $toRemove[] = $game;
      }
    }
    foreach ($toRemove as $el) {
      foreach ($el->getTeamA() as $ranking) {
        $ranking->getGamesAsA()->removeElement($el);
      }
      foreach ($el->getTeamB() as $ranking) {
        $ranking->getGamesAsB()->removeElement($el);
      }
      $group->getGames()->removeElement($el);
      $em->remove($el);
    }

    //uses Berger tables (see wikipedia Round Robin tournament)
    //team i plays against team j (index beginning from 0) in round
    //i+j mod k where k = #teams - 1 if teams is even and k = #teams
    //if teams is odd
    //an exception is if i == k or j == k then they play in round (j+j % k) if i == k and (i+i % k) if j == k

    $teams = [];
    $waitingPlayers = [];
    $playedGames = [];
    $ended = true;
    foreach ($group->getRankings() as $ranking) {
      $r = $ranking->getPhaseStartRank() - 1;
      $teams[$r] = $ranking;
      $isWaiting = true;
      foreach ($ranking->getGames() as $game) {
        if ($game->getType() != Game::TYPE_FINISHED) {
          $isWaiting = false;
          break;
        }
      }
      foreach ($ranking->getAttendants() as $attendant) {
        if ($attendant->getStatus() != Attendant::STATE_PLAYING) {
          $isWaiting = false;
          break;
        }
      }
      $nGames = $ranking->getGamesAsA()->count() +
        $ranking->getGamesAsB()->count();
      if ($nGames < count($group->getRankings()) - 1) {
        $ended = false;
      }
      $playedGames[$ranking->getPhaseStartRank() - 1] = $nGames;
      if ($isWaiting) {
        $waitingPlayers[$ranking->getPhaseStartRank() - 1] = true;
      }
    }
    ksort($teams);

    $numRounds = count($teams) - 1 + (count($teams) % 2);
    $rounds = [];
    for ($i = 0; $i < $numRounds; $i++) {
      $rounds[$i] = 0;
    }
    $playedInRound = [];
    foreach ($group->getGames() as $game) {
      $i = $game->getTeamA()->first()->getPhaseStartRank() - 1;
      $j = $game->getTeamB()->first()->getPhaseStartRank() - 1;
      $round = $this->getRound($i, $j, $numRounds);
      $rounds[$round]++;
      $playedInRound[$round][$i] = true;
      $playedInRound[$round][$j] = true;
      if ($game->getType() != Game::TYPE_FINISHED) {
        $ended = false;
      }
    }

    $games = [];
    for ($r = 0; $r < $numRounds; $r++) {
      foreach ($teams as $p => $team) {
        if (!array_key_exists($r, $playedInRound) ||
          !array_key_exists($p, $playedInRound[$r]) ||
          !$playedInRound[$r][$p]
        ) {
          $op = $this->getOponent($p, $r, $numRounds);
          if ($p < $op && $op < count($teams)) {
            assert(!$playedInRound[$r][$op] ||
              !array_key_exists($op, $playedInRound[$r]) ||
              !$playedInRound[$r][$op]);
            $game = new Game();
            $game->addToTeamA($teams[$p]);
            $game->addToTeamB($teams[$op]);
            $game->setRound($r);
            $p1 = $playedGames[$p];
            $p2 = $playedGames[$op];
            $game->setRelevance(-0.01 * (
                4 * min($p1, $p2) + max($p1, $p2) - $rounds[$r] * 0.1 + $r * 0.001 + $p * 0.00001
              )
            );
            if (array_key_exists($p, $waitingPlayers) && array_key_exists($op, $waitingPlayers)) {
              $game->setType(Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN);

            } else {
              $game->setType(Game::TYPE_SCHEDULED_DYN_CONFLICT);
            }
            $game->setGroup($group);
            $em->persist($game);
            $games[] = $game;
          }
        }
      }
    }

    usort(
      $games, function ($g1, $g2) {
      if ($g1->getRelevance() > $g2->getRelevance()) {
        return -1;
      }
      return 1;
    });

    $waiting = count($waitingPlayers);
    $groupRel = $this->computeRelevance($group);
    foreach ($games as $game) {
      $r = $game->getTeamA()[0]->getPhaseStartRank() - 1;
      $op = $game->getTeamB()[0]->getPhaseStartRank() - 1;
      $game->setRelevance($groupRel - $game->getRelevance());
      if ($game->getType() == Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN && $waitingPlayers[$r] && $waitingPlayers[$op]) {
        $game->setType(Game::TYPE_SCHEDULED_DYN);
        $groupRel = $this->computeRelevance($group);
        $waitingPlayers[$r] = false;
        $waitingPlayers[$op] = false;
        $waiting -= 2;
      }
    }
  }

  public function roundsReversed()
  {
    return false;
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function computeRelevance(Group $group)
  {
    $teams = $group->getRankings()->count();
    $gamesAtAll = $teams * ($teams - 1) / 2;
    $games = $group->getGames()->filter(function ($g) {
      return $g->getType() != Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN &&
      $g->getType() != Game::TYPE_SCHEDULED_DYN_CONFLICT;
    })->count();

    //if every team has only one open game left it is not relevant anymore
    $minGames = $teams - 2;

    foreach ($group->getRankings() as $ranking) {
      $nGames =
        $ranking->getGamesAsA()->filter(function ($g) {
          return $g->getType() != Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN &&
          $g->getType() != Game::TYPE_SCHEDULED_DYN_CONFLICT;
        })->count() + $ranking->getGamesAsB()->filter(function ($g) {
          return $g->getType() != Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN &&
          $g->getType() != Game::TYPE_SCHEDULED_DYN_CONFLICT;
        })->count();
      if ($minGames > $nGames) {
        $minGames = $nGames;
      }
    }

    return ($gamesAtAll - $games) / ($gamesAtAll + 1) *
    ($gamesAtAll - $games) / ($gamesAtAll + 1) +
    0.25 * ($teams - 2 - $minGames) / ($teams - 1) - 0.000001 * $group->getGroupNumber();
  }

  private function getOponent($i, $r, $numRounds)
  {
    if ($i == $numRounds) {
      if ($r % 2 == 0) {
        return intdiv($r, 2);
      } else {
        return intdiv($numRounds + $r, 2);
      }
    }
    $op = ($numRounds + $r - $i) % $numRounds;
    if ($op == $i) {
      return $numRounds;
    }
    return $op;
  }

  private function getRound($i, $j, $numRounds)
  {
    if ($i == $numRounds) {
      return ($j + $j) % $numRounds;
    } else if ($j == $numRounds) {
      return ($i + $i) % $numRounds;
    }
    return ($i + $j) % $numRounds;
  }
//</editor-fold desc="Private Methods">
}
