<?php
return [
  'service_manager' => [
    'factories' => [
      'FullRoundService' => '\FullRound\Factory\FullRoundServiceFactory',
      'FullRound\Service\FullRoundService' => '\FullRound\Factory\FullRoundServiceFactory'
    ],
    'invokable' => [],
  ],
  'controllers' => [
    'factories' => [],
    'invokables' => [],
  ],
  'view_manager' => [
    'template_path_stack' => [
      'fullround' => __DIR__ . '/../view',
    ],
  ],
  'doctrine' => [
    'eventmanager' => [
      'orm_default' => [
        'subscribers' => [
          'Gedmo\Timestampable\TimestampableListener',
        ],
      ],
    ],
    'driver' => [
      'fullround_entities' => [
        'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
        'cache' => 'array',
        'paths' => [
          __DIR__ . '/../src/FullRound/Entity',
        ],
      ],
    ],
  ],
  'strategies' => [
    'ViewJsonStrategy',
  ],
];
