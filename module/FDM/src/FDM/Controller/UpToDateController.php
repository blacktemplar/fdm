<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/4/16
 * Time: 1:57 PM
 */

namespace FDM\Controller;


use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class UpToDateController extends AbstractActionController
{
//<editor-fold desc="Fields">
  protected $em;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(EntityManager $entityManager)
  {
    $this->em = $entityManager;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function indexAction()
  {
    $id = $this->params('id');
    $lastrefreshed = $this->params('lastrefreshed');
    $uptodate = false;
    if ($lastrefreshed) {
      $lr = new \DateTime();
      $lr->setTimestamp($lastrefreshed);
      $uptodateArr = $this->em->createQueryBuilder()
        ->select("CASE WHEN t.updatedAt < :lastrefreshed THEN 1 ELSE 0 END as uptodate")
        ->from("FDM\Entity\Tournament", "t")
        ->where("t.id = :tid")
        ->setParameter("lastrefreshed", $lr)
        ->setParameter("tid", $id)
        ->getQuery()
        ->getResult();
      if (count($uptodateArr) > 0) {
        $uptodate = $uptodateArr[0]["uptodate"] == "1";
      }
    }
    return new JsonModel(["uptodate" => $uptodate]);
  }
//</editor-fold desc="Public Methods">
}