<?php
namespace FDM\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use FDM\Entity\Attendant;
use FDM\Entity\Player;
use FDM\Entity\SpeakTask;
use FDM\Entity\Tournament;
use FDM\Entity\TournamentPrivileges;
use FDM\Form\DeleteFilter;
use FDM\Form\DeleteForm;
use FDM\Form\LocalSettingsForm;
use FDM\Form\PasswordValidator;
use FDM\Service\FlushServiceInterface;
use FDM\Service\LoadingService;
use FDM\Service\PlayersService;
use FDM\Service\SpeakService;
use FDM\Service\TournamentAccessServiceInterface;
use FDM\Service\TournamentService;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\I18n\Translator;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use ZfcUser\Service;


class TournamentController extends AbstractActionController
{
//<editor-fold desc="Fields">
  protected $em;

  protected $fs;

  protected $ls;

  /**
   * @var PlayersService
   */
  protected $ps;

  protected $speakService;

  protected $tas;

  protected $tournamentService;

  protected $translator;

  protected $user;

  protected $userService;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    EntityManager $entityManager,
    TournamentAccessServiceInterface $tas,
    FlushServiceInterface $fs,
    Translator $translator,
    Service\User $userService,
    TournamentService $tournamentService,
    LoadingService $ls,
    PlayersService $ps,
    SpeakService $speakService,
    $user) // user is null if not logged in
  {
    $this->em = $entityManager;
    $this->tas = $tas;
    $this->fs = $fs;
    $this->user = $user;
    $this->translator = $translator;
    $this->userService = $userService;
    $this->tournamentService = $tournamentService;
    $this->ls = $ls;
    $this->ps = $ps;
    $this->speakService = $speakService;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function attendantsAction()
  {
    //$tournament = $this->getTournament();
    //$this->getServiceLocator()->get('viewhelpermanager')
    //    ->get('headScript')->appendFile('/js/editablegrid.js');
    $tournament = $this->getTournament();
    return new ViewModel(
      [
        'tournamentId' => $tournament->getId(),
        'started' => $tournament->isStarted(),
        'editable' => $this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT)
      ]
    );
  }

  public function attendantsJSONDeleteAction()
  {
    try {
      if ($this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT)) {
        $aId = $this->params()->fromPost('id');
        $result = [];
        $attendant = $this->em->find('FDM\Entity\Attendant', $aId);
        if ($attendant) {
          //change start numbers
          $tournament = $attendant->getTournament();
          $startNumber = $attendant->getStartNumber();
          $this->em->remove($attendant);
          $allAttendants = $tournament->getAttendants();
          foreach ($allAttendants as $a) {
            if ($a->getStartNumber() > $startNumber) {
              $a->setStartNumber($a->getStartNumber() - 1);
              $result["changed"][] = $this->getJSONArray($a);
            }
          }
          $this->fs->flush();
        } else {
          return new JsonModel(
            [
              "result" => "error",
              "error" => "Attendant with id " . $aId .
                " does not exist"
            ]
          );
        }
        $result["result"] = "ok";
        return new JsonModel($result);
      }
      return new JsonModel(
        ["result" => "error", "error" => "No Access!"]
      );
    } catch (\Exception $e) {
      return new JsonModel(
        ["result" => "error", "error" => $e->getMessage()]
      );
    }
  }

  public function attendantsJSONEditAction()
  {
    try {
      if ($this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT)) {
        $aId = $this->params()->fromPost('id');
        $result = [];
        $attendant = $this->getAttendant($aId);
        $value = $this->params()->fromPost('newvalue');
        $column = $this->params()->fromPost('colname');
        $attendant->setValue($column, $value);
        $this->fs->flush();
        if (substr($aId, 0, 3) === "new") {
          $result["newId"] = $attendant->getId();
        }
        $result["result"] = "ok";
        $result["column"] = $column;
        $result["value"] = $value;
        $result["newValues"] = $this->getJSONArray($attendant);
        return new JsonModel($result);
      }
      return new JsonModel(
        ["result" => "error", "error" => "No Access!"]
      );
    } catch (\Exception $e) {
      return new JsonModel(
        ["result" => "error", "error" => $e->getMessage()]
      );
    }
  }

  public function attendantsJSONFetchAction()
  {
    $data = [];
    $tournament = $this->getTournament();
    $editable =
      $this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT);
    $changeable = !$this->getTournament()->isStarted();
    $data["metadata"] = [
      [
        "name" => "startNumber",
        "label" => $this->translator->translate("Start Number"),
        "datatype" => "integer",
        "editable" => false,
      ],
      [
        "name" => "firstName",
        "label" => $this->translator->translate("First Name"),
        "datatype" => "string",
        "editable" => $editable,
      ],
      [
        "name" => "lastName",
        "label" => $this->translator->translate("Last Name"),
        "datatatype" => "string",
        "editable" => $editable,
      ],
      [
        "name" => "status",
        "label" => $this->translator->translate("Status"),
        "datatype" => "string",
        "editable" => $editable,
        "values" => [
          Attendant::STATE_PLAYING =>
            $this->translator->translate("Playing"),
          Attendant::STATE_PAUSING =>
            $this->translator->translate("Pausing"),
        ],
      ],
      [
        "name" => "payed",
        "label" => $this->translator->translate("Payed"),
        "datatype" => "boolean",
        "editable" => $editable,
      ],
      [
        "name" => "playerId",
        "label" => $this->translator->translate("id"),
        "datatype" => "string",
        "editable" => false,
      ],
      [
        "name" => "displayName",
        "label" => $this->translator->translate("Display Name"),
        "datatype" => "string",
        "editable" => $editable,
      ],
      [
        "name" => "spelledFirstName",
        "label" => $this->translator->translate("First Name Pronunciation"),
        "datatype" => "string",
        "editable" => $editable,
      ],
      [
        "name" => "spelledLastName",
        "label" => $this->translator->translate("Last Name Pronunciation"),
        "datatype" => "string",
        "editable" => $editable,
      ],
    ];
    if ($editable) {
      $data["metadata"][] = [
        "name" => "action",
        "label" => $this->translator->translate("Actions"),
        "datatype" => "html",
        "editable" => false,
      ];
    }
    $data["data"] = [];
    foreach ($tournament->getAttendants() as $attendant) {
      $data["data"][] = $this->getJSONArray($attendant);
    }
    if ($editable && $changeable) {
      $data["data"][] = [
        "id" => "new",
        "values" => [
          "firstName" => "",
          "lastName" => "",
          "startNumber" => $tournament->getAttendants()->count() + 1,
          "playerId" => "",
          "spelledName" => "",
          "payed" => false,
        ],
      ];
    }
    return new JsonModel($data);
  }

  public function attendantsJSONFetchPlayersAction()
  {
    return new JsonModel($this->ps->getPlayersAsArray());
  }

  public function attendantsJSONNewPlayerAction()
  {
    try {
      if ($this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT)) {
        $aId = $this->params()->fromPost('id');
        $result = [];
        $attendant = $this->getAttendant($aId);
        $attendant->setPlayer(null);
        $this->fs->flush();
        $result["result"] = "ok";
        return new JsonModel($result);
      }
      return new JsonModel(
        ["result" => "error", "error" => "No Access!"]
      );
    } catch (\Exception $e) {
      return new JsonModel(
        ["result" => "error", "error" => $e->getMessage()]
      );
    }
  }

  public function attendantsJSONSetPlayerAction()
  {
    try {
      if ($this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT)) {
        $aId = $this->params()->fromPost('id');
        $result = [];
        $attendant = $this->getAttendant($aId);
        $player = $this->em->find(
          'FDM\Entity\Player', $this->params()->fromPost('playerId')
        );
        $attendant->setValue("firstName", $player->getFirstName());
        $attendant->setValue("lastName", $player->getLastName());
        $attendant->setValue("birthday", $player->getBirthday());
        $attendant->setValue("spelledFirstName", $player->getSpelledFirstName());
        $attendant->setValue("spelledLastName", $player->getSpelledLastName());
        $attendant->setValue("player", $player);
        $attendant->setValue("payed", false);
        $this->fs->flush();
        if (substr($aId, 0, 3) === "new") {
          $result["newId"] = $attendant->getId();
        }
        $result["result"] = "ok";
        $result["newValues"] = $this->getJSONArray($attendant);
        return new JsonModel($result);
      }
      return new JsonModel(
        ["result" => "error", "error" => "No Access!"]
      );
    } catch (\Exception $e) {
      return new JsonModel(
        ["result" => "error", "error" => $e->getMessage()]
      );
    }
  }

  public function attendantsUpdatePlayersAction()
  {
    $tournament = $this->getTournament();
    if ($this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT) && $tournament->isStarted()) {
      $this->updateAttendants();
      $this->fs->flush();
    }
    return $this->redirect()->toRoute(
      'tournament/attendants', [
        'id' => $tournament->getId(),
      ]
    );
  }

  public function copyAction()
  {
    if ($this->user === null ||
      !$this->tas->hasAccessType(TournamentPrivileges::TYPE_READ)
    ) {
      //not logged in or no priviliges for this tournament
      //action not allowed
      return $this->redirect()->toRoute('fdm');
    }
    $tournament = $this->getTournament();
    $builder = new AnnotationBuilder();
    $form = $builder->createForm('\FDM\Entity\Tournament');
    $form->setHydrator(new DoctrineHydrator($this->em));
    $form->add(new Csrf('security'));
    $submit = new Submit('submit');
    $submit->setValue("create");
    $form->add($submit);

    $request = $this->getRequest();
    if ($request->isPost()) {
      $newTournament = new Tournament();
      $form->bind($newTournament);
      $form->setData($request->getPost());
      if ($form->isValid()) {

        /** @var Tournament $newTournament */
        $newTournament = $form->getObject();

        $newTournament->setPrivileges(new ArrayCollection());
        $newTournament->setAttendants(new ArrayCollection());
        $newTournament->setPhases(new ArrayCollection());
        $newTournament->setStarted(false);


        //add phases
        foreach ($tournament->getPhasesByTemplate(true) as $phase) {
          $clone = clone $phase;
          $clone->setGroups(new ArrayCollection());
          $clone->setModes(new ArrayCollection());

          $aStrat = clone $phase->getAttendanceStrategy();
          $clone->setAttendanceStrategy($aStrat);

          $this->em->persist($aStrat);

          //add modes
          foreach ($phase->getModesByTemplate(true) as $mode) {
            $cloneM = clone $mode;
            $cloneM->setPhase($clone);
            $this->em->persist($cloneM);
          }
          $clone->setTournament($newTournament);
          $this->em->persist($clone);
        }

        //add tournament rankings
        foreach ($tournament->getTournamentRankings() as $ranking) {
          $newTournament->getTournamentRankings()->add($ranking);
        }

        //add edit privileges for current user
        $priv = new TournamentPrivileges();
        $priv->setUser($this->user);
        $priv->setType(TournamentPrivileges::TYPE_EDIT);
        $priv->setTournament($newTournament);
        $this->em->persist($priv);
        $this->em->persist($newTournament);


        $this->em->flush(); //do not check edit rights!
        $this->redirect()->toRoute(
          'tournament', [
            'id' => $newTournament->getId()
          ]
        );
      }
    } else {
      $form->bind($tournament);
      $date = new \DateTime();
      $d = $tournament->getStart();
      $date->setTime(intval($d->format('H')), intval($d->format('M')));
      $form->get('start')->setValue($date);
    }

    return new ViewModel(["form" => $form]);

  }

  public function deleteAction()
  {
    if ($this->user === null ||
      !$this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT)
    ) {
      //not logged in or no priviliges for this tournament
      //action not allowed
      return $this->redirect()->toRoute('fdm');
    }
    $form = new DeleteForm();

    $form->setInputFilter(
      new DeleteFilter(
        new PasswordValidator(
          [
            'userService' => $this->userService,
            'user' => $this->user,
          ]
        )
      )
    );

    $tournament = $this->getTournament();

    $request = $this->getRequest();
    if ($request->isPost()) {
      $form->setData($request->getPost());
      if ($form->isValid()) {
        //delete tournament
        $tournament->setDeleted(true);
        $this->tournamentService->undoTournamentRankings($tournament);
        $this->fs->flush();
        $this->redirect()->toRoute('fdm');
      }
    }

    return new ViewModel(
      ["form" => $form, "tournament" => $tournament]
    );
  }

  public function finishAction()
  {
    $tournament = $this->getTournament();
    if (!$tournament->isStarted()) {
      throw new \Exception(
        "Tried to finish an unstarted tournament!"
      );
    }
    foreach ($tournament->getPhasesByTemplate(false) as $phase) {
      if (!$phase->isFinished()) {
        throw new \Exception(
          "Tried to finish a tournament with unfinished phases!"
        );
      }
    }
    $tournament->setFinished(true);
    $tournament->setEnd(new \DateTime());
    $this->tournamentService->applyTournamentRankings($tournament);
    $this->fs->flush();

    return $this->redirect()->toRoute(
      'tournament', [
        'id' => $tournament->getId()
      ]
    );
  }

  public function getAttendant($aId)
  {
    $attendant = null;
    if (substr($aId, 0, 3) === "new") {
      if (!$this->getTournament()->isStarted()) {
        $tournament = $this->getTournament();
        $numAttendants = $tournament->getAttendants()->count();
        $attendant = new Attendant();
        $attendant->setStartNumber($numAttendants + 1);
        $attendant->setTournament($tournament);
        $attendant->setPayed(false);
        $attendant->setStatus(Attendant::STATE_PLAYING);
        $this->em->persist($attendant);
      }
    } else {
      $attendant = $this->em->find('FDM\Entity\Attendant', $aId);
    }
    return $attendant;
  }

  public function getJSONArray(Attendant $attendant)
  {
    $editable =
      $this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT);

    return [
      "id" => $attendant->getId(),
      "values" => [
        "firstName" => $attendant->getFirstName(),
        "lastName" => $attendant->getLastName(),
        "startNumber" => $attendant->getStartNumber(),
        "playerId" => $attendant->getPlayer() !== null ?
          $attendant->getPlayer()->getId() : "",
        "action" => $editable ? $attendant->getId() : "",
        "status" => $attendant->getStatus(),
        "payed" => $attendant->getPayed(),
        "displayName" => $attendant->getDisplayName(),
        "spelledFirstName" => $attendant->getSpelledFirstName(),
        "spelledLastName" => $attendant->getSpelledLastName(),
      ],
      "attendantId" => $attendant->getId(),
    ];
  }

  public function getNewSpeakTasksAction()
  {
    $tId = $this->params('id');
    $lastTask = $this->params('lasttask');

    $lt = new \DateTime();
    $lt->setTimestamp($lastTask);
    /** @var SpeakTask[] $speakTasks */
    $speakTasks = $this->em->createQueryBuilder()
      ->select('st')
      ->from("FDM\Entity\SpeakTask", "st")
      ->where("st.tournament = :tid")
      ->andWhere("st.updatedAt > :lt")
      ->addOrderBy("st.createdAt", 'ASC')
      ->addOrderBy('st.orderNumber', 'ASC')
      ->setParameter("lt", $lt)
      ->setParameter("tid", $tId)
      ->getQuery()
      ->getResult();

    $result = [];
    foreach ($speakTasks as $task) {
      $result[] = ["createdAt" => $task->getCreatedAt()->getTimestamp(),
        "updatedAt" => $task->getUpdatedAt()->getTimestamp(), "id" => $task->getId(),
        "text" => $task->getText(), "info" => $task->getInfo()];
    }

    return new JsonModel($result);
  }

  /** @noinspection PhpMissingParentCallCommonInspection */
  public function indexAction()
  {
    $tournament = $this->getTournament();

    //find last active phase
    $maxPhaseNumber = -1;
    $maxPhase = null;

    foreach ($tournament->getPhases() as $phase) {
      if ($phase->isStarted() &&
        $phase->getPhaseNumber() > $maxPhaseNumber
      ) {
        $maxPhaseNumber = $phase->getPhaseNumber();
        $maxPhase = $phase;
      }
    }

    //redirect to last active phase
    if ($maxPhase === null || !$tournament->isStarted()) {
      return $this->redirect()->toRoute(
        'tournament/attendants', [
          'id' => $tournament->getId(),
        ]
      );
    } else {
      return $this->redirect()->toRoute(
        'tournament/phase', [
          'id' => $tournament->getId(),
          'phase' => $maxPhase->getId(),
        ]
      );
    }
  }

  public function localsettingsAction()
  {
    $form = new LocalSettingsForm(
      $this->user !== null &&
      $this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT)
    );
    $tournament = $this->getTournament();
    $form->get('tournamentId')->setValue($tournament->getId());
    return new ViewModel(
      ["form" => $form, "tournament" => $tournament]
    );
  }

  public function settingsAction()
  {
    $tournament = $this->getTournament();
    $builder = new AnnotationBuilder();
    $form = $builder->createForm('\FDM\Entity\Tournament');
    $form->setHydrator(new DoctrineHydrator($this->em));
    if (!$this->tas->hasAccessType(
      TournamentPrivileges::TYPE_EDIT
    )
    ) {
      foreach ($form as $element) {
        $element->setAttribute('readonly', '1');
      }
    }
    if ($this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT)) {
      $form->add(new Csrf('security'));
      $submit = new Submit('submit');
      $submit->setValue("save");
      $form->add($submit);
    }
    $request = $this->getRequest();
    $form->bind($tournament);
    if ($request->isPost()) {
      $form->setData($request->getPost());
      if ($form->isValid()) {
        $this->fs->flush();
      }
    }

    return new ViewModel(["form" => $form]);
  }

  public function speakerAction()
  {
    $tournament = $this->getTournament();
    $viewModel = new ViewModel(
      [
        'tournament' => $tournament
      ]
    );
    $viewModel->setTerminal(true);
    return $viewModel;
  }

  public function startAction()
  {
    $tournament = $this->getTournament();
    if ($tournament->isStarted()) {
      throw new \Exception(
        "Tried to start an already started tournament!"
      );
    }
    $this->ls->loadTournamentCompletely($tournament);

    $this->updateAttendants();

    //done updating Attendants

    foreach ($tournament->getPhasesByTemplate(true) as $phase) {
      $copiedPhase = clone $phase;
      $copiedPhase->setTournament($tournament);
      $copiedPhase->setTemplate(false);
      $copiedStrat = clone $phase->getAttendanceStrategy();
      $copiedStrat->setTemplate(false);
      $copiedPhase->setAttendanceStrategy($copiedStrat);
      $this->em->persist($copiedStrat);
      foreach ($copiedPhase->getModesByTemplate(true) as $mode) {
        $copiedMode = clone $mode;
        $copiedMode->setTemplate(false);
        $copiedMode->setPhase($copiedPhase);
        $this->em->persist($copiedMode);
      }
      $this->em->persist($copiedPhase);
    }
    $tournament->setStarted(true);

    if ($tournament->getStartText() != "") {
      $this->speakService->addTournamentTask($tournament->getStartText(), "tournament_startText", $tournament);
    }

    $this->fs->flush();

    return $this->redirect()->toRoute(
      'tournament/phase', [
        'id' => $tournament->getId(),
        'phase' => $tournament->getPhasesByTemplate(false)[0]->getId(),
      ]
    );
  }

  public function unfinishAction()
  {
    $tournament = $this->getTournament();
    if (!$tournament->isFinished()) {
      throw new \Exception(
        "Tried to unfinish a finished tournament!"
      );
    }
    $tournament->setFinished(false);
    $this->tournamentService->undoTournamentRankings($tournament);
    $this->fs->flush(true);

    return $this->redirect()->toRoute(
      'tournament', [
        'id' => $tournament->getId()
      ]
    );
  }

  public function unstartAction()
  {
    $tournament = $this->getTournament();
    if (!$tournament->isStarted()) {
      throw new \Exception(
        "Tried to unstart a not started tournament!"
      );
    }

    $this->ls->loadTournamentCompletely($tournament);

    //check if any phase is started or contains groups
    foreach ($tournament->getPhasesByTemplate(false) as $phase) {
      if ($phase->isStarted() || !$phase->getGroups()->isEmpty()) {
        throw new \Exception(
          "Cannot unstart tournament, when phases are already started!"
        );
      }
    }

    foreach ($tournament->getPhasesByTemplate(false) as $phase) {
      foreach ($phase->getModes()->toArray() as $mode) {
        $this->em->remove($mode);
      }
      $this->em->remove($phase->getAttendanceStrategy());
      $this->em->remove($phase);
    }
    $tournament->setStarted(false);
    $this->fs->flush();
    return $this->redirect()->toRoute(
      'tournament', [
        'id' => $tournament->getId(),
      ]
    );
  }

//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function adjustTwoAsym($att1, $full, $offset)
  {
    if ($att1->getGeneratedDisplayName() == $att1->getDisplayName()) {
      $offset++;
      $firstWord = strpos($full, " ");
      if ($firstWord === false) {
        $firstWord = strlen($full);
      }
      if ($offset < $firstWord) {
        $offset = $firstWord;
      }
      if ($offset > strlen($att1->getDisplayName()) ||
        (
          $offset == strlen($att1->getDisplayName()) &&
          $att1->getDisplayName()[$offset - 1] == "."
        )
      ) {
        $newDisplay = substr($full, 0, $offset);
        if ($offset < strlen($full) && $full[$offset] != " ") {
          $newDisplay .= ".";
        }
        $att1->setDisplayName($newDisplay);
        $att1->setGeneratedDisplayName($newDisplay);
      }
    }
  }

  private function adjustTwoAttendants($att1, $att2, $name1, $name2)
  {
    $offset = $this->getFirstDiff($name1, $name2);
    $this->adjustTwoAsym($att1, $name1, $offset);
    $this->adjustTwoAsym($att2, $name2, $offset);
  }

  private function getFirstDiff($s1, $s2)
  {
    for ($offset = 0;
         $offset < strlen($s1) && $offset < strlen($s2); $offset++) {
      if ($s1[$offset] != $s2[$offset]) {
        return $offset;
      }
    }
    return $offset;
  }

  /**
   * @return Tournament the tournament from the url
   */
  private function getTournament()
  {
    $id = $this->params('id');
    return $this->em->find('FDM\Entity\Tournament', $id);
  }

  private function updateAttendants()
  {
    $tournament = $this->getTournament();

    //update Attendants
    $first = "getLastName";
    $second = "getFirstName";

    //check duplicate players
    $ids = [];
    foreach ($tournament->getAttendants() as $att) {
      if ($att->getPlayer()) {
        if (array_key_exists($att->getPlayer()->getId(), $ids)) {
          throw new \Exception(
            "Some players are listed multiple times," .
            " please check the attendants list"
          );
        } else {
          $ids[$att->getPlayer()->getId()] = 1;
        }
      }
    }

    //check player names and reset generated display names
    $names = [];
    foreach ($tournament->getAttendants() as $att) {
      $name = $att->$first() . " " . $att->$second();
      if ($att->getDisplayName() != $att->getGeneratedDisplayName()) {
        //user defined display name
        $name = $att->getDisplayName();
      } else {
        $att->setDisplayName("");
        $att->setGeneratedDisplayName("");
      }
      if (array_key_exists($name, $names)) {
        throw new \Exception(
          "Some players have the same name or display name," .
          " please check the attendants list"
        );
      }
      $names[$name] = $att;
    }

    //update players
    foreach ($tournament->getAttendants() as $att) {
      if (!$att->getPlayer()) {
        //new player
        $player = new Player();
        $this->em->persist($player);
        $att->setPlayer($player);
      }
      $player = $att->getPlayer();
      $player->setFirstName($att->getFirstName());
      $player->setLastName($att->getLastName());
      $player->setSpelledFirstName($att->getSpelledFirstName());
      $player->setSpelledLastName($att->getSpelledLastName());
    }

    //calculate new display names
    $keys = array_keys($names);
    sort($keys);
    for ($i = 0; $i < count($keys) - 1; $i++) {
      $this->adjustTwoAttendants(
        $names[$keys[$i]], $names[$keys[$i + 1]], $keys[$i], $keys[$i + 1]
      );
    }
  }
//</editor-fold desc="Private Methods">

}
