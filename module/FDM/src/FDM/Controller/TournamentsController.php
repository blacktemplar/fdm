<?php
namespace FDM\Controller;

use Doctrine\ORM\Query\Expr\Select;
use FDM\Form\HideColumnsForm;
use FDM\Service\TournamentsServiceInterface;
use FDM\View\MyDateTimeColumn;
use FDM\ZfcDatagrid\Column\Type\Integer;
use IntlDateFormatter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\I18n\Translator;
use Zend\View\Model\ViewModel;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Type\DateTime;
use ZfcDatagrid\Datagrid;


class TournamentsController extends AbstractActionController
{
//<editor-fold desc="Fields">
  /** @var Datagrid */
  protected $grid;

  protected $renderer;

  /** @var TournamentsServiceInterface */
  protected $tournamentsService;

  protected $translator;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    TournamentsServiceInterface $tournamentsService,
    $renderer, Translator $translator, Datagrid $grid)
  {
    $this->tournamentsService = $tournamentsService;
    $this->renderer = $renderer;
    $this->translator = $translator;
    $this->grid = $grid;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function hidecolumnsAction()
  {
    $id = $this->params('rId');
    $columns = $this->getColumns($id);
    $hiddenColumns = $this->params('hiddenColumns');
    $this->computeHidden($columns, $hiddenColumns);
    $cleanColumns = $this->getCleanColumns($columns);

    return new ViewModel(["form" => new HideColumnsForm($columns), "rId" => $id, "columns" => $cleanColumns]);
  }

  /** @noinspection PhpMissingParentCallCommonInspection */
  public function indexAction()
  {
    $userId = null;
    if ($this->zfcUserAuthentication()->hasIdentity()) {
      $userId = $this->zfcUserAuthentication()->getIdentity()->getId();
    }
    return $this->renderGrid($this->tournamentsService->getTournaments($userId)->orderBy('t.start', 'DESC'));
  }

  public function rankingAction()
  {
    $id = $this->params('rId');

    /** @var Datagrid $grid */
    $grid = $this->grid;
    $grid->setTitle('Ranking');
    $grid->setDataSource($this->tournamentsService->getRanking($id));
    $this->tournamentsService->modifyGrid($id, $grid);

    $columns = $this->getColumns($id);

    $hiddenColumns = $this->params('hiddenColumns');
    $this->computeHidden($columns, $hiddenColumns);

    $count = 0;
    foreach ($columns as $column) {
      $grid->addColumn($column["col"]);
      if (!$column["col"]->isHidden()) {
        $count++;
      }
    }

    if ($count > 7) {
      $options = $grid->getOptions();
      $options['renderer']['TCPDF']['orientation'] = 'landscape';
      $grid->setOptions($options);
    }

    $grid->setExportRenderers(['MYPHPExcel' => 'XLS', 'MYTCPDF' => 'PDF']);

    $templateVars = $grid->getToolbarTemplateVariables();
    $templateVars["rId"] = $id;
    $templateVars["hiddenColumns"] = $hiddenColumns;
    $grid->setToolbarTemplateVariables($templateVars);

    $grid->render();

    return $grid->getResponse();
  }

  public function rankingForwardAction()
  {
    $id = $this->params('rId');
    $columns = $this->getColumns($id);
    $cleanColumns = $this->getCleanColumns($columns);
    return new ViewModel(["rId" => $id, "columns" => $cleanColumns]);
  }

  public function rankingsAction()
  {
    $grid = $this->grid;
    $grid->setTitle($this->translator->translate('Tournament Rankings'));
    $grid->setDataSource($this->tournamentsService->getTournamentRankings());

    $col = new Column\Select('id', 'r');
    $col->setHidden(true);
    $col->setIdentity(true);
    $grid->addColumn($col);

    $col = new Column\Select('name', 'r');
    $col->setLabel('Name');
    $grid->addColumn($col);

    $rowClickAction = new Column\Action\Button();
    $rowClickAction->setAttribute("data-ajax", "false");
    $rowId = $rowClickAction->getRowIdPlaceholder();
    $rowClickAction->setAttribute("onclick", "alert('Test')");
    $rowClickAction->setLink('ranking/' . $rowId);
    $grid->setRowClickAction($rowClickAction);

    $grid->render();

    return $grid->getResponse();
  }

  public function templatesAction()
  {
    $userId = null;
    if ($this->zfcUserAuthentication()->hasIdentity()) {
      $userId = $this->zfcUserAuthentication()->getIdentity()->getId();
    }
    return $this->renderGrid($this->tournamentsService->getTemplates($userId), false, false, false, "Templates");
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function computeHidden(&$columns, $hiddenColumns)
  {
    $cur = $hiddenColumns;
    foreach ($columns as &$column) {
      if (array_key_exists("force-hidden", $column) && $column["force-hidden"]) {
        continue;
      }
      $default = $cur % 2 == 0;
      $cur = intdiv($cur, 2);
      $hidden = false;
      if (array_key_exists("default-on", $column)) {
        $hidden = !$column["default-on"];
      }
      if (!$default) {
        $hidden = !$hidden;
      }
      $column["col"]->setHidden($hidden);
    }
  }

  private function getCleanColumns($columns)
  {
    $cleanColumns = [];
    foreach ($columns as $column) {
      if (array_key_exists("force-hidden", $column) && $column["force-hidden"]) {
        continue;
      }
      $cleanColumns[] = ["key" => $column["col"]->getUniqueId(), "default-on" => $column["default-on"]];
    }
    return $cleanColumns;
  }

  private function getColumns($id)
  {
    $columns = [];

    $col = new Column\Select('id', 'e');
    $col->setHidden(true);
    $col->setIdentity(true);
    $columns[] = ["col" => $col, "weight" => 0, "force-hidden" => true];

    $col = new Column\Select('rank', 'e');
    $col->setLabel('Rank');
    $col->setType(new Integer());
    $columns[] = ["col" => $col, "weight" => 1, "default-on" => true];

    $col = new Column\Select('firstName', 'p');
    $col->setLabel('First Name');
    $columns[] = ["col" => $col, "weight" => 2, "default-on" => true];

    $col = new Column\Select('lastName', 'p');
    $col->setLabel('Last Name');
    $columns[] = ["col" => $col, "weight" => 3, "default-on" => true];


    $this->tournamentsService->addColumns($id, $columns);

    $col = new Column\Select('tournaments', 'e');
    $col->setLabel('# Tournaments');
    $col->setType(new Integer());
    $columns[] = ["col" => $col, "weight" => 4, "default-on" => true];


    function cmp($a, $b)
    {
      if ($a["weight"] == $b["weight"]) {
        return 0;
      }
      return ($a["weight"] < $b["weight"]) ? -1 : 1;
    }

    usort($columns, function ($a, $b) {
      return ($a["weight"] > $b["weight"]);
    });

    return $columns;
  }

  private function renderGrid($dataSource, $start = true, $editable = true, $clickable = true, $title = "Tournaments")
  {
    $userId = null;
    if ($this->zfcUserAuthentication()->hasIdentity()) {
      $userId = $this->zfcUserAuthentication()->getIdentity()->getId();
    }

    /* @var $grid \ZfcDatagrid\Datagrid */
    $grid = $this->grid;
    $grid->setTitle($this->translator->translate($title));
    $grid->setDataSource(
      $dataSource
    );

    $col = new Column\Select('id', 't');
    $col->setHidden(true);
    $col->setIdentity(true);
    $grid->addColumn($col);


    $col = new Column\Select('name', 't');
    $col->setLabel('Name');
    $grid->addColumn($col);

    if ($start) {
      $col = new Column\Select('start', 't');
      if (extension_loaded('intl')) {
        $dateType = new DateTime(
          'Y-m-d H:i',
          IntlDateFormatter::MEDIUM,
          IntlDateFormatter::MEDIUM
        );
      } else {
        $dateType = new MyDateTimeColumn();
      }
      $dateType->setDaterangePickerEnabled();
      $col->setType($dateType);
      $col->setLabel('Start');
      $grid->addColumn($col);
    }

    if ($userId !== null) {
      if ($editable) {
        $editableSelect = new Select(
          'CASE WHEN p.user = :uid AND p.type = 1 THEN 1 ELSE 0 END'
        );
        $imageFormatter = new Column\Formatter\Image();
        $imageFormatter->
        setPrefix($this->renderer->basePath('img/privileges/'));
        $imageFormatter->setAttribute('height', '20');
        $imageFormatter->setLinkAttribute('onclick', 'return false;');
        $col = new Column\Select($editableSelect, 'editable');
        $col->setTranslationEnabled(true);
        $col->setReplaceValues(
          [
            '0' => 'read.png',
            '1' => 'edit.png',
          ]
        );
        $col->setFilterSelectOptions(
          [
            'read.png' => $this->translator->translate('read'),
            'edit.png' => $this->translator->translate('edit'),
          ]
        );
        $col->setUserFilterDisabled(false);
        $col->setUserSortDisabled(false);
        $col->addFormatter($imageFormatter);
        $col->setLabel('Editable');
        $grid->addColumn($col);
      }

      $useAsTemplateAction = new Column\Action\Button();
      $useAsTemplateAction->setAttribute("data-ajax", "false");
      $useAsTemplateAction->setLabel(
        $this->translator->translate('Create new with this template')
      );
      $rowId = $useAsTemplateAction->getRowIdPlaceholder();
      $useAsTemplateAction->setLink('tournament/' . $rowId . '/copy');

      $actions = new Column\Action();
      $actions->setLabel('');
      $actions->addAction($useAsTemplateAction);
      $grid->addColumn($actions);
    }
    if ($clickable) {
      $rowClickAction = new Column\Action\Button();
      $rowClickAction->setAttribute("data-ajax", "false");
      $rowId = $rowClickAction->getRowIdPlaceholder();
      $rowClickAction->
      setLink('tournament/' . $rowId);
      $grid->setRowClickAction($rowClickAction);
    }


    $grid->render();

    return $grid->getResponse();
  }
//</editor-fold desc="Private Methods">
}
