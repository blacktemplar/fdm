<?php
namespace FDM\Controller;

use Detection\MobileDetect;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use FDM\Entity\Game;
use FDM\Entity\Phase;
use FDM\Entity\TournamentPrivileges;
use FDM\Service\FlushServiceInterface;
use FDM\Service\LoadingService;
use FDM\Service\PhaseServiceInterface;
use FDM\Service\TournamentAccessServiceInterface;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\Form\Element\Button;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;
use Zend\Form\Form;
use Zend\Hydrator\ObjectProperty;
use Zend\I18n\Translator\TranslatorInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PhaseController extends AbstractActionController
{
//<editor-fold desc="Fields">
  protected $em;

  protected $fs;

  protected $group;

  protected $loadingService;

  /** @var  Phase */
  protected $phase;

  protected $phaseService;

  protected $tas;

  protected $translator;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    EntityManager $entityManager,
    TournamentAccessServiceInterface $tas,
    FlushServiceInterface $fs,
    PhaseServiceInterface $phaseService,
    TranslatorInterface $translator,
    LoadingService $loadingService)
  {
    $this->em = $entityManager;
    $this->tas = $tas;
    $this->fs = $fs;
    $this->phaseService = $phaseService;
    $this->translator = $translator;
    $this->loadingService = $loadingService;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function finishedGamesAction()
  {
    $this->parseRoute();
    $form = $this->getSubmitFinishedGamesForm();
    $games = [];
    foreach ($this->phase->getGroups() as $group) {
      $games = array_merge(
        $games,
        $group->getGamesByType(Game::TYPE_FINISHED)
      );
    }
    $view = new ViewModel(
      [
        'form' => $form,
        'games' => $games,
        'submitName' => "submit_finishedGames",
        'title' => "Finished Games",
        'editable' => $this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT),
      ]
    );
    $view->setTemplate('fdm/phase/run-fin-games');
    return $view;
  }

  public function groupRankingsAction()
  {
    $this->parseRoute();
    $columns = $this->phaseService->getRankingsColumns($this->group);
    return new ViewModel(
      [
        'rankings' => $this->group->getRankings(),
        'columns' => $columns,
      ]
    );
  }

  /** @noinspection PhpMissingParentCallCommonInspection */
  public function indexAction()
  {
    $this->parseRoute();
    if (!$this->phase) {
      throw new \Exception("The given phase does not exist!");
    }
    $this->validateForms();

    if (!$this->phase->isStarted()) {
      //combine multiple fieldsets in one form

      $form = $this->getSubmitSettingsForm();
      $view = new ViewModel(['form' => $form]);
      $view->setTemplate('fdm/phase/mode-settings.phtml');
      $view->setVariable("tournamentId", $this->phase->getTournament()->getId());

      return $view;
    }

    $view = $this->getContentView();

    $detect = new MobileDetect;
    $deviceType = $detect->isMobile() ?
      ($detect->isTablet() ? 'tablet' : 'phone') : 'computer';
    if ($deviceType === "phone") {
      $view->setTemplate('fdm/phase/index_phone.phtml');
    }

    return $view;
  }

  public function modeSettingsAction()
  {
    $this->parseRoute();
    $form = $this->getSubmitModeSettingsForm();
    $unstartForm = $this->getUnstartForm();
    $view = new ViewModel(['form' => $form, 'unstartForm' => $unstartForm]);
    $view->setVariable("tournamentId", $this->phase->getTournament()->getId());
    $view->setTemplate('fdm/phase/mode-settings.phtml');
    return $view;
  }

  public function prototypesAction()
  {
    $this->parseRoute();
    $view = $this->getContentView();

    $view->setTemplate('fdm/phase/prototypes.phtml')
      ->setTerminal(true);

    return $view;
  }

  public function runningGamesAction()
  {
    $this->parseRoute();
    $form = $this->getSubmitRunningGamesForm();
    $games = [];
    foreach ($this->phase->getGroups() as $group) {
      $games = array_merge(
        $games,
        $group->getGamesByType(Game::TYPE_RUNNING)
      );
    }
    $view = new ViewModel(
      [
        'form' => $form,
        'games' => $games,
        'submitName' => "submit_runningGames",
        'title' => "Running Games",
        'editable' => $this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT),
      ]
    );
    $view->setTemplate('fdm/phase/run-fin-games');
    return $view;
  }

  public function scheduledGamesAction()
  {
    $this->parseRoute();

    $games = $this->getGames();

    $games = array_merge($games,
      $this->phaseService->getGamesByType([Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN], $this->phase));

    $games = array_merge($games,
      $this->phaseService->getGamesByType([Game::TYPE_SCHEDULED_DYN_CONFLICT], $this->phase));

    $form = $this->getSubmitScheduledGamesForm();

    $view = new ViewModel(
      [
        'form' => $form,
        'games' => $games,
        'phase' => $this->phase,
        'editable' => $this->tas->hasAccessType(
          TournamentPrivileges::TYPE_EDIT
        ),
        'submitName' => "submit_scheduledGames"
      ]
    );
    return $view;
  }

  public function undoGameAction()
  {
    $this->parseRoute();
    $this->loadingService->loadPhaseCompletely($this->phase);
    $game = $this->em->find('FDM\Entity\Game', $this->params('game'));
    if ($game !== null) {
      switch ($game->getType()) {
        case Game::TYPE_FINISHED:
          $game->setType(Game::TYPE_RUNNING);
          $game->setResultA(0);
          $game->setResultB(0);
          $group = $game->getGroup();
          if ($group->isFinished()) {
            $group->setFinished(false);
          }
          if ($this->phase->isFinished()) {
            $this->phase->setFinished(false);
          }
          $this->fs->flush();
          break;
        case Game::TYPE_RUNNING:
          $game->setType(Game::TYPE_SCHEDULED_DYN);
          $game->setTable(0);
          $this->fs->flush();
          break;
        default:
          //nothing to do
          break;
      }
    }

    return $this->redirect()->toRoute(
      'tournament/phase', [
        'id' => $this->phase->getTournament()->getId(),
        'phase' => $this->phase->getId()
      ]
    );
  }

  public function unstartPhaseCallback()
  {
    $this->phaseService->unStartPhase($this->phase);
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  /**
   * @param $objects
   * @param $submitValue
   * @param $submitName
   * @param string $fieldsetClass
   * @param null $param
   * @return Form
   */
  private function createForm(
    $objects, $submitValue, $submitName, $fieldsetClass = "", $param = null
  )
  {
    $form = new Form();
    $form->setHydrator(new ObjectProperty());

    foreach ($objects as $name => $object) {
      $fieldSet = $this->getFieldSet($object, $fieldsetClass, $param);
      $fieldSet->setName($name);
      $form->add($fieldSet);
    }
    $form->bind($objects);
    if ($this->tas->hasAccessType(
      TournamentPrivileges::TYPE_EDIT
    )
    ) {
      $form->add(new Csrf('security'));
      $submit = new Submit('submit');
      $submit->setValue($submitValue);
      $submit->setName($submitName);
      $form->add($submit);
    }

    return $form;
  }

  private function dispatchStructure(
    &$contentStructure, ViewModel $view, &$descendants, $preKey
  )
  {
    foreach ($contentStructure as $key => &$widget) {
      $id = $preKey . ($preKey != "" ? "_" : "") . $key;
      if ($widget["type"] == "collection") {
        if (array_key_exists("children", $widget)) {
          $this->dispatchStructure(
            $widget["children"], $view, $descendants, $id
          );
        }
      } else {
        $widget["globalKey"] = $id;
        $dispatchOptions = [];
        if (array_key_exists("parameters", $widget)) {
          $dispatchOptions = $widget["parameters"];
        }
        $dispatchOptions["action"] = $widget["action"];
        $result = $this->forward()->dispatch(
          $widget["controller"], $dispatchOptions
        );
        $widget["globalKey"] = $id;
        $view->addChild($result, $id);
        $descendants[] = $id;

        //unset irrelevant informations for javascript
        unset($widget["controller"]);
        unset($widget["action"]);
        unset($widget["parameters"]);
      }
    }
  }

  private function getContentView()
  {
    if (!$this->phase) {
      throw new \Exception("The given phase does not exist!");
    }
    if (!$this->phase->isStarted()) {
      throw new \Exception("Phase not started, there is no content!");
    }

    /**
     * array with the following informations:
     * array(
     *     key => array(
     *         "title" => ?
     *         "type" => ? collection or element
     *
     *         in case of collection:
     *         "children" => array(
     *             ...
     *         )
     *
     *         in case of element:
     *         "title" => ?
     *         "controller" => ?
     *         "action" => ?
     *         "parameters" => array( ... ) parameters for action
     *     )
     *     ...
     * )
     */
    $this->loadingService->loadPhaseCompletely($this->phase);
    $contentStructure =
      $this->phaseService->getContentStructure($this->phase);
    $this->translateStructure($contentStructure);
    $descendants = [];
    $view = new ViewModel();
    $this->dispatchStructure($contentStructure, $view, $descendants, "");
    $view->setVariable("descendants", $descendants);
    $view->setVariable("structure", $contentStructure);
    $view->setVariable("tournamentId", $this->phase->getTournament()->getId());
    $view->setVariable("phaseId", $this->phase->getId());
    $modeIds = [];
    foreach ($this->phase->getModes() as $mode) {
      $modeIds[] = $mode->getServiceName();
    }
    $view->setVariable("modeIds", $modeIds);
    $view->setVariable("numGroups", $this->phase->getGroups()->count());

    return $view;
  }

  private function getFieldSet($object, $fieldsetClass = "", $param = null)
  {
    if ($fieldsetClass != "") {
      $fieldset = new $fieldsetClass($param);
    } else {
      // TODO this is a dirty hack, it should work with the two lines below, but proxies create additional unwanted fields
      $className = \Doctrine\Common\Util\ClassUtils::getClass($object);

      $builder = new AnnotationBuilder();
      $fieldset = $builder->createForm($className);

      /*$builder = new AnnotationBuilder($this->em);
      $fieldset = $builder->createForm($object);*/
    }
    $fieldset->setHydrator(new DoctrineHydrator($this->em));
    $fieldset->setObject($object);
    if (!$this->tas->hasAccessType(
      TournamentPrivileges::TYPE_EDIT
    )
    ) {
      foreach ($fieldset as $element) {
        $element->setAttribute('readonly', 'readonly');
      }
    }
    return $fieldset;
  }

  private function getGames()
  {
    return $this->phaseService->getGamesByType([Game::TYPE_SCHEDULED_DYN, Game::TYPE_SCHEDULED_FIX], $this->phase);
  }

  private function getSubmitFinishedGamesForm()
  {
    $objects = new \stdClass;
    foreach ($this->phase->getGroups() as $group) {
      foreach ($group->getGamesByType(Game::TYPE_FINISHED) as $game) {
        $id = $game->getId();
        $objects->$id = $game;
      }
    }

    return $this->createForm(
      $objects,
      "save",
      'submit_finishedGames',
      "\FDM\Form\RunFinGameFieldset"
    );
  }

  private function getSubmitModeSettingsForm()
  {
    $objects = new \stdClass;
    $objects->phase = $this->phase;
    $objects->mode = $this->phase->getModesByTemplate(false)[0];
    if ($this->phase->isSingleMode()) {
      $objects->mode = $this->phase->getModesByTemplate(false)[0];
    } else {
      //TODO
      throw new \Exception("Not implemented (yet)!");
    }
    return $this->createForm($objects, "save", "submit_modeSettings");
  }

  private function getSubmitRunningGamesForm()
  {
    $objects = new \stdClass;
    foreach ($this->phase->getGroups() as $group) {
      foreach ($group->getGamesByType(Game::TYPE_RUNNING) as $game) {
        $id = $game->getId();
        $objects->$id = $game;
      }
    }

    return $this->createForm(
      $objects,
      "save",
      'submit_runningGames',
      "\FDM\Form\RunFinGameFieldset"
    );
  }

  private function getSubmitScheduledGamesForm()
  {
    $objects = new \stdClass;

    $freeTables = $this->phaseService->getFreeTables($this->phase);

    foreach ($this->phase->getGroups() as $group) {
      foreach (array_merge($group->getGamesByType(Game::TYPE_SCHEDULED_DYN),
        $group->getGamesByType(Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN))
               as $game) {
        $id = $game->getId();
        $objects->$id = $game;
      }
    }

    $form = $this->createForm(
      $objects,
      "start",
      'submit_scheduledGames',
      "\FDM\Form\ScheduledGameFieldset",
      $freeTables
    );

    if ($this->tas->hasAccessType(
      TournamentPrivileges::TYPE_EDIT
    )
    ) {
      $submit = new Button('assign-button');
      $submit->setLabel("assign tables");
      $submit->setName("assign-button");
      $submit->setAttribute('onClick', 'return assignTables()');
      $form->add($submit);
    }
    $form->setAttribute('class', 'form_scheduledGames');
    return $form;
  }

  private function getSubmitSettingsForm()
  {
    $previousPhasesEnded = true;
    foreach ($this->phase->getTournament()->getPhasesByTemplate(false) as $p) {
      if ($p->getPhaseNumber() < $this->phase->getPhaseNumber()
        && !$p->isFinished()
      ) {
        $previousPhasesEnded = false;
      }
    }
    $objects = new \stdClass;
    $objects->phase = $this->phase;
    $objects->attendancestrategy =
      $this->phase->getAttendanceStrategy();
    if ($this->phase->isSingleMode()) {
      $objects->mode = $this->phase->getModesByTemplate(false)[0];
    } else {
      //TODO
      throw new \Exception("Not implemented (yet)!");
    }

    $submitName = "start";
    if (!$previousPhasesEnded) {
      $submitName = "save";
    }

    return $this->createForm($objects, $submitName, "submit_settings");
  }

  private function getUnstartForm()
  {
    if ($this->tas->hasAccessType(
      TournamentPrivileges::TYPE_EDIT
    )
    ) {
      return $this->createForm(new \stdClass, "undo start phase", "submit_phaseUnstart");
    } else {
      return NULL;
    }
  }

  private function parseRoute()
  {
    $id = $this->params('phase');
    if ($id) {
      $this->phase = $this->em->find('FDM\Entity\Phase', $id);
      if ($this->phase->getTournament()->getId() != $this->params('id')) {
        //invalid phase !!!
        throw new \Exception("Invalid phase id!");
      }
    }
    $id = $this->params('group');
    if ($id) {
      $this->group = $this->em->find('FDM\Entity\Group', $id);
      if ($this->group->getPhase()->getId() !== $this->phase->getId()) {
        //invalid group!!!
        throw new \Exception("Invalid group!");
      }
    }
  }

  /** @noinspection PhpUnusedPrivateMethodInspection */
  private function submitRunningGamesCallback()
  {
    foreach ($this->phase->getGroups() as $group) {
      foreach ($group->getGamesByType(Game::TYPE_RUNNING) as $game) {
        if ($game->getResultA() != $game->getResultB()) {
          $game->setType(Game::TYPE_FINISHED);
        }
      }
    }
  }

  /** @noinspection PhpUnusedPrivateMethodInspection */
  private function submitScheduledGamesCallback()
  {
    //change game types
    $startedRIds = [];
    foreach ($this->phase->getGroups() as $group) {
      foreach ($group->getGames() as $game) {
        /** @var Game $game */
        if (($game->getType() == Game::TYPE_SCHEDULED_DYN
            || $game->getType() == Game::TYPE_SCHEDULED_FIX
            || $game->getType() == Game::TYPE_SCHEDULED_DYN_CONFLICT_WDYN)
          && $game->getTable() > 0
        ) {
          $rId1 = $game->getTeamA()[0]->getId();
          $rId2 = $game->getTeamB()[0]->getId();
          if (!array_key_exists($rId1, $startedRIds) && !array_key_exists($rId2, $startedRIds)) {
            $startedRIds[$rId1] = true;
            $startedRIds[$rId2] = true;
            $game->setType(Game::TYPE_RUNNING);
            $game->setStartTime(new \DateTime());
          }
        }
      }
    }
  }

  /** @noinspection PhpUnusedPrivateMethodInspection */
  private function submitSettingsFormCallback()
  {
    $previousPhasesEnded = true;
    foreach ($this->phase->getTournament()->getPhasesByTemplate(false) as $p) {
      if ($p->getPhaseNumber() < $this->phase->getPhaseNumber()
        && !$p->isFinished()
      ) {
        $previousPhasesEnded = false;
      }
    }
    if ($previousPhasesEnded) {
      $this->phaseService->startPhase($this->phase);
    }
  }

  private function translateStructure(&$structure)
  {
    foreach ($structure as &$struct) {
      $struct["title"] = $this->translator->translate($struct["title"]);
      if (array_key_exists("children", $struct)) {
        $this->translateStructure($struct["children"]);
      }
    }
  }

  private function validateForms()
  {
    if ($this->tas->hasAccessType(
      TournamentPrivileges::TYPE_EDIT
    )
    ) {
      $this->loadingService->loadPhaseCompletely($this->phase);
      $groupIds = [];
      foreach ($this->phase->getGroups() as $group) {
        $groupIds[] = $group->getId();
      }
      $forms = [
        [
          "name" => 'submit_settings',
          "creator" => 'getSubmitSettingsForm',
          "callback" => 'submitSettingsFormCallback',
        ],
        [
          "name" => 'submit_modeSettings',
          "creator" => 'getSubmitModeSettingsForm',
        ],
        [
          "name" => 'submit_scheduledGames',
          "creator" => 'getSubmitScheduledGamesForm',
          "callback" => 'submitScheduledGamesCallback',
        ],
        [
          "name" => 'submit_scheduledGames_assign',
          "creator" => 'getSubmitScheduledGamesForm',
          "callback" => 'submitScheduledGamesCallback',
        ],
        [
          "name" => 'submit_runningGames',
          "creator" => 'getSubmitRunningGamesForm',
          "callback" => 'submitRunningGamesCallback',
        ],
        [
          "name" => 'submit_finishedGames',
          "creator" => 'getSubmitFinishedGamesForm',
        ],
        [
          "name" => 'submit_phaseUnstart',
          "creator" => 'getUnstartForm',
          "callback" => 'unstartPhaseCallback',
        ]
      ];
      $request = $this->getRequest();
      $creator = null;
      $callback = null;
      $param = null;
      foreach ($forms as $formInfo) {
        $params = [""];
        $hasParams = false;
        if (array_key_exists("params", $formInfo)) {
          $params = $formInfo["params"];
          $hasParams = true;
        }
        foreach ($params as $p) {
          if ($request->getPost($formInfo["name"] . $p, false)) {
            $creator = $formInfo["creator"];
            if ($hasParams) {
              $param = $p;
            }
            if (array_key_exists("callback", $formInfo)) {
              $callback = $formInfo["callback"];
            }
            break;
          }
        }
      }
      if ($creator !== null) {
        $form = $this->$creator($param);
        $form->setData($request->getPost());
        if ($form->isValid()) {
          if ($callback !== null) {
            $this->$callback($param);
          }
          $this->fs->flush();
        }
      }
    }
  }
//</editor-fold desc="Private Methods">
}
