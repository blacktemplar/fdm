<?php

namespace FDM\Form;

use Zend\Validator\ValidatorInterface;
use ZfcBase\InputFilter\ProvidesEventsInputFilter;

class DeleteFilter extends ProvidesEventsInputFilter
{
//<editor-fold desc="Fields">
  protected $passwordValidator;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(ValidatorInterface $passwordValidator)
  {
    $this->passwordValidator = $passwordValidator;

    $this->add(
      [
        'name' => 'password',
        'required' => true,
        'validators' => [
          $this->passwordValidator,
        ],
      ]
    );
  }
//</editor-fold desc="Constructor">
}
