<?php
namespace FDM\Form;

use Zend\Form\Form;

class LocalSettingsForm extends Form
{
//<editor-fold desc="Constructor">
  public function __construct($editable = false, $name = null)
  {
    // we want to ignore the name passed
    parent::__construct($name);

    $this->setAttribute('id', 'local-settings-form');

    $this->add(
      [
        'name' => 'autoRefresh',
        'type' => 'Zend\Form\Element\Checkbox',
        'options' => [
          'label' => 'Automatically refresh on changes',
          'use_hidden_element' => false,
        ],
      ]
    );


    $this->add(
      [
        'name' => 'tournamentId',
        'type' => 'Zend\Form\Element\Hidden',
        'attributes' => [
          'id' => 'hidden_tournamentId',
        ],
      ]
    );

    $this->add(
      [
        'name' => 'save',
        'type' => 'Zend\Form\Element\Button',
        'options' => [
          'label' => 'Save',
        ],
        'attributes' => [
          'onClick' => 'saveLocalSettings()',
        ],
      ]
    );
  }
//</editor-fold desc="Constructor">
}
