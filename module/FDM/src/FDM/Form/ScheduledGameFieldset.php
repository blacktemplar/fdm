<?php
namespace FDM\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class ScheduledGameFieldset extends Fieldset
  implements InputFilterProviderInterface
{
//<editor-fold desc="Constructor">
  public function __construct($freeTables)
  {
    // we want to ignore the name passed
    parent::__construct();

    $options = ["0" => ""];
    foreach ($freeTables as $id => $free) {
      $options[$id] = $id;
    }

    $this->add(
      [
        'name' => 'table',
        'type' => 'Zend\Form\Element\Select',
        'options' => [
          'value_options' => $options,
        ],
      ]
    );
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getInputFilterSpecification()
  {
    return [
      'table' => [
        'required' => false,
      ]
    ];
  }
//</editor-fold desc="Public Methods">
}
