<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 11/17/16
 * Time: 4:03 PM
 */

namespace FDM\Form;


use Zend\Filter\FilterInterface;

class DateParseFilter implements FilterInterface
{
//<editor-fold desc="Public Methods">
  public function filter($value)
  {
    // perform some transformation upon $value to arrive on $valueFiltered
    $allowedFormats = ['d.m.Y', 'd.m.y', 'm/d/Y', 'Y-m-d'];
    foreach ($allowedFormats as $format) {
      $res = date_parse_from_format($format, $value);
      if ($res !== false && $res['error_count'] === 0) {
        return $res['year'] . '-' . $res['month'] . '-' . $res['day'];
      }
    }
    return $value;
  }
//</editor-fold desc="Public Methods">
}