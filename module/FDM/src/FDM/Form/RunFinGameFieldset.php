<?php
namespace FDM\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class RunFinGameFieldset extends Fieldset
  implements InputFilterProviderInterface
{
//<editor-fold desc="Constructor">
  public function __construct()
  {
    // we want to ignore the name passed
    parent::__construct();

    $this->add(
      [
        'name' => 'resultA',
        'type' => 'Zend\Form\Element\Number',
        'attributes' => [
          'class' => 'digit',
        ],
      ]
    );
    $this->add(
      [
        'name' => 'resultB',
        'type' => 'Zend\Form\Element\Number',
        'attributes' => [
          'class' => 'digit',
        ],

      ]
    );
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getInputFilterSpecification()
  {
    return [
      'resultA' => [
        'required' => true,
        'filters' => [
          ['name' => 'Int'],
        ],
      ],
      'resultB' => [
        'required' => true,
        'filters' => [
          ['name' => 'Int'],
        ],
      ],
    ];
  }
//</editor-fold desc="Public Methods">
}
