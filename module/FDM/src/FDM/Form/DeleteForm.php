<?php
namespace FDM\Form;

use Zend\Form\Form;

class DeleteForm extends Form
{
//<editor-fold desc="Constructor">
  public function __construct($name = null)
  {
    // we want to ignore the name passed
    parent::__construct($name);

    $this->add(
      [
        'name' => 'password',
        'type' => 'Zend\Form\Element\Password',
        'attributes' => [
          'autocomplete' => 'off',
        ],
        'options' => [
          'label' => 'Password',
        ],
      ]
    );
    $this->add(
      [
        'name' => 'submit_delete',
        'type' => 'Zend\Form\Element\Submit',
        'attributes' => [
          'value' => 'Delete',
        ],
      ]
    );
    $this->add(
      [
        'name' => 'security',
        'type' => 'Zend\Form\Element\Csrf',
      ]
    );
  }
//</editor-fold desc="Constructor">
}
