<?php
namespace FDM\Form;

use Zend\Crypt\Password\Bcrypt;
use Zend\Validator\AbstractValidator;

class PasswordValidator extends AbstractValidator
{
//<editor-fold desc="Constants">
  const WRONG = 'wrong';

//</editor-fold desc="Constants">

//<editor-fold desc="Fields">
  protected $messageTemplates = [
    self::WRONG => "The given password is incorrect",
  ];

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  public function isValid($value)
  {
    $this->setValue($value);

    $user = $this->getOption("user");
    $userService = $this->getOption("userService");

    $bcrypt = new Bcrypt;
    $bcrypt->setCost($userService->getOptions()->getPasswordCost());

    if (!$bcrypt->verify($value, $user->getPassword())) {
      $this->error(self::WRONG);
      return false;
    }

    return true;
  }
//</editor-fold desc="Public Methods">
}
