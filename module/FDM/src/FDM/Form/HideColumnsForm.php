<?php
namespace FDM\Form;

use Zend\Form\Form;

class HideColumnsForm extends Form
{
//<editor-fold desc="Constructor">
  public function __construct($columns, $name = null)
  {
    // we want to ignore the name passed
    parent::__construct($name);

    $this->setAttribute('id', 'hide-columns-form');

    $i = 0;
    foreach ($columns as $column) {
      if (array_key_exists("force-hidden", $column) && $column["force-hidden"]) {
        continue;
      }
      $i += 1;
      $this->add(
        [
          'name' => $column["col"]->getUniqueId(),
          'type' => 'Zend\Form\Element\Checkbox',
          'options' => [
            'label' => $column["col"]->getLabel(),
            'use_hidden_element' => false,
          ],
          'attributes' => [
            'value' => !$column["col"]->isHidden(),
          ]
        ]
      );
    }
    $this->add(
      [
        'name' => 'save',
        'type' => 'Zend\Form\Element\Button',
        'options' => [
          'label' => 'Save',
        ],
        'attributes' => [
          'onClick' => "saveHColumns()",
        ],
      ]
    );
  }
//</editor-fold desc="Constructor">
}
