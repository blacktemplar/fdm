<?php

namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tournamentprivileges")
 */
class TournamentPrivileges
{
//<editor-fold desc="Constants">
  const TYPE_EDIT = 1;

  const TYPE_READ = 0;

//</editor-fold desc="Constants">

//<editor-fold desc="Fields">
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @var string
   */
  protected $id;

  /**
   * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="privileges")
   * @var Tournament
   */
  protected $tournament;

  /**
   * @ORM\Column(type="integer")
   * @var int
   */
  protected $type;

  /**
   * @ORM\ManyToOne(targetEntity="User")
   * @ORM\JoinColumn(name="user", referencedColumnName="user_id")
   * @var User
   */
  protected $user;

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  /**
   * Get id.
   *
   * @return {string} id.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get tournament.
   *
   * @return tournament.
   */
  public function getTournament()
  {
    return $this->tournament;
  }

  /**
   * Get type.
   *
   * @return {int} type.
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Get user.
   *
   * @return user.
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Set tournament.
   *
   * @param {Tournament} tournament the value to set.
   */
  public function setTournament($tournament)
  {
    $this->tournament = $tournament;
    $tournament->getPrivileges()->add($this);
  }

  /**
   * Set type.
   *
   * @param {int} type the value to set.
   */
  public function setType($type)
  {
    $this->type = $type;
  }

  /**
   * Set user.
   *
   * @param {User} user the value to set.
   */
  public function setUser($user)
  {
    $this->user = $user;
  }
//</editor-fold desc="Public Methods">

}
