<?php

namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Zend\Form\Annotation as Form;

/**
 * Timestampable Trait, usable with PHP >= 5.4
 *
 * @author Gediminas Morkevicius <gediminas.morkevicius@gmail.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
trait TimestampableEntity
{
//<editor-fold desc="Fields">
  /**
   * @var \DateTime
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(type="datetime")
   * @Form\Exclude
   */
  protected $createdAt;

  /**
   * @var \DateTime
   * @Gedmo\Timestampable(on="update")
   * @ORM\Column(type="datetime")
   * @Form\Exclude
   */
  protected $updatedAt;

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  /**
   * Returns createdAt.
   *
   * @return \DateTime
   */
  public function getCreatedAt()
  {
    return $this->createdAt;
  }

  /**
   * Returns updatedAt.
   *
   * @return \DateTime
   */
  public function getUpdatedAt()
  {
    return $this->updatedAt;
  }

  /**
   * Sets createdAt.
   *
   * @param  \DateTime $createdAt
   * @return $this
   */
  public function setCreatedAt(\DateTime $createdAt)
  {
    $this->createdAt = $createdAt;

    return $this;
  }

  /**
   * Sets updatedAt.
   *
   * @param  \DateTime $updatedAt
   * @return $this
   */
  public function setUpdatedAt(\DateTime $updatedAt)
  {
    $this->updatedAt = $updatedAt;

    return $this;
  }
//</editor-fold desc="Public Methods">
}
