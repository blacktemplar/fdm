<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 12/18/16
 * Time: 4:19 PM
 */

namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * @ORM\Entity
 * @ORM\Table(name="speaktask")
 */
class SpeakTask
{
  use TimestampableEntity;

//<editor-fold desc="Fields">
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @Form\Exclude
   * @var string
   */
  protected $id;

  /**
   * @ORM\Column(type="string")
   * @Form\Exclude
   * @var string
   */
  protected $info;

  /**
   * @ORM\Column(type="integer")
   * @Form\Exclude
   * @var int
   */
  protected $orderNumber;

  /**
   * @ORM\Column(type="text")
   * @Form\Exclude
   * @var string
   */
  protected $text;

  /**
   * @ORM\ManyToOne(targetEntity="Tournament")
   * @var Tournament
   */
  protected $tournament;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($text, $info, $tournament, $orderNumber)
  {
    $this->text = $text;
    $this->info = $info;
    $this->tournament = $tournament;
    $this->orderNumber = $orderNumber;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * @return string
   */
  public function getId(): string
  {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getInfo(): string
  {
    return $this->info;
  }

  /**
   * @return mixed
   */
  public function getOrderNumber()
  {
    return $this->orderNumber;
  }

  /**
   * @return string
   */
  public function getText(): string
  {
    return $this->text;
  }

  /**
   * @param mixed $orderNumber
   */
  public function setOrderNumber($orderNumber)
  {
    $this->orderNumber = $orderNumber;
  }
//</editor-fold desc="Public Methods">
}