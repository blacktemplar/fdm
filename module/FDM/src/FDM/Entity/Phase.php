<?php
namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;


/**
 * @ORM\Entity
 * @ORM\Table(name="phase")
 * @Form\Name("phase")
 * @Form\Type("fieldset")
 */
class Phase
{
//<editor-fold desc="Fields">
  /**
   * @ORM\OneToOne(
   *     targetEntity="AttendanceStrategy",
   * )
   * @Form\Exclude
   * @var AttendanceStrategy
   */
  protected $attendanceStrategy;

  /**
   * @ORM\Column(type="string")
   * @Form\Exclude
   * @var string
   */
  protected $attendanceStrategyServiceName;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $finished;

  /**
   * @ORM\OneToMany(
   *     targetEntity="Group",
   *     mappedBy="phase",
   *     indexBy="groupNumber",
   * )
   * @ORM\OrderBy({"groupNumber" = "ASC"})
   * @Form\Exclude
   * @var \Doctrine\Common\Collections\ArrayCollection
   */
  protected $groups;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")   * @ORM\Column(type="guid")
   * @Form\Exclude
   * @var string
   */
  protected $id;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   * @Form\Exclude
   * @var \DateTime
   */
  protected $lastChanged;

  /**
   * @ORM\OneToMany(
   *     targetEntity="Mode",
   *     mappedBy="phase",
   * )
   * @Form\Exclude
   * @var \Doctrine\Common\Collections\ArrayCollection
   */
  protected $modes;

  /**
   * @ORM\Column(type="string")
   * @Form\Options({"label":"Name"})
   * @var string
   */
  protected $name;

  /**
   * @ORM\Column(type="integer")
   * @Form\Options({"label":"Number of tables"})
   * @Form\Filter({"name":"Int"})
   * @var int
   */
  protected $numTables;

  /**
   * @ORM\Column(type="integer")
   * @Form\Exclude
   * @var int
   */
  protected $phaseNumber;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Options({"label":"singleMode"})
   * @Form\Type("Zend\Form\Element\Checkbox")
   * @Form\Filter({"name":"Boolean"})
   * @Form\Exclude
   * @var bool
   */
  protected $singleMode;

  /**
   * @ORM\Column(type="text")
   * @Form\Type("Zend\Form\Element\Textarea")
   * @Form\Options({"label":"Start Text"})
   * @var string
   */
  protected $startText;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $started;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $template;

  /**
   * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="phases")
   * @Form\Exclude
   * @var Tournament
   */
  protected $tournament;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->template = true;
    $this->phaseNumber = 0;
    $this->startText = "";
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function __clone()
  {
    $this->id = null;
  }

  public function addMode($mode)
  {
    $this->modes->add($mode);
  }

  /**
   * Get attendanceStrategy.
   *
   * @return {AttendanceStrategy} attendanceStrategy.
   */
  public function getAttendanceStrategy()
  {
    return $this->attendanceStrategy;
  }

  /**
   * Get attendanceStrategyServiceName.
   *
   * @return {string} attendanceStrategyServiceName.
   */
  public function getAttendanceStrategyServiceName()
  {
    return $this->attendanceStrategyServiceName;
  }

  /**
   * Get groups.
   *
   * @return \Doctrine\Common\Collections\ArrayCollection|Group[] groups.
   */
  public function getGroups()
  {
    return $this->groups;
  }

  /**
   * Get id.
   *
   * @return {string} id.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get lastChanged.
   *
   * @return {DateTime} lastChanged.
   */
  public function getLastChanged()
  {
    return $this->lastChanged;
  }

  /**
   * Get modes.
   *
   * @return \Doctrine\Common\Collections\ArrayCollection modes.
   */
  public function getModes()
  {
    return $this->modes;
  }

  /**
   * Get modes.
   *
   * @return \FDM\Entity\Mode[] modes.
   */
  public function getModesByTemplate($template)
  {
    $res = [];
    foreach ($this->modes as $mode) {
      if ($mode->isTemplate() == $template) {
        $res[$mode->getGroupNumber()] = $mode;
      }
    }
    ksort($res);
    return $res;
  }

  /**
   * Get name.
   *
   * @return {string} name.
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get numTables.
   *
   * @return {int} numTables.
   */
  public function getNumTables()
  {
    return $this->numTables;
  }

  /**
   * Get phaseNumber.
   *
   * @return {int} phaseNumber.
   */
  public function getPhaseNumber()
  {
    return $this->phaseNumber;
  }

  /**
   * @return string
   */
  public function getStartText()
  {
    return $this->startText;
  }

  /**
   * Get tournament.
   *
   * @return tournament.
   */
  public function getTournament()
  {
    return $this->tournament;
  }


  /**
   * Get finished.
   *
   * @return {bool} finished.
   */
  public function isFinished()
  {
    return $this->finished;
  }

  /**
   * Get singleMode.
   *
   * @return {bool} singleMode.
   */
  public function isSingleMode()
  {
    return $this->singleMode;
  }

  /**
   * Get started.
   *
   * @return {bool} started.
   */
  public function isStarted()
  {
    return $this->started;
  }

  /**
   * Get template.
   *
   * @return {bool} template.
   */
  public function isTemplate()
  {
    return $this->template;
  }

  /**
   * Set attendanceStrategy.
   *
   * @param {AttendanceStrategy} attendanceStrategy the value to set.
   */
  public function setAttendanceStrategy($attendanceStrategy)
  {
    $this->attendanceStrategy = $attendanceStrategy;
  }

  /**
   * Set attendanceStrategyServiceName.
   *
   * @param {string} attendanceStrategyServiceName the value to set.
   */
  public function setAttendanceStrategyServiceName(
    $attendanceStrategyServiceName
  )
  {
    $this->attendanceStrategyServiceName = $attendanceStrategyServiceName;
  }

  /**
   * Set finished.
   *
   * @param {bool} finished the value to set.
   */
  public function setFinished($finished)
  {
    $this->finished = $finished;
  }

  /**
   * Set groups.
   *
   * @param {ArrayCollection} groups the value to set.
   */
  public function setGroups($groups)
  {
    $this->groups = $groups;
  }

  /**
   * Set lastChanged.
   *
   * @param {\DateTime} lastChanged the value to set.
   */
  public function setLastChanged($lastChanged)
  {
    $this->lastChanged = $lastChanged;
  }

  /**
   * Set modes.
   *
   * @param {ArrayCollection} modes the value to set.
   */
  public function setModes($modes)
  {
    $this->modes = $modes;
  }

  /**
   * Set name.
   *
   * @param {string} name the value to set.
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Set numTables.
   *
   * @param {int} numTables the value to set.
   */
  public function setNumTables($numTables)
  {
    $this->numTables = $numTables;
  }

  /**
   * Set phaseNumber.
   *
   * @param {int} phaseNumber the value to set.
   */
  public function setPhaseNumber($phaseNumber)
  {
    $this->phaseNumber = $phaseNumber;
  }

  /**
   * Set singleMode.
   *
   * @param {bool} singleMode the value to set.
   */
  public function setSingleMode($singleMode)
  {
    $this->singleMode = $singleMode;
  }

  /**
   * @param string $startText
   */
  public function setStartText($startText)
  {
    $this->startText = $startText;
  }

  /**
   * Set started.
   *
   * @param {bool} started the value to set.
   */
  public function setStarted($started)
  {
    $this->started = $started;
  }

  /**
   * Set template.
   *
   * @param {bool} template the value to set.
   */
  public function setTemplate($template)
  {
    $this->template = $template;
  }

  /**
   * Set tournament.
   *
   * @param {Tournament} tournament the value to set.
   */
  public function setTournament($tournament)
  {
    $this->tournament = $tournament;
    $tournament->getPhases()->add($this);
  }
//</editor-fold desc="Public Methods">
}
