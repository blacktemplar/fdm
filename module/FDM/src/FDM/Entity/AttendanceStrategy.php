<?php
namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * @ORM\Entity
 * @ORM\Table(name="attendancestrategy")
 * @Form\Name("attendancestrategy")
 * @Form\Type("fieldset")
 */
class AttendanceStrategy
{
//<editor-fold desc="Fields">
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @Form\Exclude
   * @var string
   */
  protected $id;

  /**
   * @ORM\Column(type="integer")
   * @Form\Options({"label": "Maximal players per group"})
   * @Form\Filter({"name":"Int"})
   * @var int
   */
  protected $maxPlayersPerGroup;

  /**
   * @ORM\Column(type="integer")
   * @Form\Options({"label": "Merge teams"})
   * @Form\Filter({"name":"Int"})
   * @var int
   */
  protected $merge;

  /**
   * @ORM\Column(type="integer")
   * @Form\Options({"label": "Number of groups"})
   * @Form\Filter({"name":"Int"})
   * @var int
   */
  protected $numGroups;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Type("Zend\Form\Element\Checkbox")
   * @Form\Options({"label":"Shuffle after grouping"})
   * @Form\Filter({"name":"Boolean"})
   * @var bool
   */
  protected $postGroupingShuffle;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Options({"label":"Shuffle before grouping"})
   * @Form\Type("Zend\Form\Element\Checkbox")
   * @Form\Filter({"name":"Boolean"})
   * @var bool
   */
  protected $preGroupingShuffle;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Options({"label":"DYP"})
   * @Form\Type("Zend\Form\Element\Checkbox")
   * @Form\Filter({"name":"Boolean"})
   * @var bool
   */
  protected $preShuffle;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $template;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->template = true;
    $this->phase = 0;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function __clone()
  {
    $this->id = null;
  }

  /**
   * Get id.
   *
   * @return {string} id.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get maxPlayersPerGroup.
   *
   * @return {int} maxPlayersPerGroup.
   */
  public function getMaxPlayersPerGroup()
  {
    return $this->maxPlayersPerGroup;
  }

  /**
   * Get merge.
   *
   * @return {int} merge.
   */
  public function getMerge()
  {
    return $this->merge;
  }

  /**
   * Get numGroups.
   *
   * @return {int} numGroups.
   */
  public function getNumGroups()
  {
    return $this->numGroups;
  }

  /**
   * Get postGroupingShuffle.
   *
   * @return {bool} postGroupingShuffle.
   */
  public function isPostGroupingShuffle()
  {
    return $this->postGroupingShuffle;
  }

  /**
   * Get preGroupingShuffle.
   *
   * @return {bool} preGroupingShuffle.
   */
  public function isPreGroupingShuffle()
  {
    return $this->preGroupingShuffle;
  }

  /**
   * Get preShuffle.
   *
   * @return {bool} preShuffle.
   */
  public function isPreShuffle()
  {
    return $this->preShuffle;
  }

  public function isTemplate()
  {
    return $this->template;
  }

  /**
   * Set maxPlayersPerGroup.
   *
   * @param {int} maxPlayersPerGroup the value to set.
   */
  public function setMaxPlayersPerGroup($maxPlayersPerGroup)
  {
    $this->maxPlayersPerGroup = $maxPlayersPerGroup;
  }

  /**
   * Set merge.
   *
   * @param {int} merge the value to set.
   */
  public function setMerge($merge)
  {
    $this->merge = $merge;
  }

  /**
   * Set numGroups.
   *
   * @param {int} numGroups the value to set.
   */
  public function setNumGroups($numGroups)
  {
    $this->numGroups = $numGroups;
  }

  /**
   * Set postGroupingShuffle.
   *
   * @param {bool} postGroupingShuffle the value to set.
   */
  public function setPostGroupingShuffle($postGroupingShuffle)
  {
    $this->postGroupingShuffle = $postGroupingShuffle;
  }

  /**
   * Set preGroupingShuffle.
   *
   * @param {bool} preGroupingShuffle the value to set.
   */
  public function setPreGroupingShuffle($preGroupingShuffle)
  {
    $this->preGroupingShuffle = $preGroupingShuffle;
  }

  /**
   * Set preShuffle.
   *
   * @param {bool} preShuffle the value to set.
   */
  public function setPreShuffle($preShuffle)
  {
    $this->preShuffle = $preShuffle;
  }

  public function setTemplate($template)
  {
    $this->template = $template;
  }
//</editor-fold desc="Public Methods">

}
