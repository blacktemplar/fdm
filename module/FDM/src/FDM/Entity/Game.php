<?php
namespace FDM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * @ORM\Entity
 * @ORM\Table(name="game")
 * @Form\Name("game")
 * @Form\Type("fieldset")
 */
class Game
{
//<editor-fold desc="Constants">
  const TYPE_FINISHED = 2;

  const TYPE_RUNNING = 1;

  const TYPE_SCHEDULED_DYN = 0;

  const TYPE_SCHEDULED_DYN_CONFLICT = 5;

  const TYPE_SCHEDULED_DYN_CONFLICT_WDYN = 4;

  const TYPE_SCHEDULED_FIX = 3;

//</editor-fold desc="Constants">

//<editor-fold desc="Fields">
  /**
   * @ORM\ManyToOne(
   *     targetEntity="Group",
   *     inversedBy="games"
   * )
   * @Form\Exclude
   * @var Group
   */
  protected $group;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @var string
   */
  protected $id;

  /**
   * @ORM\column(type="float")
   * @Form\Exclude
   * @var double
   */
  protected $relevance;

  /**
   * @ORM\column(type="integer")
   * @Form\Attributes({"class":"digit"})
   * @Form\Type("Zend\Form\Element\Number")
   * @Form\Filter({"name":"Int"})
   * @var int
   */
  protected $resultA;

  /**
   * @ORM\column(type="integer")
   * @Form\Attributes({"class":"digit"})
   * @Form\Type("Zend\Form\Element\Number")
   * @Form\Filter({"name":"Int"})
   * @var int
   */
  protected $resultB;

  /**
   * @ORM\column(type="integer")
   * @var int
   */
  protected $round;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   * @Form\Exclude
   * @var \DateTime
   */
  protected $startTime;

  /**
   * @ORM\column(type="integer", name="tableNr")
   * @Form\Type("Zend\Form\Element\Select")
   * @Form\Filter({"name":"Int"})
   * @var int
   */
  protected $table;

  /**
   * @ORM\ManyToMany(
   *     targetEntity="Ranking",
   *     inversedBy="gamesAsA",
   * )
   * @Form\Exclude
   * @ORM\JoinTable(name="relation__gamerankings__teamA")
   * @var Ranking[]
   */
  protected $teamA;

  /**
   * @ORM\ManyToMany(
   *     targetEntity="Ranking",
   *     inversedBy="gamesAsB",
   * )
   * @Form\Exclude
   * @ORM\JoinTable(name="relation__gamerankings__teamB")
   * @var Ranking[]
   */
  protected $teamB;

  /**
   * @ORM\column(type="integer")
   * @var int
   */
  protected $type;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->teamA = new ArrayCollection();
    $this->teamB = new ArrayCollection();
    $this->resultA = 0;
    $this->resultB = 0;
    $this->table = 0;
    $this->startTime = null;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function addToTeamA($ranking)
  {
    $this->teamA->add($ranking);
    $ranking->getGamesAsA()->add($this);
  }

  public function addToTeamB($ranking)
  {
    $this->teamB->add($ranking);
    $ranking->getGamesAsB()->add($this);
  }

  public function clearTeamA()
  {
    foreach ($this->teamA as $r) {
      $r->getGamesAsA()->removeElement($this);
    }
    $this->teamA->clear();
  }

  public function clearTeamB()
  {
    foreach ($this->teamB as $r) {
      $r->getGamesAsB()->removeElement($this);
    }
    $this->teamB->clear();
  }

  /**
   * Get group.
   *
   * @return {Group} group.
   */
  public function getGroup()
  {
    return $this->group;
  }

  /**
   * Get id.
   *
   * @return {string} id.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get relevance.
   *
   * @return {double} relevance.
   */
  public function getRelevance()
  {
    return $this->relevance;
  }

  /**
   * Get resultA.
   *
   * @return {int} resultA.
   */
  public function getResultA()
  {
    return $this->resultA;
  }

  /**
   * Get resultB.
   *
   * @return {int} resultB.
   */
  public function getResultB()
  {
    return $this->resultB;
  }

  /**
   * Get round.
   *
   * @return {int} round.
   */
  public function getRound()
  {
    return $this->round;
  }

  /**
   * @return mixed
   */
  public function getStartTime()
  {
    return $this->startTime;
  }

  /**
   * Get table.
   *
   * @return {int} table.
   */
  public function getTable()
  {
    return $this->table;
  }

  /**
   * Get teamA.
   *
   * @return {Ranking[]} teamA.
   */
  public function getTeamA()
  {
    return $this->teamA;
  }

  /**
   * Get teamB.
   *
   * @return {Ranking[]} teamB.
   */
  public function getTeamB()
  {
    return $this->teamB;
  }

  /**
   * Get type.
   *
   * @return {int} type.
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set group.
   *
   * @param {Group} group the value to set.
   */
  public function setGroup($group)
  {
    $this->group = $group;
    $group->getGames()->add($this);
  }

  /**
   * Set relevance.
   *
   * @param {double} relevance the value to set.
   */
  public function setRelevance($relevance)
  {
    $this->relevance = $relevance;
  }

  /**
   * Set resultA.
   *
   * @param {int} resultA the value to set.
   */
  public function setResultA($resultA)
  {
    $this->resultA = $resultA;
  }

  /**
   * Set resultB.
   *
   * @param {int} resultB the value to set.
   */
  public function setResultB($resultB)
  {
    $this->resultB = $resultB;
  }

  /**
   * Set round.
   *
   * @param {int} round the value to set.
   */
  public function setRound($round)
  {
    $this->round = $round;
  }

  /**
   * @param mixed $startTime
   */
  public function setStartTime($startTime)
  {
    $this->startTime = $startTime;
  }

  /**
   * Set table.
   *
   * @param {int} table the value to set.
   */
  public function setTable($table)
  {
    $this->table = $table;
  }

  /**
   * Set type.
   *
   * @param {int} type the value to set.
   */
  public function setType($type)
  {
    $this->type = $type;
  }
//</editor-fold desc="Public Methods">

}
