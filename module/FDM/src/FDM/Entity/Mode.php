<?php
namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;


/**
 * @ORM\Entity
 * @ORM\Table(name="mode")
 * @Form\Name("mode")
 * @Form\Type("fieldset")
 */
class Mode
{
//<editor-fold desc="Fields">
  /**
   * @ORM\Column(type="integer")
   * @Form\Exclude
   * @var bool
   */
  protected $groupNumber;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @Form\Exclude
   * @var string
   */
  protected $id;

  /**
   * @ORM\ManyToOne(targetEntity="Phase", inversedBy="modes")
   * @Form\Exclude
   * @var Phase
   */
  protected $phase;

  /**
   * @ORM\Column(type="string")
   * @Form\Exclude
   * @var string
   */
  protected $rankingServiceName;

  /**
   * @ORM\Column(type="string")
   * @Form\Exclude
   * @var string
   */
  protected $serviceName;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $template;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->template = true;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function __clone()
  {
    $this->id = null;
  }

  /**
   * Get groupNumber.
   *
   * @return {int} groupNumber.
   */
  public function getGroupNumber()
  {
    return $this->groupNumber;
  }

  /**
   * Get id.
   *
   * @return {string} id.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get phase.
   *
   * @return phase.
   */
  public function getPhase()
  {
    return $this->phase;
  }

  /**
   * Get rankingServiceName.
   *
   * @return {string} rankingServiceName.
   */
  public function getRankingServiceName()
  {
    return $this->rankingServiceName;
  }

  /**
   * Get serviceName.
   *
   * @return {string} serviceName.
   */
  public function getServiceName()
  {
    return $this->serviceName;
  }

  /**
   * Get template.
   *
   * @return {bool} template.
   */
  public function isTemplate()
  {
    return $this->template;
  }

  /**
   * Set groupNumber.
   *
   * @param {int} groupNumber the value to set.
   */
  public function setGroupNumber($groupNumber)
  {
    $this->groupNumber = $groupNumber;
  }

  /**
   * Set phase.
   *
   * @param {Phase} phase the value to set.
   */
  public function setPhase($phase)
  {
    $this->phase = $phase;
    $phase->addMode($this);
  }

  /**
   * Set rankingServiceName.
   *
   * @param {string} rankingServiceName the value to set.
   */
  public function setRankingServiceName($rankingServiceName)
  {
    $this->rankingServiceName = $rankingServiceName;
  }

  /**
   * Set serviceName.
   *
   * @param {string} serviceName the value to set.
   */
  public function setServiceName($serviceName)
  {
    $this->serviceName = $serviceName;
  }

  /**
   * Set template.
   *
   * @param {bool} template the value to set.
   */
  public function setTemplate($template)
  {
    $this->template = $template;
  }
//</editor-fold desc="Public Methods">

}
