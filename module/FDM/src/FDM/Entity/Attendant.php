<?php
namespace FDM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="attendant")
 */
class Attendant
{
//<editor-fold desc="Constants">
  const STATE_PAUSING = 2;

  const STATE_PLAYING = 1;

  const STATE_SURRENDERED = 3;

//</editor-fold desc="Constants">

//<editor-fold desc="Fields">
  /**
   * @ORM\Column(type="date", nullable=true)
   * @var \DateTime
   */
  protected $birthday;

  /**
   * @ORM\Column(type="string", nullable=true)
   * @var string
   */
  protected $displayName;

  /**
   * @ORM\Column(type="string", nullable=true)
   * @var string
   */
  protected $firstName;

  /**
   * @ORM\Column(type="string", nullable=true)
   * @var string
   */
  protected $generatedDisplayName;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @var string
   */
  protected $id;

  /**
   * @ORM\Column(type="string", nullable=true)
   * @var string
   */
  protected $lastName;

  /**
   * @ORM\Column(type="boolean")
   * @var bool
   */
  protected $payed;

  /**
   * @ORM\ManyToOne(targetEntity="FDM\Entity\Player")
   * @var Player
   */
  protected $player;

  /**
   * @ORM\Column(type="string", nullable=false)
   * @var string
   */
  protected $spelledFirstName = "";

  /**
   * @ORM\Column(type="string", nullable=false)
   * @var string
   */
  protected $spelledLastName = "";

  /**
   * @ORM\Column(type="integer", nullable=true)
   * @var int
   */
  protected $startNumber;

  /**
   * @ORM\Column(type="integer", nullable=true)
   * @var integer
   */
  protected $status;

  /**
   * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="attendants")
   * @var Tournament
   */
  protected $tournament;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->privileges = new ArrayCollection();
    $this->status = self::STATE_PLAYING;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * Get displayName.
   *
   * @return {string} displayName.
   */
  public function getDisplayName()
  {
    return $this->displayName;
  }

  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
   * Get generatedDisplayName.
   *
   * @return {string} generatedDisplayName.
   */
  public function getGeneratedDisplayName()
  {
    return $this->generatedDisplayName;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getLastName()
  {
    return $this->lastName;
  }

  /**
   * Get payed.
   *
   * @return {bool} payed.
   */
  public function getPayed()
  {
    return $this->payed;
  }

  public function getPlayer()
  {
    return $this->player;
  }

  /**
   * @return string
   */
  public function getSpelledFirstName(): string
  {
    return $this->spelledFirstName;
  }

  /**
   * @return string
   */
  public function getSpelledLastName(): string
  {
    return $this->spelledLastName;
  }

  public function getStartNumber()
  {
    return $this->startNumber;
  }

  /**
   * Get status.
   *
   * @return {int} status.
   */
  public function getStatus()
  {
    return $this->status;
  }

  public function getTournament()
  {
    return $this->tournament;
  }

  /**
   * Set displayName.
   *
   * @param {string} displayName the value to set.
   */
  public function setDisplayName($displayName)
  {
    $this->displayName = $displayName;
  }

  /**
   * Set generatedDisplayName.
   *
   * @param {string} generatedDisplayName the value to set.
   */
  public function setGeneratedDisplayName($generatedDisplayName)
  {
    $this->generatedDisplayName = $generatedDisplayName;
  }

  /**
   * Set payed.
   *
   * @param {bool} payed the value to set.
   */
  public function setPayed($payed)
  {
    $this->payed = $payed;
  }

  public function setPlayer($player)
  {
    $this->player = $player;
  }

  /**
   * @param string $spelledFirstName
   */
  public function setSpelledFirstName(string $spelledFirstName)
  {
    $this->spelledFirstName = $spelledFirstName;
  }

  /**
   * @param string $spelledLastName
   */
  public function setSpelledLastName(string $spelledLastName)
  {
    $this->spelledLastName = $spelledLastName;
  }

  public function setStartNumber($value)
  {
    $this->startNumber = $value;
  }

  /**
   * Set status.
   *
   * @param {int} status the value to set.
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  public function setTournament($tournament)
  {
    $this->tournament = $tournament;
    $tournament->getAttendants()->add($this);
  }

  public function setValue($key, $value)
  {
    $this->$key = $value;
  }
//</editor-fold desc="Public Methods">
}
