<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/28/16
 * Time: 1:13 PM
 */

namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tournamentrankingentry")
 */
class TournamentRankingEntry
{
  use SubClassData;

//<editor-fold desc="Fields">
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @var string
   */
  protected $id;

  /**
   * @ORM\ManyToOne(targetEntity="Player")
   * @var Player
   */
  protected $player;

  /**
   * @ORM\Column(type="float")
   * @var float
   */
  protected $points;

  /**
   * @ORM\Column(type="integer")
   * @var int
   */
  protected $rank;

  /**
   * @ORM\ManyToOne(targetEntity="TournamentRankingInstance", inversedBy="entries")
   * @var TournamentRankingInstance
   */
  protected $tournamentRankingInstance;

  /**
   * @ORM\Column(type="integer")
   * @var int
   */
  protected $tournaments;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($instance, $player, $keys = [])
  {
    $this->tournamentRankingInstance = $instance;
    $this->initSubClassData($keys);
    $this->player = $player;
    $this->rank = 0;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function __clone()
  {
    $this->id = null;
  }

  /**
   * @return string
   */
  public function getId(): string
  {
    return $this->id;
  }

  /**
   * @return Player
   */
  public function getPlayer(): Player
  {
    return $this->player;
  }

  /**
   * @return float
   */
  public function getPoints(): float
  {
    return $this->points;
  }

  /**
   * @return int
   */
  public function getRank(): int
  {
    return $this->rank;
  }

  /**
   * @return TournamentRankingInstance
   */
  public function getTournamentRankingInstance(): TournamentRankingInstance
  {
    return $this->tournamentRankingInstance;
  }

  /**
   * @return int
   */
  public function getTournaments(): int
  {
    return $this->tournaments;
  }

  /**
   * @param float $points
   */
  public function setPoints(float $points)
  {
    $this->points = $points;
  }

  /**
   * @param int $rank
   */
  public function setRank(int $rank)
  {
    $this->rank = $rank;
  }

  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   */
  public function setTournamentRankingInstance(TournamentRankingInstance $tournamentRankingInstance)
  {
    $this->tournamentRankingInstance = $tournamentRankingInstance;
  }

  /**
   * @param int $tournaments
   */
  public function setTournaments(int $tournaments)
  {
    $this->tournaments = $tournaments;
  }
//</editor-fold desc="Public Methods">


}