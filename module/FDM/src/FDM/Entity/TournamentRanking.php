<?php

namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * @ORM\Entity
 * @ORM\Table(name="tournamentranking")
 */
class TournamentRanking
{
  use SubClassData;

//<editor-fold desc="Fields">
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @var string
   */
  protected $id;

  /**
   * @ORM\Column(type="string")
   * @Form\Options({"label":"Name"})
   * @var string
   */
  protected $name;

  /**
   * @ORM\Column(type="string")
   * @Form\Exclude
   * @var string
   */
  protected $serviceName;

  /**
   * @ORM\Column(type="string")
   * @Form\Exclude
   * @var string
   */
  protected $timeColumn;

  /**
   * @ORM\OneToMany(
   *     targetEntity="TournamentRankingInstance",
   *     mappedBy="tournamentRanking"
   * )
   * @var TournamentRankingInstance[]
   */
  private $instances;

  /**
   * @ORM\ManyToMany(
   *     targetEntity="Tournament",
   *     mappedBy="tournamentRankings"
   * )
   * @var Tournament[]
   */
  private $tournaments;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($keys)
  {
    $this->initSubClassData($keys);
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * @return string
   */
  public function getId(): string
  {
    return $this->id;
  }

  /**
   * @return TournamentRankingInstance[]
   */
  public function getInstances()
  {
    return $this->instances;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getServiceName(): string
  {
    return $this->serviceName;
  }

  /**
   * @return string
   */
  public function getTimeColumn()
  {
    return $this->timeColumn;
  }

  /**
   * @return Tournament[]
   */
  public function getTournaments()
  {
    return $this->tournaments;
  }
//</editor-fold desc="Public Methods">
}
