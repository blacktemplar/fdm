<?php
namespace FDM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;


/**
 * @ORM\Entity
 * @ORM\Table(name="groups")
 * Name is groups to avoid problems with keyword group
 */
class Group
{
//<editor-fold desc="Fields">
  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $finished;

  /**
   * @ORM\OneToMany(
   *     targetEntity="Game",
   *     mappedBy="group",
   *     cascade={"remove"},
   *     orphanRemoval=true,
   * )
   * @var Game[]
   */
  protected $games;

  /**
   * @ORM\Column(type="integer")
   * @Form\Exclude
   * @var int
   */
  protected $groupNumber;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @Form\Exclude
   * @var string
   */
  protected $id;

  /**
   * @ORM\ManyToOne(
   *     targetEntity="Mode",
   * )
   * @var Mode
   */
  protected $mode;

  /**
   * @ORM\Column(type="string", nullable=true)
   * @Form\Options({"label":"name"})
   * @var string
   */
  protected $name;

  /**
   * @ORM\ManyToOne(targetEntity="Phase", inversedBy="groups")
   * @Form\Exclude
   * @var Phase
   */
  protected $phase;

  /**
   * @ORM\OneToMany(
   *     targetEntity="Ranking",
   *     mappedBy="group",
   *     indexBy="rank",
   * )
   * @ORM\OrderBy({"rank" = "ASC"})
   * @var ArrayCollection
   */
  protected $rankings;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $started;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->rankings = new ArrayCollection();
    $this->games = new ArrayCollection();
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * Get games.
   *
   * @return {Game[]} games.
   */
  public function getGames()
  {
    return $this->games;
  }

  /**
   * Get games by type.
   *
   * @return Game[] games.
   */
  public function getGamesByType($type)
  {
    $res = [];
    foreach ($this->games as $game) {
      if ($game->getType() == $type) {
        $res[] = $game;
      }
    }
    return $res;
  }

  /**
   * Get groupNumber.
   *
   * @return {int} groupNumber.
   */
  public function getGroupNumber()
  {
    return $this->groupNumber;
  }

  /**
   * Get id.
   *
   * @return {string} id.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get mode.
   *
   * @return {Mode} mode.
   */
  public function getMode()
  {
    return $this->mode;
  }

  /**
   * Get name.
   *
   * @return {string} name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get phase.
   *
   * @return phase.
   */
  public function getPhase()
  {
    return $this->phase;
  }

  /**
   * Get rankings.
   *
   * @return {Ranking[]} rankings.
   */
  public function getRankings()
  {
    return $this->rankings;
  }

  /**
   * Get finished.
   *
   * @return {bool} finished.
   */
  public function isFinished()
  {
    return $this->finished;
  }

  /**
   * Get started.
   *
   * @return {bool} started.
   */
  public function isStarted()
  {
    return $this->started;
  }

  /**
   * Set finished.
   *
   * @param {bool} finished the value to set.
   */
  public function setFinished($finished)
  {
    $this->finished = $finished;
  }

  /**
   * Set groupNumber.
   *
   * @param {int} groupNumber the value to set.
   */
  public function setGroupNumber($groupNumber)
  {
    $this->groupNumber = $groupNumber;
  }

  /**
   * Set mode.
   *
   * @param {Mode} mode the value to set.
   */
  public function setMode($mode)
  {
    $this->mode = $mode;
  }

  /**
   * Set name.
   *
   * @param {string} name the value to set.
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Set phase.
   *
   * @param {Phase} phase the value to set.
   */
  public function setPhase($phase)
  {
    $this->phase = $phase;
    $phase->getGroups()->add($this);
  }

  /**
   * Set rankings.
   *
   * @param {Doctrine\Common\Collections\ArrayCollection} rankings the value to set.
   */
  public function setRankings($rankings)
  {
    $this->rankings = $rankings;
  }

  /**
   * Set started.
   *
   * @param {bool} started the value to set.
   */
  public function setStarted($started)
  {
    $this->started = $started;
  }
//</editor-fold desc="Public Methods">


}
