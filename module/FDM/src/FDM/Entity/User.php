<?php

namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="myuser")
 */
class User extends \ZfcUser\Entity\User
{
//<editor-fold desc="Constants">
  const TYPE_ADMIN = 2;

  const TYPE_NOT_LOGGED_IN = 3;

  const TYPE_PLAYER = 0;

  const TYPE_TOURNAMENT_MANAGER = 1;

//</editor-fold desc="Constants">

//<editor-fold desc="Fields">
  /**
   * @ORM\OneToOne(targetEntity="Player", inversedBy="user")
   * @var Player
   */
  protected $player;

  /**
   * @ORM\Column(type="integer")
   * @var int
   */
  protected $type;

//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->type = self::TYPE_PLAYER;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * @return int
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Is admin.
   *
   * @return {bool} admin.
   */
  public function isAdmin()
  {
    return $this->type == self::TYPE_ADMIN;
  }

  /**
   * @param int $type
   */
  public function setType($type)
  {
    $this->type = $type;
  }
//</editor-fold desc="Public Methods">
}
