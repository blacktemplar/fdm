<?php
namespace FDM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="player")
 */
class Player
{
//<editor-fold desc="Fields">
  /**
   * @ORM\Column(type="date", nullable=true)
   * @var \DateTime
   */
  protected $birthday;

  /**
   * @ORM\Column(type="string", nullable=true)
   * @var string
   */
  protected $firstName;

  /**
   * @ORM\Column(type="string", nullable=false)
   * @var string
   */
  protected $gender = "";

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @var string
   */
  protected $id;

  /**
   * @ORM\Column(type="string", nullable=true)
   * @var string
   */
  protected $lastName;

  /**
   * @ORM\Column(type="string", nullable=false)
   * @var string
   */
  protected $spelledFirstName = "";

  /**
   * @ORM\Column(type="string", nullable=false)
   * @var string
   */
  protected $spelledLastName = "";

  /**
   * @ORM\OneToOne(targetEntity="User", mappedBy="player")
   * @var User
   */
  protected $user;

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  public function getBirthday()
  {
    return $this->birthday;
  }

  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
   * @return string
   */
  public function getGender()
  {
    return $this->gender;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getLastName()
  {
    return $this->lastName;
  }

  /**
   * @return string
   */
  public function getSpelledFirstName(): string
  {
    return $this->spelledFirstName;
  }

  /**
   * @return string
   */
  public function getSpelledLastName(): string
  {
    return $this->spelledLastName;
  }

  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;
  }

  /**
   * @param string $gender
   */
  public function setGender($gender)
  {
    $this->gender = $gender;
  }

  public function setLastName($lastName)
  {
    $this->lastName = $lastName;
  }

  /**
   * @param string $spelledFirstName
   */
  public function setSpelledFirstName(string $spelledFirstName)
  {
    $this->spelledFirstName = $spelledFirstName;
  }

  /**
   * @param string $spelledLastName
   */
  public function setSpelledLastName(string $spelledLastName)
  {
    $this->spelledLastName = $spelledLastName;
  }
//</editor-fold desc="Public Methods">

}
