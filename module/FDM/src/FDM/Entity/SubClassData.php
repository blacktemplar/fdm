<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/4/16
 * Time: 10:10 PM
 */

namespace FDM\Entity;

use Zend\Form\Annotation as Form;

trait SubClassData
{
//<editor-fold desc="Fields">
  /**
   * @var array
   * @ORM\Column(type="json_array")
   * @Form\Exclude
   */
  private $subClassData;

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  public function __call($name, $arguments)
  {
    if (substr($name, 0, 3) === "get") {
      return $this->getProperty(substr($name, 3));
    } else if (substr($name, 0, 2) === "is") {
      return $this->getProperty(substr($name, 2));
    } else if (substr($name, 0, 3) === "set" && count($arguments) == 1) {
      $this->setProperty(substr($name, 3), $arguments[0]);
      return;
    }
    $trace = debug_backtrace();
    trigger_error(
      'Undefined call via __call(): ' . $name .
      ' in ' . $trace[0]['file'] .
      ' on line ' . $trace[0]['line'],
      E_USER_NOTICE);
  }

  public function addPropertyIfNotExistent($name, $default)
  {
    if (!array_key_exists(strtolower($name), $this->subClassData)) {
      $this->subClassData[strtolower($name)] = $default;
    }
  }

  public function has_property($property)
  {
    return array_key_exists(strtolower($property), $this->subClassData) || property_exists($this, $property);
  }

  public function initSubClassData($keys)
  {
    $this->subClassData = [];
    foreach ($keys as $key) {
      $this->subClassData[strtolower($key)] = null;
    }
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function getProperty($name)
  {
    if (!array_key_exists(strtolower($name), $this->subClassData)) {
      $trace = debug_backtrace();
      trigger_error(
        'Undefined property via getProperty(): ' . $name .
        ' in ' . $trace[0]['file'] .
        ' on line ' . $trace[0]['line'],
        E_USER_NOTICE);
      return null;
    }
    return $this->subClassData[strtolower($name)];
  }

  private function setProperty($name, $value)
  {
    if (!array_key_exists(strtolower($name), $this->subClassData)) {
      $trace = debug_backtrace();
      trigger_error(
        'Undefined property via setProperty(): ' . $name .
        ' in ' . $trace[0]['file'] .
        ' on line ' . $trace[0]['line'],
        E_USER_NOTICE);
      return;
    }
    $this->subClassData[strtolower($name)] = $value;
  }
//</editor-fold desc="Private Methods">
}