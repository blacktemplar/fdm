<?php
namespace FDM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ranking")
 */
class Ranking
{
  use SubClassData;

//<editor-fold desc="Fields">
  /**
   * @ORM\ManyToMany(
   *     targetEntity="Attendant",
   * )
   * @ORM\JoinTable(name="relation__rankingattendants")
   * @var Attendant[]
   */
  protected $attendants;

  /**
   * @ORM\column(type="integer")
   * @var int
   */
  protected $displayRank;

  /**
   * @ORM\ManyToMany(
   *     targetEntity="Game",
   *     mappedBy="teamA",
   * )
   * @ORM\JoinTable(name="relation__gamerankings__teamA")
   * @var Game[]
   */
  protected $gamesAsA;

  /**
   * @ORM\ManyToMany(
   *     targetEntity="Game",
   *     mappedBy="teamB",
   * )
   * @ORM\JoinTable(name="relation__gamerankings__teamB")
   * @var Game[]
   */
  protected $gamesAsB;

  /**
   * @ORM\ManyToOne(
   *     targetEntity="Group",
   *     inversedBy="rankings"
   * )
   * @var Group
   */
  protected $group;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @var string
   */
  protected $id;

  /**
   * @ORM\column(type="integer")
   * @var int
   */
  protected $phaseStartRank;

  /**
   * @ORM\column(type="integer")
   * @var int
   */
  protected $rank;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($keys)
  {
    $this->template = true;
    $this->phase = 0;
    $this->attendants = new ArrayCollection();
    $this->gamesAsA = new ArrayCollection();
    $this->gamesAsB = new ArrayCollection();
    $this->initSubClassData($keys);
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * Get attendants.
   *
   * @return {Attendant[]} attendants.
   */
  public function getAttendants()
  {
    return $this->attendants;
  }

  /**
   * Get displayRank.
   *
   * @return {int} displayRank.
   */
  public function getDisplayRank()
  {
    return $this->displayRank;
  }

  public function getGames()
  {
    return array_merge(
      $this->getGamesAsA()->toArray(),
      $this->getGamesAsB()->toArray()
    );
  }

  /**
   * Get gamesAsA.
   *
   * @return {Game[]} gamesAsA.
   */
  public function getGamesAsA()
  {
    return $this->gamesAsA;
  }

  /**
   * Get gamesAsB.
   *
   * @return {Game[]} gamesAsB.
   */
  public function getGamesAsB()
  {
    return $this->gamesAsB;
  }

  /**
   * Get group.
   *
   * @return {Group} group.
   */
  public function getGroup()
  {
    return $this->group;
  }

  /**
   * Get id.
   *
   * @return {string} id.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get phaseStartRank.
   *
   * @return {int} phaseStartRank.
   */
  public function getPhaseStartRank()
  {
    return $this->phaseStartRank;
  }

  /**
   * Get rank.
   *
   * @return {int} rank.
   */
  public function getRank()
  {
    return $this->rank;
  }

  /**
   * Set attendants.
   *
   * @param {Attendant[]} attendants the value to set.
   */
  public function setAttendants($attendants)
  {
    $this->attendants = $attendants;
  }

  /**
   * Set displayRank.
   *
   * @param {int} displayRank the value to set.
   */
  public function setDisplayRank($displayRank)
  {
    $this->displayRank = $displayRank;
  }

  /**
   * Set group.
   *
   * @param {Group} group the value to set.
   */
  public function setGroup($group)
  {
    $this->group = $group;
    $group->getRankings()->add($this);
  }

  /**
   * Set phaseStartRank.
   *
   * @param {int} phaseStartRank the value to set.
   */
  public function setPhaseStartRank($phaseStartRank)
  {
    $this->phaseStartRank = $phaseStartRank;
  }

  /**
   * Set rank.
   *
   * @param {int} rank the value to set.
   */
  public function setRank($rank)
  {
    $this->rank = $rank;
  }
//</editor-fold desc="Public Methods">

}
