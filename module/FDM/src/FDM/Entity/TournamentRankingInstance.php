<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/28/16
 * Time: 12:38 PM
 */

namespace FDM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tournamentrankinginstance")
 */
class TournamentRankingInstance
{
//<editor-fold desc="Fields">
  /**
   * @ORM\Column(type="boolean")
   * @var bool
   */
  protected $current;

  /**
   * @ORM\OneToMany(targetEntity="TournamentRankingEntry", mappedBy="tournamentRankingInstance")
   * @var TournamentRankingEntry[]
   */
  protected $entries;

  /**
   * @ORM\Column(type="boolean")
   * @var bool
   */
  protected $fixed;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @var string
   */
  protected $id;

  /**
   * @ORM\Column(type="datetime")
   * @var \DateTime
   */
  protected $lastEntryTime;

  /**
   * @ORM\ManyToOne(targetEntity="TournamentRanking", inversedBy="instances")
   * @var TournamentRanking
   */
  protected $tournamentRanking;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($tournamentRanking, $current = false)
  {
    $this->entries = new ArrayCollection();
    $this->tournamentRanking = $tournamentRanking;
    $this->current = $current;
    $this->fixed = false;
    $this->lastEntryTime = new \DateTime('2000-01-01');
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * @return TournamentRankingEntry[]
   */
  public function getEntries()
  {
    return $this->entries;
  }

  /**
   * @return \DateTime
   */
  public function getLastEntryTime()
  {
    return $this->lastEntryTime;
  }

  /**
   * @return TournamentRanking
   */
  public function getTournamentRanking(): TournamentRanking
  {
    return $this->tournamentRanking;
  }

  /**
   * @return bool
   */
  public function isCurrent(): bool
  {
    return $this->current;
  }

  /**
   * @return bool
   */
  public function isFixed()
  {
    return $this->fixed;
  }

  /**
   * @param \DateTime $lastEntryTime
   */
  public function setLastEntryTime(\DateTime $lastEntryTime)
  {
    $this->lastEntryTime = $lastEntryTime;
  }
//</editor-fold desc="Public Methods">


}