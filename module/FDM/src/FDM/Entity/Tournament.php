<?php
namespace FDM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * @ORM\Entity
 * @ORM\Table(name="tournament")
 * @Form\Name("tournament")
 */
class Tournament
{

  /**
   * Hook timestampable behavior
   * updates createdAt, updatedAt fields
   */
  use TimestampableEntity;

//<editor-fold desc="Fields">
  /**
   * @ORM\Column(type="boolean")
   * @Form\Type("Zend\Form\Element\Checkbox")
   * @Form\Options({"label":"Automatically Start Games"})
   * @var bool
   */
  protected $automaticallyStartGames;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $deleted;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   * @Form\Exclude
   * @var \DateTime
   */
  protected $end;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $finished;

  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="CUSTOM")
   * @ORM\CustomIdGenerator(class="FDM\Tools\IdGenerator")
   * @ORM\Column(type="guid")
   * @Form\Exclude
   * @var string
   */
  protected $id;

  /**
   * @ORM\Column(type="string")
   * @Form\Options({"label":"Name"})
   * @var string
   */
  protected $name;

  /**
   * @ORM\ManyToOne(targetEntity="Tournament")
   * @Form\Exclude
   * @var Tournament
   */
  protected $parent;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Type("Zend\Form\Element\Checkbox")
   * @Form\Options({"label":"Public"})
   * @var bool
   */
  protected $public;

  /**
   * @ORM\Column(type="datetime", nullable=false)
   * @Form\Type("Zend\Form\Element\DateTime")
   * @Form\Options({"label":"Start Time", "format":"Y-m-d H:i"})
   * @var \DateTime
   */
  protected $start;

  /**
   * @ORM\Column(type="text")
   * @Form\Type("Zend\Form\Element\Textarea")
   * @Form\Options({"label":"Start Text"})
   * @var string
   */
  protected $startText;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $started;

  /**
   * @ORM\Column(type="boolean")
   * @Form\Exclude
   * @var bool
   */
  protected $template;

  /**
   * @ORM\OneToMany(
   *     targetEntity="Attendant",
   *     mappedBy="tournament",
   *     indexBy="startNumber",
   * )
   * @ORM\OrderBy({"startNumber" = "ASC"})
   * @Form\Exclude
   * @var Attendant[]
   */
  private $attendants;

  /**
   * @ORM\OneToMany(
   *     targetEntity="Phase",
   *     mappedBy="tournament",
   * )
   * @ORM\OrderBy({"phaseNumber" = "ASC"})
   * @Form\Exclude
   * @var Phase[]
   */
  private $phases;

  /**
   * @ORM\OneToMany(
   *     targetEntity="TournamentPrivileges",
   *     mappedBy="tournament"
   * )
   * @Form\Exclude
   * @var TournamentPrivileges[]
   */
  private $privileges;

  /**
   * @ORM\ManyToMany(
   *     targetEntity="TournamentRanking",
   *     inversedBy="tournaments"
   * )
   * @ORM\JoinTable(name="relation__tournamentrankings")
   * @Form\Exclude
   * @var TournamentRanking[]
   */
  private $tournamentRankings;

  /**
   * @ORM\Column(type="string")
   * @Form\Exclude
   * @var string
   */
  //protected $startText;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->privileges = new ArrayCollection();
    $this->attendants = new ArrayCollection();
    $this->phases = new ArrayCollection();
    $this->tournamentRankings = new ArrayCollection();
    $this->end = null;
    $this->started = false;
    $this->template = false;
    $this->deleted = false;
    $this->finished = false;
    $this->automaticallyStartGames = true;
    $this->startText = "";
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * Get attendants.
   *
   * @return Attendant[] attendants.
   */
  public function getAttendants()
  {
    return $this->attendants;
  }

  /**
   * @return mixed
   */
  public function getAutomaticallyStartGames()
  {
    return $this->automaticallyStartGames;
  }

  /**
   * gets the end time
   *
   * @return \DateTime the end time
   */
  public function getEnd()
  {
    return $this->end;
  }

  public function getId()
  {
    return $this->id;
  }

  /**
   * Get name.
   *
   * @return string name.
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get parent.
   *
   * @return Tournament parent.
   */
  public function getParent()
  {
    return $this->parent;
  }

  /**
   * Get phases.
   *
   * @return Phase[]|ArrayCollection phases.
   */
  public function getPhases()
  {
    return $this->phases;
  }

  /**
   * @param bool $template specifies if we want all template phases or all non template phases
   * @return Phase[] an array containing all template or non template phases
   */
  public function getPhasesByTemplate($template)
  {
    $res = [];
    foreach ($this->phases as $phase) {
      if ($phase->isTemplate() == $template) {
        $res[$phase->getPhaseNumber()] = $phase;
      }
    }
    ksort($res);
    return $res;
  }

  /**
   * Get privileges.
   *
   * @return TournamentPrivileges[] privileges.
   */
  public function getPrivileges()
  {
    return $this->privileges;
  }

  /**
   * Get start.
   *
   * @return \DateTime start.
   */
  public function getStart()
  {
    return $this->start;
  }

  /**
   * @return string
   */
  public function getStartText()
  {
    return $this->startText;
  }

  /**
   * Get template.
   *
   * @return bool template.
   */
  public function getTemplate()
  {
    return $this->template;
  }

  /**
   * @return TournamentRanking[] | ArrayCollection
   */
  public function getTournamentRankings()
  {
    return $this->tournamentRankings;
  }

  /**
   * Get deleted.
   *
   * @return bool deleted.
   */
  public function isDeleted()
  {
    return $this->deleted;
  }

  /**
   * Checks if finished.
   *
   * @return bool finished.
   */
  public function isFinished()
  {
    return $this->finished;
  }

  /**
   * Get public.
   * @return bool public.
   */
  public function isPublic()
  {
    return $this->public;
  }

  /**
   * Checks if started
   * @return bool started.
   */
  public function isStarted()
  {
    return $this->started;
  }

  /**
   * Set attendants.
   *
   * @param {ArrayCollection} attendants the value to set.
   */
  public function setAttendants($attendants)
  {
    $this->attendants = $attendants;
  }

  /**
   * @param mixed $automaticallyStartGames
   */
  public function setAutomaticallyStartGames($automaticallyStartGames)
  {
    $this->automaticallyStartGames = $automaticallyStartGames;
  }

  /**
   * Set deleted.
   *
   * @param {bool} deleted the value to set.
   */
  public function setDeleted($deleted)
  {
    $this->deleted = $deleted;
  }

  /**
   * Sets the end time
   * @param \DateTime $end the end time
   */
  public function setEnd($end)
  {
    $this->end = $end;
  }

  /**
   * Set finished.
   *
   * @param bool $finished the value to set
   */
  public function setFinished($finished)
  {
    $this->finished = $finished;
  }

  /**
   * Set name.
   *
   * @param {string} name the value to set.
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Set parent.
   *
   * @param {Tournament} parent the value to set.
   */
  public function setParent($parent)
  {
    $this->parent = $parent;
  }

  /**
   * Set phases.
   *
   * @param {ArrayCollection} phases the value to set.
   */
  public function setPhases($phases)
  {
    $this->phases = $phases;
  }

  /**
   * Set privileges.
   *
   * @param {ArrayCollection} privileges the value to set.
   */
  public function setPrivileges($privileges)
  {
    $this->privileges = $privileges;
  }

  /**
   * Set public.
   *
   * @param {bool} public the value to set.
   */
  public function setPublic($public)
  {
    $this->public = $public;
  }

  /**
   * Set start.
   *
   * @param {\DateTime} start the value to set.
   */
  public function setStart($start)
  {
    $this->start = $start;
  }

  /**
   * @param string $startText
   */
  public function setStartText($startText)
  {
    $this->startText = $startText;
  }

  /**
   * Set started.
   *
   * @param bool $started the value to set.
   */
  public function setStarted($started)
  {
    $this->started = $started;
  }

  /**
   * Set template.
   *
   * @param {bool} template the value to set.
   */
  public function setTemplate($template)
  {
    $this->template = $template;
  }
//</editor-fold desc="Public Methods">

}
