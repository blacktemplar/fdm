<?php

namespace FDM\Service;

use FDM\Entity\AttendanceStrategy;

interface AttendanceStrategyServiceInterface
{
//<editor-fold desc="Public Methods">
  /**
   * @param {Ranking[]} oldRankings       collection of old rankings
   * @param {AbstractMode[]} modeService  array of the services for the new groups
   *                                      (if all groups use the same only the array
   *                                      contains only one element)
   * @returns {Ranking[]}                 collection of new rankings
   * @returns {Ranking[]}                 collection of new rankings
   */
  public function computeNewRankings($oldRankings, $modeServices);

  /**
   * sets the entity object mode for this service
   */
  public function setAttendanceStrategy(
    AttendanceStrategy $attendanceStrategy
  );
//</editor-fold desc="Public Methods">
}
