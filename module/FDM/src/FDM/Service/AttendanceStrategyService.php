<?php

namespace FDM\Service;

use Zend\ServiceManager\ServiceLocatorInterface;

class AttendanceStrategyService extends AttendanceStrategyServiceAbstract
  implements AttendanceStrategyServiceInterface
{
//<editor-fold desc="Fields">
  private $sl;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(ServiceLocatorInterface $sl)
  {
    $this->sl = $sl;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function computeNewRankings($oldRankings, $modeServices)
  {
    if (count($modeServices) > 1) {
      //TODO
      throw new \Exception(
        "The usage of multiple modes"
        . " for one phase is not implemented (yet)."
      );
    }
    if (count($modeServices) == 0) {
      throw new \Exception(
        "No mode service for this mode available!"
      );
    }

    $groups = [];
    $numericals = [];
    foreach ($oldRankings as $group => $oRankings) {
      $numericals[$group] = [];
      foreach ($oRankings as $ranking) {
        $numericals[$group][] = $ranking;
      }
      ksort($numericals[$group]);
      $groups[] = $group;
    }

    shuffle($groups);

    $this->initializeGroupOrder($groups, $oldRankings);

    $rank = 1;
    $found = true;
    $numerical = [];
    while ($found) {
      $found = false;
      foreach ($groups as $g) {
        if (array_key_exists($rank - 1, $numericals[$g])) {
          $found = true;
          $numerical[] = $numericals[$g][$rank - 1];
        }
      }
      $this->reorderGroups($groups, $rank);
      $rank++;
    }


    if ($this->aStrat->isPreShuffle()) {
      shuffle($numerical);
    }


    //merge and fix teams

    $newRankings = [];
    $merge = $this->aStrat->getMerge();
    for ($i = 0; $i <= count($numerical) - $merge; $i += $merge) {
      $new = $modeServices[0]->getNewRanking();
      for ($j = 0; $j < $merge; $j++) {
        foreach ($numerical[$i + $j]->getAttendants() as $attendant) {
          $new->getAttendants()->add($attendant);
        }
      }
      $newRankings[] = $new;
    }

    if ($this->aStrat->isPreGroupingShuffle()) {
      shuffle($newRankings);
    }

    //create groups
    $rankings = [];
    for ($i = 0; $i < $this->aStrat->getNumGroups(); $i++) {
      $rankings[$i] = [];
      $tmp = [];
      for ($j = $i; $j < count($newRankings);
           $j += $this->aStrat->getNumGroups()) {
        $tmp[] = $newRankings[$j];
      }
      if ($this->aStrat->getMaxPlayersPerGroup() > 0 &&
        $this->aStrat->getMaxPlayersPerGroup() < count($tmp)
      ) {
        $tmp = array_slice(
          $tmp, 0, $this->aStrat->getMaxPlayersPerGroup()
        );
      }
      if ($this->aStrat->isPostGroupingShuffle()) {
        shuffle($tmp);
      }
      for ($j = 0; $j < count($tmp); $j++) {
        $tmp[$j]->setRank($j + 1);
        $tmp[$j]->setPhaseStartRank($j + 1);
        $rankings[$i][$j + 1] = $tmp[$j];
      }
    }
    return $rankings;
  }

  public function initializeGroupOrder(&$permutation, $oldRankings)
  {
    if (count($permutation) == 3) {
      //three groups check 2nd place
      $best = null;
      $mode = null;
      $rankingService = null;
      foreach ($oldRankings as $key => $oRankings) {

        $rankingsByRank = [];
        foreach ($oRankings as $ranking) {
          $rankingsByRank[$ranking->getRank()] = $ranking;
        }
        $tmpMode = $rankingsByRank[1]->getGroup()->getMode();

        assert($tmpMode != null);
        if ($tmpMode != $mode) {
          if ($mode != null) {
            //abort permuting!
            //cannot compare rankings between
            //different ranking services
            return;
          }
          $mode = $tmpMode;
          $rankingService =
            $this->sl->get($mode->getRankingServiceName());
          assert($rankingService != null);
        }
        if ($best === null || $rankingService->compareBetweenGroups(
            $rankingsByRank[2], $best
          ) < 0
        ) {
          $best = $rankingsByRank[2];
        }
      }
      $best = $best->getGroup()->getGroupNumber();
      if ($best != $permutation[1]) {
        if ($best == $permutation[0]) {
          $permutation[0] = $permutation[1];
          $permutation[1] = $best;
        } else if ($best == $permutation[2]) {
          $permutation[2] = $permutation[1];
          $permutation[1] = $best;
        }
      }
    }
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function getPermNumber($gRank)
  {
    $pot = 1;
    while ($pot <= $gRank) {
      if ($gRank % ($pot * 2) == $pot) {
        return $pot * 2;
      }
      $pot = $pot * 2;
    }
    return 0;
  }

  private function isPowerOfTwo($x, &$k)
  {
    if ($x == 1) {
      return true;
    }
    if ($x % 2 == 1) {
      return false;
    }
    $k++;
    return $this->isPowerOfTwo($x / 2, $k);
  }

  private function reorderGroups(&$permutation, $gRank)
  {
    $nGroups = count($permutation);
    $k = 0;
    if ($this->isPowerOfTwo($nGroups, $k)) {
      $num = $this->getPermNumber($gRank);
      if ($num == 0) {
        return;
      }
      $newPerm = [];
      if ($num > $nGroups) {
        $num = $nGroups;
      }
      for ($i = 0; $i < $nGroups / $num; $i++) {
        for ($k = 0; $k < $num; $k++) {
          $newPerm[] = $permutation[$nGroups - ($i + 1) * $num + $k];
        }
      }
      for ($i = 0; $i < $nGroups; $i++) {
        $permutation[$i] = $newPerm[$i];
      }
    } else if ($nGroups == 3) {
      $tmp = $permutation[0];
      if ($gRank == 1 || $gRank == 3) {
        $permutation[0] = $permutation[1];
        $permutation[1] = $permutation[2];
        $permutation[2] = $tmp;
      }
      if ($gRank == 2) {
        $permutation[0] = $permutation[2];
        $permutation[2] = $tmp;
      }
    } else if ($nGroups == 5) {
      $tmp = $permutation[0];
      if ($gRank == 1) {
        $permutation[0] = $permutation[1];
        $permutation[1] = $permutation[2];
        $permutation[2] = $permutation[3];
        $permutation[3] = $permutation[4];
        $permutation[4] = $tmp;
      } else if ($gRank == 2) {
        $permutation[0] = $permutation[2];
        $permutation[2] = $permutation[1];
        $permutation[1] = $tmp;
        $tmp = $permutation[3];
        $permutation[3] = $permutation[4];
        $permutation[4] = $tmp;
      } else if ($gRank == 3) {
        $permutation[0] = $permutation[2];
        $permutation[2] = $tmp;
        $tmp = $permutation[3];
        $permutation[3] = $permutation[4];
        $permutation[4] = $tmp;
      }
    }
  }
//</editor-fold desc="Private Methods">
}
