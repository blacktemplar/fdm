<?php

namespace FDM\Service;

use FDM\Entity\Group;
use FDM\Entity\Mode;

abstract class ModeServiceAbstract implements ModeServiceInterface
{
//<editor-fold desc="Fields">
  protected $mode;

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  public function getExtraStructures(Group $group)
  {
    return [];
  }

  /**
   * sets the entity object mode for this service
   */
  public function setMode(Mode $mode)
  {
    $this->mode = $mode;
  }
//</editor-fold desc="Public Methods">
}
