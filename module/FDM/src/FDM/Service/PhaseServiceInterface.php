<?php

namespace FDM\Service;


use FDM\Entity\Phase;

interface PhaseServiceInterface
{
//<editor-fold desc="Public Methods">
  /**
   * @param {Phase} phase the current phase
   * @returns {[]}        a structure array representing the structure of
   *                      all views the array hast the following structure
   *                      array(
   *                         array(
   *                             "id" => ? (unique over all levels)
   *                             "title" => ?
   *                             "controller" => ?
   *                             "action" => ?
   *                             "parameters" => array(...) action parameters
   *                             "children" => array(
   *                                 ...
   *                             )
   *                         )
   *                         ...
   *                      )
   */
  public function getContentStructure(Phase $phase);

  public function getFreeTables(Phase $phase);

  public function getGamesByType($types, $phase);

  public function phaseChangedCallback(Phase $phase);

  /**
   * @param {Phase} phase the phase to start
   * @returns             true if started successfully and false otherwise
   */
  public function startPhase(Phase $phase);

  /**
   * @param {Phase} phase the phase to undo start
   * @returns             true if started successfully and false otherwise
   */
  public function unStartPhase(Phase $phase);
//</editor-fold desc="Public Methods">
}
