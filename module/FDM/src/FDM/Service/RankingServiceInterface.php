<?php

namespace FDM\Service;

use FDM\Entity\Group;
use FDM\Entity\Ranking;

interface RankingServiceInterface
{
//<editor-fold desc="Public Methods">
  public function compareBetweenGroups(
    Ranking $r1, Ranking $r2);

  public function recalculateRankings(Group $group);
//</editor-fold desc="Public Methods">
}
