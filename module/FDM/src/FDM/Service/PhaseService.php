<?php

namespace FDM\Service;

use Doctrine\ORM\EntityManager;
use FDM\Entity\Game;
use FDM\Entity\Group;
use FDM\Entity\Phase;
use FDM\Entity\Ranking;
use FDM\View\Helper\NamesHelper;
use Zend\Mvc\I18n\Translator;
use Zend\ServiceManager\ServiceLocatorInterface;

class PhaseService implements PhaseServiceInterface
{
//<editor-fold desc="Fields">
  protected $em;

  protected $fs;

  protected $ls;

  protected $namesHelper;

  protected $sl;

  protected $speakService;

  protected $translator;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  /**
   *
   * @param em    EntityManager
   */
  public function __construct(
    EntityManager $em,
    FlushServiceInterface $fs,
    ServiceLocatorInterface $sl,
    Translator $translator,
    NamesHelper $namesHelper,
    LoadingService $ls,
    SpeakService $speakService
  )
  {
    $this->em = $em;
    $this->fs = $fs;
    $this->sl = $sl;
    $this->translator = $translator;
    $this->namesHelper = $namesHelper;
    $this->ls = $ls;
    $this->speakService = $speakService;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getContentStructure(Phase $phase)
  {
    $mode = $phase->getModesByTemplate(false)[0];
    $modeService = $this->sl->get($mode->getServiceName());
    $modeService->setMode($mode);
    $struct = [];
    $struct["settings"] = [
      "title" => "Settings",
      "type" => "element",
      "controller" => "FDM\Controller\Phase",
      "action" => "modeSettings",
      "parameters" => [
        "id" => $phase->getTournament()->getId(),
        "phase" => $phase->getId(),
      ],
    ];
    $struct["rankings"] = [
      "title" => "Rankings",
      "type" => "collection",
      "children" => [],
    ];
    foreach ($phase->getGroups() as $group) {
      $groupName = $this->namesHelper->__invoke($group);
      $struct["rankings"]["children"][$group->getGroupNumber()] = [
        "title" => $this->translator->translate("Rankings") .
          " " . $groupName,
        "type" => "element",
        "controller" => "FDM\Controller\Phase",
        "action" => "groupRankings",
        "parameters" => [
          "id" => $phase->getTournament()->getId(),
          "phase" => $phase->getId(),
          "group" => $group->getId(),
        ],
      ];
    }
    $struct["games"] = [
      "title" => "Games",
      "type" => "collection",
      "children" => [
        "scheduled" => [
          "title" => "Scheduled games",
          "type" => "element",
          "controller" => "FDM\Controller\Phase",
          "action" => "scheduledGames",
          "parameters" => [
            "id" => $phase->getTournament()->getId(),
            "phase" => $phase->getId(),
            "group" => $group->getId()
          ],
        ],
        "running" => [
          "title" => "Running games",
          "type" => "element",
          "controller" => "FDM\Controller\Phase",
          "action" => "runningGames",
          "parameters" => [
            "id" => $phase->getTournament()->getId(),
            "phase" => $phase->getId(),
            "group" => $group->getId(),
          ],
        ],
        "finished" => [
          "title" => "Finished games",
          "type" => "element",
          "controller" => "FDM\Controller\Phase",
          "action" => "finishedGames",
          "parameters" => [
            "id" => $phase->getTournament()->getId(),
            "phase" => $phase->getId(),
            "group" => $group->getId(),
          ],
        ],
      ],
    ];
    $extraStructures = [];
    foreach ($phase->getGroups() as $group) {
      $mode = $group->getMode();
      $modeService = $this->sl->get($mode->getServiceName());
      $modeService->setMode($mode);
      $tmp = $modeService->getExtraStructures($group);
      if (count($tmp) > 0) {
        $groupName = $group->getName();
        if (!$groupName) {
          $groupName = "Group " . $group->getGroupNumber();
        }
        $extraStructures[$groupName] = $tmp;
      }
    }
    if (count($extraStructures) > 0) {
      $struct["extra"] = [
        "title" => "Extras",
        "type" => "collection",
        "children" => $extraStructures,
      ];
    }
    return $struct;
  }

  public function getFreeTables(Phase $phase)
  {
    $freeTables = [];
    for ($i = 1; $i <= $phase->getNumTables(); $i++) {
      $freeTables[$i] = true;
    }
    foreach ($phase->getGroups() as $group) {
      foreach ($group->getGamesByType(Game::TYPE_SCHEDULED_FIX)
               as $game) {
        unset($freeTables[$game->getTable()]);
      }
      foreach ($group->getGamesByType(Game::TYPE_RUNNING) as $game) {
        unset($freeTables[$game->getTable()]);
      }
    }
    return $freeTables;
  }

  /**
   * @param int[] $types
   * @param Phase $phase
   * @return Game[]
   */
  public function getGamesByType($types, $phase)
  {
    $games = [];
    foreach ($phase->getGroups() as $group) {
      foreach ($types as $type) {
        $games = array_merge(
          $games,
          $group->getGamesByType($type)
        );
      }
    }
    usort(
      $games, function ($g1, $g2) {
      if ($g1->getRelevance() > $g2->getRelevance()) {
        return -1;
      }
      return 1;
    }
    );
    return $games;
  }

  public function getRankingsColumns(Group $group)
  {
    $mode = $group->getMode();
    $rankingService = $this->sl->get($mode->getRankingServiceName());
    return $rankingService->getColumns();
  }

  public function phaseChangedCallback(Phase $phase)
  {
    $this->ls->loadPhaseCompletely($phase);
    //recalculcate games
    foreach ($phase->getGroups() as $group) {
      $group->setFinished(false);
    }
    foreach ($phase->getGroups() as $group) {
      $this->groupChangedCallback($group);
    }
  }

  public function startPhase(Phase $phase)
  {
    $this->ls->loadPhaseCompletely($phase);

    $order = $this->speakService->reserveOrder();

    /******* create rankings *******/
    //get old rankings
    if ($phase->getPhaseNumber() > 0) {
      $oldRankings = $this->getRankings(
        $phase->getTournament()->getPhasesByTemplate(false)[$phase->getPhaseNumber() - 1]
      );
    } else {
      //tournament started create artifical rankings
      $oldRankings = [];
      $oldRankings[0] = [];
      foreach ($phase->getTournament()->getAttendants() as $attendant) {
        $ranking = new Ranking([]);
        $ranking->setRank($attendant->getStartNumber());
        $ranking->setPhaseStartRank($attendant->getStartNumber());
        $ranking->setAttendants([$attendant]);
        $oldRankings[0][$ranking->getRank()] = $ranking;
      }
      ksort($oldRankings[0]);
    }

    $strategy = $phase->getAttendanceStrategy();

    $strategyService = $this->sl->get(
      $phase->getAttendanceStrategyServiceName()
    );
    $strategyService->setAttendanceStrategy($strategy);

    $modeServices = [];

    $modes = $phase->getModesByTemplate(false);

    foreach ($modes as $mode) {
      $modeServices[$mode->getGroupNumber()] =
        $this->sl->get($mode->getServiceName());
      $modeServices[$mode->getGroupNumber()]->setMode($mode);
    }

    $newRankings =
      $strategyService->computeNewRankings($oldRankings, $modeServices);

    foreach ($newRankings as $groupNumber => $groupRankings) {
      //create new group
      $group = new Group();
      $group->setPhase($phase);
      $group->setGroupNumber($groupNumber);
      $group->setStarted(true);
      $group->setFinished(false);
      if (count($modes) > 1) {
        $group->setMode($modes[$groupNumber]);
      } else {
        $group->setMode($modes[0]);
      }
      $this->em->persist($group);
      foreach ($groupRankings as $ranking) {
        $ranking->setGroup($group);
        $this->em->persist($ranking);
      }
    }

    /*******************************/

    $phase->setStarted(true);


    $this->speakService->addPhaseTask($phase->getStartText(), "phase_start_text_" . $phase->getId(), $phase, $order);


    return true;
  }

  public function unStartPhase(Phase $phase)
  {
    if (!$phase->isStarted()) {
      //nothing to do
      return true;
    }
    $this->ls->loadPhaseCompletely($phase);
    foreach (iterator_to_array($phase->getGroups()) as $group) {
      $this->deleteGroup($group);
    }
    $phase->setStarted(false);
    $phase->setFinished(false);
    return true;
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function deleteGame(Game $game)
  {
    foreach ($game->getTeamA() as $ranking) {
      $ranking->getGamesAsA()->removeElement($game);
    }
    foreach ($game->getTeamB() as $ranking) {
      $ranking->getGamesAsB()->removeElement($game);
    }
    $game->getGroup()->getGames()->removeElement($game);
    $this->em->remove($game);
  }

  private function deleteGroup(Group $group)
  {
    //delete games
    foreach (iterator_to_array($group->getGames()) as $game) {
      $this->deleteGame($game);
    }

    //delete rankings
    foreach (iterator_to_array($group->getRankings()) as $ranking) {
      $this->deleteRanking($ranking);
    }

    //delete group
    $group->getPhase()->getGroups()->removeElement($group);
    $this->em->remove($group);
  }

  private function deleteRanking(Ranking $ranking)
  {
    $ranking->getGroup()->getRankings()->removeElement($ranking);
    $ranking->getAttendants()->clear();
    $this->em->remove($ranking);
  }

  /**
   * @param {Phase} phase   the phase for which to get the rankings
   * @return {Ranking[][]}  an array of rankings,
   *                        containing for each group an array of its rankings
   */
  private function getRankings(Phase $phase)
  {
    $res = [];
    foreach ($phase->getGroups() as $group) {
      $res[$group->getGroupNumber()] = $group->getRankings();
    }
    return $res;
  }

  private function groupChangedCallback(Group $group)
  {
    $this->recalculateRankings($group);
    $this->recalculateGames($group);
  }

  private function recalculateGames(Group $group)
  {
    $mode = $group->getMode();
    /** @var ModeServiceInterface $modeService */
    $modeService = $this->sl->get($mode->getServiceName());
    $modeService->setMode($mode);
    $ended = false;
    $modeService->recalculateGames($group, $this->em, $ended);
    if ($group->getPhase()->getTournament()->getAutomaticallyStartGames()) {
      $freeTables = $this->getFreeTables($group->getPhase());
      if (count($freeTables) > 0) {
        $changed = false;
        foreach ($this->getGamesByType([Game::TYPE_SCHEDULED_DYN], $group->getPhase()) as $game) {
          reset($freeTables);
          $table = key($freeTables);


          $game->setTable($table);
          $game->setType(Game::TYPE_RUNNING);
          $spoken = $this->namesHelper->__invoke($game, false, true);
          $this->speakService->addTournamentTask("Auf Tisch " . $table . ": " . $spoken . "." . $spoken . " auf Tisch "
            . $table, "game_" . $game->getId(), $group->getPhase()->getTournament());
          $changed = true;

          unset($freeTables[$table]);
          if (empty($freeTables)) {
            break;
          }
        }
        if ($changed) {
          $modeService->recalculateGames($group, $this->em, $ended);
        }
      }
    }
    $group->setFinished($ended);
    if ($ended) {
      $phase = $group->getPhase();
      $allFinished = true;
      foreach ($phase->getGroups() as $g) {
        if (!$g->isFinished()) {
          $allFinished = false;
          break;
        }
      }
      if ($allFinished) {
        $phase->setFinished(true);
      }
    }
  }

  private function recalculateRankings(Group $group)
  {
    $mode = $group->getMode();
    $rankingService = $this->sl->get($mode->getRankingServiceName());
    $rankingService->recalculateRankings($group);
  }
//</editor-fold desc="Private Methods">
}
