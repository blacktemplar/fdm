<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/28/16
 * Time: 12:26 PM
 */

namespace FDM\Service;

use Doctrine\ORM\EntityManager;
use FDM\Entity\Player;
use FDM\Entity\Tournament;
use FDM\Entity\TournamentRanking;
use FDM\Entity\TournamentRankingEntry;
use FDM\Entity\TournamentRankingInstance;
use FDM\ZfcDatagrid\Column\Type\MyNumber;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use ZfcDatagrid\Column;

abstract class TournamentRankingService
{
//<editor-fold desc="Fields">
  /** @var EntityManager $em */
  protected $em;

  /** @var LoadingService $lm */
  protected $lm;

  private $logger;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(EntityManager $em, LoadingService $lm)
  {
    $this->em = $em;
    $this->lm = $lm;
    $this->logger = null;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function addColumns(&$columns, $ranking, $config)
  {
    $col = new Column\Select('points', 'e');
    $col->setLabel('Points');
    $col->setType(new MyNumber());
    $columns[] = ["col" => $col, "weight" => 3.5, "default-on" => true];
  }

  /**
   * @param TournamentRanking $tournamentRanking
   * @param Tournament $tournament
   */
  public function addTournament($tournamentRanking, $tournament)
  {
    $this->changeTournament($tournamentRanking, $tournament);
  }

  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   * @return double
   */
  public function getJackpot($tournamentRankingInstance)
  {
    $tournamentRanking = $tournamentRankingInstance->getTournamentRanking();
    $jackpot = 0;
    if ($tournamentRanking->has_property("jackpotPerAttendant")) {
      $amount = $tournamentRanking->getJackPotPerAttendant();
      foreach ($tournamentRankingInstance->getEntries() as $entry) {
        $jackpot += $amount * $entry->getTournaments();
      }
    }
    return $jackpot;
  }

  /**
   * @param TournamentRankingEntry $entry
   * @return float
   */
  public function getPoints($entry)
  {
    return $entry->getPoints();
  }

  /**
   * @param $tournament
   * @param TournamentRanking $tournamentRanking
   * @return mixed
   */
  public function getTournamentTime($tournament, $tournamentRanking)
  {
    $function = $tournamentRanking->getTimeColumn();
    if ($function == null) {
      $function = "getEnd";
    }
    return $tournament->$function();
  }

  /**
   * @param TournamentRanking $tournamentRanking
   * @return bool
   */
  public function hasJackpot($tournamentRanking)
  {
    return $tournamentRanking->has_property("jackpotPerAttendant");
  }

  /**
   * @param TournamentRanking $tournamentRanking
   * @param \DateTime $time
   */
  public function recompute($tournamentRanking, $time, $tournament = null)
  {
    /** @var TournamentRankingInstance[] $toUpdate */
    $toUpdate = [];

    $current = null;

    /** @var TournamentRankingInstance $lastActual */
    $lastActual = null;

    $logId = "tournamentRanking logger for ranking " . $tournamentRanking->getName();
    $startLog = "Started  " . $logId . ". ";
    if ($tournament === null) {
      $startLog .= "Refreshing ranking from begin time " . $time->format('Y-m-d H:i:s');
    } else {
      $startLog .= "Adding tournament " . $this->getTournamentIdentification($tournament);
    }
    $this->log($startLog);

    foreach ($tournamentRanking->getInstances() as $instance) {
      if ($instance->isCurrent()) {
        assert($current === null); //we do not allow multiple current instances
        $current = $instance;
      } else if ($instance->getLastEntryTime() >= $time) {
        $toUpdate[] = $instance;
      } else if ($lastActual == null || $instance->getLastEntryTime() > $lastActual->getLastEntryTime()) {
        $lastActual = $instance;
      }
    }
    usort($toUpdate, function ($a, $b) {
      if ($a->getLastEntryTime() < $b->getLastEntryTime()) {
        return -1;
      } else if ($a->getLastEntryTime() > $b->getLastEntryTime()) {
        return 1;
      }
      return 0;
    });

    if ($lastActual === null && ($tournament === null || !$this->isCommutative())) {
      $lastActual = new TournamentRankingInstance($tournamentRanking);
    }
    foreach ($toUpdate as $instance) {
      if (!$instance->isFixed()) {
        if ($this->isCommutative() && $tournament !== null) {
          $this->log("Add tournament to staged ranking (stage time = " .
            $instance->getLastEntryTime()->format('Y-m-d H:i:s') . ").");
          $this->lm->loadTournamentCompletely($tournament);
          $this->log("Recompute rankings for tournament " . $this->getTournamentIdentification($tournament) . ".");
          $this->recomputeRankings($instance, $tournament);
        } else {
          $this->log("Recompute rankings from last valid stage time " .
            $lastActual->getLastEntryTime()->format('Y-m-d H:i:s') . " for staged ranking (stage time = " .
            $instance->getLastEntryTime()->format('Y-m-d H:i:s') . ").");
          $this->recomputeBasedOn($instance, $lastActual);
        }
      }
      $lastActual = $instance;
    }


    if ($current === null) {
      //no current instance: create new one
      $current = new TournamentRankingInstance($tournamentRanking, true);
      if ($lastActual !== null) {
        //clone lastActual
        $this->cloneInto($current, $lastActual);
        $current->setLastEntryTime($lastActual->getLastEntryTime());
      }
      $tournamentRanking->getInstances()->add($current);
      $this->em->persist($current);
    }

    if ($tournament === null ||
      ((!$this->isCommutative() || ($lastActual !== null && $lastActual->getLastEntryTime() >= $time)) &&
        $current->getLastEntryTime() > $time)
    ) {
      $this->log("Recompute rankings from last valid stage time " .
        $lastActual->getLastEntryTime()->format('Y-m-d H:i:s') . " for current ranking");
      $this->recomputeBasedOn($current, $lastActual);
    } else {
      $this->log("Add tournament to current ranking.");
      $this->lm->loadTournamentCompletely($tournament);
      $this->log("Recompute rankings for tournament " . $this->getTournamentIdentification($tournament) . ".");
      $this->recomputeRankings($current, $tournament);
      if ($current->getLastEntryTime() < $time) {
        $current->setLastEntryTime($time);
      }
    }
    $entries = [];
    foreach ($current->getEntries() as $entry) {
      $entries[] = $entry;
    }

    usort($entries, array($this, 'compareEntries'));

    $sharedRank = 1;
    $counter = 1;
    $last = null;
    foreach ($entries as $entry) {
      if ($last === null || $this->compareEntries($entry, $last) != 0) {
        $sharedRank = $counter;
        $last = $entry;
      }
      $entry->setRank($sharedRank);
      $counter += 1;
    }

    $this->log("Finished " . $logId . ".");
  }

  /**
   * @param TournamentRanking $tournamentRanking
   * @param Tournament $tournament
   * @param $em
   * @param $lm
   */
  public function removeTournament($tournamentRanking, $tournament)
  {
    $this->changeTournament($tournamentRanking, $tournament, true);
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  /**
   * @param TournamentRankingInstance $instance
   * @param $player
   * @return TournamentRankingEntry
   */
  protected function addEntry($instance, $player)
  {
    $entry = $this->getNewEntry($instance, $player);
    $instance->getEntries()->add($entry);
    $this->em->persist($entry);
    return $entry;
  }

  /**
   * @param TournamentRankingInstance $target
   * @param TournamentRankingInstance $toClone
   */
  protected function cloneInto($target, $toClone)
  {
    $toRemove = [];
    foreach ($target->getEntries() as $entry) {
      $toRemove[] = $entry;
    }
    foreach ($toRemove as $entry) {
      $target->getEntries()->removeElement($entry);
      $this->em->remove($entry);
    }
    if ($toClone !== null) {
      foreach ($toClone->getEntries() as $entry) {
        $clone = clone $entry;
        $clone->setTournamentRankingInstance($target);
        $target->getEntries()->add($clone);
        $this->em->persist($clone);
      }
    }
    if ($target->isCurrent()) {
      $target->setLastEntryTime($toClone->getLastEntryTime());
    }
  }

  /**
   * @param TournamentRankingEntry $a
   * @param TournamentRankingEntry $b
   * @return int
   */
  protected function compareEntries($a, $b)
  {
    $aPoints = $this->getPoints($a);
    $bPoints = $this->getPoints($b);
    if ($aPoints == $bPoints) {
      return 0;
    }
    return $aPoints < $bPoints ? 1 : -1;
  }

  protected function getNewEntry($instance, $player)
  {
    $entry = new TournamentRankingEntry($instance, $player);
    $entry->setPoints(0);
    $entry->setTournaments(0);
    return $entry;
  }

  protected function getOrAddPlayerEntry($instance, $player)
  {
    $entry = $this->getPlayerEntry($instance, $player);
    if ($entry === null) {
      $entry = $this->addEntry($instance, $player);
    }
    return $entry;
  }

  /**
   * @param TournamentRankingInstance $instance
   * @param Player $player
   * @return TournamentRankingEntry
   */
  protected function getPlayerEntry($instance, $player)
  {
    foreach ($instance->getEntries() as $entry) {
      if ($entry->getPlayer()->getId() === $player->getId()) {
        return $entry;
      }
    }
    return null;
  }

  /**
   * @param Tournament $tournament
   * @return string
   */
  protected function getTournamentIdentification($tournament)
  {
    return $tournament->getName() . " (id='" . $tournament->getId() . "', date= " .
    $tournament->getStart()->format('Y-m-d H:i:s') . ")";
  }

  abstract protected function isCommutative();

  protected function log($message, $type = Logger::INFO)
  {
    if ($this->logger === null) {
      $this->logger = new Logger();

      $writer = new Stream(getcwd() . '/data/logs/tournamentRanking.log');

      $this->logger->addWriter($writer);
    }
    $this->logger->log($type, $message);
  }

  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   * @param TournamentRankingInstance $basedOn
   */
  protected function recomputeBasedOn($tournamentRankingInstance, $basedOn)
  {
    //reset tournamentRankingInstance to based on
    $this->cloneInto($tournamentRankingInstance, $basedOn);
    /** @var Tournament[] $tournaments */
    $tournaments = [];
    $tournamentRanking = $tournamentRankingInstance->getTournamentRanking();
    foreach ($tournamentRankingInstance->getTournamentRanking()->getTournaments() as $tournament) {
      $tournamentTime = $this->getTournamentTime($tournament, $tournamentRanking);
      if ($this->tournamentFinished($tournament) && $tournamentTime > $basedOn->getLastEntryTime() && (
          $tournamentRankingInstance->isCurrent() ||
          $tournamentTime <= $tournamentRankingInstance->getLastEntryTime())
      ) {
        $tournaments[] = $tournament;
      }
    }
    usort($tournaments, function ($a, $b) use ($tournamentRanking) {
      $ta = $this->getTournamentTime($a, $tournamentRanking);
      $tb = $this->getTournamentTime($b, $tournamentRanking);
      if ($ta < $tb) {
        return -1;
      } else if ($tb < $ta) {
        return 1;
      }
      return 0;
    });
    $this->lm->loadTournamentsCompletely($tournaments);
    foreach ($tournaments as $tournament) {
      $this->log("Recompute rankings for tournament " . $this->getTournamentIdentification($tournament) . ".");
      $this->recomputeRankings($tournamentRankingInstance, $tournament);
      if ($tournamentRankingInstance->isCurrent() &&
        $tournamentRankingInstance->getLastEntryTime() < $tournament->getStart()
      ) {
        $tournamentRankingInstance->setLastEntryTime($tournament->getStart());
      }
    }
  }

  abstract protected function recomputeRankings($tournamentRankingInstance, $tournament);

  protected function tournamentFinished($tournament)
  {
    return !$tournament->isDeleted() && $tournament->isFinished();
  }
//</editor-fold desc="Protected Methods">

//<editor-fold desc="Private Methods">
  /**
   * @param TournamentRanking $tournamentRanking
   * @param Tournament $tournament
   * @param LoadingService $lm
   * @param bool $remove
   */
  private function changeTournament($tournamentRanking, $tournament, $remove = false)
  {
    $this->recompute($tournamentRanking, $this->getTournamentTime($tournament, $tournamentRanking),
      $remove ? null : $tournament);
  }
//</editor-fold desc="Private Methods">
}