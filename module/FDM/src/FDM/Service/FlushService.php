<?php

namespace FDM\Service;

use Doctrine\ORM\EntityManager;
use FDM\Entity\TournamentPrivileges;

class FlushService implements FlushServiceInterface
{
//<editor-fold desc="Fields">
  protected $em;

  protected $tas;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  /**
   *
   * @param {EntityManager} em               EntityManager
   * @param {null|TournamentAccessService}   TAS can be null if we are not in tournament section
   */
  public function __construct(EntityManager $em, $tas)
  {
    $this->em = $em;
    $this->tas = $tas;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function flush($ignoreFinished = false)
  {
    if ($this->tas !== null &&
      $this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT, $ignoreFinished)
    ) {
      $this->em->flush();
    } else {
      throw new \Exception('User is not allowed to edit data!');
    }
  }
//</editor-fold desc="Public Methods">
}
