<?php

namespace FDM\Service;

use Doctrine\Common\Collections\ArrayCollection;
use FDM\Entity\Game;
use FDM\Entity\Group;
use FDM\Entity\Ranking;

class PointsRankingService extends RankingServiceAbstract
  implements RankingServiceInterface
{
//<editor-fold desc="Public Methods">
  public function compareBetweenGroups(
    Ranking $r1, Ranking $r2)
  {
    if ($r1->getGamesPlayed() == $r2->getGamesPlayed()) {
      /** @noinspection PhpParamsInspection */
      return $this->compare($r1, $r2);
    }
    $diff = $r2->getGamesPlayed() - $r1->getGamesPlayed();
    $clone = clone $r1;
    $clone->setPoints($clone->getPoints() + $diff);
    $clone->setGoalDifference($clone->getGoalDifference() + 4 * $diff);

    return $this->compareProperties(
      $clone, $r2, [
        "getPoints" => 1,
        "getGoalDifference" => 1,
        "getGamesPlayed" => 1
      ]
    );
  }

  public function getColumns()
  {
    return [
      "Points" => "getPoints",
      "Goal Difference" => "getGoalDifference",
      "Games Played" => "getGamesPlayed",
    ];
  }

  public function recalculateRankings(Group $group)
  {
    $rankings = [];
    //reset rankings
    foreach ($group->getRankings() as $ranking) {
      $this->reset($ranking);
      $rankings[] = $ranking;
    }

    //loop through games and adjust rankings
    foreach ($group->getGamesByType(Game::TYPE_FINISHED) as $game) {
      $teamAWon = $game->getResultA() > $game->getResultB();
      foreach ($game->getTeamA() as $ranking) {
        if ($teamAWon) {
          $ranking->setPoints($ranking->getPoints() + 1);
        }
        $ranking->setGamesPlayed($ranking->getGamesPlayed() + 1);
        $ranking->setGoalDifference(
          $ranking->getGoalDifference() +
          $game->getResultA() - $game->getResultB()
        );
        $ranking->setGoalsShot(
          $ranking->getGoalsShot() + $game->getResultA()
        );
      }
      foreach ($game->getTeamB() as $ranking) {
        if (!$teamAWon) {
          $ranking->setPoints($ranking->getPoints() + 1);
        }
        $ranking->setGamesPlayed($ranking->getGamesPlayed() + 1);
        $ranking->setGoalDifference(
          $ranking->getGoalDifference() +
          $game->getResultB() - $game->getResultA()
        );
        $ranking->setGoalsShot(
          $ranking->getGoalsShot() + $game->getResultB()
        );
      }
    }

    //order rankings;
    usort($rankings, [$this, "compare"]);

    for ($i = 0; $i < count($rankings) - 1; $i++) {
      $equal = [$rankings[$i]];
      $start = $i;
      while ($i < count($rankings) - 1 &&
        $this->compare($rankings[$i], $rankings[$i + 1]) == 0) {
        $i++;
        $equal[] = $rankings[$i];
      }
      $this->resolveEquals($group, $equal);
      for ($j = 0; $j < count($equal); $j++) {
        $rankings[$start + $j] = $equal[$j];
      }
    }


    //set ranks
    $rank = 1;
    $indexedRankings = [];
    foreach ($rankings as $ranking) {
      $ranking->setRank($rank);
      $ranking->setDisplayRank($rank);
      $indexedRankings[$rank] = $ranking;
      $rank++;
    }
    $group->setRankings(
      new ArrayCollection($indexedRankings)
    );
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  protected function compare(Ranking $r1, Ranking $r2)
  {
    return $this->compareProperties(
      $r1, $r2, [
        "getPoints" => 1,
        "getGoalDifference" => 1,
        "getGamesPlayed" => -1,
      ]
    );
  }

  protected function resolveEquals(Group $group, &$equals)
  {
    //check for direct matches
    $firstTeamResults = 0;
    if (count($equals) == 2) {
      //check if there was a direct match
      foreach ($group->getGamesByType(Game::TYPE_FINISHED) as $game) {
        if (
          $game->getTeamA()->contains($equals[0]) &&
          $game->getTeamB()->contains($equals[1])
        ) {
          if ($game->getResultA() > $game->getResultB()) {
            $firstTeamResults++;
          } else {
            $firstTeamResults--;
          }
        } else if (
          $game->getTeamB()->contains($equals[0]) &&
          $game->getTeamA()->contains($equals[1])
        ) {
          if ($game->getResultA() > $game->getResultB()) {
            $firstTeamResults--;
          } else {
            $firstTeamResults++;
          }
        }
      }
      if ($firstTeamResults < 0) {
        $tmp = $equals[0];
        $equals[0] = $equals[1];
        $equals[1] = $tmp;
      }
    } else {
      usort($equals, [$this, "compareGoalsShot"]);
    }
  }
//</editor-fold desc="Protected Methods">

//<editor-fold desc="Private Methods">
  private function compareGoalsShot(Ranking $r1, Ranking $r2)
  {
    return $this->compareProperties(
      $r1, $r2, ["getGoalsShot" => 1, "getPhaseStartRank" => -1]
    );
  }

  private function reset(Ranking $ranking)
  {
    $ranking->setPoints(0);
    $ranking->setGamesPlayed(0);
    $ranking->setRank(0);
    $ranking->setDisplayRank(0);
    $ranking->setGoalDifference(0);
    $ranking->setGoalsShot(0);
  }
//</editor-fold desc="Private Methods">
}
