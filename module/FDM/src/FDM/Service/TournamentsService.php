<?php

namespace FDM\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use FDM\Entity\Tournament;
use FDM\Entity\TournamentRanking;
use FDM\Entity\TournamentRankingInstance;
use Zend\Mvc\I18n\Translator;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentsService implements TournamentsServiceInterface
{
//<editor-fold desc="Fields">
  protected $entityManager;

  protected $sl;

  protected $translator;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(ServiceLocatorInterface $sl, EntityManager $entityManager, Translator $translator)
  {
    $this->entityManager = $entityManager;
    $this->sl = $sl;
    $this->translator = $translator;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">

  public function addColumns($id, &$columns)
  {
    /** @var TournamentRanking $ranking */
    $ranking = $this->entityManager->find('FDM\Entity\TournamentRanking', $id);
    $service = $this->sl->get($ranking->getServiceName());
    $service->addColumns($columns, $ranking, $this->sl->get("Config"));
  }

  /**
   * @param $id
   * @return TournamentRankingInstance
   */
  public function getCurrentRanking($id)
  {
    return $this->entityManager->createQueryBuilder()
      ->select('i')
      ->from('FDM\Entity\TournamentRankingInstance', 'i')
      ->where('i.tournamentRanking = :id')
      ->andWhere('i.current = 1')
      ->setParameter('id', $id)
      ->getQuery()
      ->getSingleResult();
  }

  public function getRanking($id)
  {
    return $this->entityManager->createQueryBuilder()
      ->from('FDM\Entity\TournamentRankingEntry', 'e')
      ->innerJoin('FDM\Entity\TournamentRankingInstance', 'i', Join::WITH, 'e.tournamentRankingInstance = i.id')
      ->innerJoin('FDM\Entity\Player', 'p', Join::WITH, 'e.player = p.id')
      ->where('i.tournamentRanking = :id')
      ->andWhere('i.current = 1')
      ->setParameter('id', $id)
      ->orderBy('e.rank', 'ASC');
  }

  public function getTemplates($userId)
  {
    return $this->baseQueryBuilder($userId)->andWhere('t.template = 1');
  }

  public function getTournamentRankings()
  {
    return $this->entityManager->createQueryBuilder()
      ->from('FDM\Entity\TournamentRanking', 'r');
  }

  public function getTournaments($userId)
  {
    return $this->baseQueryBuilder($userId)->andWhere('t.template = 0');
  }

  public function modifyGrid($id, $grid)
  {
    $ranking = $this->entityManager->find('FDM\Entity\TournamentRanking', $id);
    $grid->setTitle($ranking->getName());
    $instance = $this->getCurrentRanking($id);
    $last = $instance->getLastEntryTime();
    $service = $this->sl->get($ranking->getServiceName());

    $hasJackpot = $service->hasJackpot($ranking);
    $jackpot = 0;
    if ($hasJackpot) {
      $jackpot = $service->getJackpot($instance);
    }

    $grid->setToolbarTemplateVariables(['lastRefreshed' => $last, 'hasJackpot' => $hasJackpot, 'jackpot' => $jackpot]);
  }

  /**
   * @param Tournament[] $tournaments
   */
  public function refreshRankings($tournaments)
  {
    if (count($tournaments) == 0) {
      //nothing to do
      return;
    }
    $minStartDate = null;
    $minEndDate = null;
    $minGameDate = null;
    foreach ($tournaments as $t) {
      if ($t->isFinished()) {
        if ($minStartDate === null || $t->getStart() < $minStartDate) {
          $minStartDate = $t->getStart();
        }
        if ($minEndDate === null || $t->getEnd() < $minEndDate) {
          $minEndDate = $t->getEnd();
        }
        foreach ($t->getPhases() as $phase) {
          foreach ($phase->getGroups() as $group) {
            foreach ($group->getGames() as $game) {
              if ($minGameDate === null || $game->getStartTime() < $minGameDate) {
                $minGameDate = $game->getStartTime();
              }
            }
          }
        }
      }
    }

    /** @var TournamentRanking[] $rankings */
    $rankings = $this->entityManager->createQueryBuilder()
      ->select("r,i, e")
      ->from('FDM\Entity\TournamentRanking', 'r')
      ->innerJoin('r.tournaments', 't')
      ->innerJoin('r.instances', 'i')
      ->innerJoin('i.entries', 'e')
      ->where('t IN (:ts)')
      ->setParameter('ts', $tournaments)
      ->getQuery()->getResult();


    $minTimes = [];

    foreach ($tournaments as $t) {
      foreach ($t->getTournamentRankings() as $tr) {
        /** @var TournamentRankingService $service */
        $service = $this->sl->get($tr->getServiceName());
        $time = $service->getTournamentTime($t, $tr);
        if (!array_key_exists($tr->getId(), $minTimes) || $time < $minTimes[$tr->getId()]) {
          $minTimes[$tr->getId()] = $time;
        }
      }
    }

    foreach ($rankings as $ranking) {
      $service = $this->sl->get($ranking->getServiceName());
      if (array_key_exists($ranking->getId(), $minTimes)) {
        $service->recompute($ranking, $minTimes[$ranking->getId()]);
      }
    }
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function baseQueryBuilder($userId)
  {
    $qb = $this->entityManager->createQueryBuilder();
    $qb->select('t');
    $qb->from('FDM\Entity\Tournament', 't');
    $where = 't.public = true';
    if ($userId !== null) {
      $qb->leftJoin(
        'FDM\Entity\TournamentPrivileges',
        'p',
        Join::WITH,
        't.id = p.tournament AND p.user = :uid'
      );
      $where .= ' OR p.user = :uid';
      $qb->setParameter('uid', $userId);
    }
    $qb->andWhere($where);
    $qb->andWhere('t.deleted = false');

    return $qb;
  }
//</editor-fold desc="Private Methods">
}
