<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/28/16
 * Time: 2:22 PM
 */

namespace FDM\Service;


use Doctrine\ORM\Query\Expr;
use FDM\Entity\Attendant;
use FDM\Entity\Group;
use FDM\Entity\Ranking;
use FDM\Entity\Tournament;
use FDM\Entity\TournamentRankingEntry;
use FDM\Entity\TournamentRankingInstance;
use FDM\ZfcDatagrid\Column\Type\ArrayElement;
use FDM\ZfcDatagrid\Column\Type\Integer;
use FDM\ZfcDatagrid\Column\Type\MyNumber;
use ZfcDatagrid\Column;

class TournamentRankRankingService extends TournamentRankingService
{
//<editor-fold desc="Public Methods">
  public function addColumns(&$columns, $ranking, $config)
  {
    parent::addColumns($columns, $ranking, $config);
    $col = new Column\Select(new Expr\Select('e.points / e.tournaments'), 'ppT');
    $col->setLabel('Points per T.');
    $col->setType(new MyNumber());
    $columns[] = ["col" => $col, "weight" => 5, "default-on" => true];

    $col = $this->getExtraEntryColumn("golds", $config);
    $col->setLabel('1. Places');
    $columns[] = ["col" => $col, "weight" => 6, "default-on" => false];

    $col = $this->getExtraEntryColumn("silbers", $config);
    $col->setLabel('2. Places');
    $columns[] = ["col" => $col, "weight" => 7, "default-on" => false];

    $col = $this->getExtraEntryColumn("bronzes", $config);
    $col->setLabel('3. Places');
    $columns[] = ["col" => $col, "weight" => 8, "default-on" => false];
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  protected function getNewEntry($instance, $player)
  {
    $entry = new TournamentRankingEntry($instance, $player, ["golds", "silbers", "bronzes"]);
    $entry->setPoints(0);
    $entry->setTournaments(0);
    $entry->setGolds(0);
    $entry->setSilbers(0);
    $entry->setBronzes(0);
    return $entry;
  }

  protected function isCommutative()
  {
    return true;
  }

  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   * @param Tournament $tournament
   */
  protected function recomputeRankings($tournamentRankingInstance, $tournament)
  {
    $tournamentRanking = $tournamentRankingInstance->getTournamentRanking();

    $phases = $tournament->getPhasesByTemplate(false);

    $attendants = [];
    foreach ($phases[count($phases) - 1]->getGroups() as $group) {
      /** @var Group $group */
      $points = 0;
      $summedPoints = 0;
      /** @var Ranking[] $sharedRankings */
      $sharedRankings = [];
      $lastRank = null;
      foreach ($group->getRankings() as $ranking) {
        /** @var Ranking $ranking */
        if (array_key_exists($ranking->getRank(), $tournamentRanking->getRankPoints())) {
          $points = $tournamentRanking->getRankPoints()[$ranking->getRank()];
        }
        if ($lastRank !== $ranking->getDisplayRank() && count($sharedRankings) > 0) {
          $this->applyPoints($tournamentRankingInstance, $sharedRankings, $summedPoints, $attendants, $lastRank);
        }
        $summedPoints += $points;
        $sharedRankings[] = $ranking;
        $lastRank = $ranking->getDisplayRank();
      }
      if (count($sharedRankings) > 0) {
        $this->applyPoints($tournamentRankingInstance, $sharedRankings, $summedPoints, $attendants, $lastRank);
      }
    }

    $maxKey = 0;
    $maxVal = 0;
    foreach ($tournamentRanking->getRankPoints() as $key => $val) {
      if ($key > $maxKey) {
        $maxKey = $key;
        $maxVal = $val;
      }
    }
    foreach ($tournament->getAttendants() as $attendant) {
      if (!array_key_exists($attendant->getId(), $attendants)) {
        $this->addPoints($tournamentRankingInstance, $attendant, $maxVal, "eliminated");
      }
    }
  }
//</editor-fold desc="Protected Methods">

//<editor-fold desc="Private Methods">
  /**
   * @param $tournamentRankingInstance
   * @param Attendant $attendant
   * @param $points
   * @param $rank
   */
  private function addPoints($tournamentRankingInstance, $attendant, $points, $rank)
  {
    $player = $attendant->getPlayer();
    $entry = $this->getOrAddPlayerEntry($tournamentRankingInstance, $player);
    $entry->setTournaments($entry->getTournaments() + 1);
    $entry->setPoints($entry->getPoints() + $points);
    if ($rank == 1) {
      $entry->addPropertyIfNotExistent("Golds", 0);
      $entry->setGolds($entry->getGolds() + 1);
    } else if ($rank == 2) {
      $entry->addPropertyIfNotExistent("Silbers", 0);
      $entry->setSilbers($entry->getSilbers() + 1);
    } else if ($rank == 3) {
      $entry->addPropertyIfNotExistent("Bronzes", 0);
      $entry->setBronzes($entry->getBronzes() + 1);
    }
    $this->log("The player " . $player->getFirstName() . " " . $player->getLastName() . " gets " . $points . " points " .
      "(" . ($entry->getPoints() - $points) . "->" . $entry->getPoints() . ")" .
      " for occupying " . $rank . ". place.");
  }

  private function applyPoints($tournamentRankingInstance, &$sharedRankings, &$summedPoints, &$attendants, $rank)
  {
    $average = $summedPoints / count($sharedRankings);
    foreach ($sharedRankings as $r) {
      foreach ($r->getAttendants() as $attendant) {
        $this->addPoints($tournamentRankingInstance, $attendant, $average, $rank);
        $attendants[$attendant->getId()] = true;
      }
    }
    $summedPoints = 0;
    $sharedRankings = [];
  }

  private function getExtraEntryColumn($name, $config)
  {
    $hasJson = false;
    if (array_key_exists("fdm", $config) && array_key_exists("json-data-type", $config["fdm"])) {
      $hasJson = $config["fdm"]["json-data-type"] == 1;
    }

    if ($hasJson) {
      $col = new Column\Select(new Expr\Select("JSON_EXTRACT(e.subClassData, '$.$name')"), "subClassData_" . $name);
      $col->setType(new Integer());
    } else {
      $col = new Column\Select('subClassData', 'e');
      $col->setUniqueId($col->getUniqueId() . "_" . $name);
      $col->setType(new ArrayElement($name));
    }
    return $col;
  }
//</editor-fold desc="Private Methods">
}