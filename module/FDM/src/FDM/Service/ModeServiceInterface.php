<?php

namespace FDM\Service;

use Doctrine\ORM\EntityManager;
use FDM\Entity\Group;
use FDM\Entity\Mode;


interface ModeServiceInterface
{
//<editor-fold desc="Public Methods">
  public function getExtraStructures(Group $group);

  /**
   * @returns {\FDM\Entity\Ranking}  a new empty ranking of the right type
   *                                 which extends Ranking.
   */
  public function getNewRanking();

  /**
   * @param {Group} group      the group which changed and needs recalculation
   * @param {EntityManager} em entity manager for deleting and creating new games
   */
  public function recalculateGames(Group $group, EntityManager $em, &$ended);

  /**
   * @return bool true if the rounds are reversed (i.e. the first round has the highest round number)
   */
  public function roundsReversed();

  /**
   * sets the entity object mode for this service
   */
  public function setMode(Mode $mode);
//</editor-fold desc="Public Methods">
}
