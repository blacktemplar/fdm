<?php

namespace FDM\Service;

use FDM\Entity\Group;
use FDM\Entity\Ranking;

abstract class RankingServiceAbstract implements RankingServiceInterface
{
//<editor-fold desc="Public Methods">
  public abstract function compareBetweenGroups(
    Ranking $r1, Ranking $r2);

  public abstract function recalculateRankings(Group $group);
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  protected function compareProperties(Ranking $r1, Ranking $r2, $properties)
  {
    foreach ($properties as $property => $dir) {
      if ($r1->$property() > $r2->$property()) {
        return -$dir;
      } else if ($r1->$property() < $r2->$property()) {
        return $dir;
      }
    }
    return 0;
  }
//</editor-fold desc="Protected Methods">
}
