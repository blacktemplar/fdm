<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/16/16
 * Time: 4:24 PM
 */

namespace FDM\Service;


use Doctrine\ORM\EntityManager;
use FDM\Entity\Tournament;

class PlayersService
{
//<editor-fold desc="Fields">
  protected $em;

  protected $ls;

  protected $tss;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    EntityManager $em,
    LoadingService $ls,
    TournamentsService $tss
  )
  {
    $this->em = $em;
    $this->ls = $ls;
    $this->tss = $tss;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getPlayers()
  {
    return $this->em->getRepository('FDM\Entity\Player')->findAll();
  }

  public function getPlayersAsArray()
  {
    $data = [];
    $players = $this->getPlayers();
    foreach ($players as $player) {
      $info = [];
      $info["label"] =
        $player->getLastName() . " " . $player->getFirstName();
      $info["value"] = "";
      $info["firstName"] = $player->getFirstName();
      $info["lastName"] = $player->getLastName();
      $info["id"] = $player->getId();
      $info["spelledFirstName"] = $player->getSpelledFirstName();
      $info["spelledLastName"] = $player->getSpelledLastName();
      $data[] = $info;
    }
    return $data;
  }

  public function mergePlayers($id1, $id2)
  {
    if ($id1 == $id2) {
      return "Players are identical!";
    }
    $p1 = $this->em->find('FDM\Entity\Player', $id1);
    if (!$p1) {
      return "Player 1 does not exist!";
    }
    $p2 = $this->em->find('FDM\Entity\Player', $id2);
    if (!$p2) {
      return "Player 2 does not exist!";
    }

    /** @var Tournament[] $ts */
    $ts = $this->em->createQueryBuilder()
      ->select("t")
      ->from('FDM\Entity\Tournament', 't')
      ->innerJoin('t.attendants', 'a')
      ->innerJoin('a.player', 'p')
      ->where('p.id = (:id)')->setParameter('id', $id1)
      ->getQuery()->getResult();

    $this->ls->loadTournamentsCompletely($ts);

    //check if player2 is attendant in one of the tournaments of player1
    foreach ($ts as $tournament) {
      foreach ($tournament->getAttendants() as $att) {
        if ($att->getPlayer()->getId() == $id2) {
          return "Player 1 and player 2 both attended the tournament " . $tournament->getName() .
          "(" . $tournament->getStart()->format('d.m.Y h:m') . ", id='" . $tournament->getId() . "')";
        }
      }
    }

    //change players
    foreach ($ts as $tournament) {
      foreach ($tournament->getAttendants() as $att) {
        if ($att->getPlayer()->getId() == $id1) {
          $att->setPlayer($p2);
        }
      }
    }

    $this->tss->refreshRankings($ts);

    foreach ($ts as $t) {
      foreach ($tournament->getTournamentRankings() as $ranking) {
        foreach ($ranking->getInstances() as $instance) {
          if ($instance->isFixed()) {
            foreach ($instance->getEntries() as $entry) {
              if ($entry->getPlayer()->getId() == $id1) {
                return "Player 1 exists in a fixed tournament ranking instance, please change this before removing/merging this player!";
              }
            }
          }
        }
      }
    }

    $this->em->remove($p1);

    return true;
  }
//</editor-fold desc="Public Methods">
}