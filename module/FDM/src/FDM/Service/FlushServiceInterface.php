<?php

namespace FDM\Service;

interface FlushServiceInterface
{
//<editor-fold desc="Public Methods">
  public function flush($ignoreFinished = false);
//</editor-fold desc="Public Methods">
}
