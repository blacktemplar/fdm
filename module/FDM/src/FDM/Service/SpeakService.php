<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 12/19/16
 * Time: 12:14 AM
 */

namespace FDM\Service;


use Doctrine\ORM\EntityManager;
use FDM\Entity\Phase;
use FDM\Entity\Ranking;
use FDM\Entity\SpeakTask;
use FDM\Entity\Tournament;
use FDM\View\Helper\NamesHelper;

class SpeakService
{
//<editor-fold desc="Fields">
  protected $em;

  protected $namesHelper;

  private $order;

//</editor-fold desc="Fields">

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    NamesHelper $namesHelper,
    EntityManager $em
  )
  {
    $this->namesHelper = $namesHelper;
    $this->em = $em;
    $order = 0;
  }

//<editor-fold desc="Public Methods">
  public function addPhaseTask($text, $info, Phase $phase, $order = null)
  {
    $text = $this->replaceMakrosForPhaseText($text, $phase);
    $this->addTournamentTask($text, $info, $phase->getTournament(), $order);
  }

  public function addTournamentTask($text, $info, $tournament, $order = null)
  {
    $text = $this->replaceMakrosForTournamentText($text, $tournament);
    if ($order === null) {
      $order = $this->reserveOrder();
    }
    $this->em->persist(new SpeakTask($text, $info, $tournament, $order));
  }

  /**
   * @param string $text
   * @param Phase $phase
   * @return string
   */
  public function replaceMakrosForPhaseText($text, $phase)
  {
    $res = $this->replaceMakrosForTournamentText($text, $phase->getTournament());
    if (strpos($res, '%teams%') !== false) {
      $replacement = "Die heutigen Teams lauten wie folgt:";
      foreach ($phase->getGroups() as $group) {
        $replacement .= " In Gruppe " . ($group->getGroupNumber() + 1) . " spielen";
        $first1 = true;
        foreach ($group->getRankings() as $ranking) {
          /** @var Ranking $ranking */
          if (!$first1) {
            $replacement .= ",";
          }
          $first1 = false;
          $first2 = true;
          foreach ($ranking->getAttendants() as $attendant) {
            if (!$first2) {
              $replacement .= " mit";
            }
            $first2 = false;
            $replacement .= " " . $this->namesHelper->__invoke($attendant, true, true);
          }
        }
        $replacement .= ".";
      }
      $res = str_replace('%teams%', $replacement, $res);
    }
    return $res;
  }

  /**
   * @param string $text
   * @param Tournament $tournament
   * @return string
   */
  public function replaceMakrosForTournamentText($text, $tournament)
  {
    $res = $text;
    $res = $this->generalReplacements($res);
    return $res;
  }

  public function reserveOrder()
  {
    $this->order += 1;
    return $this->order;
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function generalReplacements($text)
  {
    $res = str_replace('Tisch 1', 'Tisch eins', $text);
    return $res;
  }
//</editor-fold desc="Private Methods">
}