<?php

namespace FDM\Service;

use FDM\Entity\TournamentPrivileges;
use Zend\Permissions\Acl\Acl;

class TournamentAccessService implements TournamentAccessServiceInterface
{
//<editor-fold desc="Fields">
  protected $accessTypes;

  protected $admin;

  protected $isFinished;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($user, Acl $acl, $isFinished)
  {
    $userId = $user === null ? "guest" : $user->getId();
    $this->accessTypes = [];
    if ($acl->hasRole($userId)) {
      if ($acl->isAllowed($userId, TournamentPrivileges::TYPE_READ)) {
        $this->accessTypes[TournamentPrivileges::TYPE_READ] = True;
      }
      if ($acl->isAllowed($userId, TournamentPrivileges::TYPE_EDIT)) {
        $this->accessTypes[TournamentPrivileges::TYPE_READ] = True;
        $this->accessTypes[TournamentPrivileges::TYPE_EDIT] = True;
      }
    }
    if ($acl->isAllowed("guest", TournamentPrivileges::TYPE_READ)) {
      $this->accessTypes[TournamentPrivileges::TYPE_READ] = True;
    }
    $this->admin = $user !== null && $user->isAdmin();
    $this->isFinished = $isFinished;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function hasAccessType($accessType, $ignoreFinished = false)
  {
    if ($accessType == TournamentPrivileges::TYPE_EDIT && !$ignoreFinished && $this->isFinished) {
      return false;
    }
    if ($this->admin) {
      return true;
    }
    if (array_key_exists($accessType, $this->accessTypes)) {
      return $this->accessTypes[$accessType];
    }
    return false;
  }

  public function isAdmin()
  {
    return $this->admin;
  }
//</editor-fold desc="Public Methods">
}
