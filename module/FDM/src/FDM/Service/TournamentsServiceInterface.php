<?php

namespace FDM\Service;

interface TournamentsServiceInterface
{
//<editor-fold desc="Public Methods">
  /**
   * @param $id
   * @return mixed
   */
  public function getRanking($id);

  /**
   * @return \Doctrine\ORM\QueryBuilder
   */
  public function getTournaments($userId);
//</editor-fold desc="Public Methods">
}
