<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/8/16
 * Time: 8:25 PM
 */

namespace FDM\Service;


use Doctrine\ORM\EntityManager;
use FDM\Entity\Game;
use FDM\Entity\Group;
use FDM\Entity\Phase;
use FDM\Entity\Ranking;
use FDM\Entity\Tournament;
use FDM\Entity\TournamentRanking;
use FDM\Entity\TournamentRankingEntry;
use FDM\Entity\TournamentRankingInstance;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class TournamentGameRankingService extends TournamentRankingService
{
//<editor-fold desc="Fields">
  /**
   * @var ServiceLocatorInterface
   */
  protected $sl;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(EntityManager $em, LoadingService $lm, ServiceLocatorInterface $sl)
  {
    parent::__construct($em, $lm);
    $this->sl = $sl;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * @param Tournament $tournament
   * @param TournamentRanking $tournamentRanking
   * @return mixed
   */
  public function getTournamentTime($tournament, $tournamentRanking)
  {
    $this->lm->loadTournamentCompletely($tournament);

    $minDateTime = new \DateTime();

    foreach ($tournament->getPhases() as $phase) {
      foreach ($phase->getGroups() as $group) {
        /** @var Group $group */
        foreach ($group->getGames() as $game) {
          if ($game->getStartTime() !== null && $game->getStartTime() < $minDateTime) {
            $minDateTime = $game->getStartTime();
          }
        }
      }
    }
    return $minDateTime;
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   * @param TournamentRankingInstance $basedOn
   */
  protected function recomputeBasedOn($tournamentRankingInstance, $basedOn)
  {
    //reset tournamentRankingInstance to based on
    $this->cloneInto($tournamentRankingInstance, $basedOn);
    $tournaments = [];
    $tournamentIds = [];

    $tournamentRanking = $tournamentRankingInstance->getTournamentRanking();

    foreach ($tournamentRankingInstance->getTournamentRanking()->getTournaments() as $tournament) {
      if ($this->tournamentFinished($tournament)) {
        $tournaments[] = $tournament;
      }
    }

    $qb = $this->em->createQueryBuilder()
      ->select("g")
      ->from('FDM\Entity\Game', 'g')
      ->innerJoin('g.group', 'gr')
      ->innerJoin('gr.phase', 'p')
      ->innerJoin('p.tournament', 't')
      ->where('t.id IN (:ts)')->setParameter('ts', $tournaments)
      ->andWhere('g.startTime > :t')->setParameter('t', $basedOn->getLastEntryTime());

    if (!$tournamentRankingInstance->isCurrent()) {
      $qb->andWhere('g.startTime < :endT')->setParameter('endT', $tournamentRankingInstance->getLastEntryTime());
    }

    $games = $qb->getQuery()->getResult();

    $tournaments = [];

    foreach ($games as $game) {
      /** @var Game $game */
      $tournament = $game->getGroup()->getPhase()->getTournament();
      if (!array_key_exists($tournament->getId(), $tournamentIds)) {
        $tournamentIds[$tournament->getId()] = true;
        $tournaments[] = $tournament;
      }
    }

    $this->lm->loadTournamentsCompletely($tournaments);


    $modifiedEntries = $this->recomputeGames($tournamentRankingInstance, $games);

    foreach ($tournaments as $tournament) {
      $basedOnDate = null;
      if ($basedOn !== null) {
        $basedOnDate = $basedOn->getLastEntryTime();
      }
      $this->increaseTournaments($modifiedEntries, $tournament, $basedOnDate);
    }
  }

  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   * @param Tournament $tournament
   */
  protected function recomputeRankings($tournamentRankingInstance, $tournament)
  {
    $games = [];
    foreach ($tournament->getPhases() as $phase) {
      /** @var Phase $phase */
      foreach ($phase->getGroups() as $group) {
        /** @var Group $group */
        foreach ($group->getGames() as $game) {
          /** @var Game $game */
          if ($tournamentRankingInstance->isCurrent() ||
            $game->getStartTime() <= $tournamentRankingInstance->getLastEntryTime()
          )
            $games[] = $game;
        }
      }
    }
    $basedOnDate = $tournamentRankingInstance->getLastEntryTime();
    $entries = $this->recomputeGames($tournamentRankingInstance, $games);
    $this->increaseTournaments($entries, $tournament, $basedOnDate);
  }

  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   * @param Game $game
   * @param TournamentRankingEntry[] $entries
   */
  abstract protected function recomputeRankingsByGame($tournamentRankingInstance, $game, &$entries);
//</editor-fold desc="Protected Methods">

//<editor-fold desc="Private Methods">
  /**
   * @param Game $game
   * @return string
   */
  private function getGameText($game)
  {
    $res = $this->getTeamText($game->getTeamA());
    $res .= " - ";
    $res .= $this->getTeamText($game->getTeamB());
    $res .= " (" . $game->getResultA() . " - " . $game->getResultB() . ")";
    return $res;
  }

  /**
   * @param Ranking[] $team
   * @return string
   */
  private function getTeamText($team)
  {
    $first = true;
    $res = "";
    foreach ($team as $rank) {
      foreach ($rank->getAttendants() as $att) {
        if ($first) {
          $first = false;
        } else {
          $res .= " and ";
        }
        $res .= $att->getDisplayName();
      }
    }
    return $res;
  }

  private function increaseTournaments(&$entries, $tournament, $basedOnDate)
  {
    /** @var Tournament $tournament */
    $oldGameAttendantIds = [];
    if ($basedOnDate !== null) {
      foreach ($tournament->getPhases() as $phase) {
        /** @var Phase $phase */
        foreach ($phase->getGroups() as $group) {
          /** @var Group $group */
          foreach ($group->getGames() as $game) {
            /** @var Game $game */
            if ($game->getStartTime() <= $basedOnDate) {
              foreach ($game->getTeamA() as $ranking) {
                foreach ($ranking->getAttendants() as $att) {
                  $oldGameAttendantIds[$att->getId()] = true;
                }
              }
              foreach ($game->getTeamB() as $ranking) {
                foreach ($ranking->getAttendants() as $att) {
                  $oldGameAttendantIds[$att->getId()] = true;
                }
              }
            }
          }
        }
      }
    }
    foreach ($tournament->getAttendants() as $attendant) {
      if (!array_key_exists($attendant->getId(), $oldGameAttendantIds)) {
        $id = $attendant->getPlayer()->getId();
        if (array_key_exists($id, $entries)) {
          /** @var TournamentRankingEntry[] $entries */
          $entries[$id]->setTournaments($entries[$id]->getTournaments() + 1);
        }
      }
    }
  }

  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   * @param Game[] $games
   * @return array
   */
  private function recomputeGames($tournamentRankingInstance, &$games)
  {
    usort($games, function ($a, $b) {
      /** @var Game $a */
      /** @var Game $b */
      if ($a->getStartTime() < $b->getStartTime()) {
        return -1;
      } else if ($a->getStartTime() > $b->getStartTime()) {
        return 1;
      }
      $groupA = $a->getGroup();
      $phaseA = $groupA->getPhase();
      $tournamentA = $phaseA->getTournament();
      $groupB = $b->getGroup();
      $phaseB = $groupB->getPhase();
      $tournamentB = $phaseB->getTournament();

      if ($tournamentA->getStart() < $tournamentB->getStart()) {
        return -1;
      } else if ($tournamentB->getStart() < $tournamentA->getStart()) {
        return 1;
      }

      if ($phaseA->getPhaseNumber() < $phaseB->getPhaseNumber()) {
        return -1;
      } else if ($phaseB->getPhaseNumber() < $phaseA->getPhaseNumber()) {
        return 1;
      }

      $factor = 1;
      $service = $this->sl->get($groupA->getMode()->getServiceName());
      /** @var ModeServiceInterface $service */
      if ($service->roundsReversed()) {
        $factor = -1;
      }
      if ($groupA->getMode())

        if ($factor * $a->getRound() < $factor * $b->getRound()) {
        return -1;
        } else if ($factor * $b->getRound() < $factor * $a->getRound()) {
        return 1;
      }

      // to be deterministic compare ids for games in same rounds
      if ($a->getId() < $b->getId()) {
        return -1;
      } else if ($b->getId() < $a->getId()) {
        return 1;
      }

      //$a === $b
      return 0;
    });
    $entries = [];
    $tournamentId = null;
    foreach ($games as $game) {
      $t = $game->getGroup()->getPhase()->getTournament();
      if ($t->getId() !== $tournamentId) {
        $tournamentId = $t->getId();
        $this->log("Compute games from tournament " . $this->getTournamentIdentification($t));
      }
      $this->log("Compute ranking for game " . $this->getGameText($game));
      $this->recomputeRankingsByGame($tournamentRankingInstance, $game, $entries);
    }

    if ($tournamentRankingInstance->isCurrent() && count($games) > 0) {
      $lastTime = $games[count($games) - 1]->getStartTime();
      if ($tournamentRankingInstance->getLastEntryTime() < $lastTime) {
        $tournamentRankingInstance->setLastEntryTime($lastTime);
      }
    }

    return $entries;
  }
//</editor-fold desc="Private Methods">
}