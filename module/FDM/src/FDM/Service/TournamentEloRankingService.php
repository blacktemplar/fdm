<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/28/16
 * Time: 2:22 PM
 */

namespace FDM\Service;


use Doctrine\ORM\Query\Expr;
use FDM\Entity\Game;
use FDM\Entity\TournamentRankingEntry;
use FDM\Entity\TournamentRankingInstance;
use FDM\ZfcDatagrid\Column\Type\ArrayElement;
use FDM\ZfcDatagrid\Column\Type\Integer;
use ZfcDatagrid\Column;

class TournamentEloRankingService extends TournamentGameRankingService
{
  //TODO set this options not hard coded
//<editor-fold desc="Constants">
  const expDiff = 400;

  const k = 20;

  const noNeg = true;

  const numProvisoryGames = 20;

  const provisoryPartnerFactor = 0.5;

  const start = 1200;
//</editor-fold desc="Constants">

//<editor-fold desc="Public Methods">
  public function addColumns(&$columns, $ranking, $config)
  {
    $col = new Column\Select(new Expr\Select("ROUND(e.points)"), 'points');
    $col->setLabel('Points');
    $col->setType(new Integer(true));
    $columns[] = ["col" => $col, "weight" => 3.5, "default-on" => true];
    $hasJson = false;
    if (array_key_exists("fdm", $config) && array_key_exists("json-data-type", $config["fdm"])) {
      $hasJson = $config["fdm"]["json-data-type"] == 1;
    }
    if ($hasJson) {
      $col = new Column\Select(new Expr\Select("JSON_EXTRACT(e.subClassData, '$.games')"), 'games');
      $col->setType(new Integer());
    } else {
      $col = new Column\Select('subClassData', 'e');
      $col->setType(new ArrayElement("games"));
    }
    //$col->setType(new ArrayElement("games"));
    $col->setLabel('Games');
    $columns[] = ["col" => $col, "weight" => 3.75, "default-on" => true];
  }

  /**
   * @param TournamentRankingEntry $entry
   * @return float
   */
  public function getPoints($entry)
  {
    if ($entry->getGames() < 20) {
      return 0;
    }
    return $entry->getPoints();
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  /**
   * @param TournamentRankingEntry $a
   * @param TournamentRankingEntry $b
   * @return int
   */
  protected function compareEntries($a, $b)
  {
    $res = parent::compareEntries($a, $b);
    if ($res != 0) {
      return $res;
    }
    $aPoints = $a->getPoints();
    $bPoints = $b->getPoints();
    if ($aPoints == $bPoints) {
      return 0;
    }
    return $aPoints < $bPoints ? 1 : -1;
  }

  protected function getNewEntry($instance, $player)
  {
    $entry = new TournamentRankingEntry($instance, $player, ["games"]);
    $entry->setPoints(self::start);
    $entry->setTournaments(0);
    $entry->setGames(0);
    return $entry;
  }

  protected function isCommutative()
  {
    return false;
  }

  /**
   * @param TournamentRankingInstance $tournamentRankingInstance
   * @param Game $game
   * @param TournamentRankingEntry[] $entries
   */
  protected function recomputeRankingsByGame($tournamentRankingInstance, $game, &$entries)
  {
    if ($game->getResultA() !== $game->getResultB()) {
      //TODO distinguish unplayed games with real drawed games!

      /** @var TournamentRankingEntry[] $winPlayers */
      $winPlayers = [];
      /** @var TournamentRankingEntry[] $lostPlayers */
      $lostPlayers = [];
      $aArr = null;
      $bArr = null;
      if ($game->getResultA() > $game->getResultB()) {
        $aArr = &$winPlayers;
        $bArr = &$lostPlayers;
      } else {
        $aArr = &$lostPlayers;
        $bArr = &$winPlayers;
      }
      $hasProvisoryPlayers = false;
      foreach ($game->getTeamA() as $ranking) {
        foreach ($ranking->getAttendants() as $att) {
          $entry = $this->getOrAddPlayerEntry($tournamentRankingInstance, $att->getPlayer());
          $aArr[] = $entry;
          if ($entry->getGames() < self::numProvisoryGames) {
            $hasProvisoryPlayers = true;
          }
        }
      }
      foreach ($game->getTeamB() as $ranking) {
        foreach ($ranking->getAttendants() as $att) {
          $entry = $this->getOrAddPlayerEntry($tournamentRankingInstance, $att->getPlayer());
          $bArr[] = $entry;
          if ($entry->getGames() < self::numProvisoryGames) {
            $hasProvisoryPlayers = true;
          }
        }
      }

      $avWin = 0;
      $avLost = 0;

      foreach ($winPlayers as $entry) {
        $avWin += $entry->getPoints();
      }
      $avWin /= count($winPlayers);

      foreach ($lostPlayers as $entry) {
        $avLost += $entry->getPoints();
      }
      $avLost /= count($lostPlayers);

      $expectationWinner = 1 / (1 + 10 ** (($avLost - $avWin) / self::expDiff));
      $expectationLoser = 1 - $expectationWinner;
      //calculate

      foreach ($winPlayers as $entry) {
        $text = "Player " . $entry->getPlayer()->getFirstName() . " " . $entry->getPlayer()->getLastName() . " wins ";
        $points = "points";
        $old = $entry->getPoints();
        if ($entry->getGames() < self::numProvisoryGames) {
          //provisory entry
          //recalculate provisory
          if (count($winPlayers > 1)) {
            $teamMatesAV = ($avWin * count($winPlayers) - $entry->getPoints()) / (count($winPlayers) - 1);
            if ($teamMatesAV > $avLost + 400) {
              $teamMatesAV = $avLost + 400;
            }
            if ($teamMatesAV < $avLost - 400) {
              $teamMatesAV = $avLost - 400;
            }
            $myDiff = $avLost * (1 + self::provisoryPartnerFactor) - $teamMatesAV * self::provisoryPartnerFactor;
          } else {
            $myDiff = $avLost;
          }
          if ($myDiff < self::start) {
            $myDiff = self::start;
          }
          $myDiff += self::expDiff;
          $entry->setPoints(($entry->getPoints() * $entry->getGames() + $myDiff) / ($entry->getGames() + 1));
          $entry->setGames($entry->getGames() + 1);
          $points = "provisory " . $points;
        } else if (!$hasProvisoryPlayers) {
          $entry->setPoints($entry->getPoints() + self::k * (1 - $expectationWinner));
          $entry->setGames($entry->getGames() + 1);
        }
        $text .= ($entry->getPoints() - $old) . " " . $points;
        if (self::noNeg && $entry->getGames() >= 20 && $entry->getPoints() < self::start) {
          $entry->setPoints(self::start);
        }
        $text .= " (" . $old . " -> " . $entry->getPoints() . ")";
        $this->log($text);
        $entries[$entry->getPlayer()->getId()] = $entry;
      }

      foreach ($lostPlayers as $entry) {
        $text = "Player " . $entry->getPlayer()->getFirstName() . " " . $entry->getPlayer()->getLastName() . " looses ";
        $points = "points";
        $old = $entry->getPoints();
        if ($entry->getGames() < self::numProvisoryGames) {
          //provisory entry
          //recalculate provisory
          if (count($lostPlayers > 1)) {
            $teamMatesAV = ($avLost * count($lostPlayers) - $entry->getPoints()) / (count($lostPlayers) - 1);
            if ($teamMatesAV > $avWin + 400) {
              $teamMatesAV = $avWin + 400;
            }
            if ($teamMatesAV < $avWin - 400) {
              $teamMatesAV = $avWin - 400;
            }
            $myDiff = $avWin * (1 + self::provisoryPartnerFactor) - $teamMatesAV * self::provisoryPartnerFactor;
          } else {
            $myDiff = $avWin;
          }
          if ($myDiff < self::start) {
            $myDiff = self::start;
          }
          $myDiff -= self::expDiff;
          $entry->setPoints(($entry->getPoints() * $entry->getGames() + $myDiff) / ($entry->getGames() + 1));
          $entry->setGames($entry->getGames() + 1);
          $points = "provisory " . $points;
        } else if (!$hasProvisoryPlayers) {
          $entry->setPoints($entry->getPoints() + self::k * (0 - $expectationLoser));
          $entry->setGames($entry->getGames() + 1);
        }
        $text .= ($old - $entry->getPoints()) . " " . $points;
        if (self::noNeg && $entry->getGames() >= 20 && $entry->getPoints() < self::start) {
          $entry->setPoints(self::start);
        }
        $text .= " (" . $old . " -> " . $entry->getPoints() . ")";
        $this->log($text);
        $entries[$entry->getPlayer()->getId()] = $entry;
      }
    }
  }

//</editor-fold desc="Protected Methods">

//<editor-fold desc="Private Methods">
  private function addPoints($tournamentRankingInstance, $attendant, $points)
  {
    $player = $attendant->getPlayer();
    $entry = $this->getPlayerEntry($tournamentRankingInstance, $player);
    if ($entry === null) {
      $entry = $this->addEntry($tournamentRankingInstance, $player);
      $entry->setPoints(0);
      $entry->setTournaments(0);
    }
    $entry->setTournaments($entry->getTournaments() + 1);
    $entry->setPoints($entry->getPoints() + $points);
  }

  private function applyPoints($tournamentRankingInstance, &$sharedRankings, &$summedPoints, &$attendants)
  {
    $average = $summedPoints / count($sharedRankings);
    foreach ($sharedRankings as $r) {
      foreach ($r->getAttendants() as $attendant) {
        $this->addPoints($tournamentRankingInstance, $attendant, $average);
        $attendants[$attendant->getId()] = true;
      }
    }
    $summedPoints = 0;
    $sharedRankings = [];
  }
//</editor-fold desc="Private Methods">
}