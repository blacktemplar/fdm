<?php

namespace FDM\Service;

use FDM\Entity\Tournament;

interface TournamentNavigationServiceInterface
{
//<editor-fold desc="Public Methods">
  /**
   * Computes the navigation items for the given tournament
   * @param {Tournament} tournament current tournament
   * @returns                       array of navigation elements
   */
  public function getNavigation(Tournament $tournament);
//</editor-fold desc="Public Methods">
}
