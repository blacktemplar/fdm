<?php

namespace FDM\Service;

use FDM\Entity\Tournament;
use FDM\Entity\TournamentPrivileges;

class TournamentNavigationService
  implements TournamentNavigationServiceInterface
{
//<editor-fold desc="Fields">
  protected $tas;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    TournamentAccessServiceInterface $tas)
  {
    $this->tas = $tas;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getNavigation(Tournament $tournament)
  {
    $navigation = [];
    $navigation[] = [
      'label' => 'Go Back',
      'title' => 'Back to Main Menu',
      'route' => 'fdm',
    ];
    $navigation[] = [
      'label' => 'Attendants',
      'title' => 'Attendants List',
      'route' => 'tournament/attendants',
      'params' => [
        'id' => $tournament->getId(),
      ],
    ];
    if (!$tournament->isStarted()) {
      if ($this->tas->hasAccessType(
        TournamentPrivileges::TYPE_EDIT
      )
      ) {
        //add start tournament
        $navigation[] = [
          'label' => 'Start Tournament',
          'title' => 'Start Tournament',
          'route' => 'tournament/edit/start',
          'params' => [
            'id' => $tournament->getId(),
          ],
        ];
      }
    } else {
      $phases = $tournament->getPhasesByTemplate(false);
      ksort($phases);
      foreach ($phases as $phase) {
        $name = $phase->getName();
        if (!$name) {
          $name = "Phase " . $phase->getPhaseNumber();
        }
        $navigation[] = [
          'label' => $name,
          'title' => "View details of $name",
          'route' => 'tournament/phase',
          'params' => [
            'id' => $tournament->getId(),
            'phase' => $phase->getId(),
          ],
        ];
      }
      if ($this->tas->hasAccessType(TournamentPrivileges::TYPE_EDIT, true)) {
        $phaseStarted = false;
        foreach ($phases as $phase) {
          if ($phase->isStarted() || !$phase->getGroups()->isEmpty()) {
            $phaseStarted = true;
            break;
          }
        }
        if (!$phaseStarted) {
          $navigation[] = [
            'label' => 'Unstart Tournament',
            'title' => 'Unstart Tournament',
            'route' => 'tournament/edit/unstart',
            'params' => [
              'id' => $tournament->getId(),
            ],
          ];
        }
        if (!$tournament->isFinished()) {
          $allFinished = true;
          foreach ($tournament->getPhasesByTemplate(false) as $phase) {
            if (!$phase->isFinished()) {
              $allFinished = false;
            }
          }
          if ($allFinished) {
            $navigation[] = [
              'label' => 'Finish Tournament',
              'title' => 'Finish Tournament',
              'route' => 'tournament/edit/finish',
              'params' => [
                'id' => $tournament->getId(),
              ],
            ];
          }
        } else {
          $navigation[] = [
            'label' => 'Unfinish Tournament',
            'title' => 'Unfinish Tournament',
            'route' => 'tournament/edit/unfinish',
            'params' => [
              'id' => $tournament->getId(),
            ],
          ];
        }
      }
    }
    $navigation[] = [
      'label' => "Settings",
      'title' => "Settings for the Tournament",
      'route' => 'tournament/settings',
      'params' => [
        'id' => $tournament->getId()
      ],
    ];
    $navigation[] = [
      'label' => "Speaker",
      'title' => "Speaker for the Tournament",
      'route' => 'tournament/speaker',
      'target' => '_blank',
      'params' => [
        'id' => $tournament->getId()
      ],
    ];
    $navigation[] = [
      'label' => 'Local Settings',
      'route' => 'tournament/localsettings',
      'params' => [
        'id' => $tournament->getId(),
      ],
    ];
    if ($this->tas->hasAccessType(
      TournamentPrivileges::TYPE_EDIT
    )
    ) {
      $navigation[] = [
        'label' => 'Delete Tournament',
        'title' => 'Delete Tournament',
        'route' => 'tournament/edit/delete',
        'params' => [
          'id' => $tournament->getId(),
        ],
      ];
    }
    return $navigation;
  }
//</editor-fold desc="Public Methods">
}
