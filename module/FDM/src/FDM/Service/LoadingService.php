<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/6/16
 * Time: 10:09 AM
 */

namespace FDM\Service;

use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\ORM\EntityManager;
use FDM\Entity\Phase;
use FDM\Entity\Tournament;

class LoadingService
{
//<editor-fold desc="Constructor">
  public function __construct(
    EntityManager $em
  )
  {
    $this->em = $em;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function loadPhaseCompletely(Phase $phase)
  {
    if (!$this->isInitialized($phase->getModes())) {
      $builder = $this->getJoined('FDM\Entity\Phase', ["modes"]);
      $builder->where('t1.id = :phase')
        ->setParameter("phase", $phase);
      $builder->getQuery()->getResult();
    }
    if (!$this->gamesInitialized($phase)) {
      $builder = $this->getJoined('FDM\Entity\Group', ["games" => ["teamA", "teamB"]]);
      $builder->where('t1.phase = :phase')
        ->setParameter("phase", $phase);
      $builder->getQuery()->getResult();
    }
    if (!$this->rankingsInitialized($phase)) {
      $builder = $this->getJoined('FDM\Entity\Group', ["rankings" => ["gamesAsA", "gamesAsB", "attendants" => ["player"]]]);
      $builder->where('t1.phase = :phase')
        ->setParameter("phase", $phase);
      $builder->getQuery()->getResult();
    }
  }

  public function loadTournamentCompletely(Tournament $t)
  {
    if (!$this->attendantsInitialized($t)) {
      $builder = $this->getJoined('FDM\Entity\Tournament', ["attendants" => ["player"]]);
      $builder->where('t1.id = :t')
        ->setParameter("t", $t);
      $builder->getQuery()->getResult();
    }
    if (!$this->tournamentModesInitialized($t)) {
      $builder = $this->getJoined('FDM\Entity\Tournament', ["phases" => ["modes"]]);
      $builder->where('t1.id = :t')
        ->setParameter("t", $t);
      $builder->getQuery()->getResult();
    }
    if (!$this->tournamentGamesInitialized($t)) {
      $builder = $this->getJoined('FDM\Entity\Tournament', ["phases" => ["groups" => ["games" => ["teamA", "teamB"]]]]);
      $builder->where('t1.id = :t')
        ->setParameter("t", $t);
      $builder->getQuery()->getResult();
    }
    if (!$this->tournamentRankingsInitialized($t)) {
      $builder = $this->getJoined('FDM\Entity\Tournament', ["phases" => ["groups" => ["rankings" => ["gamesAsA", "gamesAsB", "attendants" => ["player"]]]]]);
      $builder->where('t1.id = :t')
        ->setParameter("t", $t);
      $builder->getQuery()->getResult();
    }
  }

  public function loadTournamentsCompletely($ts)
  {
    if (!$this->listInitialized($ts, "attendantsInitialized")) {
      $builder = $this->getJoined('FDM\Entity\Tournament', ["attendants" => ["player"]]);
      $builder->where('t1.id IN (:ts)')
        ->setParameter("ts", $ts);
      $builder->getQuery()->getResult();
    }
    if (!$this->listInitialized($ts, "tournamentModesInitialized")) {
      $builder = $this->getJoined('FDM\Entity\Tournament', ["phases" => ["modes"]]);
      $builder->where('t1.id IN (:ts)')
        ->setParameter("ts", $ts);
      $builder->getQuery()->getResult();
    }
    if (!$this->listInitialized($ts, "tournamentGamesInitialized")) {
      $builder = $this->getJoined('FDM\Entity\Tournament', ["phases" => ["groups" => ["games" => ["teamA", "teamB"]]]]);
      $builder->where('t1.id IN (:ts)')
        ->setParameter("ts", $ts);
      $builder->getQuery()->getResult();
    }
    if (!$this->listInitialized($ts, "tournamentRankingsInitialized")) {
      $builder = $this->getJoined('FDM\Entity\Tournament', ["phases" => ["groups" => ["rankings" => ["gamesAsA", "gamesAsB", "attendants" => ["player"]]]]]);
      $builder->where('t1.id IN (:ts)')
        ->setParameter("ts", $ts);
      $builder->getQuery()->getResult();
    }
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  /**
   * @param QueryBuilder $builder
   * @param $baseName
   * @param $joinStructure
   */
  private function addJoins($builder, $baseNumber, $joinStructure)
  {
    $res = $baseNumber;
    foreach ($joinStructure as $key => $sub) {
      $prop = $key;
      if (is_string($sub)) {
        $prop = $sub;
      }
      $res++;
      $builder->addSelect("t" . (string)$res);
      $builder->leftJoin("t" . (string)$baseNumber . "." . $prop, "t" . (string)$res);
      if (is_array($sub)) {
        $res = $this->addJoins($builder, $res, $sub);
      }
    }
    return $res;
  }

  private function attendantsInitialized($t)
  {
    if (!$this->isInitialized($t->getAttendants())) {
      return false;
    }
    foreach ($t->getAttendants() as $attendant) {
      if (!$this->isInitialized($attendant)) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param Phase $phase
   * @return bool
   */
  private function gamesInitialized($phase)
  {
    if (!$this->isInitialized($phase->getGroups())) {
      return false;
    }
    foreach ($phase->getGroups() as $group) {
      if (!$this->isInitialized($group->getGames())) {
        return false;
      }
      foreach ($group->getGames() as $game) {
        if (!$this->isInitialized($game->getTeamA()) || !$this->isInitialized($game->getTeamB())) {
          return false;
        }
      }
    }
    return true;
  }

  private function getJoined($entityType, $joinStructure)
  {
    $builder = $this->em->createQueryBuilder()
      ->select("t1")
      ->from($entityType, "t1");
    $this->addJoins($builder, 1, $joinStructure);
    return $builder;
  }

  private function isInitialized($object)
  {
    if ($object instanceof AbstractLazyCollection) {
      return $object->isInitialized();
    }
    return !property_exists($object, '__isInitialized__');
  }

  private function listInitialized($ts, $check)
  {
    foreach ($ts as $t) {
      if (!$this->$check($t)) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param Phase $phase
   * @return bool
   */
  private function rankingsInitialized($phase)
  {
    if (!$this->isInitialized($phase->getGroups())) {
      return false;
    }
    foreach ($phase->getGroups() as $group) {
      if (!$this->isInitialized($group->getRankings())) {
        return false;
      }
      foreach ($group->getRankings() as $ranking) {
        if (!$this->isInitialized($ranking->getGamesAsA()) || !$this->isInitialized($ranking->getGamesAsB()) ||
          !$this->isInitialized($ranking->getAttendants())
        ) {
          return false;
        }
        foreach ($ranking->getAttendants() as $attendant) {
          if (!$this->isInitialized($attendant)) {
            return false;
          }
        }
      }
    }
    return true;
  }

  private function tournamentGamesInitialized($t)
  {
    if (!$this->isInitialized($t->getPhases())) {
      return false;
    }
    foreach ($t->getPhases() as $phase) {
      if (!$this->gamesInitialized($phase)) {
        return false;
      }
    }
    return true;
  }

  private function tournamentModesInitialized($t)
  {
    if (!$this->isInitialized($t->getPhases())) {
      return false;
    }
    foreach ($t->getPhases() as $phase) {
      if (!$this->isInitialized($phase->getModes())) {
        return false;
      }
    }
    return true;
  }

  private function tournamentRankingsInitialized($t)
  {
    if (!$this->isInitialized($t->getPhases())) {
      return false;
    }
    foreach ($t->getPhases() as $phase) {
      if (!$this->rankingsInitialized($phase)) {
        return false;
      }
    }
    return true;
  }
//</editor-fold desc="Private Methods">
}