<?php

namespace FDM\Service;

interface TournamentAccessServiceInterface
{
//<editor-fold desc="Public Methods">
  /**
   * @return boolean
   */
  public function hasAccessType($accessType, $ignoreFinished = false);

  public function isAdmin();
//</editor-fold desc="Public Methods">
}
