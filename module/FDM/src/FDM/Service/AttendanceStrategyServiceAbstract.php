<?php

namespace FDM\Service;

use FDM\Entity\AttendanceStrategy;

abstract class AttendanceStrategyServiceAbstract
  implements AttendanceStrategyServiceInterface
{
//<editor-fold desc="Fields">
  protected $aStrat;

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  /**
   * sets the entity object mode for this service
   */
  public function setAttendanceStrategy(
    AttendanceStrategy $attendanceStrategy
  )
  {
    $this->aStrat = $attendanceStrategy;
  }
//</editor-fold desc="Public Methods">
}
