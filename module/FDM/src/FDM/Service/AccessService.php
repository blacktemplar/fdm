<?php

namespace FDM\Service;

use FDM\Entity\User;

class AccessService
{
//<editor-fold desc="Fields">
  protected $type;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  /**
   * AccessService constructor.
   * @param User $user
   */
  public function __construct($user)
  {
    if ($user === null) {
      $this->type = User::TYPE_NOT_LOGGED_IN;
    }
    $this->type = $user->getType();
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">

  public function isAdmin()
  {
    return $this->type == User::TYPE_ADMIN;
  }
//</editor-fold desc="Public Methods">
}
