<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/28/16
 * Time: 3:03 PM
 */

namespace FDM\Service;


use FDM\Entity\Tournament;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentService
{
//<editor-fold desc="Fields">
  protected $sl;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(ServiceLocatorInterface $sl)
  {
    $this->sl = $sl;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * @param Tournament $tournament
   */
  public function applyTournamentRankings($tournament)
  {
    foreach ($tournament->getTournamentRankings() as $ranking) {
      /** @var TournamentRankingService $service */
      $service = $this->sl->get($ranking->getServiceName());
      $service->addTournament($ranking, $tournament);
    }
  }

  public function undoTournamentRankings($tournament)
  {
    foreach ($tournament->getTournamentRankings() as $ranking) {
      /** @var TournamentRankingService $service */
      $service = $this->sl->get($ranking->getServiceName());
      $service->removeTournament($ranking, $tournament);
    }
  }
//</editor-fold desc="Public Methods">
}