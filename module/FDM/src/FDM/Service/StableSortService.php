<?php
namespace FDM\Service;

class StableSortService
{
//<editor-fold desc="Public Methods">
  public function arsort(&$array)
  {
    $temp = [];
    $i = 0;
    foreach ($array as $key => $value) {
      $temp[] = [$i, $key, $value];
      $i++;
    }
    uasort(
      $temp,
      function ($a, $b) {
        return (
        $a[2] == $b[2] ? ($a[0] > $b[0]) : ($a[2] < $b[2])
        ) ? 1 : -1;
      }
    );
    $array = [];
    foreach ($temp as $val) {
      $array[$val[1]] = $val[2];
    }
  }
//</editor-fold desc="Public Methods">
}
