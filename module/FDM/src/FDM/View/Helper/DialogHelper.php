<?php
namespace FDM\View\Helper;

use Zend\View\Helper\AbstractHelper;

class DialogHelper extends AbstractHelper
{
//<editor-fold desc="Public Methods">
  public function __invoke($id, $text, $title = "")
  {
    $text = $this->view->translate($text);
    if ($title !== "") {
      $title = $this->view->translate($title);
    }
    if ($this->view->deviceHelper() !== 'phone') {
      return <<<HTML
<script>
$(document).ready(function() {
    if(jQuery("#$id").length === 0) {
        jQuery(document.body).append(
            '<div id="$id" title="$title">' +
                '<p>' +
                    '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' +
                    '$text' +
                '</p>' +
            '</div>'
        );
    }
    $("#$id").dialog({autoOpen: false });
});
</script>
HTML;
    } else {
      return <<<HTML
<script>
$(document).ready(function() {
    if(jQuery("#$id").length === 0) {
        jQuery(document.body).append(
            '<div data-role="dialog" id="$id">' +
                '<div data-role="header">' +
                    '<h1 id="$id-title">$title</h1>' +
                '</div>' +
                '<div role="main" class="ui-content dialog">' +
                    '<p>$text</p>' +
                    '<a data-role="button" href="#" data-rel="back" id="$id-yes"> Yes </a>' +
                    '<a data-role="button" href="#" data-rel="back"> No </a>' +
                '</div>' +
            '</div>'
        );
    }
});
</script>
HTML;
    }
  }
//</editor-fold desc="Public Methods">
}
