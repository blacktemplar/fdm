<?php
namespace FDM\View\Helper;

use Detection\MobileDetect;
use Zend\View\Helper\AbstractHelper;

class DeviceHelper extends AbstractHelper
{
//<editor-fold desc="Public Methods">
  public function __invoke()
  {
    $detect = new MobileDetect;
    return $detect->isMobile() ?
      ($detect->isTablet() ? 'tablet' : 'phone') : 'computer';
  }
//</editor-fold desc="Public Methods">
}
