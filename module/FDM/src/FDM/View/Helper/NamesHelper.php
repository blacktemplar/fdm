<?php
namespace FDM\View\Helper;

use Doctrine\Common\Collections\Collection;
use FDM\Entity\Attendant;
use FDM\Entity\Game;
use FDM\Entity\Group;
use FDM\Entity\Ranking;
use FDM\Entity\TournamentRankingEntry;
use FDM\Service\TournamentRankingService;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

class NamesHelper extends AbstractHelper
{
//<editor-fold desc="Fields">
  private static $eloList = null;

  private static $showElo = false; //todo make this dynamic
  private $sl;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(ServiceLocatorInterface $sl)
  {
    $this->sl = $sl;
  }
//</editor-fold desc="Constructor">


//<editor-fold desc="Public Methods">
  /**
   * @param {Attendant|Ranking|Collection|Group|Game} team
   * @return {string}    the name represantation of team
   * @throws \Exception  team is not valid for this function
   */
  public function __invoke($team, $connectives = true, $spoken = false)
  {
    $middle = " ";
    if ($connectives) {
      $middle = " " . $this->view->translate('and') . " ";
    }
    if ($team instanceof Attendant) {
      /** @var Attendant $team */
      $tmp = $team->getDisplayName();
      if ($spoken) {
        if ($team->getGeneratedDisplayName() == $tmp && $tmp != $team->getFirstName() && $tmp != $team->getLastName()) {
          $tmp = $team->getLastName() . " " . $team->getFirstName();
        }
        if ($team->getSpelledFirstName() != "" && $team->getFirstName() != "") {
          $tmp = str_replace($team->getFirstName(), $team->getSpelledFirstName(), $tmp);
        }
        if ($team->getSpelledLastName() != "" && $team->getLastName() != "") {
          $tmp = str_replace($team->getLastName(), $team->getSpelledLastName(), $tmp);
        }
      }

      if (self::$showElo) {
        if (self::$eloList === null) {
          self::$eloList = [];
          $em = $this->sl->get('Doctrine\ORM\EntityManager');
          $attendantIds = [];
          foreach ($team->getTournament()->getAttendants() as $attendant) {
            $attendantIds[] = $attendant->getPlayer()->getId();
          }
          $mainRanking = $this->sl->get('Config')["fdm"]["mainRanking"];
          $ranking = $em->find('FDM\Entity\TournamentRanking', $mainRanking);
          if ($ranking !== null) {
            $instance = $em->createQueryBuilder()
              ->select("i")
              ->from('FDM\Entity\TournamentRankingInstance', 'i')
              ->where('i.tournamentRanking = :id AND i.current = 1')
              ->setParameter("id", $mainRanking)
              ->getQuery()->getResult();
            if (count($instance) > 0) {
              $elos = $em->createQueryBuilder()
                ->select("e")
                ->from('FDM\Entity\TournamentRankingEntry', 'e')
                ->where('e.tournamentRankingInstance = :inst AND e.player IN (:atts)')
                ->setParameter("inst", $instance[0])
                ->setParameter("atts", $attendantIds)
                ->getQuery()->getResult();
              $eloList = [];
              /** @var TournamentRankingService $service */
              $service = $this->sl->get($ranking->getServiceName());
              foreach ($elos as $elo) {
                /** @var TournamentRankingEntry $elo */
                self::$eloList[$elo->getPlayer()->getId()] = $service->getPoints($elo);
              }
            }
          }
        }


        $elo = 0;
        if (array_key_exists($team->getPlayer()->getId(), self::$eloList)) {
          $elo = round(self::$eloList[$team->getPlayer()->getId()]);
        }
        $tmp .= ' (' . (string)($elo) . ')';
      }
      return $tmp;
    } else if ($team instanceof Ranking) {
      $name = "";
      $begin = true;
      if ($middle == " " && $spoken) {
        $middle = ", ";
      }
      foreach ($team->getAttendants() as $att) {
        if ($begin) {
          $begin = false;
        } else {
          $name .= $middle;
        }
        $name .= $this($att, $connectives, $spoken);
      }
      return $name;
    } else if ($team instanceof Collection) {
      $name = "";
      $begin = true;
      foreach ($team as $ranking) {
        if ($begin) {
          $begin = false;
        } else {
          $name .= $middle;
        }
        $name .= $this($ranking, $connectives, $spoken);
      }
      return $name;
    } else if ($team instanceof Group) {
      if ($team->getName()) {
        return $team->getName();
      }
      return $this->view->translate("Group") . " "
      . $team->getGroupNumber();
    } else if ($team instanceof Game) {
      //TODO: translate (be aware of speak !!!)
      return $this($team->getTeamA(), $connectives, $spoken) . " gegen " .
      $this($team->getTeamB(), $connectives, $spoken);
    }

    throw new \Exception("Unexpected object given to NamesHelper!");
  }
//</editor-fold desc="Public Methods">
}
