<?php

namespace FDM\View;

use ZfcDatagrid\Column\Type\DateTime;


class MyDateTimeColumn extends DateTime
{
//<editor-fold desc="Constructor">
  public function __construct()
  {
    parent::__construct('Y-m-d H:i', NULL, NULL);
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /** @noinspection PhpMissingParentCallCommonInspection */
  public function getUserValue($val)
  {
    if ('' == $val) {
      return '';
    }

    return date_format($val, 'Y-m-d H:i:s');
  }
//</editor-fold desc="Public Methods">
}