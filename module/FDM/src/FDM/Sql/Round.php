<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/29/16
 * Time: 3:07 PM
 */

namespace FDM\Sql;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;

class Round extends FunctionNode
{
//<editor-fold desc="Fields">
  private $arithmeticExpression;

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  public function getSql(SqlWalker $sqlWalker)
  {

    return 'ROUND(' . $sqlWalker->walkSimpleArithmeticExpression(
      $this->arithmeticExpression
    ) . ')';
  }

  public function parse(\Doctrine\ORM\Query\Parser $parser)
  {

    $lexer = $parser->getLexer();

    $parser->match(Lexer::T_IDENTIFIER);
    $parser->match(Lexer::T_OPEN_PARENTHESIS);

    $this->arithmeticExpression = $parser->SimpleArithmeticExpression();

    $parser->match(Lexer::T_CLOSE_PARENTHESIS);
  }
//</editor-fold desc="Public Methods">
}