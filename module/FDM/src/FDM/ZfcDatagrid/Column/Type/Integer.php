<?php

/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/8/16
 * Time: 6:42 PM
 */

namespace FDM\ZfcDatagrid\Column\Type;

class Integer extends MyNumber
{
//<editor-fold desc="Fields">
  private $ignoreFormatter;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($ignoreFormatter = false)
  {
    parent::__construct();
    $this->ignoreFormatter = $ignoreFormatter;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * Convert the user value to a general value, which will be filtered
   *
   * @param  string $val
   * @return string
   */
  public function getFilterValue($val)
  {
    return (int)$val;
  }

  public function getTypeName()
  {
    return "integer";
  }

  public function getUserValue($val)
  {
    if (!$this->ignoreFormatter) {
      return parent::getUserValue($val);
    }

    return (string)$this->getPrefix() . $val . $this->getSuffix();
  }
//</editor-fold desc="Public Methods">
}