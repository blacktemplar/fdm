<?php

namespace FDM\ZfcDatagrid\Column\Type;

use ZfcDatagrid\Column\Type\AbstractType;
use ZfcDatagrid\Filter;

class ArrayElement extends AbstractType
{
//<editor-fold desc="Fields">
  private $key;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($key)
  {
    $this->key = $key;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getFilterDefaultOperation()
  {
    return Filter::LIKE;
  }

  /**
   *
   * @param  string $val
   * @return string
   */
  public function getFilterValue($val)
  {
    return $this->getUserValue($val);
  }

  public function getTypeName()
  {
    return 'arrayElement';
  }

  /**
   * Access the key of the array
   *
   * @param  mixed $value
   * @return array
   */
  public function getUserValue($value)
  {
    if (!is_array($value)) {
      return $value;
    }
    if (!array_key_exists($this->key, $value)) {
      return null;
    }
    return $value[$this->key];
  }
//</editor-fold desc="Public Methods">
}