<?php

/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/8/16
 * Time: 6:42 PM
 */

namespace FDM\ZfcDatagrid\Column\Type;

use Locale;
use NumberFormatter;
use ZfcDatagrid\Column\Type\AbstractType;
use ZfcDatagrid\Filter;

class MyNumber extends AbstractType
{
//<editor-fold desc="Fields">
  protected $attributes = [];

  /**
   * NumberFormat style to use.
   *
   * @var int
   */
  protected $formatStyle;

  /**
   * NumberFormat type to use.
   *
   * @var int
   */
  protected $formatType;

  /**
   * Locale to use instead of the default.
   *
   * @var string
   */
  protected $locale;

  protected $pattern;

  protected $prefix = '';

  protected $suffix = '';

  private $roundPrecision;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($formatStyle = NumberFormatter::DECIMAL, $formatType = NumberFormatter::TYPE_DEFAULT, $locale = null, $roundPrecision = 2)
  {
    $this->setFormatStyle($formatStyle);
    $this->setFormatType($formatType);
    $this->setLocale($locale);
    $this->roundPrecision = 2;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  /**
   * Set an attribute.
   *
   * @link http://www.php.net/manual/en/numberformatter.setattribute.php
   *
   * @param
   *            attr int <p>
   *            Attribute specifier - one of the
   *            numeric attribute constants.
   *            </p>
   * @param
   *            value int <p>
   *            The attribute value.
   *            </p>
   */
  public function addAttribute($attr, $value)
  {
    $this->attributes[] = [
      'attribute' => $attr,
      'value' => $value,
    ];
  }

  public function getAttributes()
  {
    return $this->attributes;
  }

  public function getFilterDefaultOperation()
  {
    return Filter::EQUAL;
  }

  /**
   * @param string $val
   *
   * @return string
   */
  public function getFilterValue($val)
  {
    $formatter = $this->getFormatter();

    if (strlen($this->getPrefix()) > 0 && strpos($val, $this->getPrefix()) === 0) {
      $val = substr($val, strlen($this->getPrefix()));
    }
    if (strlen($this->getSuffix()) > 0 && strpos($val, $this->getSuffix()) > 0) {
      $val = substr($val, 0, -strlen($this->getSuffix()));
    }

    try {
      $formattedValue = $formatter->parse($val);
    } catch (\Exception $e) {
      return $val;
    }

    if (false === $formattedValue) {
      return $val;
    }

    return $formattedValue;
  }

  public function getFormatStyle()
  {
    return $this->formatStyle;
  }

  public function getFormatType()
  {
    return $this->formatType;
  }

  public function getLocale()
  {
    if (null === $this->locale) {
      $this->locale = Locale::getDefault();
    }

    return $this->locale;
  }

  public function getPattern()
  {
    return $this->pattern;
  }

  public function getPrefix()
  {
    return $this->prefix;
  }

  public function getSuffix()
  {
    return $this->suffix;
  }

  public function getTypeName()
  {
    return 'number';
  }

  /**
   * Convert the value from the source to the value, which the user will see.
   *
   * @param string $val
   *
   * @return string
   */
  public function getUserValue($val)
  {
    if ($this->roundPrecision !== null) {
      $val = round($val, $this->roundPrecision);
    }
    $formatter = $this->getFormatter();

    $formattedValue = $formatter->format($val, $this->getFormatType());

    return (string)$this->getPrefix() . $formattedValue . $this->getSuffix();
  }

  public function setFormatStyle($style = NumberFormatter::DECIMAL)
  {
    $this->formatStyle = $style;
  }

  public function setFormatType($type = NumberFormatter::TYPE_DEFAULT)
  {
    $this->formatType = $type;
  }

  public function setLocale($locale = null)
  {
    $this->locale = $locale;
  }

  public function setPattern($pattern)
  {
    $this->pattern = $pattern;
  }

  public function setPrefix($string = '')
  {
    $this->prefix = (string)$string;
  }

  public function setSuffix($string = '')
  {
    $this->suffix = (string)$string;
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  /**
   * @return NumberFormatter
   */
  protected function getFormatter()
  {
    $formatter = new NumberFormatter($this->getLocale(), $this->getFormatStyle());
    if ($this->getPattern() !== null) {
      $formatter->setPattern($this->getPattern());
    }
    foreach ($this->getAttributes() as $attribute) {
      $formatter->setAttribute($attribute['attribute'], $attribute['value']);
    }

    return $formatter;
  }
//</editor-fold desc="Protected Methods">
}
