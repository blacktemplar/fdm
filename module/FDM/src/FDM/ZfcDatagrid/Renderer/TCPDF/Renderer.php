<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/8/16
 * Time: 7:21 AM
 */

namespace FDM\ZfcDatagrid\Renderer\TCPDF;


use FDM\ZfcDatagrid\TCPDF\TCPDF;

class Renderer extends \ZfcDatagrid\Renderer\TCPDF\Renderer
{
//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->allowedColumnTypes[] = 'FDM\ZfcDatagrid\Column\Type\ArrayElement';
    $this->allowedColumnTypes[] = 'FDM\ZfcDatagrid\Column\Type\Integer';
    $this->allowedColumnTypes[] = 'FDM\ZfcDatagrid\Column\Type\MyNumber';
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Protected Methods">
  protected function createTCPDF($orientation, $papersize)
  {
    $lastRefreshed = null;
    $jackpot = null;
    if (array_key_exists('lastRefreshed', $this->toolbarTemplateVariables)) {
      $lastRefreshed = $this->toolbarTemplateVariables['lastRefreshed'];
    }
    if (array_key_exists('hasJackpot', $this->toolbarTemplateVariables) && $this->toolbarTemplateVariables['hasJackpot']) {
      $jackpot = $this->toolbarTemplateVariables['jackpot'];
    }
    return new TCPDF($this->translator, $lastRefreshed, $jackpot, $orientation, 'mm', $papersize);
  }
//</editor-fold desc="Protected Methods">
}