<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/8/16
 * Time: 7:21 AM
 */

namespace FDM\ZfcDatagrid\Renderer\PHPExcel;

use FDM\ZfcDatagrid\Column\Type\ArrayElement;
use FDM\ZfcDatagrid\Column\Type\Integer;

class Renderer extends \ZfcDatagrid\Renderer\PHPExcel\Renderer
{
//<editor-fold desc="Constructor">
  public function __construct()
  {
    $this->allowedColumnTypes[] = ArrayElement::class;
    $this->allowedColumnTypes[] = Integer::class;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Protected Methods">
  /**
   * @param \PHPExcel_Worksheet $sheet
   */
  protected function setHeaderFooter(\PHPExcel_Worksheet $sheet)
  {
    parent::setHeaderFooter($sheet);

    if (array_key_exists('lastRefreshed', $this->toolbarTemplateVariables)) {
      $lastRefreshed = $this->toolbarTemplateVariables['lastRefreshed'];
      $sheet->getHeaderFooter()->setOddHeader($sheet->getHeaderFooter()->getOddHeader() . '&R&8' .
        $this->translate('Updated') . ':' . $lastRefreshed->format('d.m.Y'));
    }

  }
//</editor-fold desc="Protected Methods">
}