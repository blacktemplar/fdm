<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 11/16/16
 * Time: 4:53 PM
 */

namespace FDM\ZfcDatagrid\TCPDF;


class TCPDF extends \TCPDF
{
//<editor-fold desc="Fields">
  private $jackpot;

  private $lastRefreshed;

  private $translator;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct($translator, $lastRefreshed = null, $jackpot = null, $orientation = 'P', $unit = 'mm', $format = 'A4')
  {
    $this->lastRefreshed = $lastRefreshed;
    parent::__construct($orientation, $unit, $format);
    $this->translator = $translator;
    $this->jackpot = $jackpot;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function Footer()
  {
    // Position at 15 mm from bottom
    $this->SetY(-15);
    // Set font
    $this->SetFont('helvetica', 'I', 8);
    // Page number
    $this->Cell(0, 10, $this->translate_string('Page') . ' ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
  }

  // Page footer

  public function Header()
  {
    // Title
    $header_data = $this->getHeaderData();
    $this->SetY(10);
    $this->SetFont('helvetica', '', 12);
    if ($this->jackpot !== null) {
      $this->Cell(0.1, 10, "Jackpot: " . str_replace(",", ".", number_format($this->jackpot)) . "€", 0, false, 'L', 0, '', 0, false, 'M', 'M');
    }
    $this->SetFont('Helvetica', 'B', '15');
    $this->Cell(0, 15, $header_data['title'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
    if ($this->lastRefreshed != null) {
      $this->SetFont('helvetica', '', 8);
      $this->Cell(0, 10, $this->translate_string('Updated') . ': ' . $this->lastRefreshed->format('d.m.Y'), 0, false, 'R', 0, '', 0, false, 'M', 'M');
    }
  }
//</editor-fold desc="Public Methods">

  //Page header

//<editor-fold desc="Private Methods">
  private function translate_string($string)
  {
    return $this->translator ? $this->translator->translate($string) : $string;
  }
//</editor-fold desc="Private Methods">
}