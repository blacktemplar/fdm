<?php

namespace FDM\Factory;

use FDM\Entity\TournamentPrivileges;
use Interop\Container\ContainerInterface;
use Zend\Navigation\Service\DefaultNavigationFactory;

class NavigationFactory extends DefaultNavigationFactory
{
//<editor-fold desc="Protected Methods">
  /** @noinspection PhpMissingParentCallCommonInspection */
  protected function getPages(ContainerInterface $containerInterface)
  {
    $mvcEvent = $containerInterface->get('Application')->getMvcEvent();
    $routeMatch = $mvcEvent->getRouteMatch();
    $userId = null;
    $auth = $containerInterface->get('zfcuser_auth_service');
    if ($auth->hasIdentity()) {
      $userId = $auth->getIdentity()->getId();
    }
    $router = $mvcEvent->getRouter();
    $navigation = [];
    if ($userId === null) {
      $navigation[] = [
        'label' => 'Sign In',
        'module' => 'ZfcUser',
        'route' => 'zfcuser/login',
      ];
      $navigation[] = [
        'label' => 'Register',
        'route' => 'zfcuser/register',
      ];
    } else {
      $navigation[] = [
        'label' => 'Sign Out',
        'route' => 'zfcuser/logout',
      ];
      $navigation[] = [
        'label' => 'Change my Password',
        'route' => 'zfcuser/changepassword',
      ];
    }
    if ($routeMatch !== null &&
      (
        $routeMatch->getMatchedRouteName() === "tournament" ||
        substr($routeMatch->getMatchedRouteName(), 0, 11) === "tournament/"
      )
      && $containerInterface->get(
        'FDM\Service\TournamentAccessServiceInterface'
      )->hasAccessType(TournamentPrivileges::TYPE_READ)
    ) {
      $tId = $routeMatch->getParam('id');
      $em = $containerInterface->get('Doctrine\ORM\EntityManager');
      $tournament = $em->find('FDM\Entity\Tournament', $tId);
      $navigationService = $containerInterface->get(
        'FDM\Service\TournamentNavigationServiceInterface'
      );
      $navigation = array_merge(
        $navigation, $navigationService->getNavigation($tournament)
      );

    } else {
      $navigation[] = [
        'label' => 'Tournaments',
        'route' => 'fdm',
      ];
      $navigation[] = [
        'label' => 'Templates',
        'route' => 'templates',
      ];
      $navigation[] = [
        'label' => 'Rankings',
        'route' => 'rankings',
      ];
    }
    foreach ($navigation as &$nav) {
      $nav['rel'] = ['external'];
    }
    $pages = $this->getPagesFromConfig($navigation);
    $this->pages = $this->injectComponents(
      $pages,
      $routeMatch,
      $router
    );
    return $this->pages;
  }
//</editor-fold desc="Protected Methods">
}
