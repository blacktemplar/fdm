<?php

namespace FDM\Factory;

use FDM\Service\TournamentNavigationService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentNavigationServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $tas = $serviceLocator->get(
      'FDM\Service\TournamentAccessServiceInterface'
    );

    return new TournamentNavigationService($tas);
  }
//</editor-fold desc="Public Methods">
}
