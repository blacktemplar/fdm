<?php

namespace FDM\Factory;

use FDM\Controller\TournamentsController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentsControllerFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $realServiceLocator = $serviceLocator->getServiceLocator();
    $tournamentsService =
      $realServiceLocator->get('FDM\Service\TournamentsServiceInterface');

    $renderer =
      $realServiceLocator->get('Zend\View\Renderer\RendererInterface');
    $translator =
      $realServiceLocator->get('translator');
    $grid = $realServiceLocator->get('ZfcDatagrid\Datagrid');


    return new
    TournamentsController($tournamentsService, $renderer, $translator, $grid);
  }
//</editor-fold desc="Public Methods">
}
