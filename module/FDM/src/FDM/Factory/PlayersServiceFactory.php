<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/16/16
 * Time: 4:27 PM
 */

namespace FDM\Factory;


use FDM\Service\PlayersService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PlayersServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $em =
      $serviceLocator->get('Doctrine\ORM\EntityManager');
    $ls =
      $serviceLocator->get('FDM\Service\LoadingService');
    $tss = $serviceLocator->get('FDM\Service\TournamentsServiceInterface');
    return new PlayersService($em, $ls, $tss);
  }
//</editor-fold desc="Public Methods">
}
