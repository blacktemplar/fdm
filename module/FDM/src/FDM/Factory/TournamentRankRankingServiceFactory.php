<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/17/16
 * Time: 8:20 AM
 */

namespace FDM\Factory;


use FDM\Service\TournamentRankRankingService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentRankRankingServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $em = $serviceLocator->get('Doctrine\ORM\EntityManager');
    $lm = $serviceLocator->get('FDM\Service\LoadingService');
    return new TournamentRankRankingService($em, $lm);
  }
//</editor-fold desc="Public Methods">
}
