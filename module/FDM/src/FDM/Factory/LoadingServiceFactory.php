<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/6/16
 * Time: 10:12 AM
 */

namespace FDM\Factory;


use FDM\Service\LoadingService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoadingServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $em =
      $serviceLocator->get('Doctrine\ORM\EntityManager');
    return new LoadingService($em);
  }
//</editor-fold desc="Public Methods">
}