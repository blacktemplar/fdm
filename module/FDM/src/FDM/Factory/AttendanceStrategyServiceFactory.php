<?php

namespace FDM\Factory;

use FDM\Service\AttendanceStrategyService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AttendanceStrategyServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    return new AttendanceStrategyService($serviceLocator);
  }
//</editor-fold desc="Public Methods">
}
