<?php

namespace FDM\Factory;

use FDM\Service\FlushService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FlushServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $em =
      $serviceLocator->get('Doctrine\ORM\EntityManager');
    $tas = $serviceLocator->get(
      'FDM\Service\TournamentAccessServiceInterface'
    );
    if ($tas === null) {
      return null;
    }

    return new FlushService($em, $tas);
  }
//</editor-fold desc="Public Methods">
}
