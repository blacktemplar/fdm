<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/7/16
 * Time: 8:07 PM
 */

namespace FDM\Factory;


use FDM\View\Helper\NamesHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class NamesHelperFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    return new NamesHelper($serviceLocator->getServiceLocator());
  }
//</editor-fold desc="Public Methods">
}