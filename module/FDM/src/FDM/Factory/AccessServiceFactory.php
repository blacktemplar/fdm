<?php

namespace FDM\Factory;

use FDM\Service\AccessService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AccessServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $auth = $serviceLocator->get('zfcuser_auth_service');
    $user = null;
    if ($auth->hasIdentity()) {
      $user = $auth->getIdentity();
    }
    return new AccessService($user);
  }
//</editor-fold desc="Public Methods">
}
