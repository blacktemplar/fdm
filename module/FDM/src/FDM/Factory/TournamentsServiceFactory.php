<?php

namespace FDM\Factory;

use FDM\Service\TournamentsService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentsServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
    $translator = $serviceLocator->get('translator');

    return new TournamentsService($serviceLocator, $entityManager, $translator);
  }
//</editor-fold desc="Public Methods">
}
