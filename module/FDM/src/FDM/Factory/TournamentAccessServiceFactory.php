<?php

namespace FDM\Factory;

use FDM\Service\TournamentAccessService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentAccessServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $acl = $serviceLocator->get('myAcl');
    if ($acl === null) {
      return null;
    }

    $router = $serviceLocator->get('router');
    $request = $serviceLocator->get('request');
    $routerMatch = $router->match($request);
    $tId = $routerMatch->getParam('id');
    $finished = false;
    if ($tId !== null) {
      $em = $serviceLocator->get('Doctrine\ORM\EntityManager');
      $tournament = $em->find('FDM\Entity\Tournament', $tId);
      $finished = $tournament->isFinished();
    }

    $auth = $serviceLocator->get('zfcuser_auth_service');
    $user = null;
    if ($auth->hasIdentity()) {
      $user = $auth->getIdentity();
    }
    return new TournamentAccessService($user, $acl, $finished);
  }
//</editor-fold desc="Public Methods">
}
