<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 9/28/16
 * Time: 3:13 PM
 */

namespace FDM\Factory;

use FDM\Service\TournamentService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    return new TournamentService($serviceLocator);
  }
//</editor-fold desc="Public Methods">
}
