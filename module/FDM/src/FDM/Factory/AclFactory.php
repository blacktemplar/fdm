<?php

namespace FDM\Factory;

use FDM\Entity\TournamentPrivileges;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AclFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $router = $serviceLocator->get('router');
    $request = $serviceLocator->get('request');
    $routerMatch = $router->match($request);

    $acl = new Acl();
    $acl->addResource(
      new Resource(
        TournamentPrivileges::TYPE_READ
      )
    );
    $acl->addResource(
      new Resource(
        TournamentPrivileges::TYPE_EDIT
      )
    );
    $acl->addRole(new Role("guest"));

    if ($routerMatch !== null) {
      $tId = $routerMatch->getParam('id');
      if ($tId !== null) {
        $em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $tournament = $em->find('FDM\Entity\Tournament', $tId);
        if ($tournament->isPublic()) {
          $acl->allow(
            "guest",
            TournamentPrivileges::TYPE_READ
          );
        }


        foreach ($tournament->getPrivileges() as $privilege) {
          $acl->addRole(new Role($privilege->getUser()->getId()));
          $acl->allow(
            $privilege->getUser()->getId(),
            $privilege->getType()
          );
          if ($privilege->getType() === TournamentPrivileges::TYPE_EDIT) {
            $acl->allow(
              $privilege->getUser()->getId(),
              TournamentPrivileges::TYPE_EDIT
            );
          }
        }
      }
    }
    return $acl;
  }
//</editor-fold desc="Public Methods">
}
