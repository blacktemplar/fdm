<?php

namespace FDM\Factory;

use FDM\Controller\UpToDateController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UpToDateControllerFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $realServiceLocator = $serviceLocator->getServiceLocator();
    $em =
      $realServiceLocator->get('Doctrine\ORM\EntityManager');


    return new UpToDateController($em);
  }
//</editor-fold desc="Public Methods">
}
