<?php

namespace FDM\Factory;

use FDM\Service\PhaseService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PhaseServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $em =
      $serviceLocator->get('Doctrine\ORM\EntityManager');
    $fs = $serviceLocator->get('FDM\Service\FlushServiceInterface');
    if ($fs === null) {
      return null;
    }
    $translator = $serviceLocator->get('translator');
    $viewHelperManager =
      $serviceLocator->get('ViewHelperManager');
    $namesHelper = $viewHelperManager->get('namesHelper');
    $ls = $serviceLocator->get('FDM\Service\LoadingService');
    $speakService = $serviceLocator->get('FDM\Service\SpeakService');

    return new PhaseService(
      $em, $fs, $serviceLocator, $translator, $namesHelper, $ls, $speakService
    );
  }
//</editor-fold desc="Public Methods">
}

