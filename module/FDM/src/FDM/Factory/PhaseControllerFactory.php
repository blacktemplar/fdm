<?php

namespace FDM\Factory;

use FDM\Controller\PhaseController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PhaseControllerFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $realServiceLocator = $serviceLocator->getServiceLocator();
    $em =
      $realServiceLocator->get('Doctrine\ORM\EntityManager');
    $tas = $realServiceLocator->get(
      'FDM\Service\TournamentAccessServiceInterface'
    );
    $fs = $realServiceLocator->get('FDM\Service\FlushServiceInterface');
    $phaseService =
      $realServiceLocator->get('FDM\Service\PhaseServiceInterface');
    $translator = $realServiceLocator->get('translator');
    $loadingService = $realServiceLocator->get('FDM\Service\LoadingService');

    return new PhaseController($em, $tas, $fs, $phaseService, $translator, $loadingService);
  }
//</editor-fold desc="Public Methods">
}
