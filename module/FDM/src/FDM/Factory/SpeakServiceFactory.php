<?php

namespace FDM\Factory;

use FDM\Service\SpeakService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SpeakServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $viewHelperManager =
      $serviceLocator->get('ViewHelperManager');
    $namesHelper = $viewHelperManager->get('namesHelper');
    $em =
      $serviceLocator->get('Doctrine\ORM\EntityManager');

    return new SpeakService($namesHelper, $em);
  }
//</editor-fold desc="Public Methods">
}

