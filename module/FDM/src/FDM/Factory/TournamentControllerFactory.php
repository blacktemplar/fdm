<?php

namespace FDM\Factory;

use FDM\Controller\TournamentController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TournamentControllerFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $realServiceLocator = $serviceLocator->getServiceLocator();
    $em =
      $realServiceLocator->get('Doctrine\ORM\EntityManager');
    $tas = $realServiceLocator->get(
      'FDM\Service\TournamentAccessServiceInterface'
    );
    $fs = $realServiceLocator->get('FDM\Service\FlushServiceInterface');
    $auth = $realServiceLocator->get('zfcuser_auth_service');
    $user = null;
    if ($auth->hasIdentity()) {
      $user = $auth->getIdentity();
    }
    $translator =
      $realServiceLocator->get('translator');

    $userService = $realServiceLocator->get('zfcuser_user_service');

    $tournamentService = $realServiceLocator->get('FDM\Service\TournamentService');

    $ls = $realServiceLocator->get('FDM\Service\LoadingService');

    $ps = $realServiceLocator->get('FDM\Service\PlayersService');

    $speakService = $realServiceLocator->get('FDM\Service\SpeakService');

    return new TournamentController($em, $tas, $fs, $translator, $userService,
      $tournamentService, $ls, $ps, $speakService, $user);
  }
//</editor-fold desc="Public Methods">
}
