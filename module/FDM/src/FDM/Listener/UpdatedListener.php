<?php
namespace FDM\Listener;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\UnitOfWork;
use FDM\Entity\Attendant;
use FDM\Entity\Phase;
use FDM\Service\PhaseServiceInterface;

class UpdatedListener
{

//<editor-fold desc="Fields">
  /** @var  EntityManager */
  private $em;

  private $phaseService;

  private $totalRecompute;

  private $tracked;

  /** @var  UnitOfWork */
  private $uow;

  private $updateGraph;

//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    PhaseServiceInterface $phaseService)
  {
    $this->phaseService = $phaseService;
    $this->updateGraph = [
      "\FDM\Entity\Attendant" => [
        "getTournament",
      ],
      "\FDM\Entity\Phase" => [
        "getTournament",
      ],
      "\FDM\Entity\Game" => [
        "getGroup",
      ],
      "\FDM\Entity\Group" => [
        "getPhase",
      ],
      "\FDM\Entity\Mode" => [
        "getPhase",
      ],
      "\FDM\Entity\Ranking" => [
        "getGroup",
      ],
    ];
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function onFlush($eventArgs)
  {
    $this->em = $eventArgs->getEntityManager();
    $this->uow = $this->em->getUnitOfWork();
    $this->tracked = [];
    $this->totalRecompute = false;
    foreach ($this->uow->getScheduledEntityInsertions() as $entity) {
      $this->trackChanged($entity);
    }

    foreach ($this->uow->getScheduledEntityUpdates() as $entity) {
      $this->trackChanged($entity);
    }

    foreach ($this->uow->getScheduledEntityDeletions() as $entity) {
      $this->trackChanged($entity);
    }

    foreach ($this->uow->getScheduledCollectionDeletions() as $entity) {
      $this->trackChanged($entity);
    }

    foreach ($this->uow->getScheduledCollectionUpdates() as $entity) {
      $this->trackChanged($entity);
    }

    if ($this->totalRecompute) {
      $this->recomputeChangeSets();
    }
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function recomputeChangeSet($entity, $meta = null)
  {
    if ($meta === null) {
      $meta = $this->em->getClassMetadata(get_class($entity));
    }
    if ($this->uow->getEntityChangeSet($entity)) {
      /** If the entity has pending changes, we need to recompute/merge. */
      $this->uow->recomputeSingleEntityChangeSet($meta, $entity);
    } else {
      /** If there are no changes, we compute from scratch? */
      $this->uow->computeChangeSet($meta, $entity);
    }
  }

  private function recomputeChangeSets()
  {
    foreach ($this->uow->getIdentityMap() as $class => $entities) {
      foreach ($entities as $entity) {
        $this->recomputeChangeSet($entity);
      }
    }
  }

  private function trackChanged($entity)
  {
    if (method_exists($entity, "getId")) {
      if (!array_key_exists($entity->getId(), $this->tracked)) {
        $this->tracked[$entity->getId()] = true;
        if (method_exists($entity, "setUpdatedAt")) {
          $entity->setUpdatedAt(new \DateTime());
          if (!$this->totalRecompute) {
            $this->recomputeChangeSet($entity);
          }
        }
        foreach ($this->updateGraph as $class => $methods) {
          if (is_a($entity, $class)) {
            foreach ($methods as $method) {
              $this->trackChanged($entity->$method());
            }
          }
        }
        //individual update callbacks
        if ($entity instanceof Phase) {
          $this->phaseService->phaseChangedCallback($entity);
          $this->totalRecompute = true;
        } else if ($entity instanceof Attendant) {
          if (array_key_exists("status", $this->uow->getEntityChangeSet($entity))) {
            //phases changed too
            foreach ($entity->getTournament()->getPhases() as $phase) {
              $this->trackChanged($phase);
            }
          }
        }
      }
    }
  }
//</editor-fold desc="Private Methods">
}
