<?php

/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/6/16
 * Time: 11:18 AM
 */

namespace FDM\Tools;

use Doctrine\ORM\EntityManager;

class IdGenerator extends \Doctrine\ORM\Id\AbstractIdGenerator
{
//<editor-fold desc="Public Methods">
  /**
   * Generates an identifier for an entity.
   *
   * @param EntityManager|EntityManager $em
   * @param \Doctrine\ORM\Mapping\Entity $entity
   * @return mixed
   */
  public function generate(EntityManager $em, $entity)
  {
    if (function_exists('com_create_guid') === true) {
      return strtolower(trim(com_create_guid(), '{}'));
    }

    return strtolower(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)));
  }
//</editor-fold desc="Public Methods">

}