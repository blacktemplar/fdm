<?php

use Syslogic\DoctrineJsonFunctions\Query\AST\Functions\Mysql as DqlFunctions;

return [
  'service_manager' => [
    'factories' => [
      'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
      'FDM\Service\TournamentsServiceInterface' =>
        'FDM\Factory\TournamentsServiceFactory',
      'Navigation' => 'FDM\Factory\NavigationFactory',
      'myAcl' => 'FDM\Factory\AclFactory',
      'FDM\Service\TournamentAccessServiceInterface' =>
        'FDM\Factory\TournamentAccessServiceFactory',
      'FDM\Service\AccessService' =>
        'FDM\Factory\AccessServiceFactory',
      'FDM\Service\TournamentNavigationServiceInterface' =>
        'FDM\Factory\TournamentNavigationServiceFactory',
      'FDM\Service\FlushServiceInterface' =>
        'FDM\Factory\FlushServiceFactory',
      'FDM\Service\PhaseServiceInterface' =>
        'FDM\Factory\PhaseServiceFactory',
      'FDM\Service\AttendanceStrategyService' =>
        'FDM\Factory\AttendanceStrategyServiceFactory',
      'FDM\Service\TournamentService' =>
        'FDM\Factory\TournamentServiceFactory',
      'FDM\Service\LoadingService' =>
        'FDM\Factory\LoadingServiceFactory',
      'FDM\Service\PlayersService' =>
        'FDM\Factory\PlayersServiceFactory',
      'TournamentRankRankingService' =>
        'FDM\Factory\TournamentRankRankingServiceFactory',
      'TournamentEloRankingService' =>
        'FDM\Factory\TournamentEloRankingServiceFactory',
      'FDM\Service\SpeakService' =>
        'FDM\Factory\SpeakServiceFactory',
    ],
    'invokables' => [
      'StableSort' => 'FDM\Service\StableSortService',
      'zfcDatagrid.renderer.MYTCPDF' => 'FDM\ZfcDatagrid\Renderer\TCPDF\Renderer',
      'zfcDatagrid.renderer.MYPHPExcel' => 'FDM\ZfcDatagrid\Renderer\PHPExcel\Renderer',
      'FDM\Service\PointsRankingService' => 'FDM\Service\PointsRankingService',
      'FDM\Form\DateParseFilter' => 'FDM\Form\DateParseFilter'
    ],
  ],
  'view_helpers' => [
    'factories' => [
      'names_helper' => 'FDM\Factory\NamesHelperFactory',
    ],
    'invokables' => [
      'device_helper' => 'FDM\View\Helper\DeviceHelper',
      'dialog_helper' => 'FDM\View\Helper\DialogHelper'
    ],
  ],
  'translator' => [
    'locale' => 'en_US',
    'translation_file_patterns' => [
      [
        'type' => 'gettext',
        'base_dir' => __DIR__ . '/../language',
        'pattern' => '%s.mo',
      ],
    ],
  ],
  'slm_locale' => [
    'supported' => [
      'de',
      'de_DE',
      'en',
      'en_US',
    ],
    'mappings' => [
      'de' => 'de_DE',
      'en' => 'en_US'
    ],
    'strategies' => ['acceptlanguage'],
  ],
  'controllers' => [
    'factories' => [
      'FDM\Controller\Tournaments' =>
        'FDM\Factory\TournamentsControllerFactory',
      'FDM\Controller\Tournament' =>
        'FDM\Factory\TournamentControllerFactory',
      'FDM\Controller\Phase' =>
        'FDM\Factory\PhaseControllerFactory',
      'FDM\Controller\UpToDate' =>
        'FDM\Factory\UpToDateControllerFactory',
    ],
    'invokables' => [],
  ],
  'router' => [
    'routes' => [
      'home' => [
        'type' => 'literal',
        'options' => [
          'route' => '/',
          'defaults' => [
            'controller' => 'FDM\Controller\Tournaments',
            'action' => 'index',
          ],
        ],
      ],
      'fdm' => [
        'type' => 'literal',
        'options' => [
          'route' => '/fdm',
          'defaults' => [
            'controller' => 'FDM\Controller\Tournaments',
            'action' => 'index',
          ],
        ],
        'may_terminate' => true,
      ],
      'tournaments' => [
        'type' => 'literal',
        'options' => [
          'route' => '/fdm',
          'defaults' => [
            'controller' => 'FDM\Controller\Tournaments',
            'action' => 'index',
          ],
        ],
        'may_terminate' => true,
      ],
      'templates' => [
        'type' => 'literal',
        'options' => [
          'route' => '/templates',
          'defaults' => [
            'controller' => 'FDM\Controller\Tournaments',
            'action' => 'templates',
          ],
        ],
        'may_terminate' => true,
      ],
      'rankings' => [
        'type' => 'literal',
        'options' => [
          'route' => '/rankings',
          'defaults' => [
            'controller' => 'FDM\Controller\Tournaments',
            'action' => 'rankings',
          ],
        ],
        'may_terminate' => true,
      ],
      'ranking' => [
        'type' => 'segment',
        'options' => [
          'route' => '/ranking/:rId',
          'defaults' => [
            'controller' => 'FDM\Controller\Tournaments',
            'action' => 'rankingForward',
          ],
        ],
        'may_terminate' => true,
        'child_routes' => [
          'hidecolumns' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'route' => '/hidecolumns',
              'defaults' => [
                'controller' => 'FDM\Controller\Tournaments',
                'action' => 'hidecolumns',
              ],
            ],
          ],
          'view' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'route' => '/view/:hiddenColumns',
              'defaults' => [
                'controller' => 'FDM\Controller\Tournaments',
                'action' => 'ranking',
              ],
            ],
          ],
        ],
      ],
      'tournament' => [
        'type' => 'segment',
        'options' => [
          'route' => '/tournament/:id',
          'constraints' => [
            'id' => '[a-f\-0-9]*'
          ],
          'defaults' => [
            'controller' => 'FDM\Controller\Tournament',
            'action' => 'index',
          ],
        ],
        'may_terminate' => true,
        'child_routes' => [
          'settings' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'route' => '/settings',
              'defaults' => [
                'controller' => 'FDM\Controller\Tournament',
                'action' => 'settings',
              ],
            ],
          ],
          'speaker' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'route' => '/speaker',
              'defaults' => [
                'controller' => 'FDM\Controller\Tournament',
                'action' => 'speaker',
              ],
            ],
            'child_routes' => [
              'getNewTasks' => [
                'type' => 'segment',
                'may_terminate' => true,
                'options' => [
                  'route' => '/getNewTasks/:lasttask',
                  'constraints' => [
                    'lasttask' => '[0-9]*',
                  ],
                  'defaults' => [
                    'controller' => 'FDM\Controller\Tournament',
                    'action' => 'getNewSpeakTasks',
                  ],
                ],
              ],
            ],
          ],
          'uptodate' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'route' => '/uptodate/:lastrefreshed',
              'constraints' => [
                'lastrefreshed' => '[0-9]*',
              ],
              'defaults' => [
                'controller' => 'FDM\Controller\UpToDate',
                'action' => 'index',
              ],
            ],
          ],
          'copy' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'route' => '/copy',
              'defaults' => [
                'action' => 'copy',
              ],
            ],
          ],
          'localsettings' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'route' => '/localsettings',
              'defaults' => [
                'action' => 'localsettings',
              ],
            ],
          ],
          'attendants' => [
            'type' => 'segment',
            'options' => [
              'route' => '/attendants',
              'defaults' => [
                'action' => 'attendants',
              ],
            ],
            'may_terminate' => true,
            'child_routes' => [
              'jsonFetch' => [
                'type' => 'segment',
                'options' => [
                  'route' => '/jsonFetch',
                  'defaults' => [
                    'action' => 'attendantsJSONFetch',
                  ],
                ],
                'may_terminate' => true,
              ],
              'jsonFetchPlayers' => [
                'type' => 'segment',
                'options' => [
                  'route' => '/jsonFetchPlayers',
                  'defaults' => [
                    'action' =>
                      'attendantsJSONFetchPlayers',
                  ],
                ],
                'may_terminate' => true,
              ],
              'edit' => [
                'may_terminat' => false,
                'type' => 'segment',
                'options' => [
                  'route' => '/edit',
                ],
                'child_routes' => [
                  'jsonEdit' => [
                    'type' => 'segment',
                    'options' => [
                      'route' => '/json',
                      'defaults' => [
                        'action' =>
                          'attendantsJSONEdit',
                      ],
                    ],
                    'may_terminate' => true,
                  ],
                  'jsonSetPlayer' => [
                    'type' => 'segment',
                    'options' => [
                      'route' => '/jsonSetPlayer',
                      'defaults' => [
                        'action' =>
                          'attendantsJSONSetPlayer',
                      ],
                    ],
                    'may_terminate' => true,
                  ],
                  'jsonDeleteRow' => [
                    'type' => 'segment',
                    'options' => [
                      'route' => '/jsonDeleteRow',
                      'defaults' => [
                        'action' =>
                          'attendantsJSONDelete',
                      ],
                    ],
                    'may_terminate' => true,
                  ],
                  'jsonNewPlayer' => [
                    'type' => 'segment',
                    'options' => [
                      'route' => '/jsonNewPlayer',
                      'defaults' => [
                        'action' =>
                          'attendantsJSONNewPlayer',
                      ],
                    ],
                    'may_terminate' => true,
                  ],
                  'updatePlayers' => [
                    'type' => 'segment',
                    'options' => [
                      'route' => '/updatePlayers',
                      'defaults' => [
                        'action' =>
                          'attendantsUpdatePlayers',
                      ],
                    ],
                    'may_terminate' => true,
                  ],
                ],
              ],
            ],
          ],
          'edit' => [
            'type' => 'segment',
            'may_terminate' => false,
            'options' => [
              'route' => '/edit',
            ],
            'child_routes' => [
              'start' => [
                'type' => 'segment',
                'options' => [
                  'route' => '/start',
                  'defaults' => [
                    'action' => 'start',
                  ],
                ],
                'may_terminate' => true,
              ],
              'unstart' => [
                'type' => 'segment',
                'options' => [
                  'route' => '/unstart',
                  'defaults' => [
                    'action' => 'unstart',
                  ],
                ],
                'may_terminate' => true,
              ],
              'delete' => [
                'type' => 'segment',
                'may_terminate' => true,
                'options' => [
                  'route' => '/delete',
                  'defaults' => [
                    'action' => 'delete',
                  ],
                ],
              ],
              'finish' => [
                'type' => 'segment',
                'may_terminate' => true,
                'options' => [
                  'route' => '/finish',
                  'defaults' => [
                    'action' => 'finish',
                  ],
                ],
              ],
              'unfinish' => [
                'type' => 'segment',
                'may_terminate' => true,
                'options' => [
                  'route' => '/unfinish',
                  'defaults' => [
                    'action' => 'unfinish',
                  ],
                ],
              ],
            ],
          ],
          'phase' => [
            'type' => 'segment',
            'options' => [
              'route' => '/phase/:phase',
              'defaults' => [
                'controller' => 'FDM\Controller\Phase',
                'action' => 'index',
              ],
              'constraints' => [
                'phase' => '[a-f\-0-9]+',
              ],
            ],
            'may_terminate' => true,
            'child_routes' => [
              'edit' => [
                'type' => 'segment',
                'may_terminate' => false,
                'options' => [
                  'route' => '/edit',
                ],
                'child_routes' => [
                  'undoGame' => [
                    'type' => 'segment',
                    'may_terminate' => true,
                    'options' => [
                      'route' => '/undoGame/:game',
                      'defaults' => [
                        'controller' => 'FDM\Controller\Phase',
                        'action' => 'undoGame',
                      ],
                      'constraints' => [
                        'game' => '[a-f\-0-9]*',
                      ],
                    ],
                  ],
                ],
              ],
              'prototypes' => [
                'type' => 'segment',
                'may_terminate' => true,
                'options' => [
                  'route' => '/prototypes',
                  'defaults' => [
                    'controller' => 'FDM\Controller\Phase',
                    'action' => 'prototypes',
                  ]
                ]
              ]
            ]
          ],
        ],
      ],
    ],
  ],
  'view_manager' => [
    'template_path_stack' => [
      'fdm' => __DIR__ . '/../view',
    ],
  ],
  'doctrine' => [
    'eventmanager' => [
      'orm_default' => [
        'subscribers' => [
          'Gedmo\Timestampable\TimestampableListener',
        ],
      ],
    ],
    'driver' => [
      'fdm_entities' => [
        'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
        'cache' => 'array',
        'paths' => [
          __DIR__ . '/../src/FDM/Entity',
        ],
      ],

      'orm_default' => [
        'drivers' => [
          'FDM\Entity' => 'fdm_entities',
        ],
      ],
    ],
    'configuration' => array(
      'orm_default' => array(
        'numeric_functions' => array(
          'ROUND' => 'FDM\Sql\Round'
        ),
        'string_functions' => [
          DqlFunctions\JsonExtract::FUNCTION_NAME => DqlFunctions\JsonExtract::class,
        ],
      )
    )
  ],
  'di' => [
    'instance' => [
      'Zend\View\HelperLoader' => [
        'parameters' => [
          'map' => [
            'zfcUserDisplayName' =>
              'ZfcUser\View\Helper\ZfcUserDisplayName',
            'zfcUserLoginWidget' =>
              'ZfcUser\View\Helper\ZfcUserLoginWidget',
          ],
        ],
      ],
    ],
  ],
  'strategies' => [
    'ViewJsonStrategy',
  ],
];
