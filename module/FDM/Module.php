<?php

namespace FDM;

use Detection\MobileDetect;
use Doctrine\ORM\Events;
use FDM\Entity\TournamentPrivileges;
use Zend\Http\Request;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
//<editor-fold desc="Fields">
  private $routeInitialized = false;

//</editor-fold desc="Fields">

//<editor-fold desc="Public Methods">
  public function checkAcl(MvcEvent $e)
  {
    $routeMatch = $e->getRouteMatch();
    if ($routeMatch === null) {
      return null;
    }
    $sm = $e->getApplication()->getServiceManager();
    if ($routeMatch->getMatchedRouteName() === "tournament" ||
      substr($routeMatch->getMatchedRouteName(), 0, 11) ===
      "tournament/"
    ) {
      $as = $sm->get('FDM\Service\TournamentAccessServiceInterface');
      if (!$as->hasAccessType(TournamentPrivileges::TYPE_READ) ||
        (strpos($routeMatch->getMatchedRouteName(), "/edit") !== false
          && !$as->hasAccessType(TournamentPrivileges::TYPE_EDIT,
            strpos($routeMatch->getMatchedRouteName(), "/edit/unfinish") !== false)
        ) ||
        (strpos($routeMatch->getMatchedRouteName(), "/admin/") !== false
          && !$as->isAdmin()
        )
      ) {
        return $this->send403($e);
      }
    } else if (strpos($routeMatch->getMatchedRouteName(), "admin/") !== false) {
      $as = $sm->get('\FDM\Service\AccessService');
      if (!$as->isAdmin()) {
        return $this->send403($e);
      }
    }
    return null;
  }

  public function getAutoloaderConfig()
  {
    return [
      'Zend\Loader\ClassMapAutoloader' => [
        __DIR__ . '/autoload_classmap.php',
      ],
      'Zend\Loader\StandardAutoloader' => [
        'namespaces' => [
          __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
          'Detection' =>
            __DIR__ . '/../..' .
            '/vendor/mobiledetect/mobiledetectlib' .
            '/namespaced/Detection',

        ],
      ],
    ];
  }

  public function getConfig()
  {
    return include __DIR__ . '/config/module.config.php';
  }

  public function onBootstrap(MvcEvent $e)
  {
    $eventManager = $e->getApplication()->getEventManager();
    $eventManager->attach('route', [$this, 'onRoute']);

    //set correct layout
    $detect = new MobileDetect;
    $deviceType = $detect->isMobile() ?
      ($detect->isTablet() ? 'tablet' : 'phone') : 'computer';
    if ($deviceType === "phone") {
      $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractController', 'dispatch', function ($e) {
        $controller = $e->getTarget();
        $controller->layout('layout/layout-mobile');
      });
    }
    $this->addRegistrationFields($e);
  }

  public function onRoute(MvcEvent $e)
  {
    $routeMatch = $e->getRouteMatch();
    if ($routeMatch !== null && $routeMatch->getMatchedRouteName() === "tournament/uptodate") {
      return null;
    }
    if (!$this->routeInitialized) {
      $this->routeInitialized = true;
      $sm = $e->getApplication()->getServiceManager();
      $phaseService = $sm->get('FDM\Service\PhaseServiceInterface');
      $doctrineEntityManager = $sm->get('Doctrine\ORM\EntityManager');
      $doctrineEventManager = $doctrineEntityManager->getEventManager();
      //$doctrineEntityManager->getConnection()->getConfiguration()->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());
      $doctrineEventManager->addEventListener(
        [Events::onFlush],
        new Listener\UpdatedListener($phaseService)
      );

      //own autodetect language
      $default = 'en-US';
      $supported = $sm->get("Config")["slm_locale"]["supported"];
      $mappings = $sm->get("Config")["slm_locale"]["mappings"];
      $app = $e->getApplication();
      $request = $app->getRequest();
      $translator = $e->getApplication()->getServiceManager()->get('translator');
      $found = false;
      if ($request instanceof Request) { //only search headers if it is a http request
        $headers = $app->getRequest()->getHeaders();
        if ($headers->has('Accept-Language')) {
          $locales = $headers->get('Accept-Language')->getPrioritized();

          // Loop through all locales, highest priority first
          foreach ($locales as $locale) {
            $tests = [strtolower($locale->getLanguage())];
            $tests[] = str_replace("-", "_", strtolower($locale->getLanguage()));
            $tests[] = str_replace("_", "-", strtolower($locale->getLanguage()));
            $tests[] = strtolower($locale->getPrimaryTag());
            foreach ($tests as $test) {
              if (in_array($test, $supported)) {
                // The locale is one of our supported list
                if (array_key_exists($test, $mappings)) {
                  $test = $mappings[$test];
                }
                $translator->setLocale($test);
                $found = true;
                break 2;
              }
            }
          }
        }
      }
      if (!$found) {
        $translator->setLocale($default);
      }
    }
    return $this->checkAcl($e);
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function addRegistrationFields(MvcEvent $e)
  {
    $eventManager = $e->getApplication()->getEventManager();
    $em = $eventManager->getSharedManager();

    $zfcServiceEvents = $e->getApplication()->getServiceManager()->get('zfcuser_user_service')->getEventManager();

    // custom form fields
    $em->attach(
      'ZfcUser\Form\Register',
      'init',
      function ($e) {
        /* @var $form \ZfcUser\Form\Register */
        $form = $e->getTarget();
        $form->add(
          array(
            'name' => 'lastName',
            'options' => array(
              'label' => 'Last Name',
            ),
            'attributes' => array(
              'type' => 'text',
            ),
          ),
          array(
            'priority' => 1000, // Increase value to move to top of form
          )
        );

        $form->add(
          array(
            'name' => 'firstName',
            'options' => array(
              'label' => 'First Name',
            ),
            'attributes' => array(
              'type' => 'text',
            ),
          ),
          array(
            'priority' => 1001, // Increase value to move to top of form
          )
        );
        $form->add(
          array(
            'type' => 'Zend\Form\Element\Date',
            'name' => 'birthday',
            'required' => true,
            'options' => array(
              'label' => 'Birthday'
            ),
            'attributes' => array(
              'min' => '1920-01-01',
              'max' => '2020-01-01',
              'step' => '1', // days; default step interval is 1 day
            ),
          )
        );
        //$form->get('birthday')->setFormat('d.m.Y');
        $form->get('birthday')->setValue('dd.mm.yyyy');
      }
    );

    // To validate new fields
    $em->attach('ZfcUser\Form\RegisterFilter', 'init', function ($e) {
      $filter = $e->getTarget();

      $filter->add(array(
        'name' => 'firstName',
        'required' => true,
        'allowEmpty' => false,
        'filters' => array(array('name' => 'StringTrim')),
        'validators' => array(
          array(
            'name' => 'NotEmpty',
          )
        ),
      ));

      $filter->add(array(
        'name' => 'lastName',
        'required' => true,
        'allowEmpty' => false,
        'filters' => array(array('name' => 'StringTrim')),
        'validators' => array(
          array(
            'name' => 'NotEmpty',
          )
        ),
      ));
      $filter->add(array(
        'name' => 'birthday',
        'required' => true,
        'allowEmpty' => false,
        'filters' => array(array('name' => 'FDM\Form\DateParseFilter'))
      ));
    });

    // Store the field
    $zfcServiceEvents->attach('register', function ($e) {
      $form = $e->getParam('form');
      $user = $e->getParam('user');

      /* @var $user \FDM\Entity\User */
      $user->setBirthday(\DateTime::createFromFormat('Y-m-d', $user->getBirthday()));
      $user->setFirstName($form->get('firstName')->getValue());
      $user->setLastName($form->get('lastName')->getValue());
    });
  }

  private function send403(MvcEvent $e)
  {
    $response = $e->getResponse();
    //location to page or what ever
    $response->setStatusCode(403);
    $response->sendHeaders();
    $e->stopPropagation();

    $stopCallBack = function ($event) use ($response) {
      $event->stopPropagation();
      return $response;
    };

    $e->getApplication()->getEventManager()
      ->attach(MvcEvent::EVENT_ROUTE, $stopCallBack, -10000);
    return $response;
  }
//</editor-fold desc="Private Methods">
}
