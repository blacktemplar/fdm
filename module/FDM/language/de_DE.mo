��    W      �     �      �     �     �     �  
   �     �     �      �     �                    (     F     J     Q     c     p     �     �     �     �  
   �     �     �     �     �     �     �  	   �     		     !	     0	     =	     W	     c	     h	     w	     �	     �	     �	     �	     �	     �	     �	  A   �	     

     
     
     
     &
     B
     W
     ^
     l
     q
     �
  
   �
     �
     �
     �
     �
     �
     �
  
   �
     �
     
            	        &     :     F     N     R     `     g     n     s     z     }     �     �     �     �     �     �  �  �  
   {     �     �  
   �     �     �  '   �  
   �     
  	          (   *     S     W     `     r     ~  
   �     �     �     �     �     �     �     �     �     �                    ,     A  &   P     w     �     �     �     �     �     �     �     �  	   �     �  C   �     >     E     Q  	   V      `  5   �     �     �  	   �     �     �  
   �                    -     D     J  	   V     `     p     w     }     �     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �  	   �            !        -   P      $      T       D          &         A       ;   #   F          N   9       =             C      B   J                       W              '      3                         
   0       7   8              )              *          (          +                             V                ,   ?   U             "   .         <   Q           O   6       I   K   %   	   M   @   /   :   E   5   R       L   4   2   1       !             S       >   H   G                # Tournaments Action Actions Attendants Automatic Speech Automatically Start Games Automatically refresh on changes Birthday Bracket Cancel Change my Password Create new with this template DYP Delete Delete Tournament Display Name Display Speak-Button Editable Extras Final Finished games First Name First Name Pronunciation Games Games Played Go Back Goal Difference Group Last Name Last Name Pronunciation Local Settings Logged in as Maximal players per group Merge teams Name New Tournament Not logged in Number of groups Number of tables Page Pausing Payed Phase Playing Please enter your password below to remove the tournament %s (%s) Points Public Rank Rankings Really delete attendant %s? Really undo game %s? Result Running games Save Scheduled games Select a table Semifinals Settings Showing Shuffle after grouping Shuffle before grouping Start Start Number Start Time Start Tournament Status Table Team Templates Tournament Rankings Tournaments Updated and assign tables create delete edit finals id item(s) of read save start undo undo start phase Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-17 15:31+0100
PO-Revision-Date: 2016-11-17 15:31+0100
Last-Translator: Evan Coury <me@evancoury.com>
Language-Team: ZF Contributors <zf-devteam@zend.com>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.9
X-Poedit-SearchPath-0: ..
 # Turniere Aktion Aktionen Teilnehmer Automatische Sprachausgabe Spiele automatisch starten Automatisches Neuladen nach Änderungen Geburtstag Baum Abbrechen Passwort Ändern Erzeuge neues Turnier mit dieser Vorlage DYP Löschen Turnier Entfernen Anzeigename Sprach-Button anzeigen Editierbar Extras Finale Beendete Spiele Vorname Vorname Lautschrift Spiele Gespielte Spiele Zurück Tordifferenz Gruppe Nachname Nachname Lautschrift Lokale Einstellungen Angemeldet als Maximale Anzahl an Spielern pro Gruppe Teams zusammenfügen Name Neues Turnier Nicht angemeldet Anzahl Gruppen Anzahl Tische Seite Pausiert Bezahlt PhaseTest Spielbereit Bitte geben Sie ihr Passwort ein um das Turnier %s (%s) zu löschen Punkte Öffentlich Rang Rangliste Teilnehmer %s wirklich löschen? Wollen Sie das Spiel %s wirklich rückgängig machen? Resultat Laufende Spiele Speichern Geplante Spiele Tisch wählen Halbfinale Einstellungen Zeige Mixen nach Gruppieren Mixen bevor Gruppieren Start Startnummer Startzeit Turnier starten Status Tisch Team Vorlagen Turnier Ranglisten Turniere Stand und Tische zuordnen erstellen löschen editieren Finale id Turniere(n) von lesen speichern starten rückgängig Phase starten rückgängig machen 