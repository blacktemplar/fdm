<?php
/** Ids for poedit
 * @method translate()
 */
translate('Tournaments');
translate('Start');
translate('Name');
translate('Editable');
translate('Create new with this template');
translate('edit');
translate('read');

//translations for zfcdatagrid
translate('Showing');
translate('of');
translate('item(s)');
translate('Page');

translate('Go Back');
translate('Attendants');
translate('Start Number');
translate('First Name');
translate('Last Name');
translate('Display Name');
translate('Status');
translate('id');
translate('Actions');
translate('Playing');
translate('Pausing');
translate('delete');
translate('Phase');
translate('Settings');
translate('Name');
translate('Number of tables');
translate('save');
translate('start');
translate('assign tables');
translate('Rankings');
translate('Group');
translate('Rank');
translate('Team');
translate('# Tournaments');
translate('and');
translate('Updated');
translate('Page');
translate('Games');
translate('Scheduled games');
translate('Running games');
translate('Finished games');
translate('Extras');
translate('Bracket');
translate('Points');
translate('Goal Difference');
translate('Games Played');
translate('Team');
translate('Result');
translate('Table');
translate('Public');
translate('Start Time');
translate('create');
translate('Start Tournament');
translate('DYP');
translate('Shuffle before grouping');
translate('Shuffle after grouping');
translate('Merge teams');
translate('Number of groups');
translate('Maximal players per group');

translate('Change my Password');
translate('Tournaments');
translate('Templates');
translate('Rankings');
translate('Tournament Rankings');
translate('Automatically Start Games');
translate('undo start phase');
translate('Display Speak-Button');
translate('Final');
translate('finals');
translate('Semifinals');

translate('Automatic Speech');
translate('Automatically refresh on changes');
translate('Save');
translate('Birthday');