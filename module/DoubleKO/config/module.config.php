<?php
return [
  'service_manager' => [
    'factories' => [
      'DoubleKOService' => '\DoubleKO\Factory\DoubleKOServiceFactory',
      '\DoubleKO\Service\DoubleKOService' => '\DoubleKO\Factory\DoubleKOServiceFactory',
    ],
    'invokables' => [
      'DoubleKO\Service\DoubleKORankingService' => 'DoubleKO\Service\DoubleKORankingService'
    ],
  ],
  'controllers' => [
    'factories' => [
      'DoubleKO\Controller\DoubleKO' =>
        '\DoubleKO\Factory\DoubleKOControllerFactory'
    ],
    'invokables' => [],
  ],
  'view_manager' => [
    'template_path_stack' => [
      'double-ko' => __DIR__ . '/../view',
    ],
  ],
];
