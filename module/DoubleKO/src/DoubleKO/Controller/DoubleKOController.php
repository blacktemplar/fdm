<?php
namespace DoubleKO\Controller;

use FDM\Entity\Game;
use FDM\Entity\Ranking;
use SingleKO\Controller\SingleKOController;

class DoubleKOController extends SingleKOController
{

//<editor-fold desc="Public Methods">
//<editor-fold desc="Protected Methods">
  /**
   * @param Game $game
   * @param $order
   * @param $numTeams
   * @param $bracket
   * @param $round
   * @param $ind
   * @param $teams
   */
  protected function getGameIndices(
    $game, $order, $numTeams, &$bracket, &$round, &$ind, &$teams
  )
  {
    $teamA = $game->getTeamA()->first()->getPhaseStartRank();
    $teams[0] = "A";
    $teams[1] = "B";
    $gameRound = $game->getRound();
    $ind = array_search($teamA, $order);
    $round = 0;

    if ($gameRound % 3 == 0) {
      //winner pool
      $bracket = 0;
      $winnerRound = $gameRound / 3;
      for ($i = $winnerRound * 2; $i < $numTeams; $i *= 2) {
        $ind = ($ind - ($ind % 2)) / 2;
        $round++;
      }

      if ($ind % 2 == 1) {
        $teams[0] = "B";
        $teams[1] = "A";
      }
      $ind = ($ind - ($ind % 2)) / 2;
    } else if ($gameRound > 1) {
      //loser pool
      $LossRound = $this->getLossRound($game->getTeamA()->first());
      if ($LossRound == 0) {
        $bracket = -1;
        return;
      }
      assert($LossRound % 3 == 0);
      $LossRound /= 3;
      $ind = $this->getWinnerGameInd($ind, $LossRound, $numTeams, $round);
      if ($round % 2 == 1) {
        $ind = $LossRound - 1 - $ind;
      }

      $firstRound = $LossRound * pow(2, $round);
      $round *= 2;
      $round--;

      $currentRound = $LossRound * 3 - 1;
      while ($game->getRound() < $currentRound) {
        if ($currentRound % 3 == 2) {
          $currentRound--;
          if ($currentRound == $game->getRound()) {
            if ($ind % 2 == 1) {
              $teams[0] = "B";
              $teams[1] = "A";
            }
          }
          $ind = intdiv($ind, 2);
        } else {
          $currentRound = $currentRound / 2;
        }
        $round++;
      }

      if ($currentRound % 3 == 2 && $currentRound == $LossRound * 3 - 1) {
        //the team coming from the winner bracket is in this case teamA but is always second team in jquery bracket
        $teams[0] = "B";
        $teams[1] = "A";
      }

      $bracket = 1;
    } else {
      //final
      $bracket = 2;
      $round = 0;
      $ind = 0;
    }


  }

  protected function initResults()
  {
    $results = [];
    $results[0] = []; //first bracket is winner pool bracket
    $results[0][0] = [];
    $results[0][0][0] = [];
    $results[1] = []; //second bracket is loser pool bracket
    $results[2] = []; //third bracket consists only of final

    return $results;
  }
//</editor-fold desc="Protected Methods">

//<editor-fold desc="Private Methods">
  private function getLossRound(Ranking $team)
  {
    $max = 0;
    foreach ($team->getGamesAsA() as $game) {
      if ($game->getResultA() < $game->getResultB()) {
        if ($game->getRound() > $max) {
          $max = $game->getRound();
        }
      }
    }
    foreach ($team->getGamesAsB() as $game) {
      if ($game->getResultB() < $game->getResultA()) {
        if ($game->getRound() > $max) {
          $max = $game->getRound();
        }
      }
    }
    return $max;
  }

  private function getWinnerGameInd($startInd, $gameRound, $numTeams, &$round)
  {
    $round = -1;
    $ind = $startInd;
    for ($i = $gameRound; $i < $numTeams; $i *= 2) {
      $ind = ($ind - ($ind % 2)) / 2;
      $round++;
    }
    return $ind;
  }
//</editor-fold desc="Private Methods">
//</editor-fold desc="Private Methods">
} 
