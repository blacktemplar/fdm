<?php

namespace DoubleKO\Factory;

use DoubleKO\Service\DoubleKOService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DoubleKOServiceFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $stableSort = $serviceLocator->get('StableSort');

    return new DoubleKOService($stableSort);
  }
//</editor-fold desc="Public Methods">
}

