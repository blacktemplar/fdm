<?php

namespace DoubleKO\Factory;

use DoubleKO\Controller\DoubleKOController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DoubleKOControllerFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $realServiceLocator = $serviceLocator->getServiceLocator();
    $em =
      $realServiceLocator->get('Doctrine\ORM\EntityManager');
    $fs = $realServiceLocator->get('FDM\Service\FlushServiceInterface');
    $ps =
      $realServiceLocator->get('FDM\Service\PhaseServiceInterface');
    $tas = $realServiceLocator->get(
      'FDM\Service\TournamentAccessServiceInterface'
    );
    $ls = $realServiceLocator->get(
      'FDM\Service\LoadingService'
    );

    return new DoubleKOController($em, $fs, $ps, $tas, $ls);
  }
//</editor-fold desc="Public Methods">
}
