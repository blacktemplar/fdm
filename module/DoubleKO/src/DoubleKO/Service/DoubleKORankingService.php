<?php

namespace DoubleKO\Service;

use FDM\Entity\Group;
use FDM\Entity\Ranking;
use SingleKO\Service\SingleKORankingService;

class DoubleKORankingService extends SingleKORankingService
{
  //round encoding
  //n-tel final: 3*n
  //round where losers from n-tel gets added 3*n - 1
  //second round after losers from n-tel get added 3*n - 2

//<editor-fold desc="Protected Methods">
  protected function compare(Ranking $r1, Ranking $r2)
  {
    return $this->compareProperties(
      $r1, $r2, [
        "getDisplayRank" => -1,
        "getNumLost" => -1,
        "getPhaseStartRank" => -1,
      ]
    );
  }

  protected function reset(Ranking $ranking, $numTeams)
  {
    parent::reset($ranking, $numTeams);

    $ranking->setLastRoundPlayed($ranking->getLastRoundPlayed() * 3);
  }

  /**
   * @param Group $group
   * @param Ranking[] $rankings
   */
  protected function setDisplayRanks(Group $group, $rankings)
  {
    //set display ranks which is the worst possible rank
    foreach ($rankings as $ranking) {
      $lostRound = $ranking->getLastRoundPlayed();
      if ($ranking->getNumLost() == 0) {
        if ($lostRound == 1) {
          $ranking->setDisplayRank(1);
        } else {
          assert($lostRound % 3 == 0);
          $lostRound /= 3;
          if ($lostRound == 1) {
            $lostRound = 0;
          } else {
            $lostRound /= 2;
          }
          $ranking->setDisplayRank($lostRound + 1);
        }
      } else {
        if ($ranking->getNumLost() == 1) {
          if ($lostRound % 3 == 0) {
            $lostRound -= 1;
          } else if ($lostRound % 3 == 2) {
            $lostRound -= 1;
          } else {
            assert($lostRound % 3 == 1);
            if ($lostRound == 1) {
              $lostRound = 0;
            } else {
              $lostRound = $lostRound / 2 - 1;
            }
          }
        }
        $lastWinnerRound = ($lostRound + 3 - ($lostRound % 3)) / 3;
        if ($lostRound % 3 == 2) {
          //$lastWinnerRound = ($lostRound + 1) / 3;
          $ranking->setDisplayRank($lastWinnerRound * 2 + 1);
        } else if ($lostRound % 3 == 1) {
          if ($lastWinnerRound == 1) {
            $ranking->setDisplayRank(2);
          } else {
            $ranking->setDisplayRank($lastWinnerRound * 3 / 2 + 1);
          }
        } else {
          assert($lostRound == 0);
          $ranking->setDisplayRank(1);
        }
      }
    }
  }
//</editor-fold desc="Protected Methods">
}
