<?php

namespace DoubleKO\Service;

use Doctrine\ORM\EntityManager;
use FDM\Entity\Game;
use FDM\Entity\Group;
use FDM\Service\StableSortService;
use SingleKO\Service\SingleKOService;

class DoubleKOService extends SingleKOService
{

//<editor-fold desc="Constructor">
  public function __construct(StableSortService $stableSort)
  {
    parent::__construct($stableSort);
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function getExtraStructures(Group $group)
  {
    $grandparent = $this->get_grandparent_class($this);
    return array_merge($grandparent::getExtraStructures($group), [
      "title" => "Bracket",
      "type" => "element",
      "controller" => "DoubleKO\Controller\DoubleKO",
      "action" => "bracket",
      "parameters" => [
        "id" => $group->getPhase()->getTournament()->getId(),
        "phase" => $group->getPhase()->getId(),
        "group" => $group->getId(),
      ],
    ]);
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Protected Methods">
  protected function calculateGames(Group $group, EntityManager $em, &$rankingByStart, &$teams, &$bestRank, &$isWaiting,
                                    &$ended)
  {
    for ($i = count($teams); $i > 1; $i = ($i + ($i % 2)) / 2) {
      foreach ($group->getGamesByType(Game::TYPE_FINISHED) as $game) {
        if ($game->getRound() < 3 * $i && $game->getRound() % 3 == 0 &&
          (2 * $game->getRound() >= 3 * $i)
        ) {
          $this->correctBestRanksAndTeams($bestRank, $teams, $game);
        }
      }

      foreach ($group->getGamesByType(Game::TYPE_FINISHED) as $game) {
        if ($game->getRound() < 3 * $i && $game->getRound() % 3 == 2 &&
          (2 * ($game->getRound() + 1) >= 3 * $i)
        ) {
          $this->correctBestRanksAndTeams($bestRank, $teams, $game);
        }
      }

      foreach ($group->getGamesByType(Game::TYPE_FINISHED) as $game) {
        if ($game->getRound() < 3 * $i && $game->getRound() % 3 == 1 &&
          (2 * ($game->getRound() + 2) >= 3 * $i)
        ) {
          $this->correctBestRanksAndTeams($bestRank, $teams, $game);
        }
      }
    }

    $refreshed = true;
    while ($refreshed) {
      $refreshed = false;
      foreach ($group->getRankings() as $ranking) {
        if ($isWaiting[$ranking->getPhaseStartRank()] &&
          $ranking->getLastRoundPlayed() > 1 &&
          $ranking->getNumLost() < 2
        ) {
          $r = $bestRank[$ranking->getPhaseStartRank()];
          $oponent = -1;
          if ($ranking->getLastRoundPlayed() % 3 == 0) {
            if ($ranking->getNumLost() == 1) {
              //lost the last game and must now be transfered to the loser pool
              $realRound = $ranking->getLastRoundPlayed() / 3;
              $minPl = $realRound + 1;
              $maxPl = $realRound * 3;
              $sum = $minPl + $maxPl - 1 + 2 * (($r - $minPl) % 2);
              if ($minPl == 2) {
                assert($maxPl == 3);
                $sum = 5;
              }
              $oponent = $sum - $r;
              $nextRound = $ranking->getLastRoundPlayed() - 1;
              if ($oponent > count($teams)) {
                $ranking->setLastRoundPlayed($ranking->getLastRoundPlayed() - 1);
                $refreshed = true;
              }
            } else {
              //still in winner pool
              if ($ranking->getLastRoundPlayed() == 3) {
                //won winner pool final
                assert($r == 1);
                $oponent = 2;
                $nextRound = 1;
              } else {
                $nextRealRound = $ranking->getLastRoundPlayed() / 3 / 2;
                $oponent = $nextRealRound * 2 + 1 - $r;
                $nextRound = 3 * $nextRealRound;
              }
            }
          }

          if ($ranking->getLastRoundPlayed() % 3 == 2 && $ranking->getLastRoundPlayed() > 3) {
            $winnerFinal = ($ranking->getLastRoundPlayed() + 1) / 3;
            $oponent = $winnerFinal * 3 + 1 - $r;
            $nextRound = $ranking->getLastRoundPlayed() - 1;
            if ($oponent > count($teams)) {
              //playing against bye
              $ranking->setLastRoundPlayed($nextRound);
              $refreshed = true;
              continue;
            }
          }

          if ($oponent >= 0) {
            $oRanking = $teams[$oponent];
            if ($isWaiting[$oRanking->getPhaseStartRank()]) {
              if (($oRanking->getLastRoundPlayed() == $ranking->getLastRoundPlayed() && $nextRound > 1)
                || ($nextRound == 1 && $ranking->getLastRoundPlayed() == 3 && $oRanking->getLastRoundPlayed() == 2)
                || $oRanking->getLastRoundPlayed() == 2 * $ranking->getLastRoundPlayed() - 2
                || $ranking->getLastRoundPlayed() == 2 * $oRanking->getLastRoundPlayed() - 2
              ) {
                $ended = false;
                $game = new Game();
                $game->addToTeamA($ranking);
                $game->addToTeamB($oRanking);
                $game->setRound($nextRound);
                $game->setType(Game::TYPE_SCHEDULED_DYN);
                $game->setRelevance(
                  $this->computeRelevance($group) + $nextRound
                );
                $game->setGroup($group);
                $em->persist($game);
                $isWaiting[$ranking->getPhaseStartRank()] = false;
                $isWaiting[$oRanking->getPhaseStartRank()] = false;
              }
            }
          }
        }
      }
    }
  }

  protected function correctBestRanksAndTeams(&$bestRank, &$teams, $game)
  {
    if ($game->getResultA() > $game->getResultB()) {
      $winner = $game->getTeamA()->first()->getPhaseStartRank();
      $loser = $game->getTeamB()->first()->getPhaseStartRank();
    } else {
      $winner = $game->getTeamB()->first()->getPhaseStartRank();
      $loser = $game->getTeamA()->first()->getPhaseStartRank();
    }

    if ($bestRank[$winner] > $bestRank[$loser]) {
      $tmp = $bestRank[$winner];
      $bestRank[$winner] = $bestRank[$loser];
      $bestRank[$loser] = $tmp;
      $tmp = $teams[$bestRank[$winner]];
      $teams[$bestRank[$winner]] = $teams[$bestRank[$loser]];
      $teams[$bestRank[$loser]] = $tmp;
    }
  }
//</editor-fold desc="Protected Methods">

//<editor-fold desc="Private Methods">
  private function get_grandparent_class($thing)
  {
    if (is_object($thing)) {
      $thing = get_class($thing);
    }
    return get_parent_class(get_parent_class($thing));
  }
//</editor-fold desc="Private Methods">
}
