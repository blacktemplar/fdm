<?php
return [
  'service_manager' => [
    'factories' => [],
    'invokable' => [],
  ],
  'controllers' => [
    'factories' => [
      'Admin\Controller\Admin' =>
        '\Admin\Factory\AdminFactory'
    ],
    'invokables' => [],
  ],
  'view_manager' => [
    'template_path_stack' => [
      'admin' => __DIR__ . '/../view',
    ],
  ],
  'router' => [
    'routes' => [
      'tournament' => [
        'child_routes' => [
          'admin' => [
            'type' => 'segment',
            'may_terminate' => false,
            'options' => [
              'route' => '/admin',
            ],
            'child_routes' => [
              'browse' => [
                'type' => 'segment',
                'may_terminate' => true,
                'options' => [
                  'defaults' => [
                    'controller' => 'Admin\Controller\Admin',
                    'action' => 'browse',
                  ],
                  'route' => '/browse',
                ],
              ],
              'testSpeak' => [
                'type' => 'segment',
                'may_terminate' => true,
                'options' => [
                  'defaults' => [
                    'controller' => 'Admin\Controller\Admin',
                    'action' => 'testSpeak',
                  ],
                  'route' => '/testSpeak',
                ],
              ],
            ],
          ],
        ],
      ],
      'admin' => [
        'type' => 'segment',
        'may_terminate' => false,
        'options' => [
          'route' => '/admin',
        ],
        'child_routes' => [
          'mod' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'defaults' => [
                'controller' => 'Admin\Controller\Admin',
                'action' => 'mod',
              ],
              'route' => '/mod',
            ],
          ],
          'mergePlayers' => [
            'type' => 'segment',
            'may_terminate' => true,
            'options' => [
              'defaults' => [
                'controller' => 'Admin\Controller\Admin',
                'action' => 'mergePlayers',
              ],
              'route' => '/mergePlayers',
            ],
          ],
        ],
      ],
    ],
  ],
  'strategies' => [
    'ViewJsonStrategy',
  ],
];
