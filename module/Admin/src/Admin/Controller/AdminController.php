<?php
namespace Admin\Controller;

use Admin\Form\MergePlayersForm;
use Doctrine\ORM\EntityManager;
use FDM\Entity\SpeakTask;
use FDM\Service\FlushServiceInterface;
use FDM\Service\PlayersService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminController extends AbstractActionController
{
//<editor-fold desc="Fields">
  protected $em;

  /**
   * @var FlushServiceInterface
   */
  protected $fs;

  /**
   * @var PlayersService
   */
  protected $ps;
//</editor-fold desc="Fields">

//<editor-fold desc="Constructor">
  public function __construct(
    EntityManager $em,
    PlayersService $ps,
    $fs
  )
  {
    $this->em = $em;
    $this->ps = $ps;
    $this->fs = $fs;
  }
//</editor-fold desc="Constructor">

//<editor-fold desc="Public Methods">
  public function browseAction()
  {
    $tournamentId = $this->params('id');
    $tournament = $this->em->find('FDM\Entity\Tournament', $tournamentId);
    return new ViewModel(
      [
        "tournament" => $tournament,
      ]
    );
  }

  public function mergePlayersAction()
  {
    $form = new MergePlayersForm();

    $error = "";

    $request = $this->getRequest();
    if ($request->isPost()) {
      $form->setData($request->getPost());

      if ($form->isValid()) {

        $id1 = $form->getData()["id1"];
        $id2 = $form->getData()["id2"];
        $result = $this->ps->mergePlayers($id1, $id2);
        if ($result !== true) {
          $error = $result;
        } else {
          $this->em->flush();
          $form = new MergePlayersForm();
        }
      }
    }
    return new ViewModel(["form" => $form, "players" => $this->ps->getPlayersAsArray(), "error" => $error]);
  }

  public function modAction()
  {
  }

  public function testSpeakAction()
  {
    $tournamentId = $this->params('id');
    $tournament = $this->em->find('FDM\Entity\Tournament', $tournamentId);
    $speakTask = new SpeakTask("Das ist ein Test.", "Test-Task", $tournament);
    $this->em->persist($speakTask);
    $this->fs->flush();

    return $this->redirect()->toRoute('tournament', ['id' => $tournamentId]);
  }
//</editor-fold desc="Public Methods">

//<editor-fold desc="Private Methods">
  private function getMergeFormView($form)
  {
    return new ViewModel(["form" => $form, "players" => $this->ps->getPlayersAsArray()]);
  }
//</editor-fold desc="Private Methods">
} 
