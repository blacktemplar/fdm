<?php

namespace Admin\Factory;

use Admin\Controller\AdminController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AdminFactory implements FactoryInterface
{
//<editor-fold desc="Public Methods">
  public function createService(ServiceLocatorInterface $serviceLocator)
  {
    $realServiceLocator = $serviceLocator->getServiceLocator();
    $em =
      $realServiceLocator->get('Doctrine\ORM\EntityManager');
    $ps =
      $realServiceLocator->get('FDM\Service\PlayersService');
    $fs = $realServiceLocator->get('FDM\Service\FlushServiceInterface');
    return new AdminController($em, $ps, $fs);
  }
//</editor-fold desc="Public Methods">
}
