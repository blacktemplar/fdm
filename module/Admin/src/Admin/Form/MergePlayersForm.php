<?php
/**
 * Created by PhpStorm.
 * User: benedikt
 * Date: 10/16/16
 * Time: 4:04 PM
 */

namespace Admin\Form;


use Zend\Form\Form;

class MergePlayersForm extends Form
{
//<editor-fold desc="Constructor">
  public function __construct()
  {
    // we want to ignore the name passed
    parent::__construct();

    $this->add(
      [
        'name' => 'name1',
        'type' => 'Text',
        'attributes' => [
          'class' => 'AutoComplete',
          'id' => 'name1',
        ],
      ]
    );

    $this->add(
      [
        'name' => 'id1',
        'type' => 'Text',
        'attributes' => [
          'id' => 'id1',
        ],
      ]
    );

    $this->add(
      [
        'name' => 'name2',
        'type' => 'Text',
        'attributes' => [
          'class' => 'AutoComplete',
          'id' => 'name2',
        ],
      ]
    );

    $this->add(
      [
        'name' => 'id2',
        'type' => 'Text',
        'attributes' => [
          'id' => 'id2',
        ],
      ]
    );

    $this->add(
      [
        'name' => 'merge',
        'type' => 'Zend\Form\Element\Submit',
        'attributes' => [
          'value' => 'Merge',
          'class' => 'merge'
        ],
      ]
    );
  }
//</editor-fold desc="Constructor">
}
