CREATE TABLE tournamentranking (
  id          CHAR(36)     NOT NULL
  COMMENT '(DC2Type:guid)',
  name        VARCHAR(255) NOT NULL,
  serviceName VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
ALTER TABLE tournament
  ADD end DATETIME DEFAULT NULL;
CREATE TABLE relation__tournamentrankings (
  tournament_id        CHAR(36) NOT NULL
  COMMENT '(DC2Type:guid)',
  tournamentranking_id CHAR(36) NOT NULL
  COMMENT '(DC2Type:guid)',
  INDEX IDX_6563C28B33D1A3E7 (tournament_id),
  INDEX IDX_6563C28BBDEA4F88 (tournamentranking_id),
  PRIMARY KEY (tournament_id, tournamentranking_id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
ALTER TABLE relation__tournamentrankings
  ADD CONSTRAINT FK_6563C28B33D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id)
  ON DELETE CASCADE;
ALTER TABLE relation__tournamentrankings
  ADD CONSTRAINT FK_6563C28BBDEA4F88 FOREIGN KEY (tournamentranking_id) REFERENCES tournamentranking (id)
  ON DELETE CASCADE;
CREATE TABLE tournamentrankinginstance (
  id                   CHAR(36)   NOT NULL
  COMMENT '(DC2Type:guid)',
  lastEntryTime        DATETIME   NOT NULL,
  isCurrent            TINYINT(1) NOT NULL,
  tournamentRanking_id CHAR(36) DEFAULT NULL
  COMMENT '(DC2Type:guid)',
  INDEX IDX_57820A01F2B74C58 (tournamentRanking_id),
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
ALTER TABLE tournamentrankinginstance
  ADD CONSTRAINT FK_57820A01F2B74C58 FOREIGN KEY (tournamentRanking_id) REFERENCES tournamentranking (id);
CREATE TABLE tournamentrankingentry (
  id                           CHAR(36)         NOT NULL
  COMMENT '(DC2Type:guid)',
  player_id                    CHAR(36) DEFAULT NULL
  COMMENT '(DC2Type:guid)',
  points                       DOUBLE PRECISION NOT NULL,
  tournaments                  INT              NOT NULL,
  tournamentRankingInstance_id CHAR(36) DEFAULT NULL
  COMMENT '(DC2Type:guid)',
  INDEX IDX_A50085599E6F5DF (player_id),
  INDEX IDX_A50085545009231 (tournamentRankingInstance_id),
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
CREATE TABLE tournamentranking__rank (
  id         CHAR(36) NOT NULL
  COMMENT '(DC2Type:guid)',
  rankPoints LONGTEXT NOT NULL
  COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
ALTER TABLE tournamentrankingentry
  ADD CONSTRAINT FK_A50085599E6F5DF FOREIGN KEY (player_id) REFERENCES player (id);
ALTER TABLE tournamentrankingentry
  ADD CONSTRAINT FK_A50085545009231 FOREIGN KEY (tournamentRankingInstance_id) REFERENCES tournamentrankinginstance (id);
ALTER TABLE tournamentranking__rank
  ADD CONSTRAINT FK_1441D1B0BF396750 FOREIGN KEY (id) REFERENCES tournamentranking (id)
  ON DELETE CASCADE;
ALTER TABLE tournamentranking
  ADD dtype VARCHAR(255) NOT NULL;
ALTER TABLE tournamentrankinginstance
  ADD fixed TINYINT(1) NOT NULL,
  CHANGE iscurrent current TINYINT(1) NOT NULL;
ALTER TABLE tournamentranking
  ADD timeColumn VARCHAR(255) NOT NULL;
