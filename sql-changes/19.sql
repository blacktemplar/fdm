ALTER TABLE tournament
  ADD startText LONGTEXT NOT NULL;
ALTER TABLE phase
  ADD startText LONGTEXT NOT NULL;
ALTER TABLE speaktask
  ADD orderNumber INT NOT NULL;