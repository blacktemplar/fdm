# cleaning duplicated groups!
DELETE g.*, rA.*, rB.* FROM groups g1
  INNER JOIN groups g2
    ON g2.phase_id = g1.phase_id AND g2.groupNumber = g1.groupNumber AND g1.id != g2.id
  INNER JOIN phase p
    ON p.id = g1.phase_id
  INNER JOIN tournament t
    ON t.id = p.tournament_id
  INNER JOIN game g
    ON g.group_id = g1.id
  LEFT JOIN relation__gamerankings__teamA rA
    ON rA.game_id = g.id
  LEFT JOIN relation__gamerankings__teamB rB
    ON rB.game_id = g.id
WHERE g1.finished = 0 AND g2.finished = 1;

DELETE ra.*, r.* FROM groups g1
  INNER JOIN groups g2
    ON g2.phase_id = g1.phase_id AND g2.groupNumber = g1.groupNumber AND g1.id != g2.id
  INNER JOIN phase p
    ON p.id = g1.phase_id
  INNER JOIN tournament t
    ON t.id = p.tournament_id
  LEFT JOIN ranking r
    ON r.group_id = g1.id
  LEFT JOIN relation__rankingattendants ra
    ON ra.ranking_id = r.id
WHERE g1.finished = 0 AND g2.finished = 1;

DELETE g1.* FROM groups g1
  INNER JOIN groups g2
    ON g2.phase_id = g1.phase_id AND g2.groupNumber = g1.groupNumber AND g1.id != g2.id
  INNER JOIN phase p
    ON p.id = g1.phase_id
  INNER JOIN tournament t
    ON t.id = p.tournament_id
WHERE g1.finished = 0 AND g2.finished = 1;