-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 10, 2016 at 03:36 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fdm-dev`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETE TOURNAMENT` (IN `tid` CHAR(36) CHARSET utf8)  MODIFIES SQL DATA
BEGIN

DELETE FROM tournamentprivileges WHERE tournament_id = tid COLLATE utf8_unicode_ci;


DELETE re.* FROM relation__rankingattendants AS re
INNER JOIN attendant as a
	ON a.id = re.attendant_id
WHERE a.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE re.* FROM relation__gamerankings__teamA AS re
INNER JOIN game as ga
	ON ga.id = re.game_id
INNER JOIN groups as g
	ON g.id = ga.group_id
INNER JOIN phase AS p
	ON p.id = g.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE re.* FROM relation__gamerankings__teamB AS re
INNER JOIN game as ga
	ON ga.id = re.game_id
INNER JOIN groups as g
	ON g.id = ga.group_id
INNER JOIN phase AS p
	ON p.id = g.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE ga.* FROM game AS ga
INNER JOIN groups as g
	ON g.id = ga.group_id
INNER JOIN phase AS p
	ON p.id = g.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE rs.* FROM ranking__singleko AS rs
INNER JOIN ranking as r
	ON r.id = rs.id
INNER JOIN groups as g
	ON g.id = r.group_id
INNER JOIN phase AS p
	ON p.id = g.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE rs.* FROM ranking__points AS rs
INNER JOIN ranking as r
	ON r.id = rs.id
INNER JOIN groups as g
	ON g.id = r.group_id
INNER JOIN phase AS p
	ON p.id = g.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE r.* FROM ranking AS r
INNER JOIN groups as g
	ON g.id = r.group_id
INNER JOIN phase AS p
	ON p.id = g.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE g.* FROM groups AS g
INNER JOIN phase AS p
	ON p.id = g.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE ms.* FROM mode__singleko AS ms
INNER JOIN mode__abstract AS m
	ON m.id = ms.id
INNER JOIN phase AS p
	ON p.id = m.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE ms.* FROM mode__fullround AS ms
INNER JOIN mode__abstract AS m
	ON m.id = ms.id
INNER JOIN phase AS p
	ON p.id = m.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE m.* FROM mode__abstract AS m
INNER JOIN phase AS p
	ON p.id = m.phase_id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE a.* FROM attendancestrategy AS a
INNER JOIN phase AS p
	ON p.attendanceStrategy_id = a.id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE p, a FROM attendancestrategy__abstract AS a
INNER JOIN phase AS p
	ON p.attendanceStrategy_id = a.id
WHERE p.tournament_id = tid COLLATE utf8_unicode_ci;

DELETE FROM phase WHERE tournament_id = tid COLLATE utf8_unicode_ci;

DELETE FROM attendant WHERE tournament_id = tid COLLATE utf8_unicode_ci;

DELETE FROM tournament WHERE id = tid COLLATE utf8_unicode_ci;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `MERGE PLAYER` (IN `ID1` CHAR(36) CHARSET utf8, IN `ID2` CHAR(36) CHARSET utf8)  NO SQL
BEGIN

UPDATE attendant SET player_id = ID1
WHERE player_id = ID2 COLLATE utf8_unicode_ci;

DELETE FROM player WHERE id = ID2 COLLATE utf8_unicode_ci;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SHOW DELETED` ()  SELECT * FROM (
    	SELECT id, "tournament" as type, name as val 
    	FROM tournament
    	WHERE tournament.deleted
    UNION
        SELECT id, "phase" as type, name as val  FROM phase 
        WHERE tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT id, "attendant" as type, CONCAT(firstName, " ", lastName) as val 
        FROM attendant 
        WHERE tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT g.id as id, "group" as type, g.groupNumber as val 
        FROM groups AS g
        INNER JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT m.id as id, "mode" as type, m.dtype as val 
        FROM mode__abstract AS m
        INNER JOIN phase AS p
            ON p.id = m.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
    	SELECT mf.id as id, "mode__fullround" as type, "" as val 
        FROM mode__fullround AS mf
    	INNER JOIN mode__abstract AS m
    		ON m.id = mf.id
        INNER JOIN phase AS p
            ON p.id = m.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
    	SELECT mf.id as id, "mode__singleko" as type, "" as val 
        FROM mode__singleko AS mf
    	INNER JOIN mode__abstract AS m
    		ON m.id = mf.id
        INNER JOIN phase AS p
            ON p.id = m.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT a.id as id, "attendancestrategy__abstract" as type, "" as val 
        FROM attendancestrategy__abstract AS a
        INNER JOIN phase AS p
        	ON p.attendanceStrategy_id = a.id
    	WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT a.id as id, "attendancestrategy" as type, "" as val 
        FROM attendancestrategy AS a
        INNER JOIN phase AS p
        	ON p.attendanceStrategy_id = a.id
    	INNER JOIN attendancestrategy__abstract AS aa
    		ON aa.id = a.id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT pl.id as id, "player" as type, 
    		CONCAT(pl.firstName, " ", pl.lastName) as val 
        FROM player AS pl
    	INNER JOIN attendant AS a
    		ON a.player_id = pl.id
        WHERE a.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT r.id as id, "ranking" as type, r.dtype as val 
        FROM ranking AS r
    	INNER JOIN groups AS g
    		ON g.id = r.group_id
    	INNER JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT rp.id as id, "ranking__points" as type, "" as val 
        FROM ranking__points AS rp
    	INNER JOIN ranking AS r
    		ON r.id = rp.id
    	INNER JOIN groups AS g
    		ON g.id = r.group_id
    	INNER JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT rp.id as id, "ranking__singleko" as type, 
    		"" as val 
        FROM ranking__singleko AS rp
    	INNER JOIN ranking AS r
    		ON r.id = rp.id
    	INNER JOIN groups AS g
    		ON g.id = r.group_id
    	INNER JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT ga.id as id, "game" as type, 
    		"" as val 
        FROM game AS ga
    	INNER JOIN groups as g
    		ON g.id = ga.group_id
    	INNER JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT re.game_id as id, 
    		"relation__gamerankings__teamA" as type, 
    		re.ranking_id as val 
        FROM relation__gamerankings__teamA AS re
    	INNER JOIN game as ga
    		ON ga.id = re.game_id
    	INNER JOIN ranking as r
    		ON r.id = re.ranking_id
    	INNER JOIN groups as g
    		ON g.id = ga.group_id AND g.id = r.group_id
    	INNER JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT re.game_id as id, 
    		"relation__gamerankings__teamB" as type, 
    		re.ranking_id as val 
        FROM relation__gamerankings__teamB AS re
    	INNER JOIN game as ga
    		ON ga.id = re.game_id
    	INNER JOIN ranking as r
    		ON r.id = re.ranking_id
    	INNER JOIN groups as g
    		ON g.id = ga.group_id AND g.id = r.group_id
    	INNER JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT re.ranking_id as id, 
    		"relation__rankingattendants" as type, 
    		re.attendant_id as val 
        FROM relation__rankingattendants AS re
    	INNER JOIN attendant as a
    		ON a.id = re.attendant_id
    	INNER JOIN ranking as r
    		ON r.id = re.ranking_id
    	INNER JOIN groups as g
    		ON g.id = r.group_id
    	INNER JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
    UNION
        SELECT tp.id as id, 
    		"tournamentprivileges" as type, 
    		tp.user as val
        FROM tournamentprivileges AS tp
    	INNER JOIN myuser AS u
    		ON u.user_id = tp.user
        WHERE tp.tournament_id NOT IN (SELECT id FROM tournament WHERE NOT tournament.deleted)
) as tmp$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SHOW UNCONNECTED` ()  SELECT * FROM (
        SELECT id, "phase" as type, name as val  FROM phase 
        WHERE tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT id, "attendant" as type, CONCAT(firstName, " ", lastName) as val 
        FROM attendant 
        WHERE tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT g.id as id, "group" as type, g.groupNumber as val 
        FROM groups AS g
        LEFT JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT m.id as id, "mode" as type, m.dtype as val 
        FROM mode__abstract AS m
        LEFT JOIN phase AS p
            ON p.id = m.phase_id
        WHERE p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
    	SELECT mf.id as id, "mode__fullround" as type, "" as val 
        FROM mode__fullround AS mf
    	LEFT JOIN mode__abstract AS m
    		ON m.id = mf.id
        LEFT JOIN phase AS p
            ON p.id = m.phase_id
        WHERE m.id IS NULL OR p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
    	SELECT mf.id as id, "mode__singleko" as type, "" as val 
        FROM mode__singleko AS mf
    	LEFT JOIN mode__abstract AS m
    		ON m.id = mf.id
        LEFT JOIN phase AS p
            ON p.id = m.phase_id
        WHERE m.id IS NULL OR p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT a.id as id, "attendancestrategy__abstract" as type, "" as val 
        FROM attendancestrategy__abstract AS a
        LEFT JOIN phase AS p
        	ON p.attendanceStrategy_id = a.id
        WHERE p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT a.id as id, "attendancestrategy" as type, "" as val 
        FROM attendancestrategy AS a
        LEFT JOIN phase AS p
        	ON p.attendanceStrategy_id = a.id
    	LEFT JOIN attendancestrategy__abstract AS aa
    		ON aa.id = a.id
        WHERE aa.id IS NULL OR p.id IS NULL OR
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT pl.id as id, "player" as type, 
    		CONCAT(pl.firstName, " ", pl.lastName) as val 
        FROM player AS pl
    	LEFT JOIN attendant AS a
    		ON a.player_id = pl.id
        WHERE a.id IS NULL OR
    		a.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT r.id as id, "ranking" as type, r.dtype as val 
        FROM ranking AS r
    	LEFT JOIN groups AS g
    		ON g.id = r.group_id
    	LEFT JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE g.id IS NULL OR p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT rp.id as id, "ranking__points" as type, "" as val 
        FROM ranking__points AS rp
    	LEFT JOIN ranking AS r
    		ON r.id = rp.id
    	LEFT JOIN groups AS g
    		ON g.id = r.group_id
    	LEFT JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE r.id IS NULL or g.id IS NULL OR p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT rp.id as id, "ranking__singleko" as type, 
    		"" as val 
        FROM ranking__singleko AS rp
    	LEFT JOIN ranking AS r
    		ON r.id = rp.id
    	LEFT JOIN groups AS g
    		ON g.id = r.group_id
    	LEFT JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE r.id IS NULL or g.id IS NULL OR p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT ga.id as id, "game" as type, 
    		"" as val 
        FROM game AS ga
    	LEFT JOIN groups as g
    		ON g.id = ga.group_id
    	LEFT JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE g.id IS NULL OR p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT re.game_id as id, 
    		"relation__gamerankings__teamA" as type, 
    		re.ranking_id as val 
        FROM relation__gamerankings__teamA AS re
    	LEFT JOIN game as ga
    		ON ga.id = re.game_id
    	LEFT JOIN ranking as r
    		ON r.id = re.ranking_id
    	LEFT JOIN groups as g
    		ON g.id = ga.group_id AND g.id = r.group_id
    	LEFT JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE ga.id IS NULL OR r.id IS NULL OR 
    		g.id IS NULL OR p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT re.game_id as id, 
    		"relation__gamerankings__teamB" as type, 
    		re.ranking_id as val 
        FROM relation__gamerankings__teamB AS re
    	LEFT JOIN game as ga
    		ON ga.id = re.game_id
    	LEFT JOIN ranking as r
    		ON r.id = re.ranking_id
    	LEFT JOIN groups as g
    		ON g.id = ga.group_id AND g.id = r.group_id
    	LEFT JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE ga.id IS NULL OR r.id IS NULL OR 
    		g.id IS NULL OR p.id IS NULL OR 
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT re.ranking_id as id, 
    		"relation__rankingattendants" as type, 
    		re.attendant_id as val 
        FROM relation__rankingattendants AS re
    	LEFT JOIN attendant as a
    		ON a.id = re.attendant_id
    	LEFT JOIN ranking as r
    		ON r.id = re.ranking_id
    	LEFT JOIN groups as g
    		ON g.id = r.group_id
    	LEFT JOIN phase AS p
        	ON p.id = g.phase_id
        WHERE a.id IS NULL OR r.id IS NULL OR 
    		g.id IS NULL OR p.id IS NULL OR
    		a.tournament_id != p.tournament_id OR
    		p.tournament_id NOT IN (SELECT id FROM tournament)
    UNION
        SELECT tp.id as id, 
    		"tournamentprivileges" as type, 
    		tp.user as val
        FROM tournamentprivileges AS tp
    	LEFT JOIN myuser AS u
    		ON u.user_id = tp.user
        WHERE u.user_id IS NULL OR
    		tp.tournament_id NOT IN (SELECT id FROM tournament)
) as tmp$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `TEST` (IN `tid` CHAR(36) CHARSET utf8)  MODIFIES SQL DATA
BEGIN

SELECT re.* FROM relation__rankingattendants AS re
INNER JOIN attendant as a
	ON a.id = re.attendant_id
WHERE a.tournament_id = tid COLLATE utf8_unicode_ci;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `attendancestrategy`
--

CREATE TABLE `attendancestrategy` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `preShuffle` tinyint(1) NOT NULL,
  `preGroupingShuffle` tinyint(1) NOT NULL,
  `postGroupingShuffle` tinyint(1) NOT NULL,
  `merge` int(11) NOT NULL,
  `numGroups` int(11) NOT NULL,
  `maxPlayersPerGroup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendancestrategy__abstract`
--

CREATE TABLE `attendancestrategy__abstract` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `template` tinyint(1) NOT NULL,
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendant`
--

CREATE TABLE `attendant` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `tournament_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `startNumber` int(11) DEFAULT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `displayName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `generatedDisplayName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `player_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `payed` tinyint(1) NOT NULL,
  `spelledName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `group_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `resultA` int(11) NOT NULL,
  `resultB` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `tableNr` int(11) NOT NULL,
  `relevance` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `phase_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `mode_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `groupNumber` int(11) NOT NULL,
  `started` tinyint(1) NOT NULL,
  `finished` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mode__abstract`
--

CREATE TABLE `mode__abstract` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `phase_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `template` tinyint(1) NOT NULL,
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `groupNumber` int(11) NOT NULL,
  `serviceName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rankingServiceName` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mode__fullround`
--

CREATE TABLE `mode__fullround` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mode__singleko`
--

CREATE TABLE `mode__singleko` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `myuser`
--

CREATE TABLE `myuser` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `state` smallint(6) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phase`
--

CREATE TABLE `phase` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `tournament_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `template` tinyint(1) NOT NULL,
  `phaseNumber` int(11) NOT NULL,
  `started` tinyint(1) NOT NULL,
  `finished` tinyint(1) NOT NULL,
  `attendanceStrategyServiceName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `singleMode` tinyint(1) NOT NULL,
  `attendanceStrategy_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `numTables` int(11) NOT NULL,
  `lastChanged` datetime DEFAULT NULL,
  `displaySpeakButton` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ranking`
--

CREATE TABLE `ranking` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `group_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `phaseStartRank` int(11) NOT NULL,
  `displayRank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ranking__points`
--

CREATE TABLE `ranking__points` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `points` int(11) NOT NULL,
  `gamesPlayed` int(11) NOT NULL,
  `goalDifference` int(11) NOT NULL,
  `goalsShot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ranking__singleko`
--

CREATE TABLE `ranking__singleko` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `numLost` int(11) NOT NULL,
  `lastRoundPlayed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `relation__gamerankings__teamA`
--

CREATE TABLE `relation__gamerankings__teamA` (
  `game_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `ranking_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `relation__gamerankings__teamB`
--

CREATE TABLE `relation__gamerankings__teamB` (
  `game_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `ranking_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `relation__rankingattendants`
--

CREATE TABLE `relation__rankingattendants` (
  `ranking_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `attendant_id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tournament`
--

CREATE TABLE `tournament` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `parent_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `started` tinyint(1) NOT NULL,
  `template` tinyint(1) NOT NULL,
  `public` tinyint(1) NOT NULL,
  `start` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tournamentprivileges`
--

CREATE TABLE `tournamentprivileges` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `tournament_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:guid)',
  `type` int(11) NOT NULL,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendancestrategy`
--
ALTER TABLE `attendancestrategy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendancestrategy__abstract`
--
ALTER TABLE `attendancestrategy__abstract`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendant`
--
ALTER TABLE `attendant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B2508F9133D1A3E7` (`tournament_id`),
  ADD KEY `IDX_B2508F9199E6F5DF` (`player_id`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_232B318CFE54D947` (`group_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F06D397099091188` (`phase_id`),
  ADD KEY `IDX_F06D397077E5854A` (`mode_id`);

--
-- Indexes for table `mode__abstract`
--
ALTER TABLE `mode__abstract`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3C2B377999091188` (`phase_id`);

--
-- Indexes for table `mode__fullround`
--
ALTER TABLE `mode__fullround`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mode__singleko`
--
ALTER TABLE `mode__singleko`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `myuser`
--
ALTER TABLE `myuser`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UNIQ_5DBD36CCF85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_5DBD36CCE7927C74` (`email`);

--
-- Indexes for table `phase`
--
ALTER TABLE `phase`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_B1BDD6CBF664FB85` (`attendanceStrategy_id`),
  ADD KEY `IDX_B1BDD6CB33D1A3E7` (`tournament_id`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80B839D0FE54D947` (`group_id`);

--
-- Indexes for table `ranking__points`
--
ALTER TABLE `ranking__points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ranking__singleko`
--
ALTER TABLE `ranking__singleko`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relation__gamerankings__teamA`
--
ALTER TABLE `relation__gamerankings__teamA`
  ADD PRIMARY KEY (`game_id`,`ranking_id`),
  ADD KEY `IDX_7020D870E48FD905` (`game_id`),
  ADD KEY `IDX_7020D87020F64684` (`ranking_id`);

--
-- Indexes for table `relation__gamerankings__teamB`
--
ALTER TABLE `relation__gamerankings__teamB`
  ADD PRIMARY KEY (`game_id`,`ranking_id`),
  ADD KEY `IDX_E92989CAE48FD905` (`game_id`),
  ADD KEY `IDX_E92989CA20F64684` (`ranking_id`);

--
-- Indexes for table `relation__rankingattendants`
--
ALTER TABLE `relation__rankingattendants`
  ADD PRIMARY KEY (`ranking_id`,`attendant_id`),
  ADD KEY `IDX_D62FF7820F64684` (`ranking_id`),
  ADD KEY `IDX_D62FF784DE0C235` (`attendant_id`);

--
-- Indexes for table `tournament`
--
ALTER TABLE `tournament`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_BD5FB8D9727ACA70` (`parent_id`);

--
-- Indexes for table `tournamentprivileges`
--
ALTER TABLE `tournamentprivileges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F549F01E33D1A3E7` (`tournament_id`),
  ADD KEY `IDX_F549F01E8D93D649` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `myuser`
--
ALTER TABLE `myuser`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendancestrategy`
--
ALTER TABLE `attendancestrategy`
  ADD CONSTRAINT `FK_67C94A92BF396750` FOREIGN KEY (`id`) REFERENCES `attendancestrategy__abstract` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attendant`
--
ALTER TABLE `attendant`
  ADD CONSTRAINT `FK_B2508F9133D1A3E7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`),
  ADD CONSTRAINT `FK_B2508F9199E6F5DF` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`);

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `FK_232B318CFE54D947` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `FK_F06D397077E5854A` FOREIGN KEY (`mode_id`) REFERENCES `mode__abstract` (`id`),
  ADD CONSTRAINT `FK_F06D397099091188` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`);

--
-- Constraints for table `mode__abstract`
--
ALTER TABLE `mode__abstract`
  ADD CONSTRAINT `FK_3C2B377999091188` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`);

--
-- Constraints for table `mode__fullround`
--
ALTER TABLE `mode__fullround`
  ADD CONSTRAINT `FK_956DC363BF396750` FOREIGN KEY (`id`) REFERENCES `mode__abstract` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mode__singleko`
--
ALTER TABLE `mode__singleko`
  ADD CONSTRAINT `FK_6DC96256BF396750` FOREIGN KEY (`id`) REFERENCES `mode__abstract` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `phase`
--
ALTER TABLE `phase`
  ADD CONSTRAINT `FK_B1BDD6CB33D1A3E7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`),
  ADD CONSTRAINT `FK_B1BDD6CBF664FB85` FOREIGN KEY (`attendanceStrategy_id`) REFERENCES `attendancestrategy__abstract` (`id`);

--
-- Constraints for table `ranking`
--
ALTER TABLE `ranking`
  ADD CONSTRAINT `FK_80B839D0FE54D947` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Constraints for table `ranking__points`
--
ALTER TABLE `ranking__points`
  ADD CONSTRAINT `FK_D6583FC0BF396750` FOREIGN KEY (`id`) REFERENCES `ranking` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ranking__singleko`
--
ALTER TABLE `ranking__singleko`
  ADD CONSTRAINT `FK_170D30D3BF396750` FOREIGN KEY (`id`) REFERENCES `ranking` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `relation__gamerankings__teamA`
--
ALTER TABLE `relation__gamerankings__teamA`
  ADD CONSTRAINT `FK_7020D87020F64684` FOREIGN KEY (`ranking_id`) REFERENCES `ranking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_7020D870E48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `relation__gamerankings__teamB`
--
ALTER TABLE `relation__gamerankings__teamB`
  ADD CONSTRAINT `FK_E92989CA20F64684` FOREIGN KEY (`ranking_id`) REFERENCES `ranking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_E92989CAE48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `relation__rankingattendants`
--
ALTER TABLE `relation__rankingattendants`
  ADD CONSTRAINT `FK_D62FF7820F64684` FOREIGN KEY (`ranking_id`) REFERENCES `ranking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_D62FF784DE0C235` FOREIGN KEY (`attendant_id`) REFERENCES `attendant` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tournament`
--
ALTER TABLE `tournament`
  ADD CONSTRAINT `FK_BD5FB8D9727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `tournament` (`id`);

--
-- Constraints for table `tournamentprivileges`
--
ALTER TABLE `tournamentprivileges`
  ADD CONSTRAINT `FK_F549F01E33D1A3E7` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`),
  ADD CONSTRAINT `FK_F549F01E8D93D649` FOREIGN KEY (`user`) REFERENCES `myuser` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
