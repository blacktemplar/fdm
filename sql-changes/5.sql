CREATE TABLE tournamentrankingentry__game (id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)', games INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE tournamentrankingentry__game ADD CONSTRAINT FK_5E9FF5FABF396750 FOREIGN KEY (id) REFERENCES tournamentrankingentry (id) ON DELETE CASCADE;
ALTER TABLE tournamentrankingentry ADD dtype VARCHAR(255) NOT NULL;
UPDATE `tournamentrankingentry`
SET dtype='tournamentrankingentry'
WHERE 1