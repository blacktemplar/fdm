DROP TABLE `attendancestrategy`;
RENAME TABLE
    `attendancestrategy__abstract` TO `attendancestrategy`;
ALTER TABLE attendancestrategy
  DROP dtype;
ALTER TABLE `attendancestrategy`
  CHANGE `maxPlayersPerGroup2` `maxPlayersPerGroup` INT(11) NOT NULL,
  CHANGE `merge2` `merge` INT(11) NOT NULL,
  CHANGE `numGroups2` `numGroups` INT(11) NOT NULL,
  CHANGE `postGroupingShuffle2` `postGroupingShuffle` TINYINT(1) NOT NULL,
  CHANGE `preGroupingShuffle2` `preGroupingShuffle` TINYINT(1) NOT NULL,
  CHANGE `preShuffle2` `preShuffle` TINYINT(1) NOT NULL;