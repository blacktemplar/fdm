CREATE TABLE speaktask (
  id            CHAR(36)     NOT NULL
  COMMENT '(DC2Type:guid)',
  tournament_id CHAR(36) DEFAULT NULL
  COMMENT '(DC2Type:guid)',
  info          VARCHAR(255) NOT NULL,
  text          LONGTEXT     NOT NULL,
  createdAt     DATETIME     NOT NULL,
  updatedAt     DATETIME     NOT NULL,
  INDEX IDX_48987F633D1A3E7 (tournament_id),
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci
  ENGINE = InnoDB;
ALTER TABLE speaktask
  ADD CONSTRAINT FK_48987F633D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id);
ALTER TABLE tournament
  ADD automaticallyStartGames TINYINT(1) NOT NULL;
