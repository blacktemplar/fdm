ALTER TABLE game
  ADD startTime DATETIME DEFAULT NULL;
UPDATE game ga
  INNER JOIN groups gr
    ON gr.id = ga.group_id
  INNER JOIN phase p
    ON p.id = gr.phase_id
  INNER JOIN tournament t
    ON t.id = p.tournament_id
SET ga.startTime = t.start;
