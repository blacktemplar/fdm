ALTER TABLE player
  ADD gender VARCHAR(255) NOT NULL;
ALTER TABLE myuser
  ADD player_id CHAR(36) DEFAULT NULL
COMMENT '(DC2Type:guid)',
  ADD type INT NOT NULL;
UPDATE myuser
SET type = 2
WHERE admin = 1;
ALTER TABLE myuser
  DROP admin,
  DROP firstName,
  DROP lastName,
  DROP birthday;
ALTER TABLE myuser
  ADD CONSTRAINT FK_5DBD36CC99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id);
CREATE UNIQUE INDEX UNIQ_5DBD36CC99E6F5DF
  ON myuser (player_id);