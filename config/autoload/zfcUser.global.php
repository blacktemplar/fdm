<?php
return [
  'translator' => [
    'translation_file_patterns' => [
      [
        'type' => 'gettext',
        'base_dir' => __DIR__ . '/../../vendor/zf-commons/zfc-user/src/ZfcUser/language',
        'pattern' => '%s.mo',
      ],
    ],
  ],
  'zfcuser' => [
    'enable_registration' => true,
    'user_entity_class' => 'FDM\Entity\User',
    'use_registration_form_captcha' => true,
  ],
];
