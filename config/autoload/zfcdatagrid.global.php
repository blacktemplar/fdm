<?php
/**
 * Copy this configuration file info config/autoload/zfcdatagrid.local.php
 * Then it will override the default settings and you can use your own!
 */
return [
  'ZfcDatagrid' => [

    'settings' => [

      'default' => [
        'renderer' => [
          //http => jqGrid,
          'http' => 'bootstrapTable',
          'console' => 'zendTable'
        ]
      ],

      'export' => [
        'enabled' => true,

        //currently only A formats are supported...
        'papersize' => 'A4',

        // landscape / portrait (we preferr landscape, because datagrids are often wide)
        'orientation' => 'portrait',

        'formats' => [
          //renderer -> display Name (can also be HTML)
          //'PHPExcel' => 'Excel',
          //'tcpdf' => 'PDF'
        ],

        // The output+save directory
        'path' => getcwd() . '/public/download',

        'mode' => 'iframe'
      ]
    ],

    'cache' => [

      'adapter' => [
        'name' => 'Filesystem',
        'options' => [
          'ttl' => 720000, // cache with 200 hours,
          'cache_dir' => 'data/ZfcDatagrid'
        ]
      ],
      'plugins' => [
        'exception_handler' => [
          'throw_exceptions' => false
        ],

        'Serializer'
      ]
    ],

    'renderer' => [
      'bootstrapTable' => array(
        'templates' => array(
          'toolbar' => 'layout/zfc-datagrid-toolbar'
        )
      ),
      'jqGrid' => [
        'templates' => [
          'layout' => 'zfc-datagrid/renderer/jqGrid/layout'
        ]
      ],
      'TCPDF' => [
        //currently only A formats are supported...
        'papersize' => 'A4',

        // landscape / portrait (we preferr landscape, because datagrids are often wide)
        'orientation' => 'portrait',

        'style' => [
          'data' => [
            'size' => 11,
            'padding' => 0,
            'contentPadding' => 0,
          ],
          'header' => [
            'size' => 12,
            'padding' => 0,
            'contentPadding' => 0,
            'height' => 5,
          ],
        ],
      ]
    ],

  ],
];
