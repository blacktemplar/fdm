#!/bin/bash
HOST='fdm.dx.am'
USER='1603515'
read -s -p "Enter FTP-Password for $USER on host $HOST:
>" PASS
TARGETFOLDER='/fdm.dx.am'
SOURCEFOLDER='~/private/fdm-new'

lftp -f "
open $HOST
user $USER $PASS
lcd $SOURCEFOLDER
mirror --reverse --delete --verbose --continue \
-x ^data/$ -x ^sql-changes/$ -x ^\..* -x ^gslint\.conf$ -x ^LICENSE\.txt$ \
-x ^one_tournament_sql_log$ -x ^one_tournament_sql_log2$ -x ^README\.md$ \
-x ^ruleset\.xml$ -x ^sync-with-server\.sh$ -x ^Vagrantfile$ \
-x ^config/autoload/.*local\..* -x ^config/autoload/\.gitignore \
-x .*/bin/.* -x .*/doc/.* \
-X *.zip -x public/dev\.index\.php -X *.htaccess -X *.directory  \
$SOURCEFOLDER $TARGETFOLDER
bye
"